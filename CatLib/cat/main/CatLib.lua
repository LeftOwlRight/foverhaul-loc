_G.CatLib = _G.CatLib or {}

CatLib._new_messages_catlib = CatLib._new_messages_catlib or {}
CatLib._num_catlib = CatLib._num_catlib or 60
CatLib._data_path = SavePath .. "CatLib_Logs.json"
local num = 0

function CatLib:LogToFile(func, message)
    num = num + 1
    local file = io.open(self._data_path, "a")
    local text = tostring(num .. ": " .. func .. " - " .. message)
	if file then
		file:write(json.encode(text) .. "\n")
		file:close()
	end
end

function CatLib:dec_round(n, decimals)
	decimals = math.pow(10, decimals or 0)
	n = n * decimals

	if n >= 0 then
		 n = math.floor(n + 0.5)
	else
 		n = math.ceil(n - 0.5)
	end

	return n / decimals
end

function CatLib:AddNewMessage(id)
    require("lib/utils/Messages")
    table.insert(CatLib._new_messages_catlib, id)
    CatLib:NewMessageReceived()
end

function CatLib:AddNum()
    CatLib._num_catlib = CatLib._num_catlib + 1
    return CatLib._num_catlib
end

function CatLib:GetCustomMessages()
    return CatLib._new_messages_catlib
end

function CatLib:Success(part)
    local debug = false
    if part and debug then
        log("[CatLib] " ..tostring(part).. " Successfully Initialized!")
    elseif not part and debug then
        log("[CatLib] Successfully Initialized!")
    end
end

CatLib:Success()
-- meow --