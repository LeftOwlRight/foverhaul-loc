function CatLib:PreventCrash(func, ...)
	local lib = "CatLib"
	local fun, error = pcall(func, ...)

	if not fun then
		log("[" .. lib .. "] An error occurred in " .. tostring(func) .. "; Result: " .. tostring(error))
	end
end

function CatLib:string_to_operator(string, x, y)
	local look_up = {
		['=='] = function (x, y) return x == y end,
		['>='] = function (x, y) return x >= y end,
		['<='] = function (x, y) return x <= y end,
		['>'] = function (x, y) return x > y end,
		['<'] = function (x, y) return x < y end,
		['+'] = function (x, y) return x + y end,
		['-'] = function (x, y) return x - y end,
		['*'] = function (x, y) return x * y end,
		['/'] = function (x, y) return x / y end,
	}

	return look_up[string](x, y)
end

function CatLib:Rainbow(speed_mul)
    local t = Application:time()
    local r, g, b = 1,1,1

    local h = (t % 5) / 5 * 6 * (speed_mul or 1)
    local i = math.floor(h)
    local f = h - i
    local q = 1 - f

    if i % 6 == 0 then
        r, g, b = 1, f, 0
    elseif i % 6 == 1 then
        r, g, b = q, 1, 0
    elseif i % 6 == 2 then
        r, g, b = 0, 1, f
    elseif i % 6 == 3 then
        r, g, b = 0, q, 1
    elseif i % 6 == 4 then
        r, g, b = f, 0, 1
    elseif i % 6 == 5 then
        r, g, b = 1, 0, q
    end

	return r,g,b
end

function CatLib:set_time_temporary_property(name, time)
	return managers.player._temporary_properties:set_time(name, time)
end

function CatLib:add_time_temporary_property(name, time)
	return managers.player._temporary_properties:add_time(name, time)
end

function CatLib:get_time_from_temporary_property(name)
	return managers.player._temporary_properties:get_time(name)
end

function CatLib:fetch_henchmans_upgrades(definition_id, upgrade_id, default)
	local data =  tweak_data.upgrades.values.team[upgrade_id][1]
	local upgrades = 0

	for index = 1, 3 do
		local loadout = managers.blackmarket:henchman_loadout(index)
		if loadout.skill == definition_id then
			upgrades = data
		end
	end

	return upgrades or default or 0
end

function CatLib:check_upgrade_value(category, upgrade, index, default)
	local upgrade_value = managers.player:upgrade_value(category, upgrade)

	if upgrade_value == 0 then
		return default or 0
	end

	return upgrade_value[index] or default or 0
end

function CatLib:upgrade_value_based_on_current_health(category, upgrade, health_threshold, default, custom_value_1, custom_value_2, operator, per_health_ratio, limited_increase_health_ratio, custom_limit_value)
	local upgrade_value = managers.player:upgrade_value(category, upgrade)

	if not managers.player:player_unit() then
		return default or 0
	end

	if upgrade_value == 0 then
		return default or 0
	end

	if not per_health_ratio then
		if CatLib:string_to_operator(operator, managers.player:player_unit():character_damage():health_ratio(), health_threshold) then
			return upgrade_value[(custom_value_2 or 2)] or default or 0
		end
	end

	if per_health_ratio then
		if CatLib:string_to_operator(operator, managers.player:player_unit():character_damage():health_ratio(), health_threshold) then
			local health_ratio = managers.player:player_unit():character_damage():health_ratio()
			local upg_value = upgrade_value[(custom_value_1 or 1)]
			local bonus_per_percentage = upgrade_value[(custom_value_2 or 2)]
			local damage_health_ratio = health_threshold - CatLib:dec_round(health_ratio, 2)
			upg_value = upg_value * (1 + bonus_per_percentage * damage_health_ratio)

			if limited_increase_health_ratio then
				upg_value = math.min(upg_value, upgrade_value[(custom_value_1 or 1)] * upgrade_value[(custom_limit_value or 3)])
			end

			return upg_value or default or 0
		end
	end

	return upgrade_value[(custom_value_1 or 1)] or default or 0
end

CatLib:Success("PlayerManager")