function CoreEnvironmentControllerManager:set_wraith_effect(_wraith_effect)
	self._wraith_effect = _wraith_effect
end

function CoreEnvironmentControllerManager:set_wraith_attack_effect(wraith_attack_effect, call, ...)
	self._wraith_attack_effect = wraith_attack_effect
    self[call](self, ...)
    tweak_data.accessibility_colors.screenflash.blurzone.wraith = Color(0.5, 77/255, 0, 8/255)

    if not wraith_attack_effect then
        DelayedCalls:Add("PlayerManager_FovRestore", 2, function()
            self:set_screenflash_color_blurzone(nil)
        end)
    end
end

Hooks:PostHook(CoreEnvironmentControllerManager, "set_post_composite", "set_post_composite___", function(self, t, dt)
    local temp_vec_1 = Vector3()
    local ids_LUT_settings_a = Idstring("LUT_settings_a")
    if self._wraith_effect then
        mvector3.add(temp_vec_1, Vector3(0.3, 0, 1, 0.5))
        self._lut_modifier_material:set_variable(ids_LUT_settings_a, temp_vec_1)
    end

    if self._wraith_attack_effect then
        self:set_screenflash_color_blurzone("wraith")
    end
end)