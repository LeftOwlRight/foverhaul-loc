CustomDOTManager = CustomDOTManager or class()
local bleedingEnemies = {}
local enemy_dot_effects = {}
local eviscerated_enemies = {}

function CustomDOTManager:CollectBleedEnemies(unit)
	for _, id in pairs(bleedingEnemies) do
		if id == unit:id() then
			return
		end
	end

	table.insert(bleedingEnemies, unit:id())
end

function CustomDOTManager:GetBleedingEnemies()
	return bleedingEnemies
end

function CustomDOTManager:is_bleeding(unit)
	for _, id in pairs(bleedingEnemies) do
		if id == unit:id() then
			return true
		end
	end

	return false
end

function CustomDOTManager:_apply_dot_damage(target_unit, damage, variant, wpn_unit, wpn_id)
	local dot_damage = damage

	if managers.player:has_category_upgrade("player", "bleed_critical_hit") then
		local critical_value = math.min(managers.player:critical_hit_chance(), 0.5)
		local critical_hit = false

		if critical_value > 0 then
			local critical_roll = math.random()
			critical_hit = critical_roll <= critical_value
		end

		if critical_hit then
			dot_damage = dot_damage * managers.player:upgrade_value("player", "bleed_critical_hit", 1)
		end
	end

	dot_damage = dot_damage * managers.player:upgrade_value("player", "bleed_bullets").dot_tick_period
	managers.player:bleed_tick_healing(dot_damage)

	if alive(target_unit) then
		self:CollectBleedEnemies(target_unit)

		local col_ray = {
			unit = target_unit
		}

        local attack_data = {
            variant = "poison",
            damage = dot_damage,
            attacker_unit = managers.player:player_unit(),
            --pos = Vector3(enemyPos.x, enemyPos.y, enemyPos.z + 80),
        	--attack_dir = Vector3(0, 0, 0),
			weapon_id = wpn_id,
			weapon_unit = wpn_unit,
			col_ray = col_ray,
			hurt_animation = false
        }

        target_unit:character_damage():damage_dot(attack_data)

		World:effect_manager():spawn({
			effect = Idstring("effects/payday2/particles/impacts/blood/blood_impact_a"),
			position = col_ray.unit:movement()._obj_spine:position(),
			normal = Vector3(math.random(-1, 1), math.random(-1, 1), math.random(-1, 1))
		})
	end
end

function CustomDOTManager:apply_custom_dot_effect(damage, duration, tick_interval, variant, target_unit, weapon_unit, weapon_id)
    local dot_damage = damage
    local dot_duration = duration
    local dot_tick_interval = tick_interval
	local dot_variant = variant
	local dot_weapon_unit = weapon_unit
	local dot_weapon_id = weapon_id

    if enemy_dot_effects[target_unit:key()] then
        enemy_dot_effects[target_unit:key()].end_time = TimerManager:game():time() + dot_duration
    else
        enemy_dot_effects[target_unit:key()] = {
            end_time = TimerManager:game():time() + dot_duration,
            damage = dot_damage,
            tick_interval = dot_tick_interval,
			dtvariant = dot_variant,
			unit = target_unit,
			next_tick = dot_tick_interval,
			weapon_unit = dot_weapon_unit,
			weapon_id = dot_weapon_id
        }
    end

	FOverhaul:add_simple_updator(nil, "DOT_Manager", self)
end

function CustomDOTManager:DOT_Manager()
	local current_time = TimerManager:game():time()
    for key, dot_effect in pairs(enemy_dot_effects) do
        if not alive(dot_effect.unit) then
            return
        end

		if not dot_effect.end_time then
			return
		end

        if current_time >= dot_effect.end_time or dot_effect.unit:character_damage():dead() then
			table.remove(bleedingEnemies, 1)
            enemy_dot_effects[key] = nil
			return
		end

		if current_time >= dot_effect.next_tick then
            CustomDOTManager:_apply_dot_damage(dot_effect.unit, dot_effect.damage, dot_effect.dtvariant, dot_effect.weapon_unit, dot_effect.weapon_id)

			if dot_effect.next_tick == dot_effect.tick_interval then
				dot_effect.next_tick = TimerManager:game():time() + dot_effect.next_tick
			else
				dot_effect.next_tick = dot_effect.next_tick + dot_effect.tick_interval
			end
        end
    end

	if next(enemy_dot_effects) == nil then
		FOverhaul:remove_updates("DOT_Manager")
	end
end

function CustomDOTManager:_add_eviscerated_enemies(unit)
	for _, id in pairs(eviscerated_enemies) do
		if id == unit:id() then
			return
		end
	end

	table.insert(eviscerated_enemies, unit:id())

	local clear_time = managers.player:upgrade_value("player", "bleed_ability_base").per_enemy_cooldown_reduction_duration
	DelayedCalls:Add("clear_eviscerated_enemies", clear_time, function()
		eviscerated_enemies = {}
	end)
end

function CustomDOTManager:_get_eviscerated_enemies()
    return eviscerated_enemies
end

function CustomDOTManager:_bleed_ability(heal)
	local enemies = 0
    local upgd_base = managers.player:upgrade_value("player", "bleed_ability_base")
	local three_enemies = upgd_base.heal_effectiveness_three_enemies
	local six_enemies = upgd_base.heal_effectiveness_six_enemies

	for key, dot_effect in pairs(enemy_dot_effects) do
		enemy_dot_effects[key] = nil
		local damage = dot_effect.damage * managers.player:upgrade_value("player", "bleed_bullets", 1).duration
		self:_apply_dot_damage(dot_effect.unit, damage, "eviscerate")
		self:_add_eviscerated_enemies(dot_effect.unit)

		enemies = enemies + 1
		local damage_to_heal_cal = damage * heal

		if enemies > 3 and enemies <= 6 then
			damage_to_heal_cal = damage_to_heal_cal * three_enemies
		elseif enemies > 6 then
			damage_to_heal_cal = damage_to_heal_cal * six_enemies
		end

		damage_to_heal_cal = math.min(damage_to_heal_cal, tweak_data.upgrades.bleed_ability_heal_cap_per_enemy)

		managers.player:add_to_property("dmg_to_hp", damage_to_heal_cal)
	end

	managers.player:activate_temporary_property("reduce_cooldown_bleed", managers.player:upgrade_value("player", "bleed_ability_base").per_enemy_cooldown_reduction_duration, 0)
	local damage_to_heal = managers.player:get_property("dmg_to_hp", 0)
	managers.player:BleedHeal(damage_to_heal)
	bleedingEnemies = {}
	managers.player:remove_property("dmg_to_hp")
	FOverhaul:remove_updates("DOT_Manager")
end