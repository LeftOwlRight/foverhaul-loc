Hooks:PostHook(EquipmentsTweakData, "init", "init_____foverhaul", function(self)
	self.armor_kit = {
		deploy_time = 3.5,
		use_function_name = "use_armor_kit",
		sound_done = "bar_armor_finished",
		dropin_penalty_function_name = "use_armor_kit_dropin_penalty",
		icon = "equipment_armor_kit",
		description_id = "des_armor_kit",
		limit_movement = false,
		sound_start = "bar_armor",
		sound_interupt = "bar_armor_cancel",
		text_id = "debug_equipment_armor_kit",
		on_use_callback = "on_use_armor_bag",
		deploying_text_id = "hud_equipment_equipping_armor_kit",
		action_timer = 3.5,
		visual_object = "g_armorbag",
		quantity = {
			3
		},
        platings = 4,
        plating_durability = 35
	}
end)