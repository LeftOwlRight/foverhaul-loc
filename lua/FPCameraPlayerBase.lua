function FPCameraPlayerBase:_wraith_form(wraith_form)
	local function chk_if_action_done()
		local unit = managers.player:player_unit()
		if unit then
			local current_state = unit:movement():current_state()
			return not (current_state:_is_meleeing() or current_state:in_melee() or current_state:_changing_weapon())
		end
		return true
	end

	local function wait_for_action_to_finish()
		DelayedCalls:Add("wait_for_action_to_finish", 0.01, function()
			if chk_if_action_done() then
				self:hide_weapon()
				self:hands("hide")
			else
				wait_for_action_to_finish()
			end
		end)
	 end

	if wraith_form then
		local unit = managers.player:player_unit()
		if unit and (unit:movement():current_state():_is_meleeing() or unit:movement():current_state():in_melee() or unit:movement():current_state():_changing_weapon()) then
			wait_for_action_to_finish()
		else
			self:hide_weapon()
			self:hands("hide")
		end
	else
		self:show_weapon()
		self:hands("show")
	end
end

function FPCameraPlayerBase:hands(set)
	set = (set == "show" and true) or false
	if self._unit then
		self._unit:set_visible(set)
		if self._unit.spawn_manager and self._unit:spawn_manager() and self._unit:spawn_manager():linked_units() then
			for unit_id, _ in pairs(self._unit:spawn_manager():linked_units()) do
				local units = self._unit:spawn_manager():spawned_units()[unit_id]
				if units and alive(units.unit) then
					units.unit:set_visible(set)
				end
			end
		end
	end
end