function HUDTeammate:set_ammo_refunded(index)
	local weapon_panel = nil
	if index == 2 then
		weapon_panel = self._player_panel:child("weapons_panel"):child("primary_weapon_panel")
	else
		weapon_panel = self._player_panel:child("weapons_panel"):child("secondary_weapon_panel")
	end

	if not weapon_panel then
		return
	end

	self._wpn_panel_refund = weapon_panel
end

function HUDTeammate:do_rainbow()
	if not FOverhaul._data.ammo_return_vfx_rainbow then
		FOverhaul:remove_updates("do_rainbow")
		if self._wpn_panel_refund then
			self._wpn_panel_refund:child("ammo_clip"):set_color(Color(1, 255/255, 215/255, 0))
		end

		return
	end

	local w = managers.player:chk_weapons_refunded() or {}

	if next(w) == nil then
		FOverhaul:remove_updates("do_rainbow")

		return
	end

	for _, wpn in pairs(w) do
		self:set_ammo_refunded(wpn:selection_index())
		if self._wpn_panel_refund then
			local r,g,b = CatLib:Rainbow()
			self._wpn_panel_refund:child("ammo_clip"):set_color(Color(1, r,g,b))
		end
	end
end

Hooks:PostHook(HUDTeammate, "set_ammo_amount_by_type", "set_ammo_amount_by_type___", function(self)
	if not FOverhaul._data.ammo_return_vfx then
		return
	end

	local w = managers.player:chk_weapons_refunded() or {}

	if next(w) == nil then
		return
	end

	if FOverhaul._data.ammo_return_vfx_rainbow and not FOverhaul:_is_already_updating("do_rainbow") then
		FOverhaul:add_simple_updator(nil, "do_rainbow", self)
	else
		for _, wpn in pairs(w) do
			self:set_ammo_refunded(wpn:selection_index())
			if self._wpn_panel_refund then
				self._wpn_panel_refund:child("ammo_clip"):set_color(Color(1, 255/255, 215/255, 0))
			end
		end
	end
end)

function HUDTeammate:activate_immunity_radial(duration)
	self._radial_health_panel:child("radial_custom"):animate(function(anim)
		anim:set_color(Color(1, 1, 1, 1))
		anim:set_visible(true)

			over(duration, function (rem)
				anim:set_color(Color(0.8, 1 - rem, 1, 1))
			end)

		anim:set_visible(false)
	end)
end

function HUDTeammate:activate_unique_effect_radial(duration)
	local uniq = self._radial_health_panel:bitmap({
		texture = "guis/textures/hud_uniq",
		name = "radial_unique",
		blend_mode = "add",
		visible = false,
		render_template = "VertexColorTexturedRadial",
		layer = 5,
		color = Color(1, 0, 0, 0),
		w = self._radial_health_panel:w(),
		h = self._radial_health_panel:h()
	})

	self._radial_health_panel:child("radial_unique"):animate(function(anim)
		anim:set_color(Color(1, 1, 1, 1))
		anim:set_visible(true)
			over(duration, function (rem)
				anim:set_color(Color(1, 1 - rem, 1, 1))
			end)
		anim:set_visible(false)
	end)
end

function HUDTeammate:set_platings()
	if self._block_hud_platings then
		return
	end

	if self._radial_health_panel:child("radial_platings") then
		self._new_value = 1
		self._block_hud_platings = true
		self._radial_health_panel:child("radial_platings"):set_color(Color(1, 1, 1, 1))
		self._radial_health_panel:child("radial_platings"):set_visible(true)

		return
	end

	local plat = self._radial_health_panel:bitmap({
		texture = "guis/textures/hud_platings",
		name = "radial_platings",
		blend_mode = "add",
		visible = true,
		render_template = "VertexColorTexturedRadial",
		layer = 0,
		texture_rect = {
			128,
			0,
			-128,
			128
		},
		color = Color(1, 1, 1, 1),
		w = self._radial_health_panel:w(),
		h = self._radial_health_panel:h(),
	})

	self._block_hud_platings = true
end

function HUDTeammate:update_platings(amount, replaced)
	if self._radial_health_panel:child("radial_platings") then
		local rad = (self._new_value or 1) - amount

		if replaced then
			rad = 1
		end

		self._radial_health_panel:child("radial_platings"):set_color(Color(1, rad, 1, 1))
		self._new_value = rad

		if rad <= 0 then
			self._radial_health_panel:child("radial_platings"):set_visible(false)
			self._block_hud_platings = false
		end
	end
end