Hooks:PostHook(HudIconsTweakData, "init", "init_foverhaul", function(self)
    local preset_path = "guis/dlcs/mom/textures/pd2/ai_abilities_new_"
    self.crew_dmgmul = {
        texture = preset_path .. "dmgmul",
        texture_rect = {
            0,
            0,
            128,
            128
        }
    }
    self.crew_bag_movement = {
        texture = preset_path .. "bagms",
        texture_rect = {
            0,
            0,
            128,
            128
        }
    }
end)