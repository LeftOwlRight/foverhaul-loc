PlayerAction.Daredevil = {
    Priority = 1,
    Function = function ()
        local player_manager = managers.player
        local data = player_manager:upgrade_value("player", "style_rank")
        local co = coroutine.running()
        local current_time = Application:time()
        local current_stacks = 1
        local max_time = data.duration + Application:time()

        local function on_kill()
            current_stacks = current_stacks + 1

            if current_stacks <= data.max_stacks then
                player_manager:add_to_property("style_rank_bonus_dmg", data.bonus_dmg)
                player_manager:add_to_property("style_rank_bonus_ms", data.bonus_ms)
                player_manager:add_to_property("style_killing_spree", 1)
                player_manager:add_to_property("style_pierce_body_armor", player_manager:upgrade_value("player", "style_pierce_body_armor", 0))
                player_manager:add_to_property("style_rate_of_fire", player_manager:upgrade_value("player", "style_rate_of_fire", 0))
                player_manager:add_to_property("lifesteal_style", player_manager:upgrade_value("player", "style_lifesteal", 0))
                player_manager:add_to_property("style_absorption", player_manager:upgrade_value("player", "daredevil_damage_absorption", 0))
            end

            if current_stacks == data.max_stacks then
                player_manager:reload_weapon_on_style_rank()
            end

            max_time = Application:time() + data.duration
        end

        player_manager:add_to_property("style_rank_bonus_dmg", data.bonus_dmg)
        player_manager:add_to_property("style_rank_bonus_ms", data.bonus_ms)
        player_manager:add_to_property("style_killing_spree", 1)
        player_manager:add_to_property("style_pierce_body_armor", player_manager:upgrade_value("player", "style_pierce_body_armor", 0))
        player_manager:add_to_property("style_rate_of_fire", player_manager:upgrade_value("player", "style_rate_of_fire", 0))
        player_manager:add_to_property("lifesteal_style", player_manager:upgrade_value("player", "style_lifesteal", 0))
        player_manager:add_to_property("style_absorption", player_manager:upgrade_value("player", "daredevil_damage_absorption", 0))

        player_manager:register_message(Message.OnEnemyKilled, co, on_kill)

        while current_time < max_time do
            current_time = Application:time()

            coroutine.yield(co)
        end

        player_manager:remove_property("style_pierce_body_armor")
        player_manager:remove_property("style_rate_of_fire")
        player_manager:remove_property("style_rank_bonus_dmg")
        player_manager:remove_property("style_rank_bonus_ms")
        player_manager:remove_property("style_killing_spree")
        player_manager:remove_property("lifesteal_style")
        player_manager:remove_property("style_absorption")
        player_manager:unregister_message(Message.OnEnemyKilled, co)
    end
}