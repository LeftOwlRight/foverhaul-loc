PlayerAction.TaserMalfunction = {
	Priority = 1,
	Function = function (player_manager, interval, chance_to_trigger)
		local co = coroutine.running()
		local elapsed = 0
		local target_elapsed = Application:time() + interval
        local bonus_ms = player_manager:upgrade_value("player", "taser_malfunction").bonus_movement_speed
        local bonus_ms_duration = player_manager:upgrade_value("player", "taser_malfunction").bonus_duration

		while player_manager:current_state() == "tased" do
			elapsed = Application:time()

			if target_elapsed <= elapsed then
				if math.random() <= chance_to_trigger then
					player_manager:send_message(Message.SendTaserMalfunction, nil, nil)
					if not player_manager:has_active_temporary_property("taser_malfunction_bonus") then
                    	player_manager:activate_temporary_property("taser_malfunction_bonus", bonus_ms_duration, bonus_ms)
					end
				end

				target_elapsed = Application:time() + interval
			end

			coroutine.yield(co)
		end
	end
}