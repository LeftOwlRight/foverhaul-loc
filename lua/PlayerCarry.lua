function PlayerCarry:_get_max_walk_speed(...)
	local multiplier = tweak_data.carry.types[self._tweak_data_name].move_speed_modifier
    local armor_init = tweak_data.player.damage.ARMOR_INIT

	if managers.player:has_category_upgrade("carry", "movement_penalty_nullifier") then
		multiplier = 1
	else
		multiplier = math.clamp(multiplier * managers.player:upgrade_value("carry", "movement_speed_multiplier", 1), 0, 1)
		multiplier = math.clamp(multiplier * managers.player:upgrade_value("player", "mrwi_carry_speed_multiplier", 1), 0, 1)
        multiplier = math.clamp(multiplier * managers.player:crew_ability_upgrade_value("crew_bag_movement", 1), 0, 1)
	end

	if managers.player:has_category_upgrade("player", "armor_carry_bonus") then
		local base_max_armor = armor_init + managers.player:body_armor_value("armor") + managers.player:body_armor_skill_addend()
		base_max_armor = base_max_armor * managers.player:body_armor_skill_multiplier()
		local transporter_skill = managers.player:upgrade_value("player", "armor_carry_bonus", 0)
		local mul =  1

		for i = 1, base_max_armor do
			mul = mul + transporter_skill
		end

		multiplier = math.clamp(multiplier * mul, 0, 1)
	end

	local mutator = nil

	if managers.mutators:is_mutator_active(MutatorCG22) then
		mutator = managers.mutators:get_mutator(MutatorCG22)
	elseif managers.mutators:is_mutator_active(MutatorPiggyRevenge) then
		mutator = managers.mutators:get_mutator(MutatorPiggyRevenge)
	end

	if mutator and mutator.get_bag_speed_increase_multiplier then
		multiplier = multiplier * mutator:get_bag_speed_increase_multiplier()
	end

	return PlayerCarry.super._get_max_walk_speed(self, ...) * multiplier
end

function PlayerCarry:_perform_jump(jump_vec)
	local base_jump_modifier = tweak_data.carry.types[self._tweak_data_name].jump_modifier

	if base_jump_modifier < 1 then
		base_jump_modifier = base_jump_modifier * managers.player:upgrade_value("player", "jump_modifier_mul", 1)
	end

	if not managers.player:has_category_upgrade("carry", "movement_penalty_nullifier") then
		mvector3.multiply(jump_vec, base_jump_modifier)
	end

	PlayerCarry.super._perform_jump(self, jump_vec)
end

function PlayerCarry:_check_action_run(...)
	if tweak_data.carry.types[self._tweak_data_name].can_run or managers.player:has_category_upgrade("player", "run_with_any_bag") then
		PlayerCarry.super._check_action_run(self, ...)
	end
end