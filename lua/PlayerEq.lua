function PlayerEquipment:use_armor_kit()
	local function redirect()
		if Network:is_client() then
			managers.network:session():send_to_host("used_deployable")
		else
			managers.network:session():local_peer():set_used_deployable(true)
		end

		managers.statistics:use_armor_bag()
		self._unit:event_listener():call("on_use_armor_bag")
	end

	if managers.player:local_player():character_damage():has_armor_platings() then
        managers.player:local_player():character_damage():replace_armor_platings()
        local text = "REPLACED PLATINGS!"
        managers.hud:show_hint({
            time = 3,
            text = text
        })

		if Network:is_client() then
			managers.network:session():send_to_host("used_deployable")
		else
			managers.network:session():local_peer():set_used_deployable(true)
		end

        return true
   end

	return true, redirect
end

function PlayerEquipment:use_armor_kit_dropin_penalty()
	--MenuCallbackHandler:_update_outfit_information()
	return true
end