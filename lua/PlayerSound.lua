local _play_land = PlayerSound.play_land
function PlayerSound:play_land(...)
	local wraith_form = managers.player:is_wraith()

	if wraith_form then
		return
	end

	return _play_land(self, ...)
end

local _play_footstep = PlayerSound.play_footstep
function PlayerSound:play_footstep(...)
	local wraith_form = managers.player:is_wraith()

	if wraith_form then
		return
	end

	return _play_footstep(self, ...)
end
