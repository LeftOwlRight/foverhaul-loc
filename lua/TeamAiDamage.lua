function TeamAIDamage:init(unit)
	self._unit = unit
	self._char_tweak = tweak_data.character[unit:base()._tweak_table]
	local damage_tweak = self._char_tweak.damage
	self._HEALTH_INIT = damage_tweak.HEALTH_INIT + self:difficulty_scaling_value("health")
	self._HEALTH_BLEEDOUT_INIT = damage_tweak.BLEED_OUT_HEALTH_INIT + self:difficulty_scaling_value("bleedout_health")
	self._HEALTH_TOTAL = self._HEALTH_INIT + self._HEALTH_BLEEDOUT_INIT
	self._HEALTH_TOTAL_PERCENT = self._HEALTH_TOTAL / 100
	self._health = self._HEALTH_INIT
	self._health_ratio = self._health / self._HEALTH_INIT
	self._invulnerable = false
	self._char_dmg_tweak = damage_tweak
	self._focus_delay_mul = 1
	self._listener_holder = EventListenerHolder:new()
	self._bleed_out_paused_count = 0
	self._dmg_interval = damage_tweak.MIN_DAMAGE_INTERVAL
	self._next_allowed_dmg_t = -100
	self._last_received_dmg = 0
	self._spine2_obj = unit:get_object(Idstring("Spine2"))
	self._tase_effect_table = {
		effect = Idstring("effects/payday2/particles/character/taser_hittarget"),
		parent = self._unit:get_object(Idstring("e_taser"))
	}
end
Hooks:PreHook(TeamAIDamage, "damage_melee", "damage_melee__ai", function(self, attack_data)
	attack_data.damage = attack_data.damage * 0.7
end)

Hooks:PreHook(TeamAIDamage, "damage_bullet", "damage_bullet__ai", function(self, attack_data)
	attack_data.damage = self:easy_difficulty_adjustment(Global.game_settings.difficulty, attack_data)
end)

function TeamAIDamage:easy_difficulty_adjustment(wanted_difficulty, attack_data)
	return PlayerDamage.easy_difficulty_adjustment(wanted_difficulty, attack_data, "Team AI") or 1
end

function TeamAIDamage:difficulty_scaling_value(type)
	local scallabe_difficulties = { "overkill", "overkill_145", "easy_wish", "overkill_290", "sm_wish" }
	local types = { health = 10, bleedout_health = 4 }
	local base = types[type] or 0
	local scalling_preset = { overkill = base, overkill_145 = base*2, easy_wish = base*3, overkill_290 = base*4, sm_wish = base*5 }

	for _, diff in pairs(scallabe_difficulties) do
		if diff == Global.game_settings.difficulty then
			return scalling_preset[diff]
		end
	end

	return 0
end