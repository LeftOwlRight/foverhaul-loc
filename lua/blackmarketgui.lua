local function format_round(num, round_value)
	return round_value and tostring(math.round(num)) or string.format("%.1f", num):gsub("%.?0+$", "")
end

Hooks:PostHook(BlackMarketGui, "show_stats", "__show_stats", function (self)
	if tweak_data.blackmarket.armors[self._slot_data.name] then
        local category = "armors"
        local value = 0
		local equipped_item = managers.blackmarket:equipped_item(category)
		local equipped_slot = managers.blackmarket:equipped_armor_slot()
		local equip_base_stats, equip_mods_stats, equip_skill_stats = self:_get_armor_stats(equipped_item)
		local base_stats, mods_stats, skill_stats = self:_get_armor_stats(self._slot_data.name)
		local slot_data = self._slot_data
		local bm_armor_tweak = tweak_data.blackmarket.armors[slot_data.name]
		local bonus_health = CatLib:fetch_henchmans_upgrades("crew_healthy", "crew_add_health", 0) * 10
		local upgrade_level = bm_armor_tweak.upgrade_level
        for _, stat in ipairs(self._armor_stats_shown) do
			self._armor_stats_texts[stat.name].name:set_text(utf8.to_upper(managers.localization:text("bm_menu_" .. stat.name)))

			value = math.max(base_stats[stat.name].value + mods_stats[stat.name].value + skill_stats[stat.name].value, 0)

			if self._slot_data.name == equipped_slot then
				local base = base_stats[stat.name].value

                if managers.player:has_category_upgrade("player", "max_health_reduction") and not (managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0) then
					self._armor_stats_texts.health.equip:set_color(tweak_data.screen_colors.stats_negative)

                   	local max_health = (skill_stats.health.value + base_stats.health.value) * managers.player:upgrade_value("player", "max_health_reduction", 1)
                   	self._armor_stats_texts.health.equip:set_text(max_health)

                end

				if stat.name == "dodge" then
					self._armor_stats_texts[stat.name].base:set_text(format_round(base, stat.round_value).."%")
					self._armor_stats_texts[stat.name].equip:set_text(format_round(value, stat.round_value).."%")
					self._armor_stats_texts[stat.name].skill:set_text(skill_stats[stat.name].skill_in_effect and (skill_stats[stat.name].value > 0 and "+" or "") .. format_round(skill_stats[stat.name].value, stat.round_value).."%" or "")
				end

				if managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0 then
					local has_frenzy = managers.player:has_category_upgrade("player", "max_health_reduction", 1)
					local health_multiplier = managers.player:health_skill_multiplier()
					local max_health = (PlayerDamage._HEALTH_INIT + managers.player:health_skill_addend()) * health_multiplier * 2 * 10
					local health_cal = (max_health + bonus_health) * managers.player:upgrade_value("player", "health_decrease", 1)
					local actual_health = (max_health + bonus_health) - health_cal
					if has_frenzy then
						self._armor_stats_texts.health.equip:set_text(health_cal * managers.player:upgrade_value("player", "max_health_reduction", 1))
					else
						self._armor_stats_texts.health.equip:set_text(actual_health)
					end
				end

			else
				local equip = math.max(equip_base_stats[stat.name].value + equip_mods_stats[stat.name].value + equip_skill_stats[stat.name].value, 0)

                if managers.player:has_category_upgrade("player", "max_health_reduction") and not (managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0) then
					self._armor_stats_texts.health.equip:set_color(tweak_data.screen_colors.stats_negative)
					self._armor_stats_texts.health.total:set_color(tweak_data.screen_colors.stats_negative)

					local max_health = (skill_stats.health.value + base_stats.health.value) * managers.player:upgrade_value("player", "max_health_reduction", 1)
                   	self._armor_stats_texts.health.total:set_text(max_health)
                   	self._armor_stats_texts.health.equip:set_text(max_health)
                end

				if managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0 then
					local has_frenzy = managers.player:has_category_upgrade("player", "max_health_reduction", 1)
					local health_multiplier = managers.player:health_skill_multiplier()
					local max_health = (PlayerDamage._HEALTH_INIT + managers.player:health_skill_addend()) * health_multiplier * 2 * 10
					local health_cal = (max_health + bonus_health) * managers.player:upgrade_value("player", "health_decrease", 1)
					local actual_health = (max_health + bonus_health) - health_cal
					if has_frenzy then
						self._armor_stats_texts.health.total:set_text(health_cal * managers.player:upgrade_value("player", "max_health_reduction", 1))
						self._armor_stats_texts.health.equip:set_text(health_cal * managers.player:upgrade_value("player", "max_health_reduction", 1))
					else
						self._armor_stats_texts.health.total:set_text(actual_health)
						self._armor_stats_texts.health.equip:set_text(actual_health)
					end
				end

				if stat.name == "dodge" then
                	self._armor_stats_texts[stat.name].total:set_text(value .."%")
					self._armor_stats_texts[stat.name].equip:set_text(equip .."%")
				end

			end
		end
	end
end)

local _get_armor_stats = BlackMarketGui._get_armor_stats
function BlackMarketGui:_get_armor_stats(name)
	local bm_armor_tweak = tweak_data.blackmarket.armors[name]
	local upgrade_level = bm_armor_tweak.upgrade_level
	local base_stats, mods_stats, skill_stats = _get_armor_stats(self, name)
	local bonus_health = CatLib:fetch_henchmans_upgrades("crew_healthy", "crew_add_health", 0) * 10
	local bonus_stamina = CatLib:fetch_henchmans_upgrades("crew_motivated", "crew_add_stamina", 0)
	local bonus_dodge = CatLib:fetch_henchmans_upgrades("crew_evasive", "crew_add_dodge", 0) * 100
	local bonus_armor = CatLib:fetch_henchmans_upgrades("crew_sturdy", "crew_add_armor", 0) * 10
	local bonus_stealth = CatLib:fetch_henchmans_upgrades("crew_quiet", "crew_add_concealment", 0)

	for i, stat in ipairs(self._armor_stats_shown) do
		if stat.name == "armor" then
			skill_stats.armor.value = skill_stats.armor.value + bonus_armor
		elseif stat.name == "health" then
			skill_stats.health.value = skill_stats.health.value + bonus_health
		elseif stat.name == "concealment" then
			skill_stats.concealment.value = skill_stats.concealment.value + bonus_stealth
		elseif stat.name == "stamina" then
			skill_stats.stamina.value = skill_stats.stamina.value + bonus_stamina
		elseif stat.name == "dodge" then
			skill_stats.dodge.value = skill_stats.dodge.value + bonus_dodge
		elseif stat.name == "damage_shake" then
			skill_stats.damage_shake.value = 0
			local base = managers.player:body_armor_value("defense", upgrade_level, nil, 1)
            local mod = managers.player:bonus_damage_defense()
			base_stats[stat.name] = {
                value = base
            }
			skill_stats[stat.name] = {
                value = mod
            }
		end
		skill_stats[stat.name].skill_in_effect = skill_stats[stat.name].skill_in_effect or skill_stats[stat.name].value ~= 0
	end

	if managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0 then
		local health_multiplier = managers.player:health_skill_multiplier()
		local health_decrease = managers.player:upgrade_value("player", "health_decrease", 1)
		local max_health = (PlayerDamage._HEALTH_INIT + managers.player:health_skill_addend()) * health_multiplier * 2

		local fake_health_to_armor = max_health * health_decrease * managers.player:upgrade_value("player", "armor_increase", 1)
		local fake_health = max_health * health_decrease

		local real_health = (max_health + bonus_health) * health_decrease
		local real_health_to_armor = real_health * managers.player:upgrade_value("player", "armor_increase", 1)

		local diff_health = math.abs(fake_health - real_health) * 3
		local diff_armor = math.abs(fake_health_to_armor - real_health_to_armor) * managers.player:upgrade_value("player", "armor_multiplier", 1)

		local bonus_armor_scenario = 0

		if bonus_armor > 0 then
			bonus_armor_scenario = bonus_armor_scenario + bonus_armor * managers.player:upgrade_value("player", "armor_multiplier", 1)
			diff_armor = diff_armor - bonus_armor
		end

		skill_stats.health.value = skill_stats.health.value - diff_health
		skill_stats.armor.value = skill_stats.armor.value + diff_armor + bonus_armor_scenario
	end

	if managers.player:has_category_upgrade("player", "bonus_from_crit") then
		local crit = managers.player:critical_hit_chance()
		local hp_from_crit = crit * managers.player:upgrade_value("player", "bonus_from_crit", 1) * 100
		local value = skill_stats.health.value + hp_from_crit
        local skill_in_effect = value ~= 0
		skill_stats.health.value = skill_stats.health.value + hp_from_crit
        skill_stats.health.skill_in_effect = skill_in_effect
	end

	return base_stats, mods_stats, skill_stats
end