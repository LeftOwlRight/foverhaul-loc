Hooks:PostHook(BlackMarketManager, "_setup_unlocked_crew_items", "_setup_unlocked_crew_items_foverhaul", function(self)
	self:_unlock_crew_item("crew_dmgmul")
	self:_unlock_crew_item("crew_bag_movement")
end)

function BlackMarketManager:equipped_armor(chk_armor_kit, chk_player_state)
	if chk_player_state and managers.player:current_state() == "civilian" then
		return self._defaults.armor
	end

	local armor = nil

	for armor_id, data in pairs(tweak_data.blackmarket.armors) do
		armor = Global.blackmarket_manager.armors[armor_id]

		if armor.equipped and armor.unlocked and armor.owned then
			local forced_armor = self:forced_armor()

			if forced_armor then
				return forced_armor
			end

			return armor_id
		end
	end

	return self._defaults.armor
end

function BlackMarketManager:damage_multiplier(name, categories, silencer, detection_risk, current_state, blueprint)
	local multiplier = 1

	if tweak_data.weapon[name] and tweak_data.weapon[name].ignore_damage_upgrades then
		return multiplier
	end

	for _, category in ipairs(categories) do
		multiplier = multiplier + 1 - managers.player:upgrade_value(category, "damage_multiplier", 1)
	end

	multiplier = multiplier + 1 - managers.player:upgrade_value(name, "damage_multiplier", 1)
	multiplier = multiplier + 1 - managers.player:upgrade_value("player", "passive_damage_multiplier", 1)
	multiplier = multiplier + 1 - managers.player:upgrade_value("weapon", "passive_damage_multiplier", 1)

	if silencer then
		multiplier = multiplier + 1 - managers.player:upgrade_value("weapon", "silencer_damage_multiplier", 1)
	end

	local detection_risk_damage_multiplier = managers.player:upgrade_value("player", "detection_risk_damage_multiplier")
	multiplier = multiplier - managers.player:get_value_from_risk_upgrade(detection_risk_damage_multiplier, detection_risk)

	if managers.player:has_category_upgrade("player", "overkill_health_to_damage_multiplier") then
		local damage_ratio = managers.player:upgrade_value("player", "overkill_health_to_damage_multiplier", 1) - 1
		multiplier = multiplier + damage_ratio
	end

	if current_state and not current_state:in_steelsight() then
		for _, category in ipairs(categories) do
			multiplier = multiplier + 1 - managers.player:upgrade_value(category, "hip_fire_damage_multiplier", 1)
		end
	end

	if blueprint and self:is_weapon_modified(managers.weapon_factory:get_factory_id_by_weapon_id(name), blueprint) then
		multiplier = multiplier + 1 - managers.player:upgrade_value("weapon", "modded_damage_multiplier", 1)
	end

	if managers.player:has_category_upgrade("player", "exceed_crit_scaling") then
		local diff = managers.player:critical_hit_chance(detection_risk)
		local exceeded_crit = diff * managers.player:upgrade_value("player", "exceed_crit_scaling", 1)
		multiplier = multiplier - exceeded_crit
	end

	return self:_convert_add_to_mul(multiplier)
end

function BlackMarketManager:get_equipped_armor()
	return self:equipped_armor()
end

function BlackMarketManager:_calculate_armor_concealment(armor)
	local armor_data = tweak_data.blackmarket.armors[armor]
	local body_armor_value = managers.player:body_armor_value("concealment", armor_data.upgrade_level)
	local bonus_stealth = CatLib:fetch_henchmans_upgrades("crew_quiet", "crew_add_concealment", 0)

	return body_armor_value + bonus_stealth
end