-- /// wpn preset types = "deathwish", "expert", "sniper", "easywish", "normal", "good" ///
local hp_mul_preset = 0.8
local hp_mul_mayhem_preset = 0.7
local hp_mul_dh_wish_preset = 0.8
local hp_mul_ds_preset = 0.8
local SM_WISH = {
	PLATINGS_DURABILITY = 0.4,
	PLATINGS_DMG_RED = 0.5
}
local DH_WISH = {
	PLATINGS_DURABILITY = 0.2,
	PLATINGS_DMG_RED = 0.3
}

_G.zeal_heavy_swat_mul = {}
zeal_heavy_swat_mul.falloff = {
	5, 5, 5, 4.5, 3.5, 3.5
}

Hooks:PostHook(CharacterTweakData, "_set_overkill", "very_hard_hp", function(self)
	self:_multiply_all_hp(hp_mul_preset, 1)
end)

Hooks:PostHook(CharacterTweakData, "_set_overkill_145", "ovk_hp", function(self)
	self:_multiply_all_hp(hp_mul_preset, 1)
end)

Hooks:PostHook(CharacterTweakData, "_set_easy_wish", "mayhem_hp", function(self)
	self:_multiply_all_hp(hp_mul_mayhem_preset, 1)
end)

Hooks:PostHook(CharacterTweakData, "_set_overkill_290", "dw_hp", function(self)
	self:_multiply_all_hp(hp_mul_dh_wish_preset, 1)
	self:_add_armor_platings(DH_WISH.PLATINGS_DURABILITY, DH_WISH.PLATINGS_DMG_RED)
end)

Hooks:PostHook(CharacterTweakData, "_set_sm_wish", "death_sentence_adjustments", function(self)
	self:_multiply_all_hp(hp_mul_ds_preset, 1)
	self:_add_armor_platings(SM_WISH.PLATINGS_DURABILITY, SM_WISH.PLATINGS_DMG_RED)
	self:_override_specific_preset("zeal_heavy_swat", "is_rifle", zeal_heavy_swat_mul.falloff)
end)

function CharacterTweakData:_add_armor_platings(durability, dmg_red)
	self.fbi.PLATINGS_DMG_RED = dmg_red
	self.fbi_female.PLATINGS_DMG_RED = dmg_red
	self.swat.PLATINGS_DMG_RED = dmg_red
	self.heavy_swat.PLATINGS_DMG_RED = dmg_red
	self.heavy_swat_sniper.PLATINGS_DMG_RED = dmg_red
	self.fbi_heavy_swat.PLATINGS_DMG_RED = dmg_red
	self.zeal_swat.PLATINGS_DMG_RED = dmg_red
	self.zeal_heavy_swat.PLATINGS_DMG_RED = dmg_red
	self.sniper.PLATINGS_DMG_RED = dmg_red
	self.gangster.PLATINGS_DMG_RED = dmg_red
	self.biker.PLATINGS_DMG_RED = dmg_red
	self.biker_female.PLATINGS_DMG_RED = dmg_red
	self.tank.PLATINGS_DMG_RED = dmg_red
	self.triad.PLATINGS_DMG_RED = dmg_red
	self.tank_mini.PLATINGS_DMG_RED = dmg_red
	self.tank_medic.PLATINGS_DMG_RED = dmg_red
	self.spooc.PLATINGS_DMG_RED = dmg_red
	self.shadow_spooc.PLATINGS_DMG_RED = dmg_red
	self.shield.PLATINGS_DMG_RED = dmg_red
	self.phalanx_minion.PLATINGS_DMG_RED = dmg_red
	self.phalanx_vip.PLATINGS_DMG_RED = dmg_red
	self.taser.PLATINGS_DMG_RED = dmg_red
	self.city_swat.PLATINGS_DMG_RED = dmg_red
	self.biker_escape.PLATINGS_DMG_RED = dmg_red
	self.fbi_swat.PLATINGS_DMG_RED = dmg_red
	self.tank_hw.PLATINGS_DMG_RED = dmg_red
	self.medic.PLATINGS_DMG_RED = dmg_red
	self.bolivian.PLATINGS_DMG_RED = dmg_red
	self.bolivian_indoors.PLATINGS_DMG_RED = dmg_red
	self.bolivian_indoors_mex.PLATINGS_DMG_RED = dmg_red
	self.drug_lord_boss.PLATINGS_DMG_RED = dmg_red
	self.drug_lord_boss_stealth.PLATINGS_DMG_RED = dmg_red
	self.ranchmanager.PLATINGS_DMG_RED = dmg_red
	self.triad_boss.PLATINGS_DMG_RED = dmg_red
	self.triad_boss_no_armor.PLATINGS_DMG_RED = dmg_red
	self.deep_boss.PLATINGS_DMG_RED = dmg_red
	self.marshal_marksman.PLATINGS_DMG_RED = dmg_red
	self.marshal_shield.PLATINGS_DMG_RED = dmg_red
	self.marshal_shield_break.PLATINGS_DMG_RED = dmg_red
	self.snowman_boss.PLATINGS_DMG_RED = dmg_red
	self.piggydozer.PLATINGS_DMG_RED = dmg_red

	self.fbi.PLATINGS_DURABILITY = durability
	self.fbi_female.PLATINGS_DURABILITY = durability
	self.swat.PLATINGS_DURABILITY = durability
	self.heavy_swat.PLATINGS_DURABILITY = durability
	self.heavy_swat_sniper.PLATINGS_DURABILITY = durability
	self.fbi_heavy_swat.PLATINGS_DURABILITY = durability
	self.zeal_swat.PLATINGS_DURABILITY = durability
	self.zeal_heavy_swat.PLATINGS_DURABILITY = durability
	self.sniper.PLATINGS_DURABILITY = durability
	self.gangster.PLATINGS_DURABILITY = durability
	self.biker.PLATINGS_DURABILITY = durability
	self.biker_female.PLATINGS_DURABILITY = durability
	self.tank.PLATINGS_DURABILITY = durability
	self.triad.PLATINGS_DURABILITY = durability
	self.tank_mini.PLATINGS_DURABILITY = durability
	self.tank_medic.PLATINGS_DURABILITY = durability
	self.spooc.PLATINGS_DURABILITY = durability
	self.shadow_spooc.PLATINGS_DURABILITY = durability
	self.shield.PLATINGS_DURABILITY = durability
	self.phalanx_minion.PLATINGS_DURABILITY = durability
	self.phalanx_vip.PLATINGS_DURABILITY = durability
	self.taser.PLATINGS_DURABILITY = durability
	self.city_swat.PLATINGS_DURABILITY = durability
	self.biker_escape.PLATINGS_DURABILITY = durability
	self.fbi_swat.PLATINGS_DURABILITY = durability
	self.tank_hw.PLATINGS_DURABILITY = durability
	self.medic.PLATINGS_DURABILITY = durability
	self.bolivian.PLATINGS_DURABILITY = durability
	self.bolivian_indoors.PLATINGS_DURABILITY = durability
	self.bolivian_indoors_mex.PLATINGS_DURABILITY = durability
	self.drug_lord_boss.PLATINGS_DURABILITY = durability
	self.drug_lord_boss_stealth.PLATINGS_DURABILITY = durability
	self.ranchmanager.PLATINGS_DURABILITY = durability
	self.triad_boss.PLATINGS_DURABILITY = durability
	self.triad_boss_no_armor.PLATINGS_DURABILITY = durability
	self.deep_boss.PLATINGS_DURABILITY = durability
	self.marshal_marksman.PLATINGS_DURABILITY = durability
	self.marshal_shield.PLATINGS_DURABILITY = durability
	self.marshal_shield_break.PLATINGS_DURABILITY = durability
	self.snowman_boss.PLATINGS_DURABILITY = durability
	self.piggydozer.PLATINGS_DURABILITY = durability

	self.fbi.has_platings = true
	self.fbi_female.has_platings = true
	self.swat.has_platings = true
	self.heavy_swat.has_platings = true
	self.heavy_swat_sniper.has_platings = true
	self.fbi_heavy_swat.has_platings = true
	self.zeal_swat.has_platings = true
	self.zeal_heavy_swat.has_platings = true
	self.sniper.has_platings = true
	self.gangster.has_platings = true
	self.biker.has_platings = true
	self.biker_female.has_platings = true
	self.tank.has_platings = true
	self.triad.has_platings = true
	self.tank_mini.has_platings = true
	self.tank_medic.has_platings = true
	self.spooc.has_platings = true
	self.shadow_spooc.has_platings = true
	self.shield.has_platings = true
	self.phalanx_minion.has_platings = true
	self.phalanx_vip.has_platings = true
	self.taser.has_platings = true
	self.city_swat.has_platings = true
	self.biker_escape.has_platings = true
	self.fbi_swat.has_platings = true
	self.tank_hw.has_platings = true
	self.medic.has_platings = true
	self.bolivian.has_platings = true
	self.bolivian_indoors.has_platings = true
	self.bolivian_indoors_mex.has_platings = true
	self.drug_lord_boss.has_platings = true
	self.drug_lord_boss_stealth.has_platings = true
	self.ranchmanager.has_platings = true
	self.triad_boss.has_platings = true
	self.triad_boss_no_armor.has_platings = true
	self.deep_boss.has_platings = true
	self.marshal_marksman.has_platings = true
	self.marshal_shield.has_platings = true
	self.marshal_shield_break.has_platings = true
	self.snowman_boss.has_platings = true
	self.piggydozer.has_platings = true
end

function CharacterTweakData:_override_specific_preset(type, weapon_type, dmg_mul)
    local x = #dmg_mul
    for i=1, x do
        self[type].weapon[weapon_type].FALLOFF[i].dmg_mul = dmg_mul[i]
    end
end

local orig_prst = CharacterTweakData._presets
function CharacterTweakData:_presets(tweak_data)
	local presets = orig_prst(self, tweak_data)

	presets.gang_member_damage.HEALTH_INIT = 50
	presets.gang_member_damage.DOWNED_TIME = 30
	presets.gang_member_damage.BLEED_OUT_HEALTH_INIT = 12

	return presets
end