local mvec_1 = Vector3()
local mvec_2 = Vector3()

CopDamage.cop_decapitation = {
	t = {},
	interval = {},
	attack_data = {},
	vfx = {},
	ragdoll = {},
	parts = {}
}

Hooks:PostHook(CopDamage, "init", "init_foverhaul2024", function(self, unit)
	self._platings_durability = unit:base():char_tweak() and unit:base():char_tweak().has_platings and (math.min(self:get_enemy_max_health(unit) * unit:base():char_tweak().PLATINGS_DMG_RED, 100)) or 0
end)

function CopDamage:_get_dis_data()
	return self.cop_decapitation or {}
end

function CopDamage:_dismember_wraith()
	local body_parts = { "Head", "LeftArm" , "LeftForeArm" , "LeftHand" , "RightArm" , "RightForeArm" , "RightHand" , "LeftLeg" , "LeftFoot" , "RightLeg" , "RightFoot" }
	local body_parts_main = { "LeftArm" , "RightArm" , "LeftLeg" , "RightLeg", "Head" }

	if self._dead then
		self._unit:sound():play("split_gen_body")
		if self._unit:base()._tweak_table == "spooc" then
			self._unit:damage():run_sequence_simple("dismember_body_top")
		else
			if not self.cop_decapitation.parts[self._unit] then
				self.cop_decapitation.parts[self._unit] = {}
			end

			self.cop_decapitation.vfx[self._unit] = World:effect_manager():spawn({
				effect = Idstring("effects/payday2/particles/impacts/blood/blood_tendrils"),
				position = self._unit:get_object(Idstring("Neck")):position(),
				rotation = self._unit:get_object(Idstring("Neck")):rotation()
			})

			local bone_head = self._unit:get_object(Idstring("Head"))
			self:_spawn_head_gadget({
				position = bone_head:position(),
				rotation = bone_head:rotation(),
				dir = -self._unit:movement():m_head_rot():y()
			})

			for _, part in pairs(body_parts) do
				self._unit:body(self._unit:get_object(Idstring(part))):set_enabled( false )
			end

			for _, main_part in pairs(body_parts_main) do
				self.cop_decapitation.parts[self._unit][main_part] = main_part
			end

			self.cop_decapitation.ragdoll[self._unit] = self._unit
		end
	end
end

function CopDamage:roll_critical_hit(attack_data)
	if not self:can_be_critical(attack_data) then
		return false
	end

	local critical_hits = self._char_tweak.critical_hits or {}
	local critical_hit = false
	local critical_value = (critical_hits.base_chance or 0) + managers.player:critical_hit_chance() * (critical_hits.player_chance_multiplier or 1)

	if critical_value > 0 then
		local critical_roll = math.rand(1)
		critical_hit = critical_roll <= critical_value
	end

	if managers.player._coroutine_mgr:is_running(PlayerAction.DireNeed) then
		return true
	end

	return critical_hit
end

function CopDamage:has_platings(unit)
	if unit and unit:base():char_tweak() then
		if unit:base():char_tweak().has_platings and self._platings_durability > 0 then
			return true
		end
	end

	return false
end

function CopDamage:change_platings_durability(damage_absorbed, destroy)
	if destroy then
		self._platings_durability = 0
		return
	end

	self._platings_durability = math.max(self._platings_durability - damage_absorbed, 0)
end

function CopDamage:calc_platings_damage(damage, unit, attacker)
	if damage * unit:base():char_tweak().PLATINGS_DMG_RED <= self._platings_durability then
		self:change_platings_durability(damage * unit:base():char_tweak().PLATINGS_DMG_RED)

		if attacker == managers.player:player_unit() and FOverhaul._data.enemy_plate then
			managers.hud:enemy_platings_ui()
		end

		return damage * unit:base():char_tweak().PLATINGS_DMG_RED
	end

	damage = damage - self._platings_durability
	self:change_platings_durability(nil, true)

	if FOverhaul._data.enemy_plate then
		if attacker == managers.player:player_unit() then
			managers.hud:platings_broken_ui()
		end
		unit:contour():add("mark_enemy", false, 0.5, Color(1,1,1))
		unit:contour():flash("mark_enemy", 0.2)
	end

	return damage
end

function CopDamage:critical_damage(attack_data, damage, is_melee, is_fire, is_explosion, is_sentry, is_tase)
	local function weapon_based_bonus_crit_damage()
		if is_melee then
			return 0
		end

		if is_sentry then
			return 0
		end

		if is_tase then
			return 0
		end

		if not attack_data.weapon_unit then
			return 0
		end

		if not attack_data.weapon_unit:base() then
			return 0
		end

		if attack_data.weapon_unit:base().thrower_unit then
			return 0
		end

		local class = {"pistol"}
		local bonus_damage = 0
		local upgrade_preset = "critical_hit_bonus_dmg"
		for _, weapon_class in pairs(class) do
			if attack_data.weapon_unit:base():is_category(weapon_class) then
				bonus_damage = bonus_damage + managers.player:upgrade_value(weapon_class, upgrade_preset, 0)
			end
		end

		if not is_fire and not is_explosion then
			if attack_data.weapon_unit:base():got_silencer() then
				bonus_damage = bonus_damage + managers.player:upgrade_value("player", "critical_silenced", 0)
			end
		end

		return bonus_damage
	end

	local bonus_crit_dmg_skill = managers.player:get_bonus_critical_damage()
	local critical_damage_mul = tweak_data.upgrades.critical_base_damage + managers.player:upgrade_value("player", "critical_damage_card", 0) + bonus_crit_dmg_skill + weapon_based_bonus_crit_damage()
	damage = damage * critical_damage_mul

	return damage
end

function CopDamage:convert_to_criminal(health_multiplier)
	self._converted = true

	self:set_mover_collision_state(false)

	self._HEALTH_INIT = self._HEALTH_INIT * managers.player:upgrade_value("player", "convert_enemies_health_mul", 1)
	self._health = self._HEALTH_INIT
	self._health_ratio = 1
	self._damage_reduction_multiplier = health_multiplier

	self._unit:set_slot(16)
end

function CopDamage:restore_health_converts(restored_health)
	if self._health_ratio >= 1 then
		return
	end

	self._health = math.min(self._health + (self._HEALTH_INIT * restored_health), self._HEALTH_INIT)
	self._health_ratio = self._health / self._HEALTH_INIT
end

function CopDamage:_apply_damage_reduction(damage, attack_data)
	local damage_reduction = self._unit:movement():team().damage_reduction or 0

	if damage_reduction > 0 then
		damage = damage * (1 - damage_reduction)
	end

	if self._damage_reduction_multiplier then
		damage = self:easy_difficulty_adjustment(Global.game_settings.difficulty, attack_data)
		damage = damage * self._damage_reduction_multiplier
	end

	return damage
end

function CopDamage:easy_difficulty_adjustment(wanted_difficulty, attack_data)
	return PlayerDamage.easy_difficulty_adjustment(wanted_difficulty, attack_data, "Convert") or 1
end

function CopDamage:low_profile_critical_execution_check()
	if managers.player:has_category_upgrade("player", "execution_crit") then
		local chance = managers.player:upgrade_value("player", "execution_crit") + (managers.player:critical_hit_chance() * managers.player:upgrade_value("player", "execution_crit_scaling", 1))
		if math.random() <= chance then
			managers.player:low_profile_immunity_proc()
			managers.environment_controller:set_buff_effect(0.15)
			return true
		end
	end

	return false
end

function CopDamage:weapon_base_damage(weapon_id, tweak_id)
	if not tweak_data[(tweak_id or "weapon")][weapon_id] or not tweak_data[(tweak_id or "weapon")][weapon_id].stats or not tweak_data[(tweak_id or "weapon")][weapon_id].stats.damage then
		FOverhaul:catch_error_and_notify_later("CopDamage:weapon_base_damage", "weapon_id - is invalid; passed weapon id - " .. tostring(weapon_id))
		return 10
	end

	return (tweak_data[(tweak_id or "weapon")][weapon_id].stats.damage or 0)/10
end

function CopDamage:headshot_damage(attack_data, origin_damage)
	local headshot_damage = 0

	if not attack_data.weapon_unit:base().thrower_unit then
		local minimal_damage = origin_damage * tweak_data.upgrades.percentage_headshot_damage_minimal
		local current_health_damage = math.max(self._health * tweak_data.upgrades.percentage_headshot_damage, minimal_damage)
		local base_hs_damage = origin_damage + current_health_damage

		headshot_damage = math.min(base_hs_damage, tweak_data.upgrades.headshot_damage_cap)
	elseif attack_data.weapon_unit:base().thrower_unit then
		headshot_damage = origin_damage * tweak_data.upgrades.projectiles_hs_damage
	end

	return headshot_damage
end

function CopDamage:bleed_bullet(bleed_data, attack_data, damage, _HEALTH_INIT, _health, unit, weapon_unit, weapon_id)
	if not attack_data.weapon_unit:base():is_category("shotgun", "saw", "bow", "crossbow") and not attack_data.weapon_unit:base().thrower_unit then
		local bleed_damage = bleed_data.damage + (_HEALTH_INIT * bleed_data.max_health_damage)
		local time = TimerManager:game():time()

		local function _was_eviscerated()
			for _, id in pairs(CustomDOTManager:_get_eviscerated_enemies()) do
				if self._unit:id() == id then
					if managers.player:has_active_temporary_property("reduce_cooldown_bleed") then
						local upgd_base = managers.player:upgrade_value("player", "bleed_ability_base")
						local reduction = upgd_base.per_enemy_cooldown_reduction
						if self._bleed_infect_cooldown then
							return self._bleed_infect_cooldown + reduction < time
						end
					end
				end
			end

			return self._bleed_infect_cooldown + bleed_data.cooldown < time
		end

		if not self._bleed_infect_cooldown or _was_eviscerated() then
			CustomDOTManager:apply_custom_dot_effect(bleed_damage, bleed_data.duration, bleed_data.dot_tick_period, "bleed", unit, weapon_unit, weapon_id)
			self._bleed_infect_cooldown = time
		end
	end
end

function CopDamage:is_valid_weapon_head_shot_ammo_return(attack_data)
	if not attack_data.weapon_unit:base():is_category("smg", "assault_rifle", "snp") then
		return false
	end

	if attack_data.weapon_unit:base():fire_mode() ~= "single" then
		return false
	end

	if not managers.player:chk_weapon_id_head_shot_ammo_return(attack_data.weapon_unit:base():get_name_id()) then
		return false
	end

	return true
end

function CopDamage:is_dmr_weapon(attack_data)
	local used_weapon_id = attack_data.weapon_unit:base():get_name_id()
	local dmr_weapons = { "new_m14", "contraband", "ching", "sub2000" }

	for _, dmr_weapon_id in pairs(dmr_weapons) do
		if used_weapon_id == dmr_weapon_id then
			return true
		end
	end

	return false
end

function CopDamage:damage_bullet(attack_data)
	if self._dead or self._invulnerable then
		return
	end

	if self:is_friendly_fire(attack_data.attacker_unit) then
		return "friendly_fire"
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	if self._char_tweak.bullet_damage_only_from_front then
		mvector3.set(mvec_1, attack_data.col_ray.ray)
		mvector3.set_z(mvec_1, 0)
		mrotation.y(self._unit:rotation(), mvec_2)
		mvector3.set_z(mvec_2, 0)

		local not_from_the_front = mvector3.dot(mvec_1, mvec_2) > 0.3

		if not_from_the_front then
			return
		end
	end

	local is_civilian = CopDamage.is_civilian(self._unit:base()._tweak_table)

	if self._has_plate and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_plate_name and not attack_data.armor_piercing then
		local armor_pierce_roll = math.rand(1)
		local armor_pierce_value = 0

		if attack_data.attacker_unit == managers.player:player_unit() and not attack_data.weapon_unit:base().thrower_unit then
			armor_pierce_value = armor_pierce_value + attack_data.weapon_unit:base():armor_piercing_chance()
			armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("player", "armor_piercing_chance", 0)
			armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("weapon", "armor_piercing_chance", 0)
			armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("weapon", "armor_piercing_chance_2", 0)
			armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("weapon", "piercing_chance", 0)
			armor_pierce_value = armor_pierce_value + managers.player:get_property("style_pierce_body_armor", 0)

			if managers.player:has_category_upgrade("pistol", "pierce_chance") then
				if attack_data.weapon_unit:base():is_category("pistol") then
					armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("pistol", "pierce_chance", 0)
				end
			end

			if managers.player:has_category_upgrade("shotgun", "pierce_chance") then
				if attack_data.weapon_unit:base():is_category("shotgun") then
					armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("shotgun", "pierce_chance", 0)
				end
			end

			if attack_data.weapon_unit:base():got_silencer() then
				armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("weapon", "armor_piercing_chance_silencer", 0)
			end

			if attack_data.weapon_unit:base():is_category("saw") then
				armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("saw", "armor_piercing_chance", 0)
			end

			if attack_data.weapon_unit:base():is_category("bow", "crossbow") then
				armor_pierce_value = armor_pierce_value + managers.player:get_temporary_property("archer_pierce_through_arrows", 0)
			end
		elseif attack_data.attacker_unit:base() and attack_data.attacker_unit:base().sentry_gun then
			local owner = attack_data.attacker_unit:base():get_owner()

			if alive(owner) then
				if owner == managers.player:player_unit() then
					armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("sentry_gun", "armor_piercing_chance", 0)
					armor_pierce_value = armor_pierce_value + managers.player:upgrade_value("sentry_gun", "armor_piercing_chance_2", 0)
				else
					armor_pierce_value = armor_pierce_value + (owner:base():upgrade_value("sentry_gun", "armor_piercing_chance") or 0)
					armor_pierce_value = armor_pierce_value + (owner:base():upgrade_value("sentry_gun", "armor_piercing_chance_2") or 0)
				end
			end
		end

		if armor_pierce_roll >= armor_pierce_value then
			return
		end
	end

	local result = nil
	local body_index = self._unit:get_body_index(attack_data.col_ray.body:name())
	local head = self._head_body_name and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_head_body_name
	local damage = attack_data.damage

	if self._unit:base():char_tweak().DAMAGE_CLAMP_BULLET then
		damage = math.min(damage, self._unit:base():char_tweak().DAMAGE_CLAMP_BULLET)
	end

	damage = damage * (self._marked_dmg_mul or 1)

	if self._marked_dmg_dist_mul then
		local spott_dst = tweak_data.upgrades.values.player.marked_inc_dmg_distance[self._marked_dmg_dist_mul]

		if spott_dst then
			local dst = mvector3.distance(attack_data.origin, self._unit:position())

			if spott_dst[1] < dst then
				damage = damage * spott_dst[2]
			end
		end
	end

	if self._unit:movement():cool() then
		damage = self._HEALTH_INIT
	end

	attack_data.executed = false
	local headshot = false
	local headshot_multiplier = 0

	if managers.groupai:state():is_unit_team_AI(attack_data.attacker_unit) then
		damage = damage * managers.player:crew_ability_upgrade_value("crew_dmgmul", 1)
	end

	if attack_data.attacker_unit == managers.player:player_unit() then
	 --	headshot_multiplier = headshot_multiplier + managers.player:upgrade_value("weapon", "passive_headshot_damage_multiplier", 1) - 1
		local damage_scale = nil

		if alive(attack_data.weapon_unit) and attack_data.weapon_unit:base() and attack_data.weapon_unit:base().is_weak_hit then
			damage_scale = attack_data.weapon_unit:base():is_weak_hit(attack_data.col_ray and attack_data.col_ray.distance, attack_data.attacker_unit) or 1
		end

		local wpn_id = attack_data.weapon_unit:base():get_name_id()
		local critical_hit, crit_damage = self:roll_critical_hit(attack_data), self:critical_damage(attack_data, damage)
		local headshot_damage = self:headshot_damage(attack_data, damage)
		local style_rank = managers.player:get_property("style_killing_spree", 0)
		local upg_value = managers.player:upgrade_value("player", "bonus_hp_enemy_damage_style")
		local die_hard_active = managers.player:get_property("instakill_downed", nil)
		local low_profile_execution = nil
		local bleed_data = managers.player:upgrade_value("player", "bleed_bullets")
		local bleed_bonus_damage_taken = managers.player:upgrade_value("player", "bleed_bonus_damage_taken")
		local bonus_damage_ammo_refund = managers.player:upgrade_value("player", "head_shot_ammo_return_bonus_damage")

		if not attack_data.weapon_unit:base().thrower_unit and bonus_damage_ammo_refund ~= 0 and self:is_valid_weapon_head_shot_ammo_return(attack_data) then
			damage = damage * bonus_damage_ammo_refund
			managers.player:_remove_weapon_id_head_shot_ammo_return(attack_data.weapon_unit:base():get_name_id())
		end

		if style_rank == 6 and managers.player:has_category_upgrade("player", "bonus_hp_enemy_damage_style") then
			damage = damage + (self._HEALTH_INIT * upg_value)
		end

		if not attack_data.weapon_unit:base().thrower_unit and bleed_data ~= 0 then
			self:bleed_bullet(bleed_data, attack_data, damage, self._HEALTH_INIT, self._health, self._unit, attack_data.weapon_unit, wpn_id)
		end

		if CustomDOTManager:is_bleeding(self._unit) and bleed_bonus_damage_taken ~= 0 and self._health/self._HEALTH_INIT < 0.5 then
			damage = damage * bleed_bonus_damage_taken
		end

		if not attack_data.weapon_unit:base().thrower_unit and (attack_data.weapon_unit:base():has_dmr_kit() or self:is_dmr_weapon(attack_data)) and managers.player:has_category_upgrade("player", "dmr_bonus_damage_health") then
			damage = damage + (self._HEALTH_INIT * managers.player:upgrade_value("player", "dmr_bonus_damage_health", 0))
		end

		if critical_hit then
			low_profile_execution = self:low_profile_critical_execution_check()
			managers.player:send_message(Message.OnCriticalDamage, nil, self._unit, attack_data)
			managers.hud:on_crit_confirmed(damage_scale)

			if not head then
				damage = crit_damage
			elseif head then
				if not self._char_tweak.ignore_headshot and not self._damage_reduction_multiplier and self._char_tweak.headshot_dmg_mul then
					damage = crit_damage + headshot_damage
				end
			end

			if low_profile_execution then
				attack_data.executed = true
				damage = self._HEALTH_INIT
			end

			attack_data.critical_hit = true
		else
			managers.hud:on_hit_confirmed(damage_scale)
		end

		if head and not critical_hit and not self._char_tweak.ignore_headshot and not self._damage_reduction_multiplier then
			if self._char_tweak.headshot_dmg_mul then
				damage = damage + headshot_damage
			end
		end

		if managers.groupai:state():is_enemy_special(self._unit) then
			damage = damage * managers.player:upgrade_value("weapon", "special_damage_taken_multiplier", 1)

			if attack_data.weapon_unit:base().weapon_tweak_data then
				damage = damage * (attack_data.weapon_unit:base():weapon_tweak_data().special_damage_multiplier or 1)
			end
		end

		if not attack_data.weapon_unit:base().thrower_unit and die_hard_active then
			if math.random() <= managers.player:upgrade_value("player", "instakill_downed", 0) then
				damage = self._HEALTH_INIT
				managers.environment_controller:set_buff_effect(0.15)
			end
		end

		if not head and attack_data.weapon_unit:base().get_add_head_shot_mul then
			local add_head_shot_mul = attack_data.weapon_unit:base():get_add_head_shot_mul()

			if add_head_shot_mul and self._char_tweak.headshot_dmg_mul then
				if self._char_tweak.no_headshot_add_mul then
					add_head_shot_mul = add_head_shot_mul/2
				end

				damage = damage + (headshot_damage * add_head_shot_mul)
			end
		end

		if head then
			managers.player:on_headshot_dealt()
			headshot = true
		end

		if not attack_data.weapon_unit:base().thrower_unit then
			attack_data.base_damage = self:weapon_base_damage(attack_data.weapon_unit:base():get_name_id(), "weapon")
		end
	end

	damage = self:_apply_damage_reduction(damage, attack_data)

	if self:has_platings(self._unit) and not head then
		damage = self:calc_platings_damage(damage, self._unit, attack_data.attacker_unit)
	end

	attack_data.raw_damage = damage
	attack_data.headshot = head
	local damage_percent = math.ceil(math.clamp(damage / self._HEALTH_INIT_PRECENT, 1, self._HEALTH_GRANULARITY))
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._immortal then
		damage = math.min(damage, self._health - 1)
	end

	if self._health <= damage then
		if self:check_medic_heal() then
			result = {
				type = "healed",
				variant = attack_data.variant
			}
		else
			if head then
				managers.player:on_lethal_headshot_dealt(attack_data.attacker_unit, attack_data)
				self:_spawn_head_gadget({
					position = attack_data.col_ray.body:position(),
					rotation = attack_data.col_ray.body:rotation(),
					dir = attack_data.col_ray.ray
				})
			end

			attack_data.damage = self._health
			result = {
				type = "death",
				variant = attack_data.variant
			}

			self:die(attack_data)
			self:chk_killshot(attack_data.attacker_unit, "bullet", headshot, attack_data.weapon_unit:base():get_name_id())
		end
	else
		attack_data.damage = damage
		local result_type = not self._char_tweak.immune_to_knock_down and (attack_data.knock_down and "knock_down" or attack_data.stagger and not self._has_been_staggered and "stagger") or self:get_damage_type(damage_percent, "bullet")
		result = {
			type = result_type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	attack_data.result = result
	attack_data.pos = attack_data.col_ray.position

	if not is_civilian and attack_data.attacker_unit == managers.player:player_unit() then
		managers.player:archer_bolt_aoe(attack_data.raw_damage, self._unit)
	end

	if result.type == "death" then
		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			head_shot = head,
			weapon_unit = attack_data.weapon_unit,
			variant = attack_data.variant
		}

		if managers.groupai:state():all_criminals()[attack_data.attacker_unit:key()] then
			managers.statistics:killed_by_anyone(data)
		end

		if attack_data.attacker_unit == managers.player:player_unit() then
			local special_comment = self:_check_special_death_conditions(attack_data.variant, attack_data.col_ray.body, attack_data.attacker_unit, attack_data.weapon_unit)

			self:_comment_death(attack_data.attacker_unit, self._unit, special_comment)
			self:_show_death_hint(self._unit:base()._tweak_table)

			local attacker_state = managers.player:current_state()
			data.attacker_state = attacker_state

			managers.statistics:killed(data)
			self:_check_damage_achievements(attack_data, head)

			if not is_civilian then
				if not attack_data.weapon_unit:base().thrower_unit then
					if managers.player:has_category_upgrade("temporary", "overkill_damage_multiplier") then
						if attack_data.weapon_unit:base():is_category("shotgun", "saw") then
							managers.player:activate_temporary_upgrade("temporary", "overkill_damage_multiplier")
						end
					end
				end
			end

			if is_civilian then
				managers.money:civilian_killed()
			end
			-- ### UP TO 235u ### --
		elseif managers.groupai:state():is_unit_team_AI(attack_data.attacker_unit) then
			local special_comment = self:_check_special_death_conditions(attack_data.variant, attack_data.col_ray.body, attack_data.attacker_unit, attack_data.weapon_unit)

			self:_comment_death(attack_data.attacker_unit, self._unit, special_comment)
		elseif attack_data.attacker_unit:base().sentry_gun then
			if Network:is_server() then
				local server_info = attack_data.weapon_unit:base():server_information()

				if server_info and server_info.owner_peer_id ~= managers.network:session():local_peer():id() then
					local owner_peer = managers.network:session():peer(server_info.owner_peer_id)

					if owner_peer then
						owner_peer:send_queued_sync("sync_player_kill_statistic", data.name, data.head_shot and true or false, data.weapon_unit, data.variant, data.stats_name)
					end
				else
					data.attacker_state = managers.player:current_state()

					managers.statistics:killed(data)
				end
			end

			local sentry_attack_data = deep_clone(attack_data)
			sentry_attack_data.attacker_unit = attack_data.attacker_unit:base():get_owner()

			if sentry_attack_data.attacker_unit == managers.player:player_unit() then
				self:_check_damage_achievements(sentry_attack_data, head)
			else
				self._unit:network():send("sync_damage_achievements", sentry_attack_data.weapon_unit, sentry_attack_data.attacker_unit, sentry_attack_data.damage, sentry_attack_data.col_ray and sentry_attack_data.col_ray.distance, head)
			end
		end
	end

	local hit_offset_height = math.clamp(attack_data.col_ray.position.z - self._unit:movement():m_pos().z, 0, 300)
	local attacker = attack_data.attacker_unit

	if attacker:id() == -1 then
		attacker = self._unit
	end

	local weapon_unit = attack_data.weapon_unit

	if alive(weapon_unit) and weapon_unit:base() and weapon_unit:base().add_damage_result then
		weapon_unit:base():add_damage_result(self._unit, result.type == "death", attacker, damage_percent)
	end

	local variant = nil

	if result.type == "knock_down" then
		variant = 1
	elseif result.type == "stagger" then
		variant = 2
		self._has_been_staggered = true
	elseif result.type == "healed" then
		variant = 3
	else
		variant = 0
	end

	self:_send_bullet_attack_result(attack_data, attacker, damage_percent, body_index, hit_offset_height, variant)
	self:_on_damage_received(attack_data)

	if not is_civilian then
		managers.player:send_message(Message.OnEnemyShot, nil, self._unit, attack_data)
	end

	if not is_civilian and attack_data.attacker_unit == managers.player:player_unit() then
		managers.player:send_message(Message.OnEnemyShotChk, nil, self._unit, attack_data)
	end

	result.attack_data = attack_data

	return result
end

function CopDamage:damage_fire(attack_data)
	if self._dead or self._invulnerable then
		return
	end

	if self:is_friendly_fire(attack_data.attacker_unit) then
		return "friendly_fire"
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	local result = nil
	local damage = attack_data.damage
	local is_civilian = CopDamage.is_civilian(self._unit:base()._tweak_table)
	local head = self._head_body_name and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_head_body_name
	local headshot_multiplier = 0
	local headshot_damage = 0

	if attack_data.weapon_unit and attack_data.weapon_unit:base() and not attack_data.is_fire_dot_damage then
		headshot_damage = self:headshot_damage(attack_data, damage)
	end

	if attack_data.attacker_unit == managers.player:player_unit() then
		local critical_hit, crit_damage = self:roll_critical_hit(attack_data), self:critical_damage(attack_data, damage, false, true)
		local bleed_bonus_damage_taken = managers.player:upgrade_value("player", "bleed_bonus_damage_taken")
		local damage_scale = nil

		if alive(attack_data.weapon_unit) and attack_data.weapon_unit:base() and attack_data.weapon_unit:base().is_weak_hit then
			damage_scale = attack_data.weapon_unit:base():is_weak_hit(attack_data.col_ray and attack_data.col_ray.distance, attack_data.attacker_unit) or 1
		end

		if CustomDOTManager:is_bleeding(self._unit) and bleed_bonus_damage_taken ~= 0 and self._health/self._HEALTH_INIT < 0.5 then
			damage = damage * bleed_bonus_damage_taken
		end

		if critical_hit then
			managers.player:send_message(Message.OnCriticalDamage, nil, self._unit, attack_data)
			if not head then
				damage = crit_damage
			elseif head then
				if not self._damage_reduction_multiplier and self._char_tweak.headshot_dmg_mul then
					damage = crit_damage + headshot_damage
				end
			end
		end

		if head and not critical_hit and not self._damage_reduction_multiplier and self._char_tweak.headshot_dmg_mul then
			damage = damage + headshot_damage
		end

		if attack_data.weapon_unit and attack_data.variant ~= "stun" then
			if critical_hit then
				managers.hud:on_crit_confirmed(damage_scale)
			else
				managers.hud:on_hit_confirmed(damage_scale)
			end
		end

		if managers.groupai:state():is_enemy_special(self._unit) then
			damage = damage * managers.player:upgrade_value("weapon", "special_damage_taken_multiplier", 1)
		end

		if head then
			managers.player:on_headshot_dealt()
		end
	end

	if self:has_platings(self._unit) and not head then
		damage = self:calc_platings_damage(damage, self._unit, attack_data.attacker_unit)
	end

	damage = self:_apply_damage_reduction(damage, attack_data)
	damage = math.clamp(damage, 0, self._HEALTH_INIT)
	local damage_percent = math.ceil(damage / self._HEALTH_INIT_PRECENT)
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._immortal then
		damage = math.min(damage, self._health - 1)
	end

	if self._health <= damage then
		if self:check_medic_heal() then
			result = {
				type = "healed",
				variant = attack_data.variant
			}
		else
			attack_data.damage = self._health
			result = {
				type = "death",
				variant = attack_data.variant
			}

			self:die(attack_data)
			self:chk_killshot(attack_data.attacker_unit, "fire", head, attack_data.weapon_unit and attack_data.weapon_unit:base():get_name_id())
		end
	else
		attack_data.damage = damage
		local result_type = "dmg_rcv"
		result = {
			type = result_type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	attack_data.result = result
	attack_data.pos = attack_data.col_ray.position
	local attacker = attack_data.attacker_unit

	if not alive(attacker) or attacker:id() == -1 then
		attacker = self._unit
	end

	local attacker_unit = attack_data.attacker_unit

	if result.type == "death" then
		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			owner = attack_data.owner,
			weapon_unit = attack_data.weapon_unit,
			variant = attack_data.variant,
			head_shot = head,
			is_molotov = attack_data.is_molotov
		}

		managers.statistics:killed_by_anyone(data)

		if not is_civilian then
			if attacker_unit == managers.player:player_unit() and alive(attack_data.weapon_unit) then
				if not attack_data.weapon_unit:base().thrower_unit and attack_data.weapon_unit:base().is_category then
					if managers.player:has_category_upgrade("temporary", "overkill_damage_multiplier") then
						if attack_data.weapon_unit:base():is_category("shotgun", "saw") then
							managers.player:activate_temporary_upgrade("temporary", "overkill_damage_multiplier")
						end
					end
				end
			end
		end

		if attacker_unit and alive(attacker_unit) and attacker_unit:base() and attacker_unit:base().thrower_unit then
			attacker_unit = attacker_unit:base():thrower_unit()
			data.weapon_unit = attack_data.attacker_unit
		end

		if attacker_unit == managers.player:player_unit() then
			if alive(attacker_unit) then
				self:_comment_death(attacker_unit, self._unit)
			end

			self:_show_death_hint(self._unit:base()._tweak_table)
			managers.statistics:killed(data)

			if is_civilian then
				managers.money:civilian_killed()
			end

			self:_check_damage_achievements(attack_data, false)
		end
	end

	local weapon_unit = attack_data.weapon_unit or attacker

	if alive(weapon_unit) and weapon_unit:base() and weapon_unit:base().add_damage_result then
		weapon_unit:base():add_damage_result(self._unit, result.type == "death", damage_percent)
	end

	local i_result = self._result_type_to_idx.fire[result.type] or 0

	self:_send_fire_attack_result(attack_data, attacker, damage_percent, attack_data.col_ray.ray, i_result)
	self:_on_damage_received(attack_data)

	if not is_civilian and attack_data.attacker_unit and alive(attack_data.attacker_unit) then
		managers.player:send_message(Message.OnEnemyShot, nil, self._unit, attack_data)
	end

	if not is_civilian and attack_data.attacker_unit == managers.player:player_unit() then
		managers.player:send_message(Message.OnEnemyShotChk, nil, self._unit, attack_data)
	end

	result.attack_data = attack_data

	return result
end

function CopDamage:damage_explosion(attack_data)
	if self._dead or self._invulnerable then
		return
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	local is_civilian = CopDamage.is_civilian(self._unit:base()._tweak_table)
	local result = nil
	local damage = attack_data.damage
	damage = managers.modifiers:modify_value("CopDamage:DamageExplosion", damage, self._unit)

	if self._unit:base():char_tweak().DAMAGE_CLAMP_EXPLOSION then
		damage = math.min(damage, self._unit:base():char_tweak().DAMAGE_CLAMP_EXPLOSION)
	end

	damage = damage * (self._char_tweak.damage.explosion_damage_mul or 1)
	damage = damage * (self._marked_dmg_mul or 1)

	if attack_data.attacker_unit == managers.player:player_unit() then
		local critical_hit, crit_damage = self:roll_critical_hit(attack_data), self:critical_damage(attack_data, damage, nil, nil, true)
		local bleed_bonus_damage_taken = managers.player:upgrade_value("player", "bleed_bonus_damage_taken")

		if CustomDOTManager:is_bleeding(self._unit) and bleed_bonus_damage_taken ~= 0 and self._health/self._HEALTH_INIT < 0.5 then
			damage = damage * bleed_bonus_damage_taken
		end

		if critical_hit then
			managers.player:send_message(Message.OnCriticalDamage, nil, self._unit, attack_data)
			damage = crit_damage
		end

		if attack_data.weapon_unit and attack_data.variant ~= "stun" then
			if critical_hit then
				managers.hud:on_crit_confirmed()
			else
				managers.hud:on_hit_confirmed()
			end
		end
	end

	if self:has_platings(self._unit) then
		damage = self:calc_platings_damage(damage, self._unit, attack_data.attacker_unit)
	end

	damage = self:_apply_damage_reduction(damage, attack_data)
	attack_data.raw_damage = damage
	damage = math.clamp(damage, 0, self._HEALTH_INIT)
	local damage_percent = math.ceil(damage / self._HEALTH_INIT_PRECENT)
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._immortal then
		damage = math.min(damage, self._health - 1)
	end

	if self._health <= damage then
		if self:check_medic_heal() then
			attack_data.variant = "healed"
			result = {
				type = "healed",
				variant = attack_data.variant
			}
		else
			attack_data.damage = self._health
			result = {
				type = "death",
				variant = attack_data.variant
			}

			self:die(attack_data)
		end
	else
		attack_data.damage = damage
		local result_type = attack_data.variant == "stun" and "hurt_sick" or self:get_damage_type(damage_percent, "explosion")
		result = {
			type = result_type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	attack_data.result = result
	attack_data.pos = attack_data.col_ray.position

	if not is_civilian and attack_data.attacker_unit == managers.player:player_unit() then
		managers.player:archer_bolt_aoe(attack_data.raw_damage, self._unit)
	end

	local head = nil

	if result.type == "death" and self._head_body_name and attack_data.variant ~= "stun" then
		head = attack_data.col_ray.body and self._head_body_key and attack_data.col_ray.body:key() == self._head_body_key
		local body = self._unit:body(self._head_body_name)

		self:_spawn_head_gadget({
			position = body:position(),
			rotation = body:rotation(),
			dir = -attack_data.col_ray.ray
		})
	end

	local attacker = attack_data.attacker_unit

	if not attacker or attacker:id() == -1 then
		attacker = self._unit
	end

	if result.type == "death" then
		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			owner = attack_data.owner,
			weapon_unit = attack_data.weapon_unit,
			variant = attack_data.variant,
			head_shot = head
		}

		managers.statistics:killed_by_anyone(data)

		local attacker_unit = attack_data.attacker_unit

		if attacker_unit and attacker_unit:base() and attacker_unit:base().thrower_unit then
			attacker_unit = attacker_unit:base():thrower_unit()
			data.weapon_unit = attack_data.attacker_unit
		end

		if not is_civilian then
			if attacker_unit == managers.player:player_unit() then
				if attack_data.weapon_unit and attack_data.weapon_unit:base().weapon_tweak_data and not attack_data.weapon_unit:base().thrower_unit then
					if managers.player:has_category_upgrade("temporary", "overkill_damage_multiplier") then
						if attack_data.weapon_unit:base():is_category("shotgun", "saw") then
							managers.player:activate_temporary_upgrade("temporary", "overkill_damage_multiplier")
						end
					end
				end
			end
		end

		self:chk_killshot(attacker_unit, "explosion", false, attack_data.weapon_unit and attack_data.weapon_unit:base():get_name_id())

		if attacker_unit == managers.player:player_unit() then
			if alive(attacker_unit) then
				self:_comment_death(attacker_unit, self._unit)
			end

			self:_show_death_hint(self._unit:base()._tweak_table)
			managers.statistics:killed(data)

			if is_civilian then
				managers.money:civilian_killed()
			end

			self:_check_damage_achievements(attack_data, false)
		end
	end

	local weapon_unit = attack_data.weapon_unit

	if alive(weapon_unit) and weapon_unit:base() and weapon_unit:base().add_damage_result then
		weapon_unit:base():add_damage_result(self._unit, result.type == "death", attacker, damage_percent)
	end

	if not self._no_blood then
		managers.game_play_central:sync_play_impact_flesh(attack_data.pos, attack_data.col_ray.ray)
	end

	self:_send_explosion_attack_result(attack_data, attacker, damage_percent, self:_get_attack_variant_index(attack_data.result.variant), attack_data.col_ray.ray)
	self:_on_damage_received(attack_data)

	if not is_civilian and attack_data.attacker_unit and alive(attack_data.attacker_unit) then
		managers.player:send_message(Message.OnEnemyShot, nil, self._unit, attack_data)
	end

	if not is_civilian and attack_data.attacker_unit == managers.player:player_unit() then
		managers.player:send_message(Message.OnEnemyShotChk, nil, self._unit, attack_data)
	end

	return result
end

function CopDamage:damage_simple(attack_data)
	if self._dead or self._invulnerable then
		return
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	local is_civilian = CopDamage.is_civilian(self._unit:base()._tweak_table)
	local result = nil
	local damage = attack_data.damage

	if self._unit:base():char_tweak().DAMAGE_CLAMP_SHOCK then
		damage = math.min(damage, self._unit:base():char_tweak().DAMAGE_CLAMP_SHOCK)
	end

	damage = math.clamp(damage, 0, self._HEALTH_INIT)
	local damage_percent = math.ceil(damage / self._HEALTH_INIT_PRECENT)
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._immortal then
		damage = math.min(damage, self._health - 1)
	end

	if self._health <= damage then
		if self:check_medic_heal() then
			attack_data.variant = "healed"
			result = {
				type = "healed",
				variant = attack_data.variant
			}
		else
			attack_data.damage = self._health
			result = {
				type = "death",
				variant = attack_data.variant
			}

			self:die(attack_data)
		end
	else
		attack_data.damage = damage
		local result_type = self:get_damage_type(damage_percent)
		result = {
			type = result_type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	attack_data.result = result
	local attacker = attack_data.attacker_unit

	if not attacker or attacker:id() == -1 then
		attacker = self._unit
	end

	if result.type == "death" then
		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			owner = attack_data.owner,
			weapon_unit = attack_data.weapon_unit,
			variant = attack_data.variant
		}

		managers.statistics:killed_by_anyone(data)

		local attacker_unit = attack_data.attacker_unit

		if attacker_unit and attacker_unit:base() and attacker_unit:base().thrower_unit then
			attacker_unit = attacker_unit:base():thrower_unit()
			data.weapon_unit = attack_data.attacker_unit
		end

		if not is_civilian then
			if attacker_unit == managers.player:player_unit() then
				if attack_data.weapon_unit and attack_data.weapon_unit:base().weapon_tweak_data and not attack_data.weapon_unit:base().thrower_unit then
					if managers.player:has_category_upgrade("temporary", "overkill_damage_multiplier") then
						if attack_data.weapon_unit:base():is_category("shotgun", "saw") then
							managers.player:activate_temporary_upgrade("temporary", "overkill_damage_multiplier")
						end
					end
				end
			end
		end

		self:chk_killshot(attacker_unit, "shock", false, attack_data.weapon_unit and attack_data.weapon_unit:base():get_name_id())

		if attacker_unit == managers.player:player_unit() then
			if alive(attacker_unit) then
				self:_comment_death(attacker_unit, self._unit)
			end

			self:_show_death_hint(self._unit:base()._tweak_table)
			managers.statistics:killed(data)

			if is_civilian then
				managers.money:civilian_killed()
			end

			self:_check_damage_achievements(attack_data, false)
		end
	end

	if not self._no_blood and not (attack_data.variant == "bleed" or attack_data.variant == "eviscerate") then
		managers.game_play_central:sync_play_impact_flesh(attack_data.pos, attack_data.attack_dir)
	end

	local i_result = ({
		healed = 3,
		knock_down = 1,
		stagger = 2
	})[result.type] or 0

	self:_send_simple_attack_result(attacker, damage_percent, self:_get_attack_variant_index(attack_data.result.variant), i_result)
	self:_on_damage_received(attack_data)

	if not is_civilian and attack_data.attacker_unit and alive(attack_data.attacker_unit) then
		managers.player:send_message(Message.OnEnemyShot, nil, self._unit, attack_data)
	end

	if not is_civilian and attack_data.attacker_unit == managers.player:player_unit() and attack_data.weapon_unit and attack_data.weapon_unit:base().weapon_tweak_data then
		managers.player:send_message(Message.OnEnemyShotChk, nil, self._unit, attack_data)
	end

	if managers.player:is_wraith() and attack_data.attacker_unit == managers.player:player_unit() and attack_data.variant == "wraith" then
		self:_dismember_wraith()
	end

	return result
end

function CopDamage:damage_tase(attack_data)
	if self._dead or self._invulnerable then
		if self._invulnerable then
			print("INVULNERABLE!  Not tasing.")
		end

		return
	end

	if PlayerDamage.is_friendly_fire(self, attack_data.attacker_unit) then
		return "friendly_fire"
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	local result = nil
	local damage = attack_data.damage

	if attack_data.attacker_unit == managers.player:player_unit() then
		local critical_hit, crit_damage = self:roll_critical_hit(attack_data), self:critical_damage(attack_data, damage, nil, nil, nil, nil, true)

		if critical_hit then
			damage = crit_damage
		end

		if attack_data.weapon_unit then
			if critical_hit then
				managers.hud:on_crit_confirmed()
			else
				managers.hud:on_hit_confirmed()
			end
		end
	end

	damage = self:_apply_damage_reduction(damage, attack_data)
	damage = math.clamp(damage, 0, self._HEALTH_INIT)
	local damage_percent = math.ceil(damage / self._HEALTH_INIT_PRECENT)
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._health <= damage then
		attack_data.damage = self._health
		result = {
			variant = "bullet",
			type = "death"
		}

		self:die(attack_data)
		self:chk_killshot(attack_data.attacker_unit, "tase", false, attack_data.weapon_unit and attack_data.weapon_unit:base():get_name_id())
	else
		attack_data.damage = damage
		local type = (attack_data.forced or self._char_tweak.can_be_tased == nil or self._char_tweak.can_be_tased) and "taser_tased" or "none"
		result = {
			type = type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	if result.type == "taser_tased" and (attack_data.forced or not self._unit:anim_data() or not self._unit:anim_data().act) then
		if self._tase_effect then
			World:effect_manager():fade_kill(self._tase_effect)
		end

		self._tase_effect = World:effect_manager():spawn(self._tase_effect_table)
	end

	attack_data.result = result
	attack_data.pos = attack_data.col_ray.position
	local head = nil

	if result.type == "death" and self._head_body_name then
		head = attack_data.col_ray and attack_data.col_ray.body and self._head_body_key and attack_data.col_ray.body:key() == self._head_body_key
		local body = self._unit:body(self._head_body_name)
		local dir_vec = head and attack_data.col_ray.ray or body:rotation():y()

		self:_spawn_head_gadget({
			position = body:position(),
			rotation = body:rotation(),
			skip_push = not head,
			dir = dir_vec
		})
	end

	local attacker = attack_data.attacker_unit

	if not attacker or attacker:id() == -1 then
		attacker = self._unit
	end

	if result.type == "death" then
		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			owner = attack_data.owner,
			weapon_unit = attack_data.weapon_unit,
			variant = attack_data.variant,
			head_shot = head
		}

		managers.statistics:killed_by_anyone(data)

		local attacker_unit = attack_data.attacker_unit

		if attacker_unit and attacker_unit:base() and attacker_unit:base().thrower_unit then
			attacker_unit = attacker_unit:base():thrower_unit()
			data.weapon_unit = attack_data.attacker_unit
		end

		if attacker_unit == managers.player:player_unit() then
			if alive(attacker_unit) then
				self:_comment_death(attacker_unit, self._unit)
			end

			self:_show_death_hint(self._unit:base()._tweak_table)
			managers.statistics:killed(data)

			if CopDamage.is_civilian(self._unit:base()._tweak_table) then
				managers.money:civilian_killed()
			end

			self:_check_damage_achievements(attack_data, false)
		end
	end

	local weapon_unit = attack_data.weapon_unit

	if alive(weapon_unit) and weapon_unit:base() and weapon_unit:base().add_damage_result then
		weapon_unit:base():add_damage_result(self._unit, result.type == "death", damage_percent)
	end

	local variant = result.variant == "heavy" and 1 or 0

	self:_send_tase_attack_result(attack_data, damage_percent, variant)
	self:_on_damage_received(attack_data)

	return result
end

function CopDamage:damage_dot(attack_data)
	if self._dead or self._invulnerable then
		return
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	local result = nil
	local damage = attack_data.damage
	damage = self:_apply_damage_reduction(damage, attack_data)
	damage = math.clamp(damage, 0, self._HEALTH_INIT)
	local damage_percent = math.ceil(damage / self._HEALTH_INIT_PRECENT)
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._immortal then
		damage = math.min(damage, self._health - 1)
	end

	if self._health <= damage then
		if self:check_medic_heal() then
			result = {
				type = "healed",
				variant = attack_data.variant
			}
		else
			attack_data.damage = self._health
			result = {
				type = "death",
				variant = attack_data.variant
			}

			self:die(attack_data)
			self:chk_killshot(attack_data.attacker_unit, attack_data.variant or "poison", nil, attack_data.weapon_id)
		end
	else
		attack_data.damage = damage
		local result_type = attack_data.hurt_animation and self:get_damage_type(damage_percent, attack_data.variant) or "dmg_rcv"
		result = {
			type = result_type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	attack_data.result = result
	attack_data.pos = attack_data.col_ray.position
	local head = self._head_body_name and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_head_body_name
	local attacker = attack_data.attacker_unit

	if not attacker or attacker:id() == -1 then
		attacker = self._unit
	end

	local attacker_unit = attack_data.attacker_unit

	if result.type == "death" then
		local variant = attack_data.weapon_id and tweak_data.blackmarket and tweak_data.blackmarket.melee_weapons and tweak_data.blackmarket.melee_weapons[attack_data.weapon_id] and "melee" or attack_data.variant
		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			owner = attack_data.owner,
			weapon_unit = attack_data.weapon_unit,
			variant = variant,
			head_shot = head,
			weapon_id = attack_data.weapon_id,
			is_molotov = attack_data.is_molotov
		}

		managers.statistics:killed_by_anyone(data)

		if attacker_unit == managers.player:player_unit() then
			if alive(attacker_unit) then
				self:_comment_death(attacker_unit, self._unit)
			end

			self:_show_death_hint(self._unit:base()._tweak_table)
			managers.statistics:killed(data)

			if CopDamage.is_civilian(self._unit:base()._tweak_table) then
				managers.money:civilian_killed()
			end

			if attack_data and attack_data.weapon_id and not attack_data.weapon_unit then
				attack_data.name_id = attack_data.weapon_id

				self:_check_melee_achievements(attack_data)
			else
				self:_check_damage_achievements(attack_data, false)
			end
		end
	end

	local i_dot_variant = self._variant_to_idx.dot[attack_data.variant] or 0
	local i_result = self._result_type_to_idx.dot[result.type] or 0

	self:_send_dot_attack_result(attack_data, attacker, damage_percent, i_dot_variant, i_result)
	self:_on_damage_received(attack_data)

	result.attack_data = attack_data
	result.damage_percent = damage_percent
	result.damage = damage

	return result
end

function CopDamage:die(attack_data)
	if self._immortal then
		debug_pause("Immortal character died!")
	end

	local impersonate_attack_data = attack_data

	if not attack_data or type(attack_data) == "string" then
		--log("GOT STRING OR NIL, CREATING FAKE TABLE")
		impersonate_attack_data = {
			executed = false,
			attacker_unit = "failsafe"
		}
	end

	managers.modifiers:run_func("OnEnemyDied", self._unit, attack_data)
	self:_check_friend_4(attack_data)
	self:_check_ranc_9(attack_data)
	CopDamage.MAD_3_ACHIEVEMENT(attack_data)
	self:_remove_debug_gui()
	self._unit:base():set_slot(self._unit, 17)
	self:drop_pickup()
	self._unit:inventory():drop_shield()
	self:_chk_unique_death_requirements(attack_data, true)

	if self._unit:unit_data().mission_element then
		self._unit:unit_data().mission_element:event("death", self._unit)

		if not self._unit:unit_data().alerted_event_called then
			self._unit:unit_data().alerted_event_called = true

			self._unit:unit_data().mission_element:event("alerted", self._unit)
		end
	end

	if self._unit:movement() then
		self._unit:movement():remove_giveaway()
	end

	if impersonate_attack_data.executed and FOverhaul._data.Exec_indi then
		managers.hud:execution_indicator(self._unit, 9999)
	end

	self._health = 0
	self._health_ratio = 0
	self._dead = true

	self:set_mover_collision_state(false)

	if self._death_sequence then
		if self._unit:damage() and self._unit:damage():has_sequence(self._death_sequence) then
			self._unit:damage():run_sequence_simple(self._death_sequence)
		else
			debug_pause_unit(self._unit, "[CopDamage:die] does not have death sequence", self._death_sequence, self._unit)
		end
	end

	if self._unit:base():char_tweak().die_sound_event then
		self._unit:sound():play(self._unit:base():char_tweak().die_sound_event, nil, nil)
	end

	self:_on_death(nil, impersonate_attack_data)
	managers.mutators:notify(Message.OnCopDamageDeath, self._unit, attack_data)

	if self._tmp_invulnerable_clbk_key then
		managers.enemy:remove_delayed_clbk(self._tmp_invulnerable_clbk_key)

		self._tmp_invulnerable_clbk_key = nil
	end
end

function CopDamage:_on_death(variant, attack_data)
	local impersonate_attack_data = attack_data
	if not attack_data or type(attack_data) == "string" then
		--log("GOT STRING OR NIL, CREATING FAKE TABLE")
		impersonate_attack_data = {
			executed = false,
			attacker_unit = "failsafe"
		}
	end
	managers.player:chk_store_armor_health_kill_counter(self._unit, variant)
	managers.player:chk_wild_kill_counter(self._unit, variant, impersonate_attack_data.attacker_unit)
end

function CopDamage:get_enemy_max_health(unit)
	return tweak_data.character[unit:base()._tweak_table].HEALTH_INIT
end

function CopDamage:damage_melee(attack_data)
	if self._dead or self._invulnerable then
		return
	end

	if PlayerDamage.is_friendly_fire(self, attack_data.attacker_unit) then
		return "friendly_fire"
	end

	if self:chk_immune_to_attacker(attack_data.attacker_unit) then
		return
	end

	local result = nil
	local is_civlian = CopDamage.is_civilian(self._unit:base()._tweak_table)
	local is_gangster = CopDamage.is_gangster(self._unit:base()._tweak_table)
	local is_cop = not is_civlian and not is_gangster
	local head = self._head_body_name and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_head_body_name
	local damage = attack_data.damage

	if attack_data.attacker_unit and attack_data.attacker_unit == managers.player:player_unit() then
		local critical_hit, crit_damage = self:roll_critical_hit(attack_data), self:critical_damage(attack_data, damage, true)

		if critical_hit then
			managers.hud:on_crit_confirmed()

			damage = crit_damage
			attack_data.critical_hit = true
		else
			managers.hud:on_hit_confirmed()
		end

		local bleed_bonus_damage_taken = managers.player:upgrade_value("player", "bleed_bonus_damage_taken")

		if CustomDOTManager:is_bleeding(self._unit) and bleed_bonus_damage_taken ~= 0 and self._health/self._HEALTH_INIT < 0.5 then
			damage = damage * bleed_bonus_damage_taken
		end

		if tweak_data.achievement.cavity.melee_type == attack_data.name_id and not CopDamage.is_civilian(self._unit:base()._tweak_table) then
			managers.achievment:award(tweak_data.achievement.cavity.award)
		end
	end

	damage = damage * (self._marked_dmg_mul or 1)

	if self._unit:movement():cool() then
		damage = self._HEALTH_INIT
	end

	local damage_effect = attack_data.damage_effect
	local damage_effect_percent = 1
	damage = self:_apply_damage_reduction(damage, attack_data)
	damage = math.clamp(damage, self._HEALTH_INIT_PRECENT, self._HEALTH_INIT)
	local damage_percent = math.ceil(damage / self._HEALTH_INIT_PRECENT)
	damage = damage_percent * self._HEALTH_INIT_PRECENT
	damage, damage_percent = self:_apply_min_health_limit(damage, damage_percent)

	if self._immortal then
		damage = math.min(damage, self._health - 1)
	end

	if self._health <= damage then
		if self:check_medic_heal() then
			result = {
				type = "healed",
				variant = attack_data.variant
			}
		else
			damage_effect_percent = 1
			attack_data.damage = self._health
			result = {
				type = "death",
				variant = attack_data.variant
			}

			self:die(attack_data)
			self:chk_killshot(attack_data.attacker_unit, "melee", false, attack_data.name_id)
		end
	else
		attack_data.damage = damage
		damage_effect = math.clamp(damage_effect, self._HEALTH_INIT_PRECENT, self._HEALTH_INIT)
		damage_effect_percent = math.ceil(damage_effect / self._HEALTH_INIT_PRECENT)
		damage_effect_percent = math.clamp(damage_effect_percent, 1, self._HEALTH_GRANULARITY)
		local result_type = attack_data.shield_knock and self._char_tweak.damage.shield_knocked and "shield_knock" or attack_data.variant == "counter_tased" and "counter_tased" or attack_data.variant == "taser_tased" and "taser_tased" or attack_data.variant == "counter_spooc" and "expl_hurt" or self:get_damage_type(damage_effect_percent, "melee") or "fire_hurt"
		result = {
			type = result_type,
			variant = attack_data.variant
		}

		self:_apply_damage_to_health(damage)
	end

	attack_data.result = result
	attack_data.pos = attack_data.col_ray.position
	local dismember_victim = false
	local snatch_pager = false

	if result.type == "death" then
		if self:_dismember_condition(attack_data) then
			self:_dismember_body_part(attack_data)

			dismember_victim = true
		end

		local data = {
			name = self._unit:base()._tweak_table,
			stats_name = self._unit:base()._stats_name,
			head_shot = head,
			weapon_unit = attack_data.weapon_unit,
			name_id = attack_data.name_id,
			variant = attack_data.variant
		}

		managers.statistics:killed_by_anyone(data)

		if attack_data.attacker_unit == managers.player:player_unit() then
			self:_comment_death(attack_data.attacker_unit, self._unit)
			self:_show_death_hint(self._unit:base()._tweak_table)
			managers.statistics:killed(data)

			if managers.player:has_category_upgrade("player", "melee_armor_restore") then
				local armor_init = tweak_data.player.damage.ARMOR_INIT
				local base_max_armor = armor_init + managers.player:body_armor_value("armor") + managers.player:body_armor_skill_addend()
				local armor_restore = base_max_armor * managers.player:upgrade_value("player", "melee_armor_restore")[1]
				local recovery_rate_reduction = managers.player:upgrade_value("player", "melee_armor_restore")[2]
				managers.player:player_unit():character_damage():restore_armor(armor_restore)
				managers.player:player_unit():character_damage():_shorten_armor_recovery_timer(recovery_rate_reduction)
			end

			if not is_civlian and managers.groupai:state():whisper_mode() and managers.blackmarket:equipped_mask().mask_id == tweak_data.achievement.cant_hear_you_scream.mask then
				managers.achievment:award_progress(tweak_data.achievement.cant_hear_you_scream.stat)
			end

			mvector3.set(mvec_1, self._unit:position())
			mvector3.subtract(mvec_1, attack_data.attacker_unit:position())
			mvector3.normalize(mvec_1)
			mvector3.set(mvec_2, self._unit:rotation():y())

			local from_behind = mvector3.dot(mvec_1, mvec_2) >= 0

			if is_cop and Global.game_settings.level_id == "nightclub" and attack_data.name_id and attack_data.name_id == "fists" then
				managers.achievment:award_progress(tweak_data.achievement.final_rule.stat)
			end

			if is_civlian then
				managers.money:civilian_killed()
			elseif math.rand(1) < managers.player:upgrade_value("player", "melee_kill_snatch_pager_chance", 0) then
				snatch_pager = true
				self._unit:unit_data().has_alarm_pager = false
			end
		end
	end

	self:_check_melee_achievements(attack_data)

	local hit_offset_height = math.clamp(attack_data.col_ray.position.z - self._unit:movement():m_pos().z, 0, 300)
	local variant = nil

	if result.type == "shield_knock" then
		variant = 1
	elseif result.type == "counter_tased" then
		variant = 2
	elseif result.type == "expl_hurt" then
		variant = 4
	elseif snatch_pager then
		variant = 3
	elseif result.type == "taser_tased" then
		variant = 5
	elseif dismember_victim then
		variant = 6
	elseif result.type == "healed" then
		variant = 7
	else
		variant = 0
	end

	local body_index = self._unit:get_body_index(attack_data.col_ray.body:name())

	self:_send_melee_attack_result(attack_data, damage_percent, damage_effect_percent, hit_offset_height, variant, body_index)
	self:_on_damage_received(attack_data)

	result.attack_data = attack_data

	return result
end