Hooks:PreHook(CopMovement, "update", "BulletDecapitationsPreCopMovementUpdate", function(self, unit)
    local t2 = CopDamage:_get_dis_data()
    for decap_unit, _ in pairs(t2.ragdoll) do
        if decap_unit == unit then
            local bone_body = self._unit:get_object(Idstring("LeftUpLeg"))
            local bone_body2 = self._unit:get_object(Idstring("RightUpLeg"))
            local bone_body3 = self._unit:get_object(Idstring("LeftShoulder"))
            local bone_body4 = self._unit:get_object(Idstring("RightShoulder"))
            local bone_body5 = self._unit:get_object(Idstring("Spine1"))

            self._need_upd = false
            self._force_head_upd = nil
            self:upd_ground_ray()

            for part, _ in pairs(t2.parts[unit]) do
                if part == "Head" then
                    local head_obj = self._unit:get_object(Idstring("Head"))
                    head_obj:m_position(bone_body5:position():with_z(self._gnd_ray.position.z))
                    head_obj:set_position(bone_body5:position():with_z(self._gnd_ray.position.z))
                    head_obj:set_rotation(bone_body5:rotation())
                elseif part == "LeftArm" then
                    local left_arm_obj = self._unit:get_object(Idstring("LeftArm"))
                    local left_forearm_obj = self._unit:get_object(Idstring("LeftForeArm"))
                    local left_hand_obj = self._unit:get_object(Idstring("LeftHand"))

                    left_arm_obj:m_position(bone_body3:position():with_z(self._gnd_ray.position.z))
                    left_arm_obj:set_position(bone_body3:position():with_z(self._gnd_ray.position.z))
                    left_arm_obj:set_rotation(bone_body3:rotation())

                    left_forearm_obj:m_position(left_arm_obj:position():with_z(self._gnd_ray.position.z))
                    left_forearm_obj:set_position(left_arm_obj:position():with_z(self._gnd_ray.position.z))
                    left_forearm_obj:set_rotation(self._unit:get_object(Idstring("Spine1")):rotation())

                    left_hand_obj:m_position(self._unit:get_object(Idstring("Spine1")):position():with_z(self._gnd_ray.position.z))
                    left_hand_obj:set_position(self._unit:get_object(Idstring("Spine1")):position():with_z(self._gnd_ray.position.z))
                    left_hand_obj:set_rotation(self._unit:get_object(Idstring("Spine1")):rotation())
                elseif part == "RightArm" then
                    local right_arm_obj = self._unit:get_object(Idstring("RightArm"))
                    local right_forearm_obj = self._unit:get_object(Idstring("RightForeArm"))
                    local right_hand_obj = self._unit:get_object(Idstring("RightHand"))

                    right_arm_obj:m_position(bone_body4:position():with_z(self._gnd_ray.position.z))
                    right_arm_obj:set_position(bone_body4:position():with_z(self._gnd_ray.position.z))
                    right_arm_obj:set_rotation(bone_body4:rotation())

                    right_forearm_obj:m_position(right_arm_obj:position():with_z(self._gnd_ray.position.z))
                    right_forearm_obj:set_position(right_arm_obj:position():with_z(self._gnd_ray.position.z))
                    right_forearm_obj:set_rotation(self._unit:get_object(Idstring("Spine1")):rotation())

                    right_hand_obj:m_position(self._unit:get_object(Idstring("Spine1")):position():with_z(self._gnd_ray.position.z))
                    right_hand_obj:set_position(self._unit:get_object(Idstring("Spine1")):position():with_z(self._gnd_ray.position.z))
                    right_hand_obj:set_rotation(self._unit:get_object(Idstring("Spine1")):rotation())
                elseif part == "LeftLeg" then
                    local left_leg_obj = self._unit:get_object(Idstring("LeftLeg"))
                    local left_foot_obj = self._unit:get_object(Idstring("LeftFoot"))

                    left_leg_obj:m_position(bone_body:position():with_z(self._gnd_ray.position.z))
                    left_leg_obj:set_position(bone_body:position():with_z(self._gnd_ray.position.z))
                    left_leg_obj:set_rotation(bone_body:rotation())

                    left_foot_obj:m_position(self._unit:get_object(Idstring("Hips")):position():with_z(self._gnd_ray.position.z))
                    left_foot_obj:set_position(self._unit:get_object(Idstring("Hips")):position():with_z(self._gnd_ray.position.z))
                    left_foot_obj:set_rotation(self._unit:get_object(Idstring("Hips")):rotation())
                elseif part == "RightLeg" then
                    local right_leg_obj = self._unit:get_object(Idstring("RightLeg"))
                    local right_foot_obj = self._unit:get_object(Idstring("RightFoot"))

                    right_leg_obj:m_position(bone_body2:position():with_z(self._gnd_ray.position.z))
                    right_leg_obj:set_position(bone_body2:position():with_z(self._gnd_ray.position.z))
                    right_leg_obj:set_rotation(bone_body2:rotation())

                    right_foot_obj:m_position(self._unit:get_object(Idstring("Hips")):position():with_z(self._gnd_ray.position.z))
                    right_foot_obj:set_position(self._unit:get_object(Idstring("Hips")):position():with_z(self._gnd_ray.position.z))
                    right_foot_obj:set_rotation(self._unit:get_object(Idstring("Hips")):rotation())
                end
            end
        end
    end
end)