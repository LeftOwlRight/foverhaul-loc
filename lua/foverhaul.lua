FOverhaul = FOverhaul or class()
FOverhaul._delayed_functions = FOverhaul._delayed_functions or {}
FOverhaul._update_function = FOverhaul._update_function or {}
ErrorPath = SavePath .. "FOverhaul_Logs.json"
FOverhaul.error_data = FOverhaul.error_data or {}
FOverhaul.catched_error = FOverhaul.catched_error or {}

function FOverhaul:_is_already_updating(func)
    for _, upd in pairs(self._update_function) do
        if upd.func == func then
            return true
        end
    end

    return false
end

function FOverhaul:add_simple_updator(delay, func, global)
    if self:_is_already_updating(func) then
        return
    end

    local update_data = { func = func, global = global }
    table.insert(self._update_function, update_data)

    if delay then
        self:add_delay_to_update(delay, func)
    end

    self:init_updates(true)
end

function FOverhaul:init_updates(updator)
    self._updating = updator

    Hooks:Add("GameSetupUpdate", "update_functions_", function(t, dt)
        if self._update_function and self._updating then
            for i, update in pairs(self._update_function) do
                local func = update.func
                local global = update.global
                if self._delayed_functions[func] then
                    self._delay = self._delay or self._delayed_functions[func]
                    if self._delay <= t then
                        global[func](global)
                        self._delay = t + self._delayed_functions[func]
                    end
                else
                    global[func](global)
                end
            end
        end
    end)
end

function FOverhaul:add_delay_to_update(delay, func)
    self._delayed_functions[func] = delay
end

function FOverhaul:remove_updates(func)
    for i, updates in pairs(self._update_function) do
        if updates.func == func then
            table.remove(self._update_function, i)
            break
        end
    end
end

function FOverhaul:hold_all_updates()
    self._updating = false
end

function FOverhaul:clear_updates()
    self._update_function = {}
end

function FOverhaul:_chk_incompatibility(mod_check)
    local nonomods = { "Player Stats Panel Enhancements" }
    for _, mod in pairs(nonomods) do
        if mod_check == mod and BeardLib.Utils:ModExists(mod) then
            return true
        end
    end

    return false
end

function FOverhaul:catch_error_and_notify_later(caller, text)
    if not FOverhaul._data.error_logger then
        return
    end

    if self.catched_error[tostring(caller)] == tostring(text) then
        return
    end

    self.catched_error[tostring(caller)] = tostring(text)
    self:save_errors()
end

function FOverhaul:save_errors()
    local file = io.open(ErrorPath, "w+")
    local text = self.catched_error
	if file then
		file:write(json.encode(text))
		file:close()
	end
end

function FOverhaul:read_errors()
	local file = io.open(ErrorPath, "r")
	if file then
		self.error_data = json.decode(file:read("*all"))
		file:close()
	end
end

function FOverhaul:check_error_data_and_notify()
    local state = game_state_machine and game_state_machine:current_state_name()
    self:read_errors()
    if next(self.error_data) then
        if state == "menu_main" then
            local num = 0
            for i, v in pairs(self.error_data) do
                num = num + 1
                if num <= 5 then
                    self.errors_listed = self.errors_listed or ""
                    self.errors_listed = self.errors_listed .. "|" .. tostring(i) .. "|\n"
                end
            end

            if num > 5 then
                self.errors_listed = self.errors_listed .. "(..and " .. tostring(num - 5) .. " more)\n"
            end

            local errors = self.errors_listed or ""
            local dialog_data = {
                title = "[Freyah's Skills Overhaul] - ERROR OCCURED",
                text = "Woops, it looks like an error occured while you were playing and it was catched to notify you later. Please tell me about them!\nFull log can be found at: |" .. ErrorPath .. "|\n\nList of errors:\n" .. errors
            }
            local yes_button = {
                text = "Close",
                cancel_button = true
            }
            local no_button = {
                text = "Close and Clear Log",
                cancel_button = true,
                callback_func = callback(self, self, "clear_errors_cache")
            }
            dialog_data.button_list = {
                yes_button,
                no_button
            }
            managers.system_menu:show(dialog_data)
        end
    end
end

function FOverhaul:clear_errors_cache()
    self.catched_error = {}
    self.error_data = {}
    self:save_errors()
end