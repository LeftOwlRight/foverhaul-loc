local style_sound_path = ModPath
local style_S_sound = style_sound_path.."assets/style_sfx/Style_S.ogg"
local time_buffer = tweak_data.upgrades.values.player.style_rank[1].duration or 0
local volume = 0.75
local schinese = Idstring("schinese"):key() == SystemInfo:language():key()

function HUDManager:activate_teammate_immunity_radial(duration)
	managers.hud:get_teammate_panel_by_peer():activate_immunity_radial(duration)
end

function HUDManager:activate_teammate_unique_radial(duration)
	managers.hud:get_teammate_panel_by_peer():activate_unique_effect_radial(duration)
end

function HUDManager:init_platings()
	managers.hud:get_teammate_panel_by_peer():set_platings()
end

function HUDManager:upd_platings(amount, replaced)
	managers.hud:get_teammate_panel_by_peer():update_platings(amount, replaced)
end

function HUDManager:set_absorb_bar(percent)
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)
	local barElement = hud.panel:child("custom_bar_id")
	local barElementsh = hud.panel:child("custom_bar_id_underline")
	local texte = hud.panel:child("custom_text_id")
	local shadowTextElement = hud.panel:child("custom_text_id_shadow")
	local bar_width = 100
	local bar_height = 5
	local bar_x = hud.panel:w() - bar_width
	local bar_y = (hud.panel:h() / 2 - bar_height / 2) + 150
	local color = Color(0.8 + percent, 0.1 - percent, 1.2 - percent)
	local text_x = bar_x + bar_width / 2
	local text_y = bar_y - 30
	local absorb = managers.player:get_property("style_absorption", 0)

	local addons = {
		barElement,
		texte,
		shadowTextElement,
		barElementsh
	}

	if not hud then
		return
	end

	if percent <= 0 or not FOverhaul._data.daredevil_absorb_bar then
		if barElement and alive(barElement) then
			local fade_duration = 2
			local fade_out_rate = 1 / (fade_duration * 30)
			local function fadeOutUpdate()
				for _, huds in pairs(addons) do
					local alpha = huds:alpha()
					alpha = math.max(0, alpha - fade_out_rate)
					huds:set_alpha(alpha)

					if alpha <= 0 then
						for _, el in pairs(addons) do
							hud.panel:remove(el)
						end

						return
					end
				end
			end
			fadeOutUpdate()
		end

		return
	end

	if barElement and alive(barElement) then
		hud.panel:remove(barElement)
	end

	if barElementsh and alive(barElementsh) then
		hud.panel:remove(barElementsh)
	end

	if texte and alive(texte) then
		hud.panel:remove(texte)
	end

	if shadowTextElement and alive(shadowTextElement) then
		hud.panel:remove(shadowTextElement)
	end

	if not alive(barElement) then
		hud.panel:rect({
			name = "custom_bar_id",
			alpha = 1,
			x = bar_x,
			y = bar_y,
			w = bar_width * percent,
			h = bar_height,
			layer = 5,
			color = color
		})
	end

	if not alive(barElementsh) then
		hud.panel:rect({
			name = "custom_bar_id_underline",
			alpha = 1,
			x = bar_x + 1,
			y = bar_y + 1,
			w = bar_width + 1,
			h = bar_height + 1,
			layer = 4,
			color = Color(0, 0, 0)
		})
	end

	hud.panel:text({
		name = "custom_text_id",
		text = "BONUS DEFENSE: "..absorb,
		x = text_x - 47,
		y = text_y + 10,
		alpha = 1,
		layer = 5,
		color = color,
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hud.small_font_size,
	})

	hud.panel:text({
		name = "custom_text_id_shadow",
		text = "BONUS DEFENSE: "..absorb,
		x = text_x - 48,
		y = text_y + 11,
		alpha = 1,
		layer = 4,
		color = Color(0,0,0),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hud.small_font_size,
	})
end

function HUDManager:wraith_lifeline_cooldown_vfx()
	local data = managers.player:upgrade_value("player", "semi_wraith_lifeline")
	local cooldown = data.cooldown

	if FOverhaul._data.life_line_wraith_cooldown_enabled then
		self:activate_teammate_immunity_radial(cooldown)
	end

	if FOverhaul._data.wraith_hint_enabled then
		managers.hud:show_hint({
			time = 3,
			text = "LIFELINE TRIGGERED!"
		})
	end
end

function HUDManager:set_wraith_energy_bar(percent, energy_left)
	if not FOverhaul._data.wraith_energy_enabled then
		return
	end

	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)

	if not hud then
		return
	end

	local barElement = hud.panel:child("custom_bar_id2")
	local barElementsh = hud.panel:child("custom_bar_id_underline2")
	local texte = hud.panel:child("custom_text_id2")
	local shadowTextElement = hud.panel:child("custom_text_id_shadow2")
	local bar_width = 5
	local bar_height = -100
	local bar_x = hud.panel:w() - bar_width
	local bar_y = (hud.panel:h() / 2 - bar_height / 2) + 150
	local color = Color(1, 0, 47/255)

	local addons = {
		barElement,
		barElementsh,
		texte,
		shadowTextElement
	}

	if percent <= 0 then
		if barElement and alive(barElement) then
			for _, el in pairs(addons) do
				hud.panel:remove(el)
			end
		end

		return
	end

	if barElement and alive(barElement) then
		hud.panel:remove(barElement)
	end

	if barElementsh and alive(barElementsh) then
		hud.panel:remove(barElementsh)
	end

	if texte and alive(texte) then
		hud.panel:remove(texte)
	end

	if shadowTextElement and alive(shadowTextElement) then
		hud.panel:remove(shadowTextElement)
	end

	if not alive(barElement) then
		hud.panel:rect({
			name = "custom_bar_id2",
			alpha = 1,
			x = bar_x,
			y = bar_y,
			w = bar_width,
			h = bar_height * percent,
			layer = 5,
			color = color
		})
	end

	if not alive(barElementsh) then
		hud.panel:rect({
			name = "custom_bar_id_underline2",
			alpha = 0.5,
			x = bar_x,
			y = bar_y,
			w = bar_width,
			h = bar_height,
			layer = 4,
			color = Color(0, 0, 0)
		})
	end

	hud.panel:text({
		name = "custom_text_id2",
		text = "ENERGY: "..tostring(energy_left),
		x = bar_x - 61,
		y = bar_y - 12,
		alpha = 1,
		layer = 5,
		color = color,
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hud.small_font_size,
	})

	hud.panel:text({
		name = "custom_text_id_shadow2",
		text = "ENERGY: "..tostring(energy_left),
		x = bar_x - 62,
		y = bar_y - 11,
		alpha = 1,
		layer = 4,
		color = Color(0,0,0),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hud.small_font_size,
	})
end

function HUDManager:swan_song_fight_or_die()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)

	if not hud then
		return
	end

	local u_h = hud.panel:child("fod_swan_song")

	if u_h and alive(u_h) then
		hud.panel:remove(u_h)
	end

	local u = hud.panel:text({
		name = "fod_swan_song",
		text = "FIGHT OR DIE!",
		x = hud.panel:center_x() - 70,
		y = hud.panel:center_y() - 130,
		alpha = 1,
		layer = 99,
		color = Color.white,
		font = "fonts/font_large_mf",
		font_size = 30
	})

	if schinese then
		u = hud.panel:text({
			name = "fod_swan_song",
			text = "不战斗，就会死！",
			x = hud.panel:center_x() - 70,
			y = hud.panel:center_y() - 130,
			alpha = 1,
			layer = 99,
			color = Color.white,
			font = "fonts/font_large_mf",
			font_size = 30
		})
	end

	local function func2(o, dir)
		local mt = 0.25
		local t = mt

		while t > 0 do
			local dt = coroutine.yield()
			t = math.clamp(t - dt, 0, mt)
			local speed = dt * 600

			o:move(dir * speed, (1 - math.abs(dir)) * -speed)
			u:set_alpha(t / mt)
		end

		u:remove(o)
		u:hide()
	end

	local function func(o)
		local mt = 2
		local t = mt

		while t >= 0 do
			local dt = coroutine.yield()
			t = math.clamp(t - dt, 0, mt)
			local v = t / mt
			local a = 1
			local r = 1
			local g = 0.25 + 0.75 * (1 - v)
			local b = 0.25 + 0.75 * (1 - v)

			o:set_color(Color(a, r, g, b))
		end
	end

	local dir = 0

	u:animate(func)
	DelayedCalls:Add("m_d", 1.3, function()
		u:animate(func2, dir)
	end)
end

function HUDManager:_remove_gui_exec(unit)
	local unit_key = tostring(unit:key())
	if alive(self[unit_key ..  "gui_" .. "exec"]) and alive(self[unit_key ..  "ws_" .. "exec"]) then
		self[unit_key ..  "gui_" .. "exec"]:destroy_workspace(self[unit_key ..  "ws_" .. "exec"])
		self[unit_key ..  "ws_" .. "exec"] = nil
		self[unit_key ..  "gui_" .. "exec"] = nil
	end
end

function HUDManager:world_gui_text_create(unit, id)
	local unit_key = tostring(unit:key())
	self[unit_key ..  "gui_" .. tostring(id)] = World:newgui()
	local obj = unit:get_object(Idstring("Head"))
	self[unit_key ..  "ws_" .. tostring(id)] = self[unit_key ..  "gui_" .. tostring(id)]:create_linked_workspace(50, 50, obj, obj:position() + obj:rotation():y(), obj:rotation():x() * 20, obj:rotation():y() * 20)
	self[unit_key ..  "ws_" .. tostring(id)]:set_billboard(self[unit_key ..  "ws_" .. tostring(id)].BILLBOARD_BOTH)
end

function HUDManager:show_enemy_shield()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local child = hud.panel:child("enemy_shield")

	if child and alive(child) then
		hud.panel:remove(child)
	end

	self._show_enemy_shield = hud.panel:bitmap({
		y = -100,
		rotation = 360,
		vertical = "center",
		h = 20,
		visible = true,
		w = 20,
		align = "center",
		render_template = "OverlayVertexColorTextured",
		color = Color.white,
		name = "enemy_shield",
		texture = "guis/textures/pd2/enemy-shield",
		alpha = 1
	})

	self._show_enemy_shield:set_center(hud.panel:w() / 2, hud.panel:h() / 2)
	self._show_enemy_shield:set_x(self._show_enemy_shield:x() + 20)
	self._show_enemy_shield:set_y(self._show_enemy_shield:y() - 10)

	local ui = self._show_enemy_shield
	local function func2(o)
		local mt = 0.5
		local t = mt
		local rotation_speed = 400
		local total_rotation = 0
		local direction = 1

		while t > 0 do
			local dt = coroutine.yield()
			t = math.clamp(t - dt, 0, mt)

			local rotation_amount = rotation_speed * dt * direction
			total_rotation = total_rotation + rotation_amount

			if total_rotation >= 5 then
				direction = -1
			elseif total_rotation <= -5 then
				direction = 1
			end

			o:rotate(rotation_amount)
			ui:set_alpha(t / mt)
		end

		ui:remove(o)
		ui:hide()
	end
	ui:animate(func2)
end

function HUDManager:def_debuff_visual()
	if not FOverhaul._data.def_debuff_vis then
		return
	end

	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	self._show_def_debuff = hud.panel:bitmap({
		vertical = "center",
		h = 20,
		visible = true,
		w = 20,
		align = "center",
		name = "def_debuff",
		texture = "guis/textures/pd2/def_debuff",
		alpha = 1
	})

	self._show_def_debuff:set_center(hud.panel:w() / 2, hud.panel:h() / 2)
	self._show_def_debuff:set_x(self._show_def_debuff:x() + 380)
	self._show_def_debuff:set_y(self._show_def_debuff:y() + 325)

	self._def_enabled = self._def_enabled ~= nil or true

	local function func2(o, dir)
		local mt = 0.25
		local t = mt

		while t > 0 do
			local dt = coroutine.yield()
			t = math.clamp(t - dt, 0, mt)

			self._show_def_debuff:set_alpha(t / mt)
		end

		self._show_def_debuff:remove(o)
		self._show_def_debuff:hide()
	end

	local function func(o, dir)
		local mt = 2
		local t = mt
		local decreasing = true
		while self._def_enabled do
			local dt = coroutine.yield()

			if decreasing then
				t = t - dt
				if t <= 0.3 * mt then
					t = 0.3 * mt
					decreasing = false
				end
			else
				t = t + dt
				if t >= mt then
					t = mt
					decreasing = true
				end
			end
			self._show_def_debuff:set_alpha(t / mt)
		end
	end

	self._show_def_debuff:animate(func)

	DelayedCalls:Add("dfd", tweak_data.upgrades.sniper_ds_defense_debuff[1], function()
		self._def_enabled = false
		self._show_def_debuff:animate(func2)
	end)
end

function HUDManager:show_cracked_shield()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local children = { "cracked_shield_l", "cracked_shield_r", "enemy_shield" }

	for _, child in pairs(children) do
		local panel = hud.panel:child(child)
		if panel and alive(panel) then
			hud.panel:remove(panel)
		end
	end

	self._show_cracked_shield_left = hud.panel:bitmap({
		y = -100,
		rotation = 360,
		vertical = "center",
		h = 20,
		visible = true,
		w = 20,
		align = "center",
		render_template = "OverlayVertexColorTextured",
		color = Color.white,
		name = "cracked_shield_l",
		texture = "guis/textures/pd2/cracked-shield-left",
		alpha = 1
	})

	self._show_cracked_shield_right = hud.panel:bitmap({
		y = -100,
		rotation = 360,
		vertical = "center",
		h = 20,
		visible = true,
		w = 20,
		align = "center",
		render_template = "OverlayVertexColorTextured",
		color = Color.white,
		name = "cracked_shield_r",
		texture = "guis/textures/pd2/cracked-shield-right",
		alpha = 1
	})

	self._show_cracked_shield_left:set_center(hud.panel:w() / 2, hud.panel:h() / 2)
	self._show_cracked_shield_left:set_x(self._show_cracked_shield_left:x() + 20)
	self._show_cracked_shield_left:set_y(self._show_cracked_shield_left:y() - 10)
	self._show_cracked_shield_right:set_center(hud.panel:w() / 2, hud.panel:h() / 2)
	self._show_cracked_shield_right:set_x(self._show_cracked_shield_left:x())
	self._show_cracked_shield_right:set_y(self._show_cracked_shield_left:y())

	local ui_l = self._show_cracked_shield_left
	local ui_r = self._show_cracked_shield_right

	local function func_left(o)
		local mt = 1.5
		local t = mt
		local rotation_speed = -30
		local move_speed = 8
		local total_rotation = 0
		local move_distance = -10/mt

		while t >= 0 do
			local dt = coroutine.yield()
			t = math.clamp(t - dt, 0, mt)

			local rotation_amount = rotation_speed * dt
			local move_amount = move_speed * dt

			total_rotation = total_rotation + rotation_amount

			if total_rotation <= -20 then
				rotation_amount = -20 - (total_rotation - rotation_amount)
				total_rotation = -20
			end

			o:rotate(rotation_amount)
			o:move(move_distance * dt, move_amount)
			ui_l:set_alpha(t / mt)
		end

		ui_l:remove(o)
		ui_l:hide()
	end

	local function func_right(o)
		local mt = 1.5
		local t = mt
		local rotation_speed = 30
		local move_speed = 8
		local total_rotation = 0
		local move_distance = 10/mt

		while t >= 0 do
			local dt = coroutine.yield()
			t = math.clamp(t - dt, 0, mt)

			local rotation_amount = rotation_speed * dt
			local move_amount = move_speed * dt

			total_rotation = total_rotation + rotation_amount

			if total_rotation >= 20 then
				rotation_amount = 20 - (total_rotation - rotation_amount)
				total_rotation = 20
			end

			o:rotate(rotation_amount)
			o:move(move_distance * dt, move_amount)
			ui_r:set_alpha(t / mt)
		end

		ui_r:remove(o)
		ui_r:hide()
	end

	ui_l:animate(func_left)
	ui_r:animate(func_right)
end

function HUDManager:show_damage_execution(damage, unit)
	local options_data = FOverhaul._data
	local dur = options_data.text_dur_exec or 2
	local unit_key = tostring(unit:key())
	local text_exec = ""

	if options_data.style_exec_indi == 1 then
		text_exec = tostring(damage)
	elseif options_data.style_exec_indi == 2 then
		text_exec = "EXECUTED!"
		if schinese then
			text_exec = "秒杀！"
		end
	end

	if damage and damage > 0 then
		local text = self[unit_key ..  "ws_" .. "exec"]:panel():text({
			y = -100,
			rotation = 360,
			font_size = 90,
			vertical = "center",
			h = 40,
			visible = true,
			w = 40,
			align = "center",
			render_template = "OverlayVertexColorTextured",
			font = "fonts/font_large_mf",
			text = text_exec,
			color = Color.white
		})

		local function func2(o, dir)
			local mt = dur
			local t = mt

			while t > 0 do
				local dt = coroutine.yield()
				t = math.clamp(t - dt, 0, mt)
				local speed = dt * 100

				o:move(dir * speed, (1 - math.abs(dir)) * -speed)
				text:set_alpha(t / mt)
			end

			self[unit_key ..  "ws_" .. "exec"]:panel():remove(o)
			self:_remove_gui_exec(unit)
		end

		local function func(o)
			local mt = dur
			local t = mt

			while t >= 0 do
				local dt = coroutine.yield()
				t = math.clamp(t - dt, 0, mt)
				local a = 1
				local r,g,b = CatLib:Rainbow(options_data.rainbow_speed_exec)

				o:set_color(Color(a, r, g, b))
			end
		end


		local dir = math.sin(Application:time() * 1000)

		if options_data.rainbow_color_exec then
			text:animate(func)
		end

		text:animate(func2, dir)
	end
end

function HUDManager:execution_indicator(enemy, damage)
	if not alive(self[tostring(enemy:key()) ..  "ws_" .. "exec"]) then
		self:world_gui_text_create(enemy, "exec")
	end

	self:show_damage_execution(damage, enemy)
end

function HUDManager:platings_broken_ui()
	self:show_cracked_shield()
end

function HUDManager:enemy_platings_ui()
	self:show_enemy_shield()
end

function HUDManager:activate_bullet_efficiency_vfx()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local function make_bitmap_2()
		return hud.panel:bitmap({
			name = "custom_fullscreen_effect_3",
			texture = "guis/textures/full_screen_effect_2",
			w = hud.panel:w(),
			h = hud.panel:h(),
			layer = 1,
			alpha = 0,
			x = 0,
			y = 0,
			blend_mode = "add",
			color = Color.white,
			visible = false
		})
	end

	local custom_fullscreen_effect = hud.panel:child("custom_fullscreen_effect_3") or make_bitmap_2()
	local function anim(o)
		custom_fullscreen_effect:set_visible(true)
		custom_fullscreen_effect:animate(function(anim_object)
			over(1, function(p)
				local alpha_value = math.abs(math.sin(p * 180))
				custom_fullscreen_effect:set_alpha(alpha_value)
			end)
			custom_fullscreen_effect:set_alpha(0)
			custom_fullscreen_effect:set_visible(false)
		end)
	end
	custom_fullscreen_effect:stop()
	custom_fullscreen_effect:animate(anim)
end

function HUDManager:activate_style_S_overlay()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local function make_bitmap_2()
		return hud.panel:bitmap({
			name = "custom_fullscreen_effect_2",
			texture = "guis/textures/full_screen_effect_1",
			w = hud.panel:w(),
			h = hud.panel:h(),
			layer = 1,
			alpha = 0,
			x = 0,
			y = 0,
			blend_mode = "add",
			color = Color.white,
			visible = false
		})
	end

	local custom_fullscreen_effect = hud.panel:child("custom_fullscreen_effect_2") or make_bitmap_2()
	local function anim(o)
		custom_fullscreen_effect:set_visible(true)
		custom_fullscreen_effect:animate(function(anim_object)
			over(1, function(p)
				local alpha_value = math.abs(math.sin(p * 180))
				custom_fullscreen_effect:set_alpha(alpha_value)
			end)
			custom_fullscreen_effect:set_alpha(0)
			custom_fullscreen_effect:set_visible(false)
		end)
	end
	custom_fullscreen_effect:stop()
	custom_fullscreen_effect:animate(anim)
end

function HUDManager:dire_need_overlay()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local function make_bitmap_2()
		return hud.panel:bitmap({
			name = "custom_fullscreen_effect_5",
			texture = "guis/textures/full_screen_effect_5",
			w = hud.panel:w(),
			h = hud.panel:h(),
			layer = 1,
			alpha = 0,
			x = 0,
			y = 0,
			blend_mode = "add",
			color = Color.white,
			visible = false
		})
	end

	local custom_fullscreen_effect = hud.panel:child("custom_fullscreen_effect_5") or make_bitmap_2()
	local function anim(o)
		custom_fullscreen_effect:set_visible(true)
		custom_fullscreen_effect:animate(function(anim_object)
			over(1, function(p)
				local alpha_value = math.abs(math.sin(p * 180))
				custom_fullscreen_effect:set_alpha(alpha_value)
			end)
			custom_fullscreen_effect:set_alpha(0)
			custom_fullscreen_effect:set_visible(false)
		end)
	end
	custom_fullscreen_effect:stop()
	custom_fullscreen_effect:animate(anim)
end

function HUDManager:swan_song_overlay()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local function make_bitmap_2()
		return hud.panel:bitmap({
			name = "custom_fullscreen_effect_3",
			texture = "guis/textures/full_screen_effect_3",
			w = hud.panel:w(),
			h = hud.panel:h(),
			layer = 1,
			alpha = 0,
			x = 0,
			y = 0,
			blend_mode = "add",
			color = Color.white,
			visible = false
		})
	end

	local custom_fullscreen_effect = hud.panel:child("custom_fullscreen_effect_3") or make_bitmap_2()
	local function anim(o)
		custom_fullscreen_effect:set_visible(true)
		custom_fullscreen_effect:animate(function(anim_object)
			over(1, function(p)
				local alpha_value = math.abs(math.sin(p * 180))
				custom_fullscreen_effect:set_alpha(alpha_value)
			end)
			custom_fullscreen_effect:set_alpha(0)
			custom_fullscreen_effect:set_visible(false)
		end)
	end
	custom_fullscreen_effect:stop()
	custom_fullscreen_effect:animate(anim)
end

function HUDManager:swan_song_overlay_survived()
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local function make_bitmap_2()
		return hud.panel:bitmap({
			name = "custom_fullscreen_effect_4",
			texture = "guis/textures/full_screen_effect_4",
			w = hud.panel:w(),
			h = hud.panel:h(),
			layer = 1,
			alpha = 0,
			x = 0,
			y = 0,
			blend_mode = "add",
			color = Color.white,
			visible = false
		})
	end

	local custom_fullscreen_effect = hud.panel:child("custom_fullscreen_effect_4") or make_bitmap_2()
	local function anim(o)
		custom_fullscreen_effect:set_visible(true)
		custom_fullscreen_effect:animate(function(anim_object)
			over(1, function(p)
				local alpha_value = math.abs(math.sin(p * 180))
				custom_fullscreen_effect:set_alpha(alpha_value)
			end)
			custom_fullscreen_effect:set_alpha(0)
			custom_fullscreen_effect:set_visible(false)
		end)
	end
	custom_fullscreen_effect:stop()
	custom_fullscreen_effect:animate(anim)
end

function HUDManager:wraith_healbullet_overlay()
	if not FOverhaul._data.wraith_heal_bullet_enabled then
		return
	end

	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	local function make_bitmap_2()
		return hud.panel:bitmap({
			name = "custom_fullscreen_effect_6",
			texture = "guis/textures/full_screen_effect_6",
			w = hud.panel:w(),
			h = hud.panel:h(),
			layer = 1,
			alpha = 0,
			x = 0,
			y = 0,
			blend_mode = "add",
			color = Color.white,
			visible = false
		})
	end

	local custom_fullscreen_effect = hud.panel:child("custom_fullscreen_effect_6") or make_bitmap_2()
	local function anim(o)
		custom_fullscreen_effect:set_visible(true)
		custom_fullscreen_effect:animate(function(anim_object)
			over(1, function(p)
				local alpha_value = math.abs(math.sin(p * 180))
				custom_fullscreen_effect:set_alpha(alpha_value)
			end)
			custom_fullscreen_effect:set_alpha(0)
			custom_fullscreen_effect:set_visible(false)
		end)
	end
	custom_fullscreen_effect:stop()
	custom_fullscreen_effect:animate(anim)
end

Hooks:PostHook(HUDManager, "_setup_player_info_hud_pd2", "style_ranks", function(self, ...)
    local has_dreadevil = managers.player:has_category_upgrade("player", "style_rank")
    if has_dreadevil then
        self._style_rank_hud = Style_Hud:new(managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2))
        self:add_updator("style_ranks_udp", callback(self._style_rank_hud, self._style_rank_hud, "update"))
    end
end)

function HUDManager:StyleOnKillshot()
    self._style_rank_hud:OnKillshot()
end

Style_Hud = Style_Hud or class()
function Style_Hud:init(hud)
    self._t = 0
    self._kill_time = 0
    self._kills = 0
    self._stage = 0
    self._hud_panel = hud.panel
    self._full_hud_panel = managers.hud._fullscreen_workspace:panel():gui(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2, {})
    self._combo_panel = self._full_hud_panel:panel({
        visible = false,
        name = "style_panel",
        x = 0,
        y = 0,
        valign = "top",
        blend_mode = "normal"
    })
    self._combo_icon = self._combo_panel:bitmap({
        name = "Combo_icon",
        visible = true,
        layer = 0,
        texture = "guis/textures/Rank_D",
        x = 0,
        w = 96,
        h = 96,
        vertical = "top"
    })
    self._combo_icon:set_left(self._full_hud_panel:left())
    self._combo_icon:set_top(160)
    if blt.xaudio then
        blt.xaudio.setup()
	end
end

function Style_Hud:set_combo(combo)
    if Utils:IsInHeist() and combo >= 1 then
		self._combo_panel:set_visible(true)
	elseif Utils:IsInHeist() and combo < 1 and self._stage > 0 then
		self._stage = 0
		self._combo_icon:animate(callback(self, self, "close_anim"))
	elseif not Utils:IsInHeist() then
		self._combo_panel:set_visible(false)
	end

    if combo > 0 then
        if self._stage == 0 and combo >= 1 then
            self._combo_icon:set_image("guis/textures/Rank_E")
            self._combo_icon:animate(callback(self, self, "open_anim"))
            self._stage = 1
        elseif self._stage == 1 and combo >= 2 then
            self._combo_icon:set_image("guis/textures/Rank_D")
            self._combo_icon:animate(callback(self, self, "open_anim"))
            self._stage = 2
        elseif self._stage == 2 and combo >= 3 then
            self._combo_icon:set_image("guis/textures/Rank_C")
            self._combo_icon:animate(callback(self, self, "open_anim"))
            self._stage = 3
        elseif self._stage == 3 and combo >= 4 then
            self._combo_icon:set_image("guis/textures/Rank_B")
            self._combo_icon:animate(callback(self, self, "open_anim"))
            self._stage = 4
        elseif self._stage == 4 and combo >= 5 then
            self._combo_icon:set_image("guis/textures/Rank_A")
            self._combo_icon:animate(callback(self, self, "open_anim"))
            self._stage = 5
        elseif self._stage == 5 and combo >= 6 then
            self._combo_icon:set_image("guis/textures/Rank_S")
            self._combo_icon:animate(callback(self, self, "open_anim"))
            XAudio.UnitSource:new(XAudio.PLAYER, XAudio.Buffer:new(style_S_sound)):set_volume(volume)
            managers.hud:activate_style_S_overlay()
            self._stage = 6
        end
    end
end

function Style_Hud:open_anim(panel)
    local panel_x = 64
    local panel_y = 160
    local speed = 75
    panel:set_x(panel_x-96)
    panel:set_top(panel_y)
    local TOTAL_T = 10/speed
    local t = TOTAL_T
    while t > 0 do
        local dt = coroutine.yield()
        t = t - dt
        panel:set_x(panel_x - 96 + (1 - t / TOTAL_T) * 96)
    end
end

function Style_Hud:close_anim(panel)
    local panel_x = 64
    local panel_y = 160
	local speed = 150
	local TOTAL_T = 10/speed
	local t = TOTAL_T
	while t > 0 do
		local dt = coroutine.yield()
		t = t - dt
		panel:set_x(panel_x - (1 - t/TOTAL_T) * 200)
	end
	self._combo_panel:set_visible(false)
end

function Style_Hud:OnKillshot()
	self._combo_icon:set_w(96)
	self._combo_icon:set_h(96)
	time_buffer = tweak_data.upgrades.values.player.style_rank[1].duration or 0
    self._kills = self._kills + 1
    self._kill_time = self._t
end

function Style_Hud:update(t, dt)
    self._t = t
    if (t - self._kill_time) > time_buffer then
        self._kills = 0
    end
    self:set_combo(self._kills)
end