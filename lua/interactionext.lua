BaseInteractionExt = BaseInteractionExt or class()
function BaseInteractionExt:_get_timer()
	local modified_timer = self:_get_modified_timer()

	if modified_timer then
		return modified_timer
	end

	local multiplier = 1

	if self.tweak_data ~= "corpse_alarm_pager" then
		multiplier = multiplier * managers.player:crew_ability_upgrade_value("crew_interact", 1)
	end

	if self._tweak_data.upgrade_timer_multiplier and self.tweak_data ~= "revive" then
		multiplier = multiplier * managers.player:upgrade_value(self._tweak_data.upgrade_timer_multiplier.category, self._tweak_data.upgrade_timer_multiplier.upgrade, 1)
	end

	if self._tweak_data.upgrade_timer_multipliers and self.tweak_data ~= "revive" then
		for _, upgrade_timer_multiplier in pairs(self._tweak_data.upgrade_timer_multipliers) do
			multiplier = multiplier * managers.player:upgrade_value(upgrade_timer_multiplier.category, upgrade_timer_multiplier.upgrade, 1)
		end
	end

	local lockpicking = {"pick_lock_easy","pick_lock_easy_no_skill","pick_lock_hard_no_skill"}

	for _, id in pairs(lockpicking) do
		if self._tweak_data.upgrade_timer_multipliers and self.tweak_data == id then
			if managers.player:has_active_temporary_property("picked_a_lock") then
				multiplier = multiplier * 0
			end
		end
	end

	if self.tweak_data == "revive" and managers.player:has_category_upgrade("player", "revive_interaction_speed_multiplier") then
		local data = managers.player:upgrade_value("player", "revive_interaction_speed_multiplier")
		local player_rank = managers.experience:current_rank() or 0
		multiplier = multiplier * math.max((data[1] - (data[2] * math.floor(player_rank / data[3]))), data[4])
	elseif self.tweak_data == "revive" and not managers.player:has_category_upgrade("player", "revive_interaction_speed_multiplier") then
		multiplier = multiplier
	end

	if managers.player:has_category_upgrade("player", "level_interaction_timer_multiplier") and self.tweak_data ~= "revive" then
		local data = managers.player:upgrade_value("player", "level_interaction_timer_multiplier") or {}
        local player_rank = managers.experience:current_rank() or 0
		multiplier = multiplier * math.max((data[4] - (data[1] * math.floor(player_rank / data[2]))), data[3])
	end

	return self:_timer_value() * multiplier * managers.player:toolset_value()
end

Hooks:PostHook(BaseInteractionExt, "interact_interupt", "interact_interupt__", function(self, player, complete)
	local lockpicking_ids = {"pick_lock_easy","pick_lock_easy_no_skill","pick_lock_hard_no_skill"}

	for _, id in pairs(lockpicking_ids) do
		if self.tweak_data == id then
			if complete then
				managers.player:send_message(Message.OnLockPicked, nil)
			end
		end
	end
end)

DoctorBagBaseInteractionExt = DoctorBagBaseInteractionExt or class(UseInteractionExt)
function DoctorBagBaseInteractionExt:_interact_blocked(player)
	local is_berserker = player:character_damage():is_berserker()
	local is_decaying = managers.player:get_property("decaying")

	if is_decaying then
		return true, false, "hint_health_berserking"
	end

	return is_berserker, false, is_berserker and "hint_health_berserking" or false
end