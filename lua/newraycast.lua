local reload_speed_multiplier = NewRaycastWeaponBase.reload_speed_multiplier
function NewRaycastWeaponBase:reload_speed_multiplier(...)
	local multiplier = reload_speed_multiplier(self, ...)

	local pm = managers.player

	multiplier = multiplier + pm:get_property("stacking_archer_reload", 0)

	if managers.player:has_category_upgrade("player", "reload_speed_tier_infamy") then
		local data = managers.player:upgrade_value("player", "reload_speed_tier_infamy")
        local player_rank = managers.experience:current_rank() or 0
		multiplier = multiplier + math.min((data[4] + (data[1] * math.floor(player_rank / data[2]))), data[3])
	end

	if pm:has_category_upgrade("pistol", "stacked_reload_bonus") then
        multiplier = multiplier + pm:get_property("desperado", 0)
    end

	return multiplier
end

function NewRaycastWeaponBase:conditional_accuracy_multiplier(current_state)
	local mul = 1

	if not current_state then
		return mul
	end

	local pm = managers.player

	if current_state:in_steelsight() and self:is_single_shot() then
		mul = mul + 1 - pm:upgrade_value("player", "single_shot_accuracy_inc", 1)
	end

	if current_state:in_steelsight() then
		for _, category in ipairs(self:categories()) do
			mul = mul + 1 - managers.player:upgrade_value(category, "steelsight_accuracy_inc", 1)
		end
	end

	if current_state._moving then
		mul = mul + 1 - pm:upgrade_value("player", "weapon_movement_stability", 1)
	end

	mul = mul + pm:get_property("desperado", 0)

	return self:_convert_add_to_mul(mul)
end

local math_map_range_clamped = math.map_range_clamped

function NewRaycastWeaponBase:get_damage_falloff(damage, col_ray, user_unit)
	if self._optimal_distance + self._optimal_range == 0 then
		return damage
	end

	local distance = col_ray.distance or mvector3.distance(col_ray.unit:position(), user_unit:position())
	local near_dist = self._optimal_distance - self._near_falloff
	local optimal_start = self._optimal_distance
	local optimal_end = self._optimal_distance + self._optimal_range
	local far_dist = optimal_end + self._far_falloff
	local near_mul = self._near_multiplier
	local optimal_mul = 1
	local far_mul = self._far_multiplier
	local primary_category = self:weapon_tweak_data().categories and self:weapon_tweak_data().categories[1]
	local current_state = user_unit and user_unit:movement() and user_unit:movement()._current_state

	if current_state and current_state:in_steelsight() then
		local mul = managers.player:upgrade_value(primary_category, "steelsight_range_inc", 1)
		optimal_end = optimal_end * mul
		far_dist = far_dist * mul
	end

	for _, category in ipairs(self:categories()) do
		optimal_end = optimal_end * managers.player:upgrade_value(category, "fall_off_range_inc", 1)
		far_dist = far_dist * managers.player:upgrade_value(category, "fall_off_range_inc", 1)
	end

	local damage_mul = 1

	if distance < self._optimal_distance then
		if self._near_falloff > 0 then
			damage_mul = math_map_range_clamped(distance, near_dist, optimal_start, near_mul, optimal_mul)
		else
			damage_mul = near_mul
		end
	elseif distance < optimal_end then
		damage_mul = optimal_mul
	elseif self._far_falloff > 0 then
		damage_mul = math_map_range_clamped(distance, optimal_end, far_dist, optimal_mul, far_mul)
	else
		damage_mul = far_mul
	end

	return damage * damage_mul
end

local fire_rate_multiplier = NewRaycastWeaponBase.fire_rate_multiplier
function NewRaycastWeaponBase:fire_rate_multiplier(...)
	local multiplier = fire_rate_multiplier(self, ...)
	local archer_rate_of_fire = 0

	if managers.player:is_current_weapon_of_category("bow", "crossbow") then
	 	archer_rate_of_fire = managers.player:get_temporary_property("reload_archer", 0)
	end

	multiplier = multiplier + archer_rate_of_fire
	multiplier = multiplier + CatLib:dec_round(managers.player:get_property("style_rate_of_fire", 0), 3)

	return multiplier
end

function NewRaycastWeaponBase:has_dmr_kit()
	if not self._factory_id or not self._blueprint then
		return
	end

	local parts = managers.weapon_factory:get_assembled_blueprint(self._factory_id, self._blueprint)
	local dmr_name_ids = { "bm_wp_upg_ass_ak_b_zastava", "bm_wp_upg_ass_m4_b_beowulf" }
	local override = managers.weapon_factory:_get_override_parts(self._factory_id, self._blueprint)
	local factory = tweak_data.weapon.factory
	local t = nil

	for _, id in ipairs(parts) do
		if factory.parts[id].type == "barrel" then
			local part = managers.weapon_factory:_part_data(id, self._factory_id, override)
			t = part.name_id
		end
	end

	for _, id in pairs(dmr_name_ids) do
		if t and t == id then
			return true
		end
	end

	return false
end