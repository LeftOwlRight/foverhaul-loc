PlayerAction.Archer = {
    Priority = 1,
    Function = function()
        local player_manager = managers.player
        local data = player_manager:upgrade_value("player", "archer_base")
        local co = coroutine.running()
        local current_time = Application:time()
        local max_time = data.max_time + Application:time()
        local current_stacks = 1

        local function on_hit()
            current_stacks = current_stacks + 1

            if current_stacks <= data.max_stacks then
                player_manager:add_to_property("stacking_archer_reload", data.bonus_reload_speed)
            end

            max_time = Application:time() + data.max_time
        end

        player_manager:add_to_property("stacking_archer_reload", data.bonus_reload_speed)
        player_manager:register_message(Message.OnEnemyShotChk, co, on_hit)

        while current_time < max_time do
            current_time = Application:time()

            if not player_manager:is_current_weapon_of_category("bow", "crossbow") then
                break
            end

            coroutine.yield(co)
        end

        player_manager:remove_property("stacking_archer_reload")
        player_manager:unregister_message(Message.OnEnemyShotChk, co)
    end
}