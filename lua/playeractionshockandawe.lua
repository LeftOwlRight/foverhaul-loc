PlayerAction.ShockAndAwe = {
	Priority = 1,
	Function = function (player_manager, target_enemies, max_reload_increase, min_reload_increase, penalty, min_bullets, weapon_unit)
		local co = coroutine.running()
		local running = true

		local function on_player_reload()
			if alive(weapon_unit) and running then
				running = false
				local half_ammo_inc = penalty
				local ammo = weapon_unit:base():get_ammo_remaining_in_clip()
				local max_mag = weapon_unit:base():get_ammo_max_per_clip()
				local inc = min_reload_increase

				if ammo <= max_mag/2 and ammo > 0 then
					inc = half_ammo_inc
				end

				if ammo <= 0 then
					inc = max_reload_increase
				end

				player_manager:set_property("shock_and_awe_reload_multiplier", inc)
			end
		end

		local function on_switch_weapon_quit()
			running = false
		end

		player_manager:register_message(Message.OnSwitchWeapon, co, on_switch_weapon_quit)
		player_manager:register_message(Message.OnPlayerReload, co, on_player_reload)

		while running and alive(weapon_unit) and weapon_unit == player_manager:equipped_weapon_unit() do
			coroutine.yield(co)
		end

		player_manager:unregister_message(Message.OnSwitchWeapon, co)
		player_manager:unregister_message(Message.OnPlayerReload, co)
	end
}