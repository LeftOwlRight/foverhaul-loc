local schinese = Idstring("schinese"):key() == SystemInfo:language():key()

function PlayerDamage:init(unit)
	self._lives_init = tweak_data.player.damage.LIVES_INIT

	if Global.game_settings.one_down then
		self._lives_init = 2
	end

	self._lives_init = managers.modifiers:modify_value("PlayerDamage:GetMaximumLives", self._lives_init)
	self._unit = unit
	self._max_health_reduction = managers.player:upgrade_value("player", "max_health_reduction", 1)
	self._healing_reduction = managers.player:upgrade_value("player", "healing_reduction", 1)
	self._revives = Application:digest_value(0, true)
	self._uppers_elapsed = 0
	self._passive_health_regen_base_timer = 0
	self._amount_of_platings = 0
	self._plating_durability = 0
	self._has_armor_platings = false

	self:replenish()

	local player_manager = managers.player
	local blhealth = math.max((self:_max_health() * tweak_data.player.damage.BLEED_OUT_HEALTH_INIT) * player_manager:upgrade_value("player", "bleed_out_health_multiplier", 1), 8)
	self._bleed_out_health = Application:digest_value(blhealth, true)
	self._god_mode = Global.god_mode
	self._invulnerable = false
	self._mission_damage_blockers = {}
	self._gui = Overlay:newgui()
	self._ws = self._gui:create_screen_workspace()
	self._focus_delay_mul = 1
	self._dmg_interval = tweak_data.player.damage.MIN_DAMAGE_INTERVAL
	self._next_allowed_dmg_t = Application:digest_value(-100, true)
	self._last_received_dmg = 0
	self._next_allowed_sup_t = -100
	self._last_received_sup = 0
	self._supperssion_data = {}
	self._inflict_damage_body = self._unit:body("inflict_reciever")

	self._inflict_damage_body:set_extension(self._inflict_damage_body:extension() or {})

	local body_ext = PlayerBodyDamage:new(self._unit, self, self._inflict_damage_body)
	self._inflict_damage_body:extension().damage = body_ext

	managers.sequence:add_inflict_updator_body("fire", self._unit:key(), self._inflict_damage_body:key(), self._inflict_damage_body:extension().damage)

	self._doh_data = tweak_data.upgrades.damage_to_hot_data or {}
	self._damage_to_hot_stack = {}
	self._armor_stored_health = 0
	self._can_take_dmg_timer = 0
	self._regen_on_the_side_timer = 0
	self._regen_on_the_side = false
	self._interaction = managers.interaction
	self._armor_regen_mul = managers.player:upgrade_value("player", "armor_regen_time_mul", 1)
	self._dire_need = managers.player:has_category_upgrade("player", "armor_depleted_stagger_shot")
	self._has_damage_speed = managers.player:has_inactivate_temporary_upgrade("temporary", "damage_speed_multiplier")
	self._has_damage_speed_team = managers.player:upgrade_value("player", "team_damage_speed_multiplier_send", 0) ~= 0
	self._has_mrwi_health_invulnerable = player_manager:has_category_upgrade("temporary", "mrwi_health_invulnerable")

	if self._has_mrwi_health_invulnerable then
		local upgrade_values = player_manager:upgrade_value("temporary", "mrwi_health_invulnerable")
		local health_threshold = upgrade_values[1]
		local duration = upgrade_values[2]
		local cooldown = upgrade_values[3]
		self._mrwi_health_invulnerable_threshold = health_threshold
		self._mrwi_health_invulnerable_cooldown = cooldown
	end

	local function revive_player()
		self:revive(true)
	end

	managers.player:register_message(Message.RevivePlayer, self, revive_player)

	self._current_armor_fill = 0
	local has_swansong_skill = player_manager:has_category_upgrade("temporary", "berserker_damage_multiplier")
	self._current_state = nil
	self._listener_holder = unit:event_listener()

	if player_manager:has_category_upgrade("player", "damage_to_armor") then
		local damage_to_armor_data = player_manager:upgrade_value("player", "damage_to_armor", nil)
		local armor_data = tweak_data.blackmarket.armors[managers.blackmarket:equipped_armor(true, true)]

		if damage_to_armor_data and armor_data then
			local idx = armor_data.upgrade_level
			self._damage_to_armor = {
				armor_value = damage_to_armor_data[idx][1],
				target_tick = damage_to_armor_data[idx][2],
				elapsed = 0
			}

			local function on_damage(damage_info)
				local attacker_unit = damage_info and damage_info.attacker_unit

				if alive(attacker_unit) and attacker_unit:base() and attacker_unit:base().thrower_unit then
					attacker_unit = attacker_unit:base():thrower_unit()
				end

				if self._unit == attacker_unit then
					local time = Application:time()

					if self._damage_to_armor.target_tick < time - self._damage_to_armor.elapsed then
						self._damage_to_armor.elapsed = time

						self:restore_armor(self._damage_to_armor.armor_value, true)
					end
				end
			end

			CopDamage.register_listener("on_damage", {
				"on_damage"
			}, on_damage)
		end
	end

	self._listener_holder:add("on_use_armor_bag", {
		"on_use_armor_bag"
	}, callback(self, self, "_on_use_armor_bag_event"))

	if self:_init_armor_grinding_data() then
		function self._on_damage_callback_func()
			return callback(self, self, "_on_damage_armor_grinding")
		end

		self:_add_on_damage_event()
		self._listener_holder:add("on_enter_bleedout", {
			"on_enter_bleedout"
		}, callback(self, self, "_on_enter_bleedout_event"))

		if has_swansong_skill then
			self._listener_holder:add("on_enter_swansong", {
				"on_enter_swansong"
			}, callback(self, self, "_on_enter_swansong_event"))
			self._listener_holder:add("on_exit_swansong", {
				"on_enter_bleedout"
			}, callback(self, self, "_on_exit_swansong_event"))
		end

		self._listener_holder:add("on_revive", {
			"on_revive"
		}, callback(self, self, "_on_revive_event"))
	else
		self:_init_standard_listeners()
	end

	if player_manager:has_category_upgrade("player", "revive_damage_reduction") or managers.player:has_category_upgrade("temporary", "dmg_red_revived") then
		local function on_revive_interaction_start()
			managers.player:set_property("revive_damage_reduction", player_manager:upgrade_value("player", "revive_damage_reduction"), 1)
		end

	  	local function on_exit_interaction()
			managers.player:remove_property("revive_damage_reduction")
	  	end

	  	local function on_revive_interaction_success()
			if managers.player:has_category_upgrade("player", "dmg_red_revived") then
				local upgrade_data = managers.player:upgrade_value("player", "dmg_red_revived")
				local duration = upgrade_data.duration
				local reduction = upgrade_data.reduction
				managers.player:activate_temporary_property("dmg_red_revived", duration, reduction)
			end
	  	end

		self._listener_holder:add("on_revive_interaction_start", {
			"on_revive_interaction_start"
		}, on_revive_interaction_start)
		self._listener_holder:add("on_revive_interaction_interrupt", {
			"on_revive_interaction_interrupt"
		}, on_exit_interaction)
		self._listener_holder:add("on_revive_interaction_success", {
			"on_revive_interaction_success"
		}, on_revive_interaction_success)
	end

	managers.mission:add_global_event_listener("player_regenerate_armor", {
		"player_regenerate_armor"
	}, callback(self, self, "_regenerate_armor"))
	managers.mission:add_global_event_listener("player_force_bleedout", {
		"player_force_bleedout"
	}, callback(self, self, "force_into_bleedout", false))

	local level_tweak = tweak_data.levels[managers.job:current_level_id()]

	if level_tweak and level_tweak.is_safehouse and not level_tweak.is_safehouse_combat then
		self:set_mission_damage_blockers("damage_fall_disabled", true)
		self:set_mission_damage_blockers("invulnerable", true)
	end

	self._delayed_damage = {
		epsilon = 0.001,
		chunks = {}
	}

	self:clear_delayed_damage()

	self._slowdowns = {}
	self._can_play_tinnitus = not managers.user:get_setting("accessibility_sounds_tinnitus") or false
	self._can_play_tinnitus_clbk_func = callback(self, self, "clbk_tinnitus_toggle_changed")

	managers.user:add_setting_changed_callback("accessibility_sounds_tinnitus", self._can_play_tinnitus_clbk_func)
end

function PlayerDamage:_calc_defense(dmg)
	local defense = managers.player:combined_damage_defense()
	local damage = dmg
	local constant = 2000 * managers.player:upgrade_value("player", "damage_defense_effi", 1)
	local reductionFactor = defense / (defense + constant)
	local reducedDamage = damage * (1 - reductionFactor)
	--log( "| Reduced Damage : " .. CatLib:dec_round(reducedDamage, 2) .. " | Original Damage: " .. damage .. " | Reduction: " .. reductionFactor)
	return CatLib:dec_round(reducedDamage, 2)
end

function PlayerDamage:_regenerated(no_messiah)
	self:set_health(self:_max_health())
	self:_send_set_health()
	self:_set_health_effect()

	self._said_hurt = false
	self._revives = Application:digest_value(self._lives_init + managers.player:upgrade_value("player", "additional_lives", 0), true)

	self:_send_set_revives(true)

	self._revive_health_i = 1

	managers.environment_controller:set_last_life(false)

	self._down_time = tweak_data.player.damage.DOWNED_TIME + managers.player:upgrade_value("player", "bleed_out_time", 0)

	if not no_messiah then
		self._messiah_charges = managers.player:upgrade_value("player", "pistol_revive_from_bleed_out", 0)
	end
end

function PlayerDamage:update_downed(t, dt)
	if self._downed_timer and self._downed_paused_counter == 0 then
		self._downed_timer = self._downed_timer - dt

		if self._downed_start_time == 0 then
			self._downed_progression = 100
		else
			self._downed_progression = math.clamp(1 - self._downed_timer / self._downed_start_time, 0, 1) * 100
		end

		if not _G.IS_VR then
			if managers.player:has_category_upgrade("player", "bleed_out_time") then
				managers.environment_controller:set_downed_value(self._downed_progression * 0.7)
			else
				managers.environment_controller:set_downed_value(self._downed_progression * 0.7)
			end
		end

		SoundDevice:set_rtpc("downed_state_progression", self._downed_progression)

		return self._downed_timer <= 0
	end

	return false
end

function PlayerDamage:_set_health_effect()
	local hp = self:get_real_health() / self:_max_health()

	math.clamp(hp, 0, 1)

	if managers.player:has_category_upgrade("player", "swan_song_basic") then
		if managers.player:get_property("decaying") then
			managers.environment_controller:set_health_effect_value(0)
		else
			managers.environment_controller:set_health_effect_value(hp)
		end
	else
		managers.environment_controller:set_health_effect_value(hp)
	end
end

function PlayerDamage:_shorten_armor_recovery_timer(reduction)
	if self._regenerate_timer then
		self._regenerate_timer = math.abs(self._regenerate_timer - (self._regenerate_timer * reduction))
	end
end

function PlayerDamage:interacting_damage_reduction()
	if not managers.player:player_unit() then
		return 1
	end

	local health_ratio = self:health_ratio()

	if not health_ratio then
		return 1
	end

	local damage_reduction = managers.player:upgrade_value("player", "interacting_damage_reduction").base
	local per_step_seconds = 0.8
	local damage_health_ratio = 1 - CatLib:dec_round(health_ratio, 2)
	local inc_based_on_missing_health = math.min(1 * per_step_seconds * damage_health_ratio, managers.player:upgrade_value("player", "interacting_damage_reduction").max_bonus)

	return damage_reduction - inc_based_on_missing_health
end

function PlayerDamage:_upd_swan_song()
	if not managers.player:has_category_upgrade("player", "swan_song_basic") then
		return
	end

	local pm = managers.player
	local is_decaying = pm:get_property("decaying")

	if not is_decaying then
		return
	end

	if pm:has_category_upgrade("player", "swan_song_ace") then
		local upg = pm:upgrade_value("player", "swan_song_ace")
		local kills_req = upg.enemies_kill_req
		if pm:get_property("killed_decay", 0) >= kills_req then
			self:_reset_swan_song()
			managers.hud:swan_song_overlay_survived()
			self:set_health(self:_max_health() * upg.set_health)
			pm:activate_temporary_property("swansong_cooldown_alive", pm:upgrade_value("player", "swan_song_ace").reduced_cooldown)
			pm:activate_temporary_property("swansong_dmg_reduction_linger", 0.75)
			pm:remove_property("killed_decay")
		end
	end

	if self:get_real_health() <= 0 then
		self._activated_swansong = false
		self:force_into_bleedout()
		pm:activate_temporary_property("swansong_cooldown_dead", pm:upgrade_value("player", "swan_song_basic").cooldown)
	end
end

function PlayerDamage:_reset_swan_song()
	managers.player:remove_property("decaying")
	self._activated_swansong = false
	self._block_swan_song = false
	self:enable_armor_regen_and_restore()
	managers.hud:set_teammate_condition(HUDManager.PLAYER_PANEL, "mugshot_normal", "")
end

function PlayerDamage:_on_enter_bleedout_event()
	self:_remove_on_damage_event()

	if managers.player:has_category_upgrade("player", "swan_song_basic") and managers.player:get_property("decaying") then
		managers.player:remove_property("decaying")
		self:enable_armor_regen_and_restore()
		managers.hud:set_teammate_condition(HUDManager.PLAYER_PANEL, "mugshot_normal", "")
	end

	if managers.player:is_wraith() then
		managers.player:_wraith_form_end()
	end
end

function PlayerDamage:_decaying_health(health)
	self:_check_update_max_health()

	local max_health = self:_max_health()
	health = math.min(health, max_health)
	local prev_health = self._health and Application:digest_value(self._health, false) or health
	self._health = Application:digest_value(math.clamp(health, 0, max_health), true)

	self:_send_set_health()
	self:_set_health_effect()

	if self._said_hurt and self:get_real_health() / self:_max_health() > 0.2 then
		self._said_hurt = false
	end

	if self:health_ratio() < 0.3 then
		self._heartbeat_start_t = TimerManager:game():time()
		self._heartbeat_t = self._heartbeat_start_t + tweak_data.vr.heartbeat_time
	end

	managers.hud:set_player_health({
		current = self:get_real_health(),
		total = self:_max_health(),
		revives = Application:digest_value(self._revives, false)
	})

	return prev_health ~= Application:digest_value(self._health, false)
end

function PlayerDamage:set_health(health, init_override)
	if managers.player:get_property("decaying", false) or init_override then
		if init_override then
			self._health = health
		end

		self:_decaying_health(health)

		return
	end

	self:_check_update_max_health()

	local max_health = self:_max_health() * self._max_health_reduction
	health = math.min(health, max_health)
	local prev_health = self._health and Application:digest_value(self._health, false) or health
	self._health = Application:digest_value(math.clamp(health, 0, max_health), true)

	self:_send_set_health()
	self:_set_health_effect()

	if self._said_hurt and self:get_real_health() / self:_max_health() > 0.2 then
		self._said_hurt = false
	end

	if self:health_ratio() < 0.3 then
		self._heartbeat_start_t = TimerManager:game():time()
		self._heartbeat_t = self._heartbeat_start_t + tweak_data.vr.heartbeat_time
	end

	managers.hud:set_player_health({
		current = self:get_real_health(),
		total = self:_max_health(),
		revives = Application:digest_value(self._revives, false)
	})

	return prev_health ~= Application:digest_value(self._health, false)
end

function PlayerDamage:_check_bleed_out(can_activate_berserker, ignore_movement_state, ignore_reduce_revive)
	if self:get_real_health() == 0 and not self._check_berserker_done then
		if self._unit:movement():zipline_unit() then
			self._bleed_out_blocked_by_zipline = true

			return
		end

		if not ignore_movement_state and self._unit:movement():current_state():bleed_out_blocked() then
			self._bleed_out_blocked_by_movement_state = true

			return
		end

		if managers.player:has_activate_temporary_upgrade("temporary", "copr_ability") and managers.player:has_category_upgrade("player", "copr_out_of_health_move_slow") then
			return
		end

		local time = Application:time()

		if not self._block_medkit_auto_revive and not ignore_reduce_revive and time > self._uppers_elapsed + self._UPPERS_COOLDOWN and not managers.player:get_property("decaying") then
			local auto_recovery_kit = FirstAidKitBase.GetFirstAidKit(self._unit:position())

			if auto_recovery_kit then
				auto_recovery_kit:take(self._unit)
				self._unit:sound():play("pickup_fak_skill")

				self._uppers_elapsed = time

				return
			end
		end

		if not managers.player:is_wraith() and managers.player:has_category_upgrade("player", "semi_wraith_lifeline") and not managers.player:has_active_temporary_property("semi_wraith_lifeline_cooldown") then
			local upgrade_data = managers.player:upgrade_value("player", "semi_wraith_lifeline")
			managers.player:activate_temporary_property("semi_wraith_lifeline_cooldown", upgrade_data.cooldown)
			managers.player:semi_wraith_form()
			managers.hud:wraith_lifeline_cooldown_vfx()
			self:set_health(upgrade_data.health_set)

			return
		end

		if can_activate_berserker and not self._check_berserker_done then
			local has_berserker_skill = managers.player:has_category_upgrade("temporary", "berserker_damage_multiplier")

			if has_berserker_skill and not self._disable_next_swansong then
				managers.hud:set_teammate_condition(HUDManager.PLAYER_PANEL, "mugshot_swansong", managers.localization:text("debug_mugshot_downed"))
				managers.player:activate_temporary_upgrade("temporary", "berserker_damage_multiplier")

				self._current_state = nil
				self._check_berserker_done = true

				if alive(self._interaction:active_unit()) and not self._interaction:active_unit():interaction():can_interact(self._unit) then
					self._unit:movement():interupt_interact()
				end

				self._listener_holder:call("on_enter_swansong")
			end

			self._disable_next_swansong = nil
		end

		if managers.player:has_category_upgrade("player", "swan_song_basic") and not managers.player:has_active_temporary_property("swansong_cooldown_dead") and not managers.player:has_active_temporary_property("swansong_cooldown_alive") and not self._activated_swansong and not self._block_swan_song then
			self:set_health(self:_max_health(), true)
			managers.player:set_property("decaying", true)
			self:set_armor(0)
			self._activated_swansong = true
			self._block_swan_song = true
			self:block_armor_regen()

			managers.hud:swan_song_overlay()
			managers.hud:set_teammate_condition(HUDManager.PLAYER_PANEL, "mugshot_swansong", managers.localization:text("debug_mugshot_downed"))

			managers.player:activate_temporary_property("brief_slow_swansong", 0.25, tweak_data.upgrades.brief_slow_swan_song)
			managers.player:activate_temporary_property("brief_immunity_swansong", 0.25, 0)

			if managers.player:has_category_upgrade("player", "swan_song_ace") and FOverhaul._data.SwanSong_fod then
				if FOverhaul._data.swan_song_style_indi == 1 then
					managers.hud:swan_song_fight_or_die()
				elseif FOverhaul._data.swan_song_style_indi == 2 then
					local text = "FIGHT OR DIE!"
					if schinese then
						text = "不战斗，就会死！"
					end
					managers.hud:show_hint({
						time = 3,
						text = text
					})
				end
			end
		end

		self._hurt_value = 0.2
		self._damage_to_hot_stack = {}

		managers.environment_controller:set_downed_value(0)
		SoundDevice:set_rtpc("downed_state_progression", 0)

		if not self._check_berserker_done or not can_activate_berserker then
			if managers.player:has_category_upgrade("player", "swan_song_basic") and self._activated_swansong then
				return
			end

			if not ignore_reduce_revive then
				self._revives = Application:digest_value(Application:digest_value(self._revives, false) - 1, true)

				self:_send_set_revives()
			end

			self._check_berserker_done = nil

			self._block_swan_song = false

			managers.environment_controller:set_last_life(Application:digest_value(self._revives, false) <= 1)

			if Application:digest_value(self._revives, false) == 0 then
				self._down_time = 0
			end

			self._bleed_out = true
			self._current_state = nil

			managers.player:set_player_state("bleed_out")

			self._critical_state_heart_loop_instance = self._unit:sound():play("critical_state_heart_loop")
			self._slomo_sound_instance = self._unit:sound():play("downed_slomo_fx")
			self._bleed_out_health = Application:digest_value(tweak_data.player.damage.BLEED_OUT_HEALTH_INIT * managers.player:upgrade_value("player", "bleed_out_health_multiplier", 1), true)

			self:_drop_blood_sample()
			self:on_downed()
		end
	elseif not self._said_hurt and self:get_real_health() / self:_max_health() < 0.2 then
		self._said_hurt = true

		PlayerStandard.say_line(self, "g80x_plu")
	end
end

function PlayerDamage.easy_difficulty_adjustment(wanted_difficulty, attack_data, caller)
	local preset = tweak_data.upgrades.difficulty_adjustments[wanted_difficulty] or 0

	if not attack_data then
		FOverhaul:catch_error_and_notify_later("PlayerDamage.easy_difficulty_adjustment " .. tostring(caller), "NO ATTACK DATA")
		return 1
	end

	if preset <= 0 then
		return attack_data.damage
	end

	local change = preset

	if not attack_data.attacker_unit then
		return attack_data.damage
	end

	if not attack_data.attacker_unit:base()._tweak_table then
		return attack_data.damage
	end

	if wanted_difficulty == "sm_wish" and attack_data.attacker_unit:base()._tweak_table ~= "zeal_heavy_swat" then
		return attack_data.damage * change
	end

	return attack_data.damage * change
end

function PlayerDamage:set_armor(armor)
	if self._armor_change_blocked then
		return
	end

	self:_check_update_max_armor()

	armor = math.clamp(armor, 0, self:_max_armor())

	if self._armor then
		local current_armor = self:get_real_armor()

		if current_armor == 0 and armor ~= 0 then
			self:consume_armor_stored_health()
		end
	end

	self._armor = Application:digest_value(armor, true)
end

function PlayerDamage:is_bleeding(unit)
	local bleedingEnemies = CustomDOTManager:GetBleedingEnemies()

	for _, id in pairs(bleedingEnemies) do
		if id == unit:id() then
			return true
		end
	end

	return false
end

function PlayerDamage:_chk_dmg_too_soon(damage)
	local next_allowed_dmg_t = type(self._next_allowed_dmg_t) == "number" and self._next_allowed_dmg_t or Application:digest_value(self._next_allowed_dmg_t, false)

	if managers.player:player_timer():time() < next_allowed_dmg_t then
		return true
	end
end

function PlayerDamage:damage_bullet(attack_data)
	local pm = managers.player
	if not self:_chk_can_take_dmg() then
		return
	end

	local damage_info = {
		result = {
			variant = "bullet",
			type = "hurt"
		},
		attacker_unit = attack_data.attacker_unit,
		attack_dir = attack_data.attacker_unit and attack_data.attacker_unit:movement():m_pos() - self._unit:movement():m_pos() or Vector3(1, 0, 0),
		pos = mvector3.copy(self._unit:movement():m_head_pos())
	}

	attack_data.damage = self.easy_difficulty_adjustment(Global.game_settings.difficulty, attack_data, "Player")
	attack_data.raw_damage_btb = attack_data.damage
	attack_data.damage = self:_calc_defense(attack_data.damage)

	local dmg_mul = pm:damage_reduction_skill_multiplier("bullet")

	if managers.player:has_category_upgrade("player", "bleeding_enemy_dmg_red") then
		if self:is_bleeding(attack_data.attacker_unit) then
			attack_data.damage = attack_data.damage * managers.player:upgrade_value("player", "bleeding_enemy_dmg_red", 1)
		end
	end

	attack_data.damage = attack_data.damage * dmg_mul
	attack_data.damage = managers.mutators:modify_value("PlayerDamage:TakeDamageBullet", attack_data.damage)
	attack_data.damage = managers.modifiers:modify_value("PlayerDamage:TakeDamageBullet", attack_data.damage, attack_data.attacker_unit:base()._tweak_table)

	if _G.IS_VR then
		local distance = mvector3.distance(self._unit:position(), attack_data.attacker_unit:position())

		if tweak_data.vr.long_range_damage_reduction_distance[1] < distance then
			local step = math.clamp(distance / tweak_data.vr.long_range_damage_reduction_distance[2], 0, 1)
			local mul = 1 - math.step(tweak_data.vr.long_range_damage_reduction[1], tweak_data.vr.long_range_damage_reduction[2], step)
			attack_data.damage = attack_data.damage * mul
		end
	end

	local damage_absorption = pm:damage_absorption() + self:custom_absorption()

	if damage_absorption > 0 then
		attack_data.damage = math.max(0.1, attack_data.damage - damage_absorption)
	end

	self:copr_update_attack_data(attack_data)

	if self._god_mode then
		if attack_data.damage > 0 then
			self:_send_damage_drama(attack_data, attack_data.damage)
		end

		self:_call_listeners(damage_info)

		return
	elseif self._invulnerable or self._mission_damage_blockers.invulnerable then
		self:_call_listeners(damage_info)

		return
	elseif self:incapacitated() then
		return
	elseif self:is_friendly_fire(attack_data.attacker_unit) then
		return
	elseif self:_chk_dmg_too_soon(attack_data.damage) then
		return
	elseif self._unit:movement():current_state().immortal then
		return
	elseif self._revive_miss and math.random() < self._revive_miss then
		self:play_whizby(attack_data.col_ray.position)

		return
	end

	self._last_received_dmg = attack_data.damage
	self._next_allowed_dmg_t = Application:digest_value(pm:player_timer():time() + self._dmg_interval, true)
	local dodge_roll = math.random()
	local dodge_value = tweak_data.player.damage.DODGE_INIT or 0
	local armor_dodge_chance = pm:body_armor_value("dodge")
	local skill_dodge_chance = pm:skill_dodge_chance(self._unit:movement():running(), self._unit:movement():crouching(), self._unit:movement():zipline_unit())
	dodge_value = dodge_value + armor_dodge_chance + skill_dodge_chance

	if self._temporary_dodge_t and TimerManager:game():time() < self._temporary_dodge_t then
		dodge_value = dodge_value + self._temporary_dodge
	end

	local smoke_dodge = 0

	for _, smoke_screen in ipairs(managers.player._smoke_screen_effects or {}) do
		if smoke_screen:is_in_smoke(self._unit) then
			smoke_dodge = tweak_data.projectiles.smoke_screen_grenade.dodge_chance

			break
		end
	end

	dodge_value = 1 - (1 - dodge_value) * (1 - smoke_dodge)

	if dodge_roll < dodge_value then
		if attack_data.damage > 0 then
			self:_send_damage_drama(attack_data, 0)
		end

		self:_call_listeners(damage_info)
		self:play_whizby(attack_data.col_ray.position)
		self:_hit_direction(attack_data.attacker_unit:position(), attack_data.col_ray and attack_data.col_ray.ray or damage_info.attacK_dir)

		self._next_allowed_dmg_t = Application:digest_value(pm:player_timer():time() + self._dmg_interval, true)
		self._last_received_dmg = attack_data.damage
		self._dodges_in_a_row = (self._dodges_in_a_row or 0)

		if self._dodges_in_a_row < 2 then
			if not managers.player._coroutine_mgr:is_running(PlayerAction.DireNeed) then
				self._dodges_in_a_row = self._dodges_in_a_row + 1
			end
		end

		managers.player:send_message(Message.OnPlayerDodge, nil, attack_data)

		if self._dire_need and self._dodges_in_a_row == 2 and not managers.player._coroutine_mgr:is_running(PlayerAction.DireNeed) then
			self._dodges_in_a_row = 0
			if FOverhaul._data.dire_need_vfx then
				managers.hud:dire_need_overlay()
			end
			managers.player:add_coroutine(PlayerAction.DireNeed, PlayerAction.DireNeed, managers.player:upgrade_value("player", "armor_depleted_stagger_shot", 0))
		end

		if managers.player:has_category_upgrade("player", "heal_on_dodge") then
			local upgrade_data = pm:upgrade_value("player", "heal_on_dodge")
			local duration = upgrade_data.duration
			if not managers.player:has_active_temporary_property("heal_dodge_cd") then
				managers.player:activate_temporary_property("heal_dodge_proc", duration)
			end
		end

		return
	end

	if managers.player:is_semi_wraith() and managers.player:has_category_upgrade("player", "semi_wraith_heal_bullet") then
		local roll_chance = math.random()
		local upgrade_data = managers.player:upgrade_value("player", "semi_wraith_heal_bullet")
		local chance_to_heal = upgrade_data.chance
		local max_heal = upgrade_data.max_heal

		if roll_chance <= chance_to_heal then
			local heal = math.min(attack_data.damage * upgrade_data.heal_from_bullet_damage, max_heal)
			self:restore_health(heal, true)
			managers.hud:wraith_healbullet_overlay()

			return
		end
	end

	if attack_data.attacker_unit:base()._tweak_table == "tank" then
		managers.achievment:set_script_data("dodge_this_fail", true)
	end

	if self:get_real_armor() > 0 then
		self._unit:sound():play("player_hit")
	else
		self._unit:sound():play("player_hit_permadamage")
	end

	local shake_armor_multiplier = pm:body_armor_value("damage_shake") * pm:upgrade_value("player", "damage_shake_multiplier", 1)
	local gui_shake_number = tweak_data.gui.armor_damage_shake_base / shake_armor_multiplier
	gui_shake_number = gui_shake_number + pm:upgrade_value("player", "damage_shake_addend", 0)
	shake_armor_multiplier = tweak_data.gui.armor_damage_shake_base / gui_shake_number
	local shake_multiplier = math.clamp(attack_data.damage, 0.2, 2) * shake_armor_multiplier

	self._unit:camera():play_shaker("player_bullet_damage", 1 * shake_multiplier)

	if not _G.IS_VR then
		managers.rumble:play("damage_bullet")
	end

	self:_hit_direction(attack_data.attacker_unit:position(), attack_data.col_ray and attack_data.col_ray.ray or damage_info.attacK_dir)
	pm:check_damage_carry(attack_data)

	attack_data.damage = managers.player:modify_value("damage_taken", attack_data.damage, attack_data)

	if self._bleed_out then
		self:_bleed_out_damage(attack_data)

		return
	end

	if not attack_data.ignore_suppression and not self:is_suppressed() then
		return
	end

	self:mutator_update_attack_data(attack_data)
	self:_check_chico_heal(attack_data)

	local health_subtracted = self:_calc_armor_damage(attack_data)

	attack_data.damage = self:_cal_damage_to_health(attack_data, "bullet")

	health_subtracted = health_subtracted + self:_calc_health_damage(attack_data)

	if not self._bleed_out and health_subtracted > 0 then
		self:_send_damage_drama(attack_data, health_subtracted)
	elseif self._bleed_out then
		self:chk_queue_taunt_line(attack_data)
	end

	pm:send_message(Message.OnPlayerDamage, nil, attack_data)
	self:_call_listeners(damage_info)
	self:reset_dodges_in_a_row()
end

function PlayerDamage:_cal_damage_to_health(attack_data, type)
	local bonus_redirect = 0

	if BlackMarketManager:get_equipped_armor() == "level_7" then
		bonus_redirect = managers.player:upgrade_value("player", "iron_man_damage_redirect_bonus", 0)
	end

	local damage_to_health = 1 - (tweak_data.player.damage.DAMAGE_TO_ARMOR or 0) + bonus_redirect

	if managers.player:_in_berserker_state() then
		damage_to_health = 0
	end

	if managers.player:has_category_upgrade("player", "anarchist_damage_redirect") then
		damage_to_health = 0
	end

	if self._damage_too_high then
		self._damage_too_high = nil
	end

	local health_subtracted = self:_calc_armor_damage(attack_data)
	local damage_too_high = (self._damage_too_high or 0)

	if self:get_real_armor() <= 0 then
		damage_to_health = 1
	end

	if self:get_real_armor() > 0 then
		damage_too_high = 0
	end

	if type == "bullet" or type == "fire_hit" then
		if attack_data.armor_piercing then
			if Global.game_settings.difficulty == "sm_wish" then
				managers.hud:def_debuff_visual()
				managers.player:activate_temporary_property("defense_reduction", tweak_data.upgrades.sniper_ds_defense_debuff[1], tweak_data.upgrades.sniper_ds_defense_debuff[2])
			end
			attack_data.damage = attack_data.damage - health_subtracted
		elseif damage_too_high and damage_too_high > 0 then
			attack_data.damage = (attack_data.damage * (1 - (tweak_data.player.damage.DAMAGE_TO_ARMOR or 0) + bonus_redirect)) + damage_too_high
		else
			attack_data.damage = attack_data.damage * damage_to_health
		end
	end

	if type == "killzone" or type == "simple" then
		if damage_too_high and damage_too_high > 0 then
			attack_data.damage = (attack_data.damage * (1 - (tweak_data.player.damage.DAMAGE_TO_ARMOR or 0) + bonus_redirect)) + damage_too_high
		else
			attack_data.damage = (attack_data.damage * damage_to_health)
		end
	end

	return attack_data.damage
end

function PlayerDamage:reset_dodges_in_a_row()
	if self._dodges_in_a_row then
		local function reset_dodges()
			self._dodges_in_a_row = 0
		end
		managers.player:register_message(Message.OnPlayerDamage, "reset_dodges", reset_dodges)
	else
		managers.player:unregister_message(Message.OnPlayerDamage, "reset_dodges")
	end
end

function PlayerDamage:damage_fire_hit(attack_data)
	if not self:_chk_can_take_dmg() then
		return
	end

	local damage_info = {
		result = {
			variant = "fire",
			type = "hurt"
		},
		attacker_unit = attack_data.attacker_unit
	}

	attack_data.damage = self.easy_difficulty_adjustment(Global.game_settings.difficulty, attack_data, "Player")
	attack_data.raw_damage_btb = attack_data.damage
	attack_data.damage = self:_calc_defense(attack_data.damage)

	local pm = managers.player
	local dmg_mul = pm:damage_reduction_skill_multiplier("bullet")
	attack_data.damage = attack_data.damage * dmg_mul
	attack_data.damage = managers.mutators:modify_value("PlayerDamage:TakeDamageBullet", attack_data.damage)
	attack_data.damage = managers.modifiers:modify_value("PlayerDamage:TakeDamageBullet", attack_data.damage, attack_data.attacker_unit:base()._tweak_table)

	if _G.IS_VR then
		local distance = mvector3.distance(self._unit:position(), attack_data.attacker_unit:position())

		if tweak_data.vr.long_range_damage_reduction_distance[1] < distance then
			local step = math.clamp(distance / tweak_data.vr.long_range_damage_reduction_distance[2], 0, 1)
			local mul = 1 - math.step(tweak_data.vr.long_range_damage_reduction[1], tweak_data.vr.long_range_damage_reduction[2], step)
			attack_data.damage = attack_data.damage * mul
		end
	end

	local damage_absorption = pm:damage_absorption() + self:custom_absorption()

	if damage_absorption > 0 then
		attack_data.damage = math.max(0.1, attack_data.damage - damage_absorption)
	end

	self:copr_update_attack_data(attack_data)

	if self._god_mode then
		if attack_data.damage > 0 then
			self:_send_damage_drama(attack_data, attack_data.damage)
		end

		self:_call_listeners(damage_info)

		return
	elseif self._invulnerable or self._mission_damage_blockers.invulnerable then
		self:_call_listeners(damage_info)

		return
	elseif self:incapacitated() then
		return
	elseif self:is_friendly_fire(attack_data.attacker_unit) then
		return
	elseif self:_chk_dmg_too_soon(attack_data.damage) then
		return
	elseif self._unit:movement():current_state().immortal then
		return
	end

	self._last_received_dmg = attack_data.damage
	self._next_allowed_dmg_t = Application:digest_value(pm:player_timer():time() + self._dmg_interval, true)

	if self:get_real_armor() > 0 then
		self._unit:sound():play("player_hit")
	else
		self._unit:sound():play("player_hit_permadamage")
	end

	self:_hit_direction(attack_data.attacker_unit:position(), attack_data.col_ray and attack_data.col_ray.ray)
	pm:check_damage_carry(attack_data)

	attack_data.damage = managers.player:modify_value("damage_taken", attack_data.damage, attack_data)

	if self._bleed_out then
		self:_bleed_out_damage(attack_data)

		return
	end

	self:mutator_update_attack_data(attack_data)
	self:_check_chico_heal(attack_data)

	local health_subtracted = self:_calc_armor_damage(attack_data)

	attack_data.damage = self:_cal_damage_to_health(attack_data, "fire_hit")

	health_subtracted = health_subtracted + self:_calc_health_damage(attack_data)

	if not self._bleed_out and health_subtracted > 0 then
		self:_send_damage_drama(attack_data, health_subtracted)
	elseif self._bleed_out then
		self:chk_queue_taunt_line(attack_data)
	end

	pm:send_message(Message.OnPlayerDamage, nil, attack_data)
	self:_call_listeners(damage_info)
end

function PlayerDamage:_on_use_armor_bag_event()
	self:_armor_platings_init()
end

function PlayerDamage:_armor_platings_init()
	local max_platings = tweak_data.equipments.armor_kit.platings
	local plating_durability = tweak_data.equipments.armor_kit.plating_durability/10

	self._amount_of_platings = max_platings
	self._plating_durability = plating_durability
	self._has_armor_platings = true
	self._combined_durability = self._plating_durability * self._amount_of_platings
	managers.hud:init_platings()
end

function PlayerDamage:replace_armor_platings()
	self._combined_durability = self._plating_durability * self._amount_of_platings
	managers.hud:upd_platings(1, true)
end

function PlayerDamage:_upd_armor_platings()
	local combined_durability = self._combined_durability or 0

	if combined_durability <= 0 and self._has_armor_platings then
		self._plating_durability = 0
		self._amount_of_platings = 0
		self._has_armor_platings = false
		local text = "ARMOR PLATINGS BROKEN!"
		managers.hud:show_hint({
			time = 3,
			text = text
		})
	end
end

function PlayerDamage:has_armor_platings()
	return self._has_armor_platings
end

function PlayerDamage:_calc_armor_damage(attack_data)
	local health_subtracted = 0
	local armor_damage_reduction = managers.player:upgrade_value("player", "damage_reduction_to_armor", 1)
	local bonus_redirect = 0

	if BlackMarketManager:get_equipped_armor() == "level_7" then
		bonus_redirect = managers.player:upgrade_value("player", "iron_man_damage_redirect_bonus", 0)
	end

	if self:get_real_armor() > 0 then
		health_subtracted = self:get_real_armor()

		local damage_armor_data = (tweak_data.player.damage.DAMAGE_TO_ARMOR or 1) + bonus_redirect

		if managers.player:_in_berserker_state() then
			damage_armor_data = 1
		end

		if managers.player:has_category_upgrade("player", "anarchist_damage_redirect") then
			damage_armor_data = 1
		end

		local armor_platings_absorb = 1
		local damage_to_armor = (attack_data.damage * damage_armor_data) * armor_damage_reduction

		if self._has_armor_platings then
			if attack_data.armor_piercing then
				health_subtracted = health_subtracted + self._combined_durability
				managers.hud:upd_platings(1)
				self._combined_durability = 0
			end

			if not attack_data.armor_piercing then
				armor_platings_absorb = 0.5
				managers.hud:upd_platings((damage_to_armor * armor_platings_absorb)/14)
				self._combined_durability = self._combined_durability - (damage_to_armor * armor_platings_absorb)
			end
		end

		damage_to_armor = damage_to_armor * armor_platings_absorb

		if self:get_real_armor() < damage_to_armor and not attack_data.armor_piercing then
			self._damage_too_high = math.max(damage_to_armor - self:get_real_armor(), 0)
		end

		self:change_armor(-damage_to_armor)

		health_subtracted = health_subtracted - self:get_real_armor()

		self:_damage_screen()
		SoundDevice:set_rtpc("shield_status", self:armor_ratio() * 100)
		self:_send_set_armor()

		if self:get_real_armor() <= 0 then
			self._unit:sound():play("player_armor_gone_stinger")

			if attack_data.armor_piercing then
				self._unit:sound():play("player_sniper_hit_armor_gone")
			end

			local pm = managers.player

			self:_start_regen_on_the_side(pm:upgrade_value("player", "passive_always_regen_armor", 0))

			if pm:has_inactivate_temporary_upgrade("temporary", "armor_break_invulnerable") then
				pm:activate_temporary_upgrade("temporary", "armor_break_invulnerable")

				self._can_take_dmg_timer = pm:temporary_upgrade_value("temporary", "armor_break_invulnerable", 0)
			end
		end
	end

	managers.hud:damage_taken()

	return health_subtracted
end

Hooks:PreHook(PlayerDamage, "damage_melee", "damage_melee__", function(self, attack_data)
	attack_data.damage = attack_data.damage * 0.7

	attack_data.raw_damage_btb = attack_data.damage
	attack_data.damage = self:_calc_defense(attack_data.damage)
	managers.player:send_message(Message.OnPlayerDamageMelee, nil, attack_data)
end)

function PlayerDamage:damage_killzone(attack_data)
	local damage_info = {
		result = {
			variant = "killzone",
			type = "hurt"
		}
	}

	if self._god_mode or self._invulnerable or self._mission_damage_blockers.invulnerable then
		self:_call_listeners(damage_info)

		return
	elseif self:incapacitated() then
		return
	elseif self._unit:movement():current_state().immortal then
		return
	end

	self._unit:sound():play("player_hit")

	if attack_data.instant_death then
		self:set_armor(0)
		self:set_health(0)
		self:_send_set_armor()
		self:_send_set_health()
		managers.hud:set_player_health({
			current = self:get_real_health(),
			total = self:_max_health(),
			revives = Application:digest_value(self._revives, false)
		})
		self:_set_health_effect()
		self:_damage_screen()
		self:_check_bleed_out(nil)
	else
		self:_hit_direction(attack_data.col_ray.origin, attack_data.col_ray.ray)

		if self._bleed_out then
			return
		end

		attack_data.damage = managers.player:modify_value("damage_taken", attack_data.damage, attack_data)

		self:mutator_update_attack_data(attack_data)
		self:_check_chico_heal(attack_data)

		local health_subtracted = self:_calc_armor_damage(attack_data)

		attack_data.damage = self:_cal_damage_to_health(attack_data, "killzone")

		health_subtracted = health_subtracted + self:_calc_health_damage(attack_data)
	end

	self:_call_listeners(damage_info)
end

function PlayerDamage:damage_simple(attack_data)
	local damage_info = {
		result = {
			type = "hurt",
			variant = attack_data.variant
		}
	}

	if self._god_mode or self._invulnerable or self._mission_damage_blockers.invulnerable then
		self:_call_listeners(damage_info)

		return
	elseif self:incapacitated() then
		return
	elseif self._unit:movement():current_state().immortal then
		return
	end

	self._unit:sound():play("player_hit")

	attack_data.damage = managers.player:modify_value("damage_taken", attack_data.damage, attack_data)

	if self._bleed_out then
		self:_bleed_out_damage(attack_data)

		return
	end

	self:mutator_update_attack_data(attack_data)
	self:_check_chico_heal(attack_data)

	local health_subtracted = self:_calc_armor_damage(attack_data)

	attack_data.damage = self:_cal_damage_to_health(attack_data, "simple")

	health_subtracted = health_subtracted + self:_calc_health_damage(attack_data)

	self:_call_listeners(damage_info)
end

Hooks:PostHook(PlayerDamage, "_on_revive_event", "foverhaulf2022", function(self)
	if managers.player:has_category_upgrade("player", "instakill_downed") then
		local pm = managers.player
		pm:remove_property("instakill_downed")
		pm:remove_property("dmg_red_downed")
	end
end)

Hooks:PostHook(PlayerDamage, "on_downed", "foverhaulf2022", function(self)
	if managers.player:has_category_upgrade("player", "instakill_downed") then
		local pm = managers.player
		local dmg_red_downed = pm:upgrade_value("player", "damage_reduction_downed" , 1)
		pm:set_property("instakill_downed", true)
		pm:set_property("dmg_red_downed", dmg_red_downed)
	end

	if managers.player:has_active_temporary_property("dmg_red_bite_the_bullet") or managers.player:has_active_temporary_property("armor_to_health_bite_the_bullet") then
		managers.player:remove_temporary_property("dmg_red_bite_the_bullet")
		managers.player:remove_temporary_property("armor_to_health_bite_the_bullet")
		self:enable_armor_regen_and_restore()
		managers.player:unregister_message(Message.OnPlayerDamage, "shockwave_init")
		managers.player:unregister_message(Message.OnPlayerDamageMelee, "shockwave_init_melee")
		DelayedCalls:Remove("finish_bite_the_bullet")
	end

	if managers.player:has_active_temporary_property("quick_fix_healing_period") or managers.player:has_active_temporary_property("qf_movement_speed") then
		managers.player:remove_temporary_property("quick_fix_healing_period")
		managers.player:remove_temporary_property("qf_movement_speed")
		DelayedCalls:Remove("qf_delay")
	end
end)

function PlayerDamage:revive(silent)
	if Application:digest_value(self._revives, false) == 0 then
		self._revive_health_multiplier = nil

		return
	end

	local arrested = self:arrested()

	managers.player:set_player_state("standard")
	managers.player:remove_copr_risen_cooldown()

	if not silent then
		PlayerStandard.say_line(self, "s05x_sin")
	end

	self._bleed_out = false
	self._incapacitated = nil
	self._downed_timer = nil
	self._downed_start_time = nil

	if not arrested then
		local cheat_death_bonus_health = 1
		if self._cheat_death_happened then
			cheat_death_bonus_health = managers.player:upgrade_value("player", "revived_health_regain_cheat_death", 1)
			self._cheat_death_happened = false
		end

		self:set_health(self:_max_health() * tweak_data.player.damage.REVIVE_HEALTH_STEPS[self._revive_health_i] * (self._revive_health_multiplier or 1) * managers.player:upgrade_value("player", "revived_health_regain", 1) * cheat_death_bonus_health)
		self:set_armor(self:_max_armor())

		self._revive_health_i = math.min(#tweak_data.player.damage.REVIVE_HEALTH_STEPS, self._revive_health_i + 1)
		self._revive_miss = 2
	end

	self:_regenerate_armor()
	managers.hud:set_player_health({
		current = self:get_real_health(),
		total = self:_max_health(),
		revives = Application:digest_value(self._revives, false)
	})
	self:_send_set_health()
	self:_set_health_effect()
	managers.hud:pd_stop_progress()

	self._revive_health_multiplier = nil

	self._listener_holder:call("on_revive")

	if managers.player:has_inactivate_temporary_upgrade("temporary", "revived_damage_resist") then
		managers.player:activate_temporary_upgrade("temporary", "revived_damage_resist")
	end

	if managers.player:has_inactivate_temporary_upgrade("temporary", "increased_movement_speed") then
		managers.player:activate_temporary_upgrade("temporary", "increased_movement_speed")
	end

	if managers.player:has_inactivate_temporary_upgrade("temporary", "swap_weapon_faster") then
		managers.player:activate_temporary_upgrade("temporary", "swap_weapon_faster")
	end

	if managers.player:has_inactivate_temporary_upgrade("temporary", "reload_weapon_faster") then
		managers.player:activate_temporary_upgrade("temporary", "reload_weapon_faster")
	end
end

function PlayerDamage:override_set_regenerate_timer_to_max()
	if self:_max_armor() > 0 and self:get_real_armor() <= 0 then
		if (self._regenerate_timer or 0) > 0 then
			self._regenerate_timer = self._regenerate_timer
		else
			-- INCASE previous somehow fails pls dont 🙏
			local mul = managers.player:body_armor_regen_multiplier(alive(self._unit) and self._unit:movement():current_state()._moving, self:health_ratio())
			self._regenerate_timer = tweak_data.player.damage.REGENERATE_TIME * mul
			self._regenerate_timer = self._regenerate_timer * managers.player:upgrade_value("player", "armor_regen_time_mul", 1)
			self._current_state = self._update_regenerate_timer
		end
	else
		self:set_regenerate_timer_to_max()
	end
end

Hooks:PostHook(PlayerDamage, "_update_regenerate_timer", "_update_regenerate_timer___", function(self)
	if self._regenerate_timer and self._regenerate_timer <= 0 then
		if managers.player:get_property("defense_up_scav", false) then
			managers.player:remove_property("defense_up_scav")
			managers.player:activate_temporary_property("defense_up_scav_after", managers.player:upgrade_value("player", "defense_up_scav")[2], managers.player:upgrade_value("player", "defense_up_scav")[1])
		end
	end
end)

function PlayerDamage:_on_damage_event()
	self:override_set_regenerate_timer_to_max()
	local armor_broken = self:_max_armor() > 0 and self:get_real_armor() <= 0

	if armor_broken and self._has_damage_speed then
		managers.player:activate_temporary_upgrade("temporary", "damage_speed_multiplier")

		if self._has_damage_speed_team then
			managers.player:send_activate_temporary_team_upgrade_to_peers("temporary", "team_damage_speed_multiplier_received")
			managers.player:set_property("defense_up_scav", managers.player:upgrade_value("player", "defense_up_scav")[1])
		end
	end

	self:_start_passive_health_regen_base(tweak_data.player.damage.CONSIDER_NO_DAMAGE_TAKEN or 0)
	self:_upd_armor_platings()
end

function PlayerDamage:_start_passive_health_regen_base(time)
	if self._passive_health_regen_base then
		self._passive_health_regen_base = false
	end

	if self._passive_health_regen_base_timer <= 0 and time > 0 then
		self.passive_health_regen_base_timer = time
		self._passive_health_regen_base = true
	end
end

function PlayerDamage:_update_start_passive_health_regen_base(dt)
	if self._passive_health_regen_base then
		self.passive_health_regen_base_timer = math.max(self.passive_health_regen_base_timer - dt, 0)

		if self.passive_health_regen_base_timer <= 0 then
			self:_base_health_regen_init(dt)
		end
	end
end

function PlayerDamage:_base_health_regen_init(dt)
	if self.health_regen_timer then
		self.health_regen_timer = self.health_regen_timer - dt
		if self.health_regen_timer <= 0 then
			self.health_regen_timer = nil
		end
	end

	if not self.health_regen_timer and not self:is_downed() and not managers.player:is_wraith() then
		self:restore_health(tweak_data.player.damage.BASE_HEALTH_REGEN or 0, false, false, true)
		self.health_regen_timer = 0.75
	end
end

function PlayerDamage:_chk_cheat_death(ignore_reduce_revive)
	local can_revive = (Application:digest_value(self._revives, false) > 1 or ignore_reduce_revive) and not self._check_berserker_done

	if can_revive and managers.player:has_category_upgrade("player", "cheat_death_chance") then
		local r = math.rand(1)

		if r <= managers.player:upgrade_value("player", "cheat_death_chance", 0) then
			self._cheat_death_happened = true
			self._auto_revive_timer = 0.8
		end
	end

	if can_revive and not self._auto_revive_timer then
		local mutator = nil

		if managers.mutators:is_mutator_active(MutatorPiggyRevenge) then
			mutator = managers.mutators:get_mutator(MutatorPiggyRevenge)
		end

		if mutator and mutator.auto_revive_timer then
			self._auto_revive_timer = mutator:auto_revive_timer()
		end
	end
end

function PlayerDamage:restore_health(health_restored, is_static, chk_health_ratio, ignore_bonus_healing)
	if chk_health_ratio and managers.player:is_damage_health_ratio_active(self:health_ratio()) then
		return false
	end

	local bonus_healing = managers.player:upgrade_value("player", "bonus_healing", 1)
	local is_decaying = managers.player:get_property("decaying", false)

	if is_decaying then
		return false
	end

	if self:get_real_health() + 0.01 >= self:_max_health() then
		return false
	end

	if health_restored * self._healing_reduction == 0 then
		return false
	end

	if managers.player:get_property("current_health_before_use") then
		if ignore_bonus_healing then
			local max_health = self:_raw_max_health()
			managers.player:add_to_property("current_health_before_use",(max_health * health_restored) * self._healing_reduction)
		elseif is_static then
			managers.player:add_to_property("current_health_before_use",(health_restored * bonus_healing) * self._healing_reduction)
		else
			local max_health = self:_raw_max_health()

			managers.player:add_to_property("current_health_before_use",(max_health * (health_restored * bonus_healing) * self._healing_reduction))
		end
	end

	if ignore_bonus_healing then
		local max_health = self:_max_health()
		return self:change_health((max_health * health_restored) * self._healing_reduction)
	end

	if is_static then
		return self:change_health((health_restored * bonus_healing) * self._healing_reduction)
	else
		local max_health = self:_max_health()

		return self:change_health(max_health * (health_restored * bonus_healing) * self._healing_reduction)
	end
end

Hooks:PostHook(PlayerDamage, "update", "update__", function(self, unit, t, dt)
	if managers.player:has_category_upgrade("player", "daredevil_damage_absorption") then
		self:DareDevil_bar_init()
	end

	self:_upd_swan_song()
	self:_update_start_passive_health_regen_base(dt)
end)

function PlayerDamage:custom_absorption()
	local absorption = 0
	return absorption
end

function PlayerDamage:DareDevil_bar_init()
	local bar_value = managers.player:get_property("style_absorption", 0)/(managers.player:upgrade_value("player", "daredevil_damage_absorption", 0) * managers.player:upgrade_value("player", "style_rank").max_stacks)

	managers.hud:set_absorb_bar(bar_value)
end

Hooks:PostHook(PlayerDamage, "_upd_health_regen", "_upd_health_regen__", function(self, t, dt)
	local pm = managers.player
	local max_health = self:_max_health()
	local current_health = self:get_real_health()
	local bonus_healing = managers.player:upgrade_value("player", "bonus_healing", 1)
	local is_decaying = pm:get_property("decaying", false)

	if pm:has_category_upgrade("player", "converts_restore_health") then
		if self._man_cnv_heal then
			self._man_cnv_heal = self._man_cnv_heal - dt
			if self._man_cnv_heal <= 0 then
				self._man_cnv_heal = nil
			end
		end

    	if not self._man_cnv_heal then
			pm:converts_health_regen()
			self._man_cnv_heal = 5
		end
	end

	if is_decaying then
		if self.decay_health then
			self.decay_health = self.decay_health - dt
			if self.decay_health <= 0 then
				self.decay_health = nil
			end
		end

    	if not self.decay_health and not pm:has_active_temporary_property("pause_decay") then
			local upgrade_data = pm:upgrade_value("player", "swan_song_basic")
			local decay_rate = upgrade_data.health_decay_per_tick
			local decay_tick = upgrade_data.tick_period
			if self:get_real_health() > 0 then
				self:change_health(-(max_health * decay_rate))
				self.decay_health = decay_tick
			end
		end
	end

	if pm:has_category_upgrade("player", "heal_on_dodge") then
		if self.dodge_heal then
			self.dodge_heal = self.dodge_heal - dt
			if self.dodge_heal <= 0 then
				self.dodge_heal = nil
			end
		end

    	if not self.dodge_heal and pm:has_active_temporary_property("heal_dodge_proc") then
			local upgrade_data = pm:upgrade_value("player", "heal_on_dodge")
			local cooldown = upgrade_data.cooldown
			local hp_limit = tweak_data.upgrades.low_hp
			local heal = CatLib:upgrade_value_based_on_current_health("player", "heal_on_dodge", hp_limit, 0, "heal", "increased", "<")
			if self:get_real_health() < max_health then
				managers.player:activate_temporary_property("heal_dodge_cd", cooldown)
				self:restore_health(heal, false)
				self.dodge_heal = 0.4
			end
		end
	end

	if pm:has_category_upgrade("player", "quick_fix") then
		if self.quick_fix_timer then
			self.quick_fix_timer = self.quick_fix_timer - dt
			if self.quick_fix_timer <= 0 then
				self.quick_fix_timer = nil
			end
		end

    	if not self.quick_fix_timer and not is_decaying then
			if pm:has_active_temporary_property("quick_fix_healing_period") then
				local upgd = pm:upgrade_value("player", "quick_fix")
				local interval = upgd.heal_interval
				local heal = upgd.heal
				local missing_health = upgd.missing_health
				if self:get_real_health() < max_health then
					self:change_health(((max_health - current_health) * missing_health) + (max_health * heal) * bonus_healing * self._healing_reduction)
					self.quick_fix_timer = interval
				end
			end
		end
	end
end)

function PlayerDamage:_max_health()
	local max_health = self:_raw_max_health()

	if managers.player:has_category_upgrade("player", "armor_to_health_conversion") then
		local max_armor = self:_raw_max_armor()
		local conversion_factor = managers.player:upgrade_value("player", "armor_to_health_conversion") * 0.01
		max_health = max_health + max_armor * conversion_factor
	end

	if managers.player:has_category_upgrade("player", "bonus_from_crit") then
		local crit = managers.player:critical_hit_chance() * 10
		max_health = max_health + (crit * managers.player:upgrade_value("player", "bonus_from_crit", 1))
	end

	max_health = max_health + managers.player:get_temporary_property("armor_to_health_bite_the_bullet", 0)
	max_health = max_health + managers.player:get_temporary_property("overheal_bleed", 0)

	return max_health
end

function PlayerDamage:block_armor_regen()
	self._armor_change_blocked = true
end

function PlayerDamage:regenerate_armor(no_sound)
	self:_regenerate_armor(no_sound)
end

function PlayerDamage:enable_armor_regen_and_restore()
	if managers.player:get_property("decaying", false) then
		return
	end

	self._armor_change_blocked = false
	if managers.player:current_state() ~= "bleed_out" then
		self:set_regenerate_timer_to_max()
		self:_regenerate_armor()
	end
end