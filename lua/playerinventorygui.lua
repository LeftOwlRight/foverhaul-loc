local function format_round(num, round_value)
	return round_value and tostring(math.round(num)) or string.format("%.1f", num):gsub("%.?0+$", "")
end

local IS_WIN_32 = SystemInfo:platform() == Idstring("WIN32")
local NOT_WIN_32 = not IS_WIN_32
local TOP_ADJUSTMENT = NOT_WIN_32 and 50 or 55
local BOT_ADJUSTMENT = NOT_WIN_32 and 40 or 60
local join_stinger_binding = "menu_respec_tree_all"

local function _animate(objects, scale, anim_time)
	local anims = {}

	for _, object in pairs(objects) do
		if object then
			local data = {
				sw = object.gui:width(),
				sh = object.gui:height(),
				ew = object.shape[3] * scale,
				eh = object.shape[4] * scale,
				sf = object.gui.font_scale and object.gui:font_scale() or scale,
				ef = scale
			}

			if object.font_scale then
				data.sf = data.sf
				data.ef = data.ef * object.font_scale
			end

			table.insert(anims, {
				object = object,
				data = data
			})
		end
	end

	if anim_time == 0 then
		for _, anim in ipairs(anims) do
			local cx, cy = anim.object.gui:center()

			anim.object.gui:set_size(anim.data.ew, anim.data.eh)
			anim.object.gui:set_center(cx, cy)

			if anim.object.gui.set_font_scale then
				anim.object.gui:set_font_scale(anim.data.ef)
			end
		end

		return
	end

	local time = 0

	while anim_time > time do
		local dt = coroutine.yield()
		time = time + dt
		local p = time / anim_time

		for _, anim in ipairs(anims) do
			local cx, cy = anim.object.gui:center()

			anim.object.gui:set_size(math.lerp(anim.data.sw, anim.data.ew, p), math.lerp(anim.data.sh, anim.data.eh, p))
			anim.object.gui:set_center(cx, cy)

			if anim.object.gui.set_font_scale then
				anim.object.gui:set_font_scale(math.lerp(anim.data.sf, anim.data.ef, p))
			end
		end
	end
end

local function select_anim(o, box, instant)
	_animate({
		box.image_object,
		box.animate_text and box.text_object
	}, 1, instant and 0 or 0.2)
end

local function unselect_anim(o, box, instant)
	_animate({
		box.image_object,
		box.animate_text and box.text_object
	}, 0.8, instant and 0 or 0.2)
end

local function make_fine_text(text)
	local x, y, w, h = text:text_rect()

	text:set_size(w, h)
	text:set_position(math.round(text:x()), math.round(text:y()))
end

function PlayerInventoryGui:_update_info_skilltree(name, skilltrees)
	return
end

local function GetCustomColor(value)
	local c = {
		pastel_pink = {255, 161, 220},
		purple = {128, 0, 128},
		aqua = {0, 255, 221},
		strawb = {251, 41, 65},
		orange = {255, 85, 0},
		red = {255, 0, 0},
		navy = {56, 63, 255},
		pink = {255, 0, 160},
		lilac = {223, 168, 255},
		black = {0, 0, 0},
		blue_violet = {169, 48, 255},
		white = {255, 255, 255},
		green = {119, 230, 174},
		dark_green = {0, 158, 78},
		light_orange = {255, 204, 153}
	}

	local convert = {
		"pastel_pink",
		"purple",
		"aqua",
		"strawb",
		"orange",
		"red",
		"navy",
		"pink",
		"lilac",
		"black",
		"blue_violet",
		"white",
		"green",
		"dark_green",
		"light_orange"
	}

	if not value then
		return c.white[1]/255, c.white[2]/255, c.white[3]/255
	end

	return c[convert[value]][1]/255, c[convert[value]][2]/255, c[convert[value]][3]/255
end

local prev_crash = false

local function _add_lines(text_panel, y_pspe, i)
	if PSPE._data.PSPE_disable_cosmetic_lines then
		return
	end

	local Lines_Color, Lines_Color2 = FetchInfoPSPE("lines_color")

	text_panel:rect({
		name = tostring(i),
		color = Lines_Color:with_alpha(1),
		h = 1,
		w = 9,
		y = y_pspe,
		rotation = 90,
		x = -11
	})
	text_panel:rect({
		name = tostring(i),
		color = Lines_Color2:with_alpha(0.6),
		h = 1,
		w = 5,
		y = y_pspe,
		rotation = 90,
		x = -7
	})
end

function FetchInfoPSPE(type)
	PSPE:Load()
	local r,g,b = GetCustomColor(PSPE._data.PSPE_color)
	local r2,g2,b2 = GetCustomColor(PSPE._data.PSPE_color_2)
	local r3,g3,b3 = GetCustomColor(PSPE._data.PSPE_color_3)
	local CColor = Color(1,r,g,b)
	local CColor2 = Color(1,r2,g2,b2)
	local CColor3 = Color(1,r3,g3,b3)

	local rh,gh,bh = GetCustomColor(PSPE._data.PSPE_colorh)
	local rh2,gh2,bh2 = GetCustomColor(PSPE._data.PSPE_colorh_2)
	local rh3,gh3,bh3 = GetCustomColor(PSPE._data.PSPE_colorh_3)
	local CColorH = Color(1,rh,gh,bh)
	local CColorH2 = Color(1,rh2,gh2,bh2)
	local CColorH3 = Color(1,rh3,gh3,bh3)

	local rv,gv,bv = GetCustomColor(PSPE._data.PSPE_colorv)
	local rv2,gv2,bv2 = GetCustomColor(PSPE._data.PSPE_colorv_2)
	local rv3,gv3,bv3 = 0,0,0
	local rv4,gv4,bv4 = GetCustomColor(PSPE._data.PSPE_colorv_4)
	local rv5,gv5,bv5 = 0,0,0
	local rv6,gv6,bv6 = 0,0,0

	if PSPE._data.PSPE_colorv_5 ~= 16 then
		rv5,gv5,bv5 = GetCustomColor(PSPE._data.PSPE_colorv_5)
	end

	if PSPE._data.PSPE_colorv_6 ~= 16 then
		rv6,gv6,bv6 = GetCustomColor(PSPE._data.PSPE_colorv_6)
	end

	if PSPE._data.PSPE_colorv_3 ~= 16 then
		rv3,gv3,bv3 = GetCustomColor(PSPE._data.PSPE_colorv_3)
	end

	local CColorV = Color(1,rv,gv,bv)
	local CColorV2 = Color(1,rv2,gv2,bv2)
	local CColorV3 = Color(1,rv3,gv3,bv3)
	local CColorV4 = Color(1,rv4,gv4,bv4)
	local CColorV5 = Color(1,rv5,gv5,bv5)
	local CColorV6 = Color(1,rv6,gv6,bv6)

	local rl,gl,bl = GetCustomColor(PSPE._data.PSPE_color_lines)
	local rl2,gl2,bl2 = GetCustomColor(PSPE._data.PSPE_color_lines2)
	local CColorl = Color(1,rl,gl,bl)
	local CColorl2 = Color(1,rl2,gl2,bl2)

	local rls,gls,bls = GetCustomColor(PSPE._data.PSPE_cosmetic_split)
	local CColorLS = Color(1,rls,gls,bls)

	if PSPE._data.PSPE_colorv_3 == 16 then
		CColorV3 = tweak_data.screen_colors.resource
	end

	if PSPE._data.PSPE_colorv_6 == 16 then
		CColorV6 = tweak_data.screen_colors.stats_negative
	end

	if PSPE._data.PSPE_colorv_5 == 16 then
		CColorV5 = tweak_data.screen_colors.stats_positive
	end

	if type == "split_line" then
		return CColorLS
	end

	if type == "lines_color" then
		return CColorl, CColorl2
	end

	if type == "vanilla_colors_adjust" then
		return CColorV, CColorV2, CColorV3, CColorV4, CColorV5, CColorV6
	end

	if type == "dmg_red" then
		return CColor, CColor2, CColor3
	end

	if type == "heal" then
		return CColorH, CColorH2, CColorH3
	end

	if type == "vanilla_resize" then
		local y_u = 35
		local y = 8.45
		local y_inf = 37
		local font_size = 5
		local p_y = 15
		local u_name = 28
		if PSPE._data.PSPE_vanillafont then
			y = 9.5
			y_u = 40
			y_inf = 40
			font_size = 8
			p_y = 18
			u_name = 33
		end
		return y, y_u, y_inf, font_size, p_y, u_name
	end
end

local function incompatibility_check()
	if FOverhaul:_chk_incompatibility("Player Stats Panel Enhancements") then
		local dialog_data = {
            title = managers.localization:text("menu_incompatibility_f_overhaul"),
            text = managers.localization:text("menu_incompatibility_f_overhaul_desc")
        }
        local yes_button = {
            text = "OKAY",
            cancel_button = true
        }
		local dont_show_again = {
            text = "DON'T SHOW AGAIN [PERMANENTLY DISABLE THIS POPUP]",
            callback_func = callback(FOverhaul, FOverhaul, "block_"),
            cancel_button = true
        }
		local remind_me_later = {
            text = "REMIND ME IN 1 HOUR",
            callback_func = callback(FOverhaul, FOverhaul, "remind_later", true),
            cancel_button = true
        }
		local attempt_delet = {
            text = "REMOVE MOD",
            callback_func = callback(FOverhaul, FOverhaul, "_attempt_delete"),
            cancel_button = true
        }
        dialog_data.button_list = {
            yes_button,
			dont_show_again,
			remind_me_later,
			attempt_delet
        }

		if not FOverhaul._data.block_popup and FOverhaul:remind_later() then
        	managers.system_menu:show(dialog_data)
		end

		prev_crash = true
		return true
	end

	return false
end

Hooks:PostHook(PlayerInventoryGui, "_update_player_stats", "__update_player_stats_foverhaul", function (self)
	if incompatibility_check() then
		return
	end

	local reducedDamage = 0

	if managers and managers.player then
		local defense = managers.player:combined_damage_defense()
		local constant = 2000 * managers.player:upgrade_value("player", "damage_defense_effi", 1)
		local reductionFactor = defense / (defense + constant)
		if reductionFactor > 0.8 then
			reducedDamage = CatLib:dec_round(reductionFactor * 100)
		else
			reducedDamage = CatLib:dec_round(reductionFactor * 100, 1)
		end
		managers.player:chk_player_per_level_stats()
	end

	local category = "armors"
	local CColor, CColor2 = FetchInfoPSPE("dmg_red")
	local CColorh, CColorh2 = FetchInfoPSPE("heal")
	local CColorV, CColorV2, CColorV3, CColorV4, CColorV5, CColorV6 = FetchInfoPSPE("vanilla_colors_adjust")
	local equipped_item = managers.blackmarket:equipped_item(category)
	local temp_stats_shown = self._stats_shown
	self._stats_shown = self._player_stats_shown
	local base_stats, mods_stats, skill_stats = self:_get_armor_stats(equipped_item)
	self._stats_shown = temp_stats_shown
	local bonus_health = CatLib:fetch_henchmans_upgrades("crew_healthy", "crew_add_health", 0) * 10

	for _, stat in ipairs(self._player_stats_shown) do
		local value = math.max(base_stats[stat.name].value + mods_stats[stat.name].value + skill_stats[stat.name].value, 0)
		local base = base_stats[stat.name].value
		local health = skill_stats.health.value + base_stats.health.value
		self._player_stats_texts[stat.name].total:set_color(CColorV or Color(1,1,1,1))

		if base < skill_stats[stat.name].value + base_stats[stat.name].value then
			self._player_stats_texts[stat.name].total:set_color(CColorV5 or tweak_data.screen_colors.stats_positive)
		end

		if base > skill_stats[stat.name].value + base_stats[stat.name].value then
			self._player_stats_texts[stat.name].total:set_color(CColorV6 or tweak_data.screen_colors.stats_negative)
		end

		self._player_stats_texts.bonus_crit_dmg.base:set_text(base_stats.bonus_crit_dmg.value.."%")
		self._player_stats_texts.bonus_crit_dmg.total:set_text((skill_stats.bonus_crit_dmg.value + base_stats.bonus_crit_dmg.value).."%")
		self._player_stats_texts.bonus_crit_dmg.skill:set_text(skill_stats.bonus_crit_dmg.skill_in_effect and "+"..skill_stats.bonus_crit_dmg.value.."%")

		if stat.name == "dodge" or stat.name == "crit" then
			self._player_stats_texts[stat.name].total:set_text(format_round(value, stat.round_value).."%")
			self._player_stats_texts[stat.name].base:set_text(format_round(base, stat.round_value).."%")
			self._player_stats_texts[stat.name].skill:set_text(skill_stats[stat.name].skill_in_effect and (skill_stats[stat.name].value > 0 and "+" or "") .. format_round(skill_stats[stat.name].value, stat.round_value).."%" or "")
		end

		if stat.name == "damage_shake" then
			self._player_stats_texts[stat.name].name:set_text(managers.localization:to_upper_text("bm_menu_" .. stat.name) .. " (~" .. reducedDamage .. "%)")
		end

		if skill_stats.crit.value > 100 then
			self._player_stats_texts.crit.skill:set_text(skill_stats.crit.skill_in_effect and (skill_stats.crit.value > 0 and "+" or "")..format_round(skill_stats.crit.value, stat.round_value).."%"or "")
			self._player_stats_texts.crit.skill:set_color(Color(255, 252, 0, 168) / 255)
			self._player_stats_texts.crit.total:set_text(100 .."%")
		else
			self._player_stats_texts.crit.skill:set_text(skill_stats.crit.skill_in_effect and (skill_stats.crit.value > 0 and "+" or "") .. format_round(skill_stats.crit.value, stat.round_value) .. "%" or "")
			self._player_stats_texts.crit.total:set_text(format_round(skill_stats.crit.value).."%")
		end

		if managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0 then
			local upgrade_value = managers.player:upgrade_value("player", "max_health_reduction", 1) * 100
			local has_frenzy = managers.player:has_category_upgrade("player", "max_health_reduction", 1)
			local health_multiplier = managers.player:health_skill_multiplier()
			local max_health = (PlayerDamage._HEALTH_INIT + managers.player:health_skill_addend()) * health_multiplier * 2 * 10
			local health_cal = (max_health + bonus_health) * managers.player:upgrade_value("player", "health_decrease", 1)
			local actual_health = (max_health + bonus_health) - health_cal
			if has_frenzy then
				self._player_stats_texts.health.total:set_text(health_cal * managers.player:upgrade_value("player", "max_health_reduction", 1)..'('..upgrade_value.."%)")
			else
				self._player_stats_texts.health.total:set_text(actual_health)
			end
		end

		if managers.player:has_category_upgrade("player", "max_health_reduction") and not (managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0) then
			self._player_stats_texts.health.total:set_color(tweak_data.screen_colors.stats_negative)
			local upgrade_value = managers.player:upgrade_value("player", "max_health_reduction", 1) * 100
			local max_health = health * managers.player:upgrade_value("player", "max_health_reduction", 1)
			self._player_stats_texts.health.total:set_text(max_health..'('..upgrade_value.."%)")
		end

	end

	for _, stat in ipairs(self._player_stats_shown_add) do
		self._player_stats_texts_add[stat.name].reduction_highest:set_color(CColor or Color(255, 255, 94, 0) / 255)
		self._player_stats_texts_add[stat.name].reduction_basic:set_color(CColor2 or Color(255, 255, 204, 153) / 255)

		local ds_damage_ex = 30 * zeal_heavy_swat_mul.falloff[1]
		local damage_red_player_manager = managers.player:damage_reduction_skill_multiplier_hehe()
		local combine_all_cond_values = managers.player:cal_conditional_damage_reduction()
		local abs_damage_reduction = math.abs(((damage_red_player_manager or 1) * 100) - 100)
		local add_conditional_values = math.abs(((damage_red_player_manager or 1) * combine_all_cond_values * 100) - 100)
		local round_cond_values = CatLib:dec_round(add_conditional_values, 1)
		local cal_ds_example_cond = CatLib:dec_round(ds_damage_ex * (damage_red_player_manager or 1) * combine_all_cond_values, 2)
		local cal_ds_example = CatLib:dec_round(ds_damage_ex * (damage_red_player_manager or 1), 2)

		if stat.name == "dmg_red" then
			self._player_stats_texts_add[stat.name].reduction_basic:set_text(abs_damage_reduction.."%")
			self._player_stats_texts_add[stat.name].reduction_highest:set_text(round_cond_values.."%")
		end

		if stat.name == "dmg_red_ds_example" then
			self._player_stats_texts_add[stat.name].reduction_basic:set_text(cal_ds_example)
			self._player_stats_texts_add[stat.name].reduction_highest:set_text(cal_ds_example_cond)
		end

		if stat.name == "arrow" then
			self._player_stats_texts_add[stat.name].reduction_basic:set_text("")
			self._player_stats_texts_add[stat.name].reduction_highest:set_text("")
		end
	end

	for _, stat in ipairs(self._player_stats_shown_add_hp) do
		self._player_stats_texts_add_hp[stat.name].health_per_kill:set_color(CColorh2)
		self._player_stats_texts_add_hp[stat.name].health_per_kill_percentage:set_color(CColorh)

		local health_regen_flat_per_kill = managers.player:fetch_health_flat_regen_per_kill()
		local health_regen_per_kill_percentage = managers.player:fetch_health_regen_per_kill_percentages()
		local health_regen_flat_per_second = managers.player:fetch_health_flat_regen_per_seconds()
		local health_regen_per_second_percentage = managers.player:fetch_health_regen_per_seconds_percentages()
		self._player_stats_texts_add_hp[stat.name].health_per_kill:set_text("-")
		self._player_stats_texts_add_hp[stat.name].health_per_kill_percentage:set_text("-")

		if stat.name == "health_per_kill_stat" then
			if health_regen_flat_per_kill > 0 then
				self._player_stats_texts_add_hp[stat.name].health_per_kill:set_text("+"..health_regen_flat_per_kill)
			end

			if health_regen_per_kill_percentage > 0 then
				self._player_stats_texts_add_hp[stat.name].health_per_kill_percentage:set_text("+"..health_regen_per_kill_percentage.."%")
			end
		end

		if stat.name == "health_per_second_stat" then
			if health_regen_flat_per_second > 0 then
				self._player_stats_texts_add_hp[stat.name].health_per_kill:set_text("+"..health_regen_flat_per_second)
			end

			if health_regen_per_second_percentage > 0 then
				self._player_stats_texts_add_hp[stat.name].health_per_kill_percentage:set_text("+"..health_regen_per_second_percentage.."%")
			end
		end
	end
end)

if prev_crash then
	return
end

function PlayerInventoryGui:_update_stats(name)
	if name == self._current_stat_shown then
		return
	end

	self._current_stat_shown = name

	self:set_info_text("")
	self._info_panel:clear()

	if name == "primary" or name == "secondary" then
		local stats = {
			{
				round_value = true,
				name = "magazine",
				stat_name = "extra_ammo"
			},
			{
				round_value = true,
				name = "totalammo",
				stat_name = "total_ammo_mod"
			},
			{
				round_value = true,
				name = "fire_rate"
			},
			{
				name = "damage"
			},
			{
				percent = true,
				name = "spread",
				offset = true,
				revert = true
			},
			{
				percent = true,
				name = "recoil",
				offset = true,
				revert = true
			},
			{
				index = true,
				name = "concealment"
			},
			{
				percent = false,
				name = "suppression",
				offset = true
			}
		}

		table.insert(stats, {
			inverted = true,
			name = "reload"
		})
		self:set_weapon_stats(self._info_panel, stats)
		self:_update_info_weapon(name)
	elseif name == "melee" then
		self:set_melee_stats(self._info_panel, {
			{
				range = true,
				name = "damage"
			},
			{
				range = true,
				name = "damage_effect",
				multiple_of = "damage"
			},
			{
				inverse = true,
				name = "charge_time",
				num_decimals = 1,
				suffix = managers.localization:text("menu_seconds_suffix_short")
			},
			{
				range = true,
				name = "range"
			},
			{
				index = true,
				name = "concealment"
			}
		})
		self:_update_info_melee(name)
	elseif name == "skilltree" or name == "outfit_armor" or name == "outfit_player_style" or name == "specialization" or name == "character" or name == "mask" or name == "throwable" or name == "deployable_primary" or name == "deployable_secondary" or name == "infamy" or name == "crew" then
		self:set_blank(self._info_panel)
	else
		local box = self._boxes_by_name[name]
		local stats = {
			{
				round_value = true,
				name = "magazine",
				stat_name = "extra_ammo"
			},
			{
				round_value = true,
				name = "totalammo",
				stat_name = "total_ammo_mod"
			},
			{
				round_value = true,
				name = "fire_rate"
			},
			{
				name = "damage"
			},
			{
				percent = true,
				name = "spread",
				offset = true,
				revert = true
			},
			{
				percent = true,
				name = "recoil",
				offset = true,
				revert = true
			},
			{
				index = true,
				name = "concealment"
			},
			{
				percent = false,
				name = "suppression",
				offset = true
			}
		}

		table.insert(stats, {
			inverted = true,
			name = "reload"
		})

		if box and box.params and box.params.mod_data then
			if box.params.mod_data.selected_tab == "weapon_cosmetics" then
				local cosmetics = managers.blackmarket:get_weapon_cosmetics(box.params.mod_data.category, box.params.mod_data.slot)

				if cosmetics then
					local c_td = tweak_data.blackmarket.weapon_skins[cosmetics.id] or {}

					if c_td.default_blueprint then
						self:set_weapon_stats(self._info_panel, stats)
					end

					self:_update_info_weapon_cosmetics(name, cosmetics)
				end
			else
				self:set_weapon_mods_stats(self._info_panel, stats)
				self:_update_info_weapon_mod(box)
			end
		end
	end
end

function PlayerInventoryGui:set_melee_stats(panel, data)
	local stats_panel = panel:child("stats_panel")
	local wpn_pn = self._panel_wpn

	wpn_pn:show()
	self._info_panel_fr:hide()


	if alive(stats_panel) then
		panel:remove(stats_panel)

		stats_panel = nil
	end

	stats_panel = panel:panel({
		name = "stats_panel"
	})
	local panel = stats_panel:panel({
		h = 20,
		layer = 1,
		w = stats_panel:w()
	})
	self._stats_shown = data
	self._stats_titles = {
		total = stats_panel:text({
			x = 135,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_total"))
		}),
		base = stats_panel:text({
			alpha = 0.75,
			x = 200,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_base"))
		}),
		skill = stats_panel:text({
			alpha = 0.75,
			x = 259,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.resource,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_skill"))
		})
	}
	local x = 0
	local y = 20
	local text_panel = nil
	local text_columns = {
		{
			size = 100,
			name = "name"
		},
		{
			size = 70,
			name = "total",
			align = "right"
		},
		{
			align = "right",
			name = "base",
			blend = "add",
			alpha = 0.75,
			size = 60
		},
		{
			align = "right",
			name = "skill",
			blend = "add",
			alpha = 0.75,
			size = 60,
			color = tweak_data.screen_colors.resource
		}
	}
	self._stats_texts = {}
	self._stats_panel = stats_panel:panel()

	for i, stat in ipairs(data) do
		panel = self._stats_panel:panel({
			name = "weapon_stats",
			h = 20,
			x = 0,
			layer = 1,
			y = y,
			w = self._stats_panel:w()
		})

		if math.mod(i, 2) ~= 0 and not panel:child(tostring(i)) then
			panel:rect({
				name = tostring(i),
				color = Color.black:with_alpha(0.4)
			})
		end

		x = 2
		y = y + 20
		self._stats_texts[stat.name] = {}

		for _, column in ipairs(text_columns) do
			text_panel = panel:panel({
				layer = 0,
				x = x,
				w = column.size,
				h = panel:h()
			})
			self._stats_texts[stat.name][column.name] = text_panel:text({
				text = "0",
				layer = 1,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_medium_font,
				align = column.align,
				alpha = column.alpha,
				blend_mode = column.blend,
				color = column.color or tweak_data.screen_colors.text
			})
			x = x + column.size

			if column.name == "name" then
				self._stats_texts[stat.name].name:set_text(managers.localization:to_upper_text("bm_menu_" .. stat.name))
			end
		end
	end
end

function PlayerInventoryGui:set_blank(panel)
	local wpn_pn = self._panel_wpn

	wpn_pn:hide()
	self._info_panel_fr:show()
end

function PlayerInventoryGui:set_weapon_stats(panel, data)
	local stats_panel = panel:child("weapon_info_panel")
	local wpn_pn = self._panel_wpn

	wpn_pn:show()
	self._info_panel_fr:hide()

	stats_panel = panel:panel({
		name = "weapon_info_panel"
	})
	local panel = stats_panel:panel({
		h = 20,
		layer = 1,
		w = stats_panel:w()
	})
	self._stats_shown = data
	self._stats_titles = {
		total = stats_panel:text({
			x = 120,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_total"))
		}),
		base = stats_panel:text({
			alpha = 0.75,
			x = 170,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_base"))
		}),
		mod = stats_panel:text({
			alpha = 0.75,
			x = 219,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.stats_mods,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_mod"))
		}),
		skill = stats_panel:text({
			alpha = 0.75,
			x = 259,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.resource,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_skill"))
		})
	}
	local x = 0
	local y = 20
	local text_panel = nil
	local text_columns = {
		{
			size = 100,
			name = "name"
		},
		{
			size = 45,
			name = "total",
			align = "right"
		},
		{
			align = "right",
			name = "base",
			blend = "add",
			alpha = 0.75,
			size = 45
		},
		{
			align = "right",
			name = "mods",
			blend = "add",
			alpha = 0.75,
			size = 45,
			color = tweak_data.screen_colors.stats_mods
		},
		{
			align = "right",
			name = "skill",
			blend = "add",
			alpha = 0.75,
			size = 45,
			color = tweak_data.screen_colors.resource
		}
	}
	self._stats_texts = {}
	self._stats_panel = stats_panel:panel()

	for i, stat in ipairs(data) do
		panel = self._stats_panel:panel({
			name = "weapon_stats",
			h = 20,
			x = 0,
			layer = 1,
			y = y,
			w = self._stats_panel:w()
		})

		if math.mod(i, 2) ~= 0 and not panel:child(tostring(i)) then
			panel:rect({
				name = tostring(i),
				color = Color.black:with_alpha(0.4),
			})
		end

		x = 2
		y = y + 19
		self._stats_texts[stat.name] = {}

		for _, column in ipairs(text_columns) do
			text_panel = panel:panel({
				layer = 0,
				x = x,
				w = column.size,
				h = panel:h(),
			})
			self._stats_texts[stat.name][column.name] = text_panel:text({
				text = "0",
				layer = 1,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_tiny_font,
				align = column.align,
				alpha = column.alpha,
				blend_mode = column.blend,
				color = column.color or tweak_data.screen_colors.text
			})
			x = x + column.size

			if column.name == "name" then
				self._stats_texts[stat.name].name:set_text(managers.localization:to_upper_text("bm_menu_" .. stat.name))
			end
		end
	end
end

function PlayerInventoryGui:set_weapon_mods_stats(panel, data)
	local stats_panel = panel:child("stats_panel")
	local wpn_pn = self._panel_wpn

	wpn_pn:show()
	self._info_panel_fr:hide()

	if alive(stats_panel) then
		panel:remove(stats_panel)

		stats_panel = nil
	end

	stats_panel = panel:panel({
		name = "stats_panel"
	})
	local panel = stats_panel:panel({
		h = 20,
		layer = 1,
		w = stats_panel:w()
	})
	self._stats_shown = data
	self._stats_titles = {
		total = stats_panel:text({
			alpha = 1,
			x = 120,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_total"))
		}),
		equip = stats_panel:text({
			alpha = 1,
			x = 200,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_equipped"))
		})
	}
	local x = 0
	local y = 19
	local text_panel = nil
	local text_columns = {
		{
			size = 100,
			name = "name"
		},
		{
			size = 45,
			name = "total",
			align = "right"
		},
		{
			size = 90,
			name = "equip",
			align = "right"
		}
	}
	self._stats_texts = {}
	self._stats_panel = stats_panel:panel()

	for i, stat in ipairs(data) do
		panel = self._stats_panel:panel({
			name = "weapon_stats",
			h = 20,
			x = 0,
			layer = 1,
			y = y,
			w = self._stats_panel:w()
		})

		if math.mod(i, 2) ~= 0 and not panel:child(tostring(i)) then
			panel:rect({
				name = tostring(i),
				color = Color.black:with_alpha(0.4)
			})
		end

		x = 2
		y = y + 19
		self._stats_texts[stat.name] = {}

		for _, column in ipairs(text_columns) do
			text_panel = panel:panel({
				layer = 0,
				x = x,
				w = column.size,
				h = panel:h()
			})
			self._stats_texts[stat.name][column.name] = text_panel:text({
				text = "0",
				layer = 1,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_medium_font,
				align = column.align,
				alpha = column.alpha,
				blend_mode = column.blend,
				color = column.color or tweak_data.screen_colors.text
			})
			x = x + column.size

			if column.name == "name" then
				self._stats_texts[stat.name].name:set_text(managers.localization:to_upper_text("bm_menu_" .. stat.name))
			end
		end
	end
end

function PlayerInventoryGui:init(ws, fullscreen_ws, node)
	self._ws = ws
	self._fullscreen_ws = fullscreen_ws
	self._node = node
	self._panel = self._ws:panel():panel()
	self._fullscreen_panel = self._fullscreen_ws:panel():panel()
	self._show_all_panel = self._ws:panel():panel({
		visible = false,
		w = self._ws:panel():w() * 0.75,
		h = tweak_data.menu.pd2_medium_font_size
	})

	self._show_all_panel:set_righttop(self._ws:panel():w(), 0)

	local show_all_text = self._show_all_panel:text({
		blend_mode = "add",
		text = managers.localization:to_upper_text(managers.menu:is_pc_controller() and "menu_show_all" or "menu_legend_show_all", {
			BTN_X = managers.localization:btn_macro("menu_toggle_voice_message")
		}),
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size
	})

	make_fine_text(show_all_text)
	show_all_text:set_right(self._show_all_panel:w())
	show_all_text:set_center_y(self._show_all_panel:h() / 2)

	if not managers.menu:is_pc_controller() then
		self._show_all_panel:set_bottom(self._ws:panel():h())
	end

	show_all_text:set_world_position(math.round(show_all_text:world_x()), math.round(show_all_text:world_y()))

	self._boxes = {}
	self._boxes_by_name = {}
	self._boxes_by_layer = {}
	self._mod_boxes = {}
	self._max_layer = 1
	self._data = node:parameters().menu_component_data or {
		selected_box = "character"
	}
	self._node:parameters().menu_component_data = self._data
	self._input_focus = 1

	WalletGuiObject.set_wallet(self._panel)

	local y = TOP_ADJUSTMENT
	local title_string = "menu_player_inventory"
	local title_text = self._panel:text({
		name = "title",
		layer = 1,
		text = managers.localization:to_upper_text(""),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = tweak_data.screen_colors.text
	})

	local back_button = self._panel:text({
		vertical = "bottom",
		name = "back_button",
		align = "right",
		layer = 1,
		text = managers.localization:to_upper_text("menu_back"),
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		color = tweak_data.screen_colors.button_stage_3
	})

	make_fine_text(back_button)
	back_button:set_right(self._panel:w())
	back_button:set_bottom(self._panel:h())
	back_button:set_visible(managers.menu:is_pc_controller())

	if MenuBackdropGUI then
		local massive_font = tweak_data.menu.pd2_massive_font
		local massive_font_size = tweak_data.menu.pd2_massive_font_size
		local bg_text = self._fullscreen_panel:text({
			vertical = "top",
			h = 90,
			align = "left",
			alpha = 0.4,
			text = title_text:text(),
			font = massive_font,
			font_size = massive_font_size,
			color = tweak_data.screen_colors.button_stage_3
		})
		local x, y = managers.gui_data:safe_to_full_16_9(title_text:world_x(), title_text:world_center_y())

		bg_text:set_world_left(x)
		bg_text:set_world_center_y(y)
		bg_text:move(-13, 9)
		MenuBackdropGUI.animate_bg_text(self, bg_text)

		if managers.menu:is_pc_controller() then
			local bg_back = self._fullscreen_panel:text({
				name = "back_button",
				vertical = "bottom",
				h = 90,
				alpha = 0.4,
				align = "right",
				layer = 0,
				text = back_button:text(),
				font = massive_font,
				font_size = massive_font_size,
				color = tweak_data.screen_colors.button_stage_3
			})
			local x, y = managers.gui_data:safe_to_full_16_9(back_button:world_right(), back_button:world_center_y())

			bg_back:set_world_right(x)
			bg_back:set_world_center_y(y)
			bg_back:move(13, -9)
			MenuBackdropGUI.animate_bg_text(self, bg_back)
		end
	end

	local padding_x = 10
	local padding_y = 0
	local x = self._panel:w() - 500
	local y = TOP_ADJUSTMENT + tweak_data.menu.pd2_small_font_size
	local width = self._panel:w() - x
	local height = 540
	local combined_width = width - padding_x * 2
	local combined_height = height - padding_y * 3
	local box_width = combined_width / 3
	local box_height = combined_height / 4
	local player_loadout_data = managers.blackmarket:player_loadout_data()
	local primary_box_data = {
		name = "primary",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_primaries"),
		text = player_loadout_data.primary.info_text,
		image = player_loadout_data.primary.item_texture,
		bg_image = player_loadout_data.primary.item_bg_texture,
		text_selected_color = player_loadout_data.primary.info_text_color,
		use_background = player_loadout_data.primary.item_bg_texture and true or false,
		akimbo_gui_data = player_loadout_data.primary.akimbo_gui_data,
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.white:with_alpha(0.75),
		clbks = {
			left = callback(self, self, "open_primary_menu"),
			right = callback(self, self, "preview_primary"),
			up = callback(self, self, "previous_primary"),
			down = callback(self, self, "next_primary")
		}
	}
	local secondary_box_data = {
		name = "secondary",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_secondaries"),
		text = player_loadout_data.secondary.info_text,
		image = player_loadout_data.secondary.item_texture,
		bg_image = player_loadout_data.secondary.item_bg_texture,
		text_selected_color = player_loadout_data.secondary.info_text_color,
		use_background = player_loadout_data.secondary.item_bg_texture and true or false,
		akimbo_gui_data = player_loadout_data.secondary.akimbo_gui_data,
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.white:with_alpha(0.75),
		clbks = {
			left = callback(self, self, "open_secondary_menu"),
			right = callback(self, self, "preview_secondary"),
			up = callback(self, self, "previous_secondary"),
			down = callback(self, self, "next_secondary")
		}
	}
	local melee_box_data = {
		name = "melee",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_melee_weapons"),
		text = player_loadout_data.melee_weapon.info_text,
		image = player_loadout_data.melee_weapon.item_texture,
		dual_image = not player_loadout_data.melee_weapon.item_texture and {
			player_loadout_data.melee_weapon.dual_texture_1,
			player_loadout_data.melee_weapon.dual_texture_2
		},
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			left = callback(self, self, "open_melee_menu"),
			right = callback(self, self, "preview_melee"),
			up = callback(self, self, "previous_melee"),
			down = callback(self, self, "next_melee")
		}
	}
	local throwable_box_data = {
		name = "throwable",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_grenades"),
		text = player_loadout_data.grenade.info_text,
		image = player_loadout_data.grenade.item_texture,
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			left = callback(self, self, "open_throwable_menu"),
			right = callback(self, self, "preview_throwable"),
			up = callback(self, self, "previous_throwable"),
			down = callback(self, self, "next_throwable")
		}
	}
	local armor_box_data = {
		name = "armor",
		redirect_box = "outfit_armor",
		can_select = false,
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_armors"),
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		data = player_loadout_data.outfit,
		clbks = {
			create = callback(self, self, "create_outfit_box")
		}
	}
	local deployable_box_data = {
		name = "deployable",
		redirect_box = "deployable_primary",
		can_select = false,
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_deployables"),
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		data = player_loadout_data.deployable,
		clbks = {
			create = callback(self, self, "create_deployable_box")
		}
	}
	local mask_box_data = {
		name = "mask",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("bm_menu_masks"),
		text = player_loadout_data.mask.info_text,
		image = player_loadout_data.mask.item_texture,
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			left = callback(self, self, "open_mask_menu"),
			right = callback(self, self, "preview_mask"),
			up = callback(self, self, "previous_mask"),
			down = callback(self, self, "next_mask")
		}
	}
	local character_box_data = {
		alpha = 1,
		name = "character",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("menu_preferred_character"),
		text = player_loadout_data.character.info_text,
		image = player_loadout_data.character.item_texture,
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			right = false,
			left = callback(self, self, "open_character_menu"),
			up = callback(self, self, "previous_character"),
			down = callback(self, self, "next_character")
		}
	}
	local infamy_box_data = {
		name = "infamy",
		alpha = 1,
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("menu_infamytree"),
		text = managers.localization:to_upper_text("menu_infamytree"),
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			down = false,
			up = false,
			right = false,
			left = callback(self, self, "open_infamy_menu")
		}
	}
	local crew_box_data = {
		image = "guis/dlcs/mom/textures/pd2/crewmanagement_icon",
		name = "crew",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("menu_crew_management"),
		text = managers.localization:to_upper_text("menu_crew_management"),
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			down = false,
			up = false,
			right = false,
			left = callback(self, self, "open_crew_menu")
		}
	}
	local skill_box_data = {
		image = false,
		name = "skilltree",
		bg_blend_mode = "normal",
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("menu_st_skilltree"),
		text = managers.localization:text("menu_st_skill_switch_set", {
			skill_switch = managers.skilltree:get_skill_switch_name(managers.skilltree:get_selected_skill_switch(), true)
		}),
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			right = false,
			left = callback(self, self, "open_skilltree_menu"),
			up = callback(self, self, "previous_skilltree"),
			down = callback(self, self, "next_skilltree")
		}
	}
	local texture_rect_x = 0
	local texture_rect_y = 0
	local current_specialization = managers.skilltree:get_specialization_value("current_specialization")
	local specialization_data = tweak_data.skilltree.specializations[current_specialization]
	local specialization_text = specialization_data and managers.localization:text(specialization_data.name_id) or " "
	local guis_catalog = "guis/"

	if specialization_data then
		local current_tier = managers.skilltree:get_specialization_value(current_specialization, "tiers", "current_tier")
		local max_tier = managers.skilltree:get_specialization_value(current_specialization, "tiers", "max_tier")
		local tier_data = specialization_data[max_tier]

		if tier_data then
			texture_rect_x = tier_data.icon_xy and tier_data.icon_xy[1] or 0
			texture_rect_y = tier_data.icon_xy and tier_data.icon_xy[2] or 0

			if tier_data.texture_bundle_folder then
				guis_catalog = guis_catalog .. "dlcs/" .. tostring(tier_data.texture_bundle_folder) .. "/"
			end

			specialization_text = specialization_text .. " (" .. tostring(current_tier) .. "/" .. tostring(max_tier) .. ")"
		end
	end

	local icon_atlas_texture = guis_catalog .. "textures/pd2/specialization/icons_atlas"
	local specialization_box_data = {
		name = "specialization",
		bg_blend_mode = "normal",
		image_size_mul = 0.8,
		w = box_width,
		h = box_height,
		unselected_text = managers.localization:to_upper_text("menu_specialization"),
		text = specialization_text,
		image = icon_atlas_texture,
		image_rect = {
			texture_rect_x * 64,
			texture_rect_y * 64,
			64,
			64
		},
		select_anim = select_anim,
		unselect_anim = unselect_anim,
		bg_color = Color.black:with_alpha(0.05),
		clbks = {
			right = false,
			left = callback(self, self, "open_specialization_menu"),
			up = callback(self, self, "previous_specialization"),
			down = callback(self, self, "next_specialization")
		}
	}

	if managers.network:session() then
		character_box_data.alpha = 0.2
		character_box_data.clbks = {
			right = false
		}
		infamy_box_data.alpha = 0.2
		infamy_box_data.clbks = {
			right = false
		}

		if not Network:is_server() then
			crew_box_data.alpha = 0.2
			crew_box_data.clbks = {
				right = false
			}
		end
	end

	local primary_box = self:create_box(primary_box_data)
	local secondary_box = self:create_box(secondary_box_data)
	local melee_box = self:create_box(melee_box_data)
	local throwable_box = self:create_box(throwable_box_data)
	local armor_box = self:create_box(armor_box_data)
	local deployable_box = self:create_box(deployable_box_data)
	local mask_box = self:create_box(mask_box_data)
	local character_box = self:create_box(character_box_data)
	local infamy_box = self:create_box(infamy_box_data)
	local crew_box = self:create_box(crew_box_data)
	local skill_box = self:create_box(skill_box_data)
	local specialization_box = self:create_box(specialization_box_data)
	local box_matrix = {
		{
			"character",
			"primary",
			"skilltree"
		},
		{
			"mask",
			"secondary",
			"specialization"
		},
		{
			"armor",
			"throwable",
			"crew"
		},
		{
			"deployable",
			"melee",
			"infamy"
		}
	}

	self:sort_boxes_by_matrix(box_matrix)

	local character_panel = self._panel:panel({
		name = "character_panel"
	})
	local weapons_panel = self._panel:panel({
		name = "weapons_panel"
	})
	local eqpt_skills_panel = self._panel:panel({
		name = "eqpt_skills_panel"
	})

	local function get_matrix_column_boxes(column)
		local boxes = {}

		for _, row in ipairs(box_matrix) do
			if row[column] and self._boxes_by_name[row[column]] then
				table.insert(boxes, self._boxes_by_name[row[column]])
			end
		end

		return boxes
	end

	for i, column_panel in ipairs({
		character_panel,
		weapons_panel,
		eqpt_skills_panel
	}) do
		local boxes = get_matrix_column_boxes(i)

		if #boxes > 0 then
			local first = boxes[1].panel
			local last = boxes[#boxes].panel

			column_panel:set_shape(first:left(), first:top(), box_width, last:bottom() - first:top())
		end
	end

	character_panel:rect({
		alpha = 0.4,
		color = Color.black
	})
	weapons_panel:rect({
		alpha = 0.4,
		color = Color.black
	})
	eqpt_skills_panel:rect({
		alpha = 0.4,
		color = Color.black
	})

	local column_one_box_panel = self._panel:panel({
		name = "column_one_box_panel"
	})

	column_one_box_panel:set_shape(character_panel:shape())

	local column_two_box_panel = self._panel:panel({
		name = "column_two_box_panel"
	})

	column_two_box_panel:set_shape(weapons_panel:shape())

	local column_three_box_panel = self._panel:panel({
		name = "column_three_box_panel"
	})

	column_three_box_panel:set_shape(eqpt_skills_panel:shape())

	self._column_one_box = BoxGuiObject:new(column_one_box_panel, {
		sides = {
			1,
			1,
			2,
			2
		}
	})
	self._column_two_box = BoxGuiObject:new(column_two_box_panel, {
		sides = {
			1,
			1,
			2,
			2
		}
	})
	self._column_three_box = BoxGuiObject:new(column_three_box_panel, {
		sides = {
			1,
			1,
			2,
			2
		}
	})
	local character_text = self._panel:text({
		name = "character_text",
		blend_mode = "add",
		text = managers.localization:to_upper_text("menu_player_column_one_title"),
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = tweak_data.screen_colors.text
	})
	local weapons_text = self._panel:text({
		name = "weapons_text",
		blend_mode = "add",
		text = managers.localization:to_upper_text("menu_player_column_two_title"),
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = tweak_data.screen_colors.text
	})
	local eqpt_skills_text = self._panel:text({
		name = "eqpt_skills_text",
		blend_mode = "add",
		text = managers.localization:to_upper_text("menu_player_column_three_title"),
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = tweak_data.screen_colors.text
	})

	make_fine_text(character_text)
	make_fine_text(weapons_text)
	make_fine_text(eqpt_skills_text)
	character_text:set_rightbottom(character_panel:right(), character_panel:top())
	weapons_text:set_rightbottom(weapons_panel:right(), weapons_panel:top())
	eqpt_skills_text:set_rightbottom(eqpt_skills_panel:right(), eqpt_skills_panel:top())

	local player_panel = self._panel:panel({
		name = "player_panel",
		h = 415,
		w = 355,
		x = 0,
		y = TOP_ADJUSTMENT + tweak_data.menu.pd2_small_font_size - 75
	})
	local CColorLS = FetchInfoPSPE("split_line")

	if not PSPE._data.PSPE_disable_cosmetic_split then
		player_panel:rect({
			name = "pnl_underline_split",
			color = CColorLS:with_alpha(0.3) or Color.white:with_alpha(0.3),
			h = 2,
			w = player_panel:w() - 20,
			y = 250,
			x = 10,
		})
	end

	player_panel:text({
		vertical = "top",
		align = "center",
		rotation = 360,
		layer = 1,
		y = player_panel:y() + 15,
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		text = managers.localization:to_upper_text("bm_menu_player_stats"),
		color = Color.white
	})

	local y_pspe, y_u, y_inf, f_size, p_y, n_u = FetchInfoPSPE("vanilla_resize")
	player_panel:rect({
		name = "pnl_underline",
		color = Color.white:with_alpha(1),
		h = 1,
		w = 104.5,
		y = player_panel:y() + y_u,
		x = 125
	})

	self._player_panel = player_panel

	self._player_panel:rect({
		alpha = 0.2,
		layer = -101,
		color = Color(255, 10, 10, 10) / 255
	})

	self._player_panel:bitmap({
		texture = "guis/textures/test_blur_df",
		name = "blur",
		render_template = "VertexColorTexturedBlur3D",
		layer = -100,
		w = player_panel:w(),
		h = player_panel:h()
	})

	self._player_box_panel = self._panel:panel({
		name = "player_box_panel"
	})

	self._player_box_panel:set_shape(player_panel:shape())

	self._player_box = BoxGuiObject:new(self._player_box_panel, {
		sides = {
			1,
			1,
			2,
			1
		}
	})
	local next_level_data = managers.experience:next_level_data() or {}
	local player_level = managers.experience:current_level()
	local player_rank = managers.experience:current_rank()
	local is_infamous = player_rank > 0
	local size = 80
	local info_panel = self._panel:panel({
		name = "info_panel",
		x = 0,
		y = player_panel:bottom() + 15,
		w = player_panel:w(),
		h = self._panel:h() - player_panel:top() - BOT_ADJUSTMENT - player_panel:h() - 40
	})

	self._info_panel_fr = info_panel

	info_panel:bitmap({
		texture = "guis/textures/test_blur_df",
		name = "blur",
		render_template = "VertexColorTexturedBlur3D",
		layer = -100,
		w = player_panel:w(),
		h = info_panel:h()
	})

	info_panel:rect({
		alpha = 0.2,
		layer = -101,
		color = Color(255, 10, 10, 10) / 255
	})

	local player_level_panel = info_panel:panel({
		name = "player_level_panel",
		y = 40,
		x = 2,
		w = size,
		h = size
	})

	local alias_text = info_panel:text({
		name = "alias_text",
		blend_mode = "add",
		x = 8,
		y = player_level_panel:y() - 35,
		text = tostring(managers.network.account:username() or managers.blackmarket:get_preferred_character_real_name()),
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
		color = tweak_data.screen_colors.text
	})

	info_panel:rect({
		name = "alias_txt",
		color = Color.white:with_alpha(1),
		h = 2,
		w = 13,
		y = n_u,
		x = 8
	})

	local current_exp = managers.experience:next_level_data_current_points()
	local next_level_exp = managers.experience:next_level_data_points()
	local precentage_cal_exp = math.round((current_exp/next_level_exp) * 100)
	local current_presitge = managers.experience:get_current_prestige_xp()
	local max_prestige = managers.experience:get_max_prestige_xp()
	local precentage_cal = math.round((current_presitge/max_prestige) * 100)
	local y_text_exp = 80
	local x_text_exp = 5
	local player_exp = {}
	local bonus_exp = {}
	if current_exp == next_level_exp then
		player_exp = player_level_panel:text({
			vertical = "top",
			align = "center",
			rotation = 360,
			layer = 1,
			y = y_text_exp,
			x = x_text_exp,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size - 5,
			text = managers.localization:text("bm_menu_reputation_progress")..precentage_cal_exp.."%",
			color = tweak_data.screen_colors.resource
		})
	else
		player_exp = player_level_panel:text({
			vertical = "top",
			align = "center",
			rotation = 360,
			layer = 1,
			y = y_text_exp,
			x = x_text_exp,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size - 5,
			text = managers.localization:text("bm_menu_reputation_progress")..precentage_cal_exp.."%",
			color = Color.white
		})
	end

	if player_level == 100 then
		bonus_exp = player_level_panel:text({
			vertical = "top",
			align = "center",
			rotation = 360,
			layer = 1,
			y = y_text_exp + p_y,
			x = x_text_exp,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size - 5,
			text = managers.localization:text("bm_menu_infamy_pool")..precentage_cal.. "%",
			color = tweak_data.screen_colors.infamy_color
		})
	else
		bonus_exp = player_level_panel:text({
			vertical = "top",
			align = "center",
			rotation = 360,
			layer = 1,
			y = y_text_exp + p_y,
			x = x_text_exp,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size - 5,
			text = managers.localization:text("bm_menu_infamy_pool").. 0 .. "%",
			color = tweak_data.screen_colors.text,
			alpha = 0.5
		})
	end

	make_fine_text(player_exp)
	make_fine_text(bonus_exp)

	if is_infamous then
		local rank_string = managers.experience:rank_string(player_rank)
		local level_string = tostring(player_level)
		local scale = 0

		if type(rank_string) ~= "number" and #rank_string > 3 then
			scale = #rank_string - 1
		end

		local rank_text = player_level_panel:text({
			vertical = "top",
			align = "center",
			rotation = 360,
			layer = 1,
			y = y_inf,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size - f_size,
			text = "[" .. rank_string .. "]",
			color = tweak_data.screen_colors.infamy_color
		})
		local Level_text = player_level_panel:text({
			vertical = "top",
			align = "center",
			rotation = 360,
			layer = 1,
			y = 25,
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size - 5,
			text = level_string,
			color = tweak_data.screen_colors.text
		})

		if scale > 0 then
			rank_text:set_font_size(rank_text:font_size() - scale)
			rank_text:set_y(rank_text:y() + (scale/5) + 0.2)
		end
	else
		local level_text = player_level_panel:text({
			vertical = "center",
			align = "center",
			text = tostring(player_level),
			font = tweak_data.menu.pd2_medium_font,
			font_size = tweak_data.menu.pd2_medium_font_size
		})
	end

	make_fine_text(alias_text)

	local size_level = 80
	local dec_alpha = 0.2
	local dec_alpha_real = 0.7
	player_level_panel:bitmap({
		texture = "guis/textures/pd2/endscreen/exp_ring",
		alpha = 0.4,
		w = size_level,
		h = size_level,
		color = Color.white
	})
	player_level_panel:bitmap({
		texture = "guis/textures/pd2/endscreen/exp_ring",
		blend_mode = "add",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		color = Color((next_level_data.current_points or 1) / (next_level_data.points or 1), 1, 1),
		w = size_level,
		h = size_level
	})
	player_level_panel:bitmap({
		texture = "guis/textures/pd2/exp_ring_purple",
		name = "bg_infamy_progress_circle",
		alpha = 1,
		blend_mode = "add",
		render_template = "VertexColorTexturedRadial",
		layer = 15,
		h = size_level,
		w = size_level,
		color = Color(managers.experience:get_prestige_xp_percentage_progress(), 1, 1)
	})

	local detection_panel = info_panel:panel({
		name = "detection_panel",
		layer = 0,
		w = 60,
		h = 60
	})
	local detection_ring_left_bg = detection_panel:bitmap({
		blend_mode = "add",
		name = "detection_left_bg",
		alpha = dec_alpha,
		texture = "guis/textures/pd2/blackmarket/inv_detection_meter",
		layer = 0,
		w = detection_panel:w(),
		h = detection_panel:h()
	})
	local detection_ring_right_bg = detection_panel:bitmap({
		blend_mode = "add",
		name = "detection_right_bg",
		alpha = dec_alpha,
		texture = "guis/textures/pd2/blackmarket/inv_detection_meter",
		layer = 0,
		w = detection_panel:w(),
		h = detection_panel:h()
	})

	detection_ring_right_bg:set_texture_rect(64, 0, -64, 64)

	local detection_ring_left = detection_panel:bitmap({
		texture = "guis/textures/pd2/blackmarket/inv_detection_meter",
		name = "detection_left",
		alpha = dec_alpha_real,
		blend_mode = "add",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		w = detection_panel:w(),
		h = detection_panel:h()
	})
	local detection_ring_right = detection_panel:bitmap({
		texture = "guis/textures/pd2/blackmarket/inv_detection_meter",
		name = "detection_right",
		alpha = dec_alpha_real,
		blend_mode = "add",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		w = detection_panel:w(),
		h = detection_panel:h()
	})

	detection_ring_right:set_texture_rect(64, 0, -64, 64)

	local detection_ring_left2 = detection_panel:bitmap({
		texture = "guis/textures/pd2/blackmarket/inv_detection_meter",
		name = "detection_left2",
		alpha = dec_alpha_real,
		blend_mode = "add",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		w = detection_panel:w(),
		h = detection_panel:h()
	})
	local detection_ring_right2 = detection_panel:bitmap({
		texture = "guis/textures/pd2/blackmarket/inv_detection_meter",
		name = "detection_right2",
		alpha = dec_alpha_real,
		blend_mode = "add",
		render_template = "VertexColorTexturedRadial",
		layer = 1,
		w = detection_panel:w(),
		h = detection_panel:h()
	})

	detection_ring_right2:set_texture_rect(64, 0, -64, 64)

	local y_offset = 15

	local detection_value = detection_panel:text({
		alpha = 1,
		name = "detection_value",
		h = 64,
		text = "",
		w = 64,
		blend_mode = "add",
		visible = true,
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size
	})
	local detection_text = info_panel:text({
		name = "detection_text",
		alpha = 1,
		visible = true,
		blend_mode = "add",
		y = player_level_panel:top(),
		text = managers.localization:to_upper_text("bm_menu_stats_detection"),
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_small_font_size
	})

	make_fine_text(detection_text)
	detection_text:set_right(info_panel:w() - 10)
	detection_panel:set_center_x(detection_text:center_x())
	detection_panel:set_right(math.min(detection_panel:right(), detection_text:right()))
	detection_panel:set_top(detection_text:bottom() - y_offset)
	detection_text:set_top(detection_panel:top() - y_offset - 5)

	self._additional_player_stats = player_panel:panel({
		name = "additional_player_stats",
		x = 10,
		h = 80,
		w = player_panel:w() - 20
	})

	self._player_health_regen_stats = player_panel:panel({
		name = "_player_health_regen_stats",
		x = 10,
		h = 150,
		w = player_panel:w() - 20
	})

	self._player_stats_panel = player_panel:panel({
		name = "player_stats_panel",
		x = 10,
		h = 355,
		w = player_panel:w() - 20
	})

	self._additional_player_stats:set_bottom(player_panel:h() - 10)
	self:setup_add_player_stats(self._additional_player_stats)
	self._player_health_regen_stats:set_bottom(player_panel:h() - 10)
	self:player_health_regen_stats(self._player_health_regen_stats)
	self._player_stats_panel:set_bottom(player_panel:h() - 10)
	self:setup_player_stats(self._player_stats_panel)

	local weapon_info_panel = self._panel:panel({
		name = "weapon_info_panel",
		x = info_panel:x(),
		y = player_panel:bottom() + 8,
		w = info_panel:w() + 20,
		h = info_panel:h() + 85
	})

	weapon_info_panel:bitmap({
		texture = "guis/textures/test_blur_df",
		name = "blur",
		render_template = "VertexColorTexturedBlur3D",
		layer = -100,
		w = weapon_info_panel:w(),
		h = weapon_info_panel:h()
	})

	weapon_info_panel:rect({
		alpha = 0.2,
		layer = -100,
		color = Color(255, 10, 10, 10) / 255
	})

	BoxGuiObject:new(weapon_info_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	BoxGuiObject:new(info_panel, {
		sides = {
			1,
			1,
			1,
			1
		}
	})

	self._panel_wpn = weapon_info_panel

	self._info_text = weapon_info_panel:text({
		text = "",
		wrap = true,
		word_wrap = true,
		blend_mode = "add",
		font = tweak_data.menu.pd2_small_font,
		font_size = tweak_data.menu.pd2_small_font_size,
		color = tweak_data.screen_colors.text,
		x = 6,
		y = 4
	})

	weapon_info_panel:grow(-20, -20)
	weapon_info_panel:move(0, 0)
	weapon_info_panel:hide()

	self._info_panel = weapon_info_panel:panel({
		layer = -1,
		alpha = 1
	})

	self:set_info_text("")

	self._legends_panel = self._panel:panel({
		name = "legends_panel",
		w = self._panel:w() * 0.75,
		h = tweak_data.menu.pd2_medium_font_size
	})
	self._legends = {}

	if managers.menu:is_pc_controller() then
		self._legends_panel:set_righttop(self._panel:w(), 0)

		if not managers.menu:is_steam_controller() then
			local panel = self._legends_panel:panel({
				visible = false,
				name = "select"
			})
			local icon = panel:bitmap({
				texture = "guis/textures/pd2/mouse_buttons",
				name = "icon",
				h = 23,
				blend_mode = "add",
				w = 17,
				texture_rect = {
					1,
					1,
					17,
					23
				}
			})
			local text = panel:text({
				blend_mode = "add",
				text = managers.localization:to_upper_text("menu_mouse_select"),
				font = tweak_data.menu.pd2_small_font,
				font_size = tweak_data.menu.pd2_small_font_size,
				color = tweak_data.screen_colors.text
			})

			make_fine_text(text)
			text:set_left(icon:right() + 2)
			text:set_center_y(icon:center_y())
			panel:set_w(text:right())

			self._legends.select = panel
			local panel = self._legends_panel:panel({
				visible = false,
				name = "preview"
			})
			local icon = panel:bitmap({
				texture = "guis/textures/pd2/mouse_buttons",
				name = "icon",
				h = 23,
				blend_mode = "add",
				w = 17,
				texture_rect = {
					18,
					1,
					17,
					23
				}
			})
			local text = panel:text({
				blend_mode = "add",
				text = managers.localization:to_upper_text("menu_mouse_preview"),
				font = tweak_data.menu.pd2_small_font,
				font_size = tweak_data.menu.pd2_small_font_size,
				color = tweak_data.screen_colors.text
			})

			make_fine_text(text)
			text:set_left(icon:right() + 2)
			text:set_center_y(icon:center_y())
			panel:set_w(text:right())

			self._legends.preview = panel
			local panel = self._legends_panel:panel({
				visible = false,
				name = "switch"
			})
			local icon = panel:bitmap({
				texture = "guis/textures/pd2/mouse_buttons",
				name = "icon",
				h = 23,
				blend_mode = "add",
				w = 17,
				texture_rect = {
					35,
					1,
					17,
					23
				}
			})
			local text = panel:text({
				blend_mode = "add",
				text = managers.localization:to_upper_text("menu_mouse_switch"),
				font = tweak_data.menu.pd2_small_font,
				font_size = tweak_data.menu.pd2_small_font_size,
				color = tweak_data.screen_colors.text
			})

			make_fine_text(text)
			text:set_left(icon:right() + 2)
			text:set_center_y(icon:center_y())
			panel:set_w(text:right())

			self._legends.switch = panel
		else
			local panel = self._legends_panel:panel({
				visible = false,
				name = "select"
			})
			local text = panel:text({
				blend_mode = "add",
				text = managers.localization:steam_btn("grip_l") .. " " .. managers.localization:to_upper_text("menu_mouse_select"),
				font = tweak_data.menu.pd2_small_font,
				font_size = tweak_data.menu.pd2_small_font_size,
				color = tweak_data.screen_colors.text
			})

			make_fine_text(text)
			text:set_center_y(panel:h() / 2)
			panel:set_w(text:right())

			self._legends.select = panel
			local panel = self._legends_panel:panel({
				visible = false,
				name = "preview"
			})
			local text = panel:text({
				blend_mode = "add",
				text = managers.localization:steam_btn("grip_r") .. " " .. managers.localization:to_upper_text("menu_mouse_preview"),
				font = tweak_data.menu.pd2_small_font,
				font_size = tweak_data.menu.pd2_small_font_size,
				color = tweak_data.screen_colors.text
			})

			make_fine_text(text)
			text:set_center_y(panel:h() / 2)
			panel:set_w(text:right())

			self._legends.preview = panel
			local panel = self._legends_panel:panel({
				visible = false,
				name = "switch"
			})
			local text = panel:text({
				blend_mode = "add",
				text = managers.localization:btn_macro("previous_page") .. managers.localization:btn_macro("next_page") .. " " .. managers.localization:to_upper_text("menu_mouse_switch"),
				font = tweak_data.menu.pd2_small_font,
				font_size = tweak_data.menu.pd2_small_font_size,
				color = tweak_data.screen_colors.text
			})

			make_fine_text(text)
			text:set_center_y(panel:h() / 2)
			panel:set_w(text:right())

			self._legends.switch = panel
		end

		local panel = self._legends_panel:panel({
			visible = false,
			name = "hide_all"
		})
		local text = panel:text({
			blend_mode = "add",
			text = managers.localization:to_upper_text("menu_hide_all", {
				BTN_X = managers.localization:btn_macro("menu_toggle_voice_message")
			}),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			color = tweak_data.screen_colors.text
		})

		make_fine_text(text)
		text:set_center_y(panel:h() / 2)
		panel:set_w(text:right())

		self._legends.hide_all = panel
	else
		self._legends_panel:set_righttop(self._panel:w(), 0)
		self._legends_panel:text({
			text = "",
			name = "text",
			vertical = "bottom",
			align = "right",
			blend_mode = "add",
			halign = "right",
			valign = "bottom",
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			color = tweak_data.screen_colors.text
		})
	end

	self._text_buttons = {}
	local alias_show_button, alias_hide_button, column_one_show_button, column_one_hide_button, column_two_show_button, column_two_hide_button, column_three_show_button, column_three_hide_button = nil

	local function alias_hide_func()
		if alive(alias_show_button) then
			alias_show_button:hide()
		end

		if alive(alias_hide_button) then
			alias_hide_button:show()
		end

		if alive(self._player_box_panel) then
			self._player_box:create_sides(self._player_box_panel, {
				sides = {
					0,
					0,
					2,
					0
				}
			})
		end
	end

	local function alias_show_func()
		if alive(alias_show_button) then
			alias_show_button:show()
		end

		if alive(alias_hide_button) then
			alias_hide_button:hide()
		end

		if alive(self._player_box_panel) then
			self._player_box:create_sides(self._player_box_panel, {
				sides = {
					1,
					1,
					2,
					1
				}
			})
		end
	end

	local function column_one_hide_func()
		if alive(column_one_show_button) then
			column_one_show_button:hide()
		end

		if alive(column_one_hide_button) then
			column_one_hide_button:show()
		end

		if alive(character_panel) then
			character_panel:hide()
		end

		if alive(column_one_box_panel) then
			self._column_one_box:create_sides(column_one_box_panel, {
				sides = {
					0,
					0,
					2,
					0
				}
			})
		end

		local box = nil

		for _, boxes in ipairs(box_matrix) do
			box = boxes[1] and self._boxes_by_name[boxes[1]]

			if box then
				box.panel:hide()
			end
		end
	end

	local function column_one_show_func()
		if alive(column_one_show_button) then
			column_one_show_button:show()
		end

		if alive(column_one_hide_button) then
			column_one_hide_button:hide()
		end

		if alive(character_panel) then
			character_panel:show()
		end

		if alive(column_one_box_panel) then
			self._column_one_box:create_sides(column_one_box_panel, {
				sides = {
					1,
					1,
					2,
					1
				}
			})
		end

		local box = nil

		for _, boxes in ipairs(box_matrix) do
			box = boxes[1] and self._boxes_by_name[boxes[1]]

			if box then
				box.panel:show()
			end
		end
	end

	local function column_two_hide_func()
		if alive(column_two_show_button) then
			column_two_show_button:hide()
		end

		if alive(column_two_hide_button) then
			column_two_hide_button:show()
		end

		if alive(weapons_panel) then
			weapons_panel:hide()
		end

		if alive(column_two_box_panel) then
			self._column_two_box:create_sides(column_two_box_panel, {
				sides = {
					0,
					0,
					2,
					0
				}
			})
		end

		local box = nil

		for _, boxes in ipairs(box_matrix) do
			box = boxes[2] and self._boxes_by_name[boxes[2]]

			if box then
				box.panel:hide()
			end
		end

		for _, box in ipairs(self._boxes_by_layer[2]) do
			box.panel:hide()
		end
	end

	local function column_two_show_func()
		if alive(column_two_show_button) then
			column_two_show_button:show()
		end

		if alive(column_two_hide_button) then
			column_two_hide_button:hide()
		end

		if alive(weapons_panel) then
			weapons_panel:show()
		end

		if alive(column_two_box_panel) then
			self._column_two_box:create_sides(column_two_box_panel, {
				sides = {
					1,
					1,
					2,
					1
				}
			})
		end

		local box = nil

		for _, boxes in ipairs(box_matrix) do
			box = boxes[2] and self._boxes_by_name[boxes[2]]

			if box then
				box.panel:show()
			end
		end

		for _, box in ipairs(self._boxes_by_layer[2]) do
			box.panel:show()
		end
	end

	local function column_three_hide_func()
		if alive(column_three_show_button) then
			column_three_show_button:hide()
		end

		if alive(column_three_hide_button) then
			column_three_hide_button:show()
		end

		if alive(eqpt_skills_panel) then
			eqpt_skills_panel:hide()
		end

		if alive(column_three_box_panel) then
			self._column_three_box:create_sides(column_three_box_panel, {
				sides = {
					0,
					0,
					2,
					0
				}
			})
		end

		local box = nil

		for _, boxes in ipairs(box_matrix) do
			box = boxes[3] and self._boxes_by_name[boxes[3]]

			if box then
				box.panel:hide()
			end
		end

		for _, box in ipairs(self._boxes_by_layer[3]) do
			if alive(box.panel) then
				box.panel:hide()
			end
		end
	end

	local function column_three_show_func()
		if alive(column_three_show_button) then
			column_three_show_button:show()
		end

		if alive(column_three_hide_button) then
			column_three_hide_button:hide()
		end

		if alive(eqpt_skills_panel) then
			eqpt_skills_panel:show()
		end

		if alive(column_three_box_panel) then
			self._column_three_box:create_sides(column_three_box_panel, {
				sides = {
					1,
					1,
					2,
					1
				}
			})
		end

		local box = nil

		for _, boxes in ipairs(box_matrix) do
			box = boxes[3] and self._boxes_by_name[boxes[3]]

			if box then
				box.panel:show()
			end
		end

		for _, box in ipairs(self._boxes_by_layer[3]) do
			if alive(box.panel) then
				box.panel:show()
			end
		end
	end

	self._show_hide_data = {
		panels = {
			character_panel,
			weapons_panel,
			eqpt_skills_panel
		},
		show_funcs = {
			alias_show_func,
			column_one_show_func,
			column_two_show_func,
			column_three_show_func
		},
		hide_funcs = {
			alias_hide_func,
			column_one_hide_func,
			column_two_hide_func,
			column_three_hide_func
		}
	}

	if managers.menu:is_pc_controller() then
		local show_string = managers.localization:to_upper_text("menu_button_hide")
		local hide_string = managers.localization:to_upper_text("menu_button_show")
		column_one_show_button = self:create_text_button({
			alpha = 0.1,
			right = character_text:left() - 2,
			top = character_text:top(),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = show_string,
			clbk = column_one_hide_func
		})
		column_one_hide_button = self:create_text_button({
			disabled = true,
			right = column_one_show_button:right(),
			top = column_one_show_button:top(),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = hide_string,
			clbk = column_one_show_func
		})
		column_two_show_button = self:create_text_button({
			alpha = 0.1,
			right = weapons_text:left() - 2,
			top = weapons_text:top(),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = show_string,
			clbk = column_two_hide_func
		})
		column_two_hide_button = self:create_text_button({
			disabled = true,
			right = column_two_show_button:right(),
			top = column_two_show_button:top(),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = hide_string,
			clbk = column_two_show_func
		})
		column_three_show_button = self:create_text_button({
			alpha = 0.1,
			right = eqpt_skills_text:left() - 2,
			top = eqpt_skills_text:top(),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = show_string,
			clbk = column_three_hide_func
		})
		column_three_hide_button = self:create_text_button({
			disabled = true,
			right = column_three_show_button:right(),
			top = column_three_show_button:top(),
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = hide_string,
			clbk = column_three_show_func
		})

		local column_one_big_panel = self._panel:panel({
			name = "column_one_big_panel"
		})

		column_one_big_panel:set_w(character_panel:w())
		column_one_big_panel:set_x(character_panel:x())

		local column_two_big_panel = self._panel:panel({
			name = "column_two_big_panel"
		})

		column_two_big_panel:set_w(weapons_panel:w())
		column_two_big_panel:set_x(weapons_panel:x())

		local column_three_big_panel = self._panel:panel({
			name = "column_three_big_panel"
		})

		column_three_big_panel:set_w(eqpt_skills_panel:w())
		column_three_big_panel:set_x(eqpt_skills_panel:x())

		self._change_alpha_table = {
			{
				panel = column_one_big_panel,
				button = column_one_show_button
			},
			{
				panel = column_two_big_panel,
				button = column_two_show_button
			},
			{
				panel = column_three_big_panel,
				button = column_three_show_button
			}
		}
	end

	self._multi_profile_item = MultiProfileItemGui:new(self._ws, self._panel)

	if #managers.infamy:get_unlocked_join_stingers() > 0 then
		local selected_join_stinger = managers.infamy:selected_join_stinger()
		self._select_join_stinger_button = self:create_text_button({
			right = column_one_box_panel:left() - 5,
			bottom = column_one_box_panel:bottom() - 0,
			font = tweak_data.menu.pd2_small_font,
			font_size = tweak_data.menu.pd2_small_font_size,
			text = managers.localization:to_upper_text("menu_join_stinger_title", {
				button = managers.localization:btn_macro(join_stinger_binding),
				join_stinger = managers.localization:text("menu_" .. tostring(selected_join_stinger) .. "_name")
			}),
			clbk = callback(self, self, "select_join_stinger"),
			binding = join_stinger_binding
		})
	end

	self:_round_everything()
	self:_update_selected_box(true)
	self:update_detection()
	self:_update_player_stats()
	self:_update_mod_boxes()

	local deployable_secondary = self._boxes_by_name.deployable_secondary

	if deployable_secondary then
		deployable_secondary.links = self._boxes_by_name.deployable.links
	end

	local outfit_player_style = self._boxes_by_name.outfit_player_style

	if outfit_player_style then
		outfit_player_style.links = self._boxes_by_name.armor.links
	end
end

function PlayerInventoryGui:update_detection()
	local detection_panel = self._info_panel_fr:child("detection_panel")

	if not alive(detection_panel) then
		return
	end

	local detection_ring_left = detection_panel:child("detection_left")
	local detection_ring_right = detection_panel:child("detection_right")
	local detection_ring_left2 = detection_panel:child("detection_left2")
	local detection_ring_right2 = detection_panel:child("detection_right2")
	local detection_value = detection_panel:child("detection_value")
	local value, maxed_reached, min_reached = managers.blackmarket:get_suspicion_offset_of_local(tweak_data.player.SUSPICION_OFFSET_LERP or 0.75)

	detection_value:set_text(math.round(value * 100))
	detection_ring_left:set_color(Color(0.5 + value * 0.5, 1, 1))
	detection_ring_right:set_color(Color(0.5 + value * 0.5, 1, 1))

	if maxed_reached then
		detection_value:set_color(Color(255, 255, 42, 0) / 255)
	elseif min_reached then
		detection_value:set_color(tweak_data.screen_colors.text)
	else
		detection_value:set_color(tweak_data.screen_colors.text)
	end

	make_fine_text(detection_value)
	detection_value:set_center(detection_panel:w() / 2, detection_panel:h() / 2 + 2)
	detection_value:set_position(math.round(detection_value:x()), math.round(detection_value:y()))

	local value, maxed_reached, min_reached = managers.blackmarket:get_suspicion_offset_of_local(tweak_data.player.SUSPICION_OFFSET_LERP or 0.75, true)

	detection_ring_left2:set_color(Color(0.5 + value * 0.5, 1, 1))
	detection_ring_right2:set_color(Color(0.5 + value * 0.5, 1, 1))
end

function PlayerInventoryGui:setup_player_stats(panel)
	local y_pspe = FetchInfoPSPE("vanilla_resize")
	local CColorV, CColorV2, CColorV3, CColorV4 = FetchInfoPSPE("vanilla_colors_adjust")
	local data = {
		{
			name = "armor"
		},
		{
			name = "health"
		},
		{
			index = true,
			name = "concealment"
		},
		{
			procent = true,
			name = "movement"
		},
		{
			name = "dodge",
			procent = true,
			revert = true
		},
		{
			name = "crit",
			procent = true,
			revert = true
		},
		{
			name = "bonus_crit_dmg",
		},
		{
			name = "damage_shake"
		},
		{
			name = "stamina"
		}
	}
	local stats_panel = panel:child("stats_panel")

	if alive(stats_panel) then
		panel:remove(stats_panel)

		stats_panel = nil
	end

	stats_panel = panel:panel({
		name = "stats_panel"
	})
	local panel = stats_panel:panel({
		h = 20,
		layer = 1,
		w = stats_panel:w()
	})
	self._player_stats_shown = data
	self._player_stats_titles = {
		total = stats_panel:text({
			x = 135,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = CColorV or tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_total"))
		}),
		base = stats_panel:text({
			alpha = 0.75,
			x = 200,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = CColorV2 or tweak_data.screen_colors.text,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_base"))
		}),
		skill = stats_panel:text({
			alpha = 1,
			x = 261,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_small_font,
			color = CColorV3 or tweak_data.screen_colors.resource,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_skill"))
		})
	}
	local x = 0
	local y = 20
	local text_panel = nil
	local text_columns = {
		{
			size = 110,
			name = "name"
		},
		{
			size = 60,
			name = "total",
			align = "right",
			color = CColorV or tweak_data.screen_colors.text
		},
		{
			align = "right",
			name = "base",
			blend = "add",
			alpha = 0.75,
			size = 50,
			color = CColorV2 or tweak_data.screen_colors.text
		},
		{
			align = "right",
			name = "skill",
			blend = "add",
			alpha = 1,
			size = 60,
			color = CColorV3 or tweak_data.screen_colors.resource
		}
	}
	self._player_stats_texts = {}
	self._player_stats_panel = stats_panel:panel()

	for i, stat in ipairs(data) do
		panel = self._player_stats_panel:panel({
			name = "weapon_stats",
			h = 20,
			x = 0,
			layer = 3,
			y = y,
			w = self._player_stats_panel:w()
		})

		x = 2
		y = y + 20
		self._player_stats_texts[stat.name] = {}

		for _, column in ipairs(text_columns) do
			text_panel = panel:panel({
				layer = 0,
				x = x,
				w = column.size,
				h = panel:h()
			})
			self._player_stats_texts[stat.name][column.name] = text_panel:text({
				text = "0",
				layer = 4,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_small_font,
				align = column.align,
				alpha = column.alpha,
				blend_mode = column.blend,
				color = column.color or CColorV4 or tweak_data.screen_colors.text
			})
			x = x + column.size

			if column.name == "name" then
				self._player_stats_texts[stat.name].name:set_text(managers.localization:to_upper_text("bm_menu_" .. stat.name))
				_add_lines(text_panel, y_pspe, i)
			end
		end
	end
end

function PlayerInventoryGui:player_health_regen_stats(panel)
	local CColorh, CColorh2, CColorh3 = FetchInfoPSPE("heal")
	local y_pspe = FetchInfoPSPE("vanilla_resize")
	local data = {
		{
			name = "health_per_kill_stat"
		},

		{
			name = "health_per_second_stat"
		}
	}
	local stats_panel = panel:child("stats_panel_add_hp")

	if alive(stats_panel) then
		panel:remove(stats_panel)

		stats_panel = nil
	end

	stats_panel = panel:panel({
		name = "stats_panel_add_hp"
	})
	local panel = stats_panel:panel({
		h = 20,
		layer = 1,
		w = stats_panel:w()
	})
	self._player_stats_shown_add_hp = data
	self._player_stats_titles = {
		total = stats_panel:text({
			x = 185,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = CColorh2 or Color(255, 119, 230, 174) / 255,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_health_per_kill"))
		}),
		skill = stats_panel:text({
			x = 250,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = CColorh or Color(255, 0, 158, 78) / 255,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_health_per_kill_percentage"))
		})
	}
	local x = 0
	local y = 20
	local text_panel = nil
	local text_columns = {
		{
			size = 135,
			name = "name",
			color = CColorh3 or Color(255, 0, 199, 80) / 255
		},
		{
			size = 70,
			name = "health_per_kill",
			align = "right"
		},
		{
			size = 95,
			name = "health_per_kill_percentage",
			align = "right"
		}
	}
	self._player_stats_texts_add_hp = {}
	self._player_health_regen_stats = stats_panel:panel()

	for i, stat in ipairs(data) do
		panel = self._player_health_regen_stats:panel({
			name = "weapon_stats_add",
			h = 20,
			x = 0,
			layer = 1,
			y = y,
			w = self._player_health_regen_stats:w()
		})

		x = 2
		y = y + 20
		self._player_stats_texts_add_hp[stat.name] = {}

		for _, column in ipairs(text_columns) do
			text_panel = panel:panel({
				layer = 0,
				x = x,
				w = column.size,
				h = panel:h()
			})
			self._player_stats_texts_add_hp[stat.name][column.name] = text_panel:text({
				text = "0",
				layer = 1,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_medium_font,
				align = column.align,
				alpha = column.alpha,
				blend_mode = column.blend,
				color = column.color or tweak_data.screen_colors.text
			})
			x = x + column.size

			if column.name == "name" then
				self._player_stats_texts_add_hp[stat.name].name:set_text(managers.localization:text("bm_menu_" .. stat.name))
				if stat.name == "health_per_kill_stat" or stat.name == "health_per_second_stat" then
					_add_lines(text_panel, y_pspe, i)
				end
			end
		end
	end
end

function PlayerInventoryGui:setup_add_player_stats(panel)
	local CColor, CColor2, CColor3 = FetchInfoPSPE("dmg_red")
	local y_pspe = FetchInfoPSPE("vanilla_resize")
	local data = {
		{
			name = "dmg_red"
		},

		{
			name = "arrow"
		},

		{
			name = "dmg_red_ds_example"
		}
	}
	local stats_panel = panel:child("stats_panel_add")

	if alive(stats_panel) then
		panel:remove(stats_panel)

		stats_panel = nil
	end

	stats_panel = panel:panel({
		name = "stats_panel_add"
	})
	local panel = stats_panel:panel({
		h = 20,
		layer = 1,
		w = stats_panel:w()
	})
	self._player_stats_shown_add = data
	self._player_stats_titles = {
		total = stats_panel:text({
			x = 175,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color = CColor2 or Color(255, 255, 204, 153) / 255,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_reduction_basic"))
		}),
		skill = stats_panel:text({
			x = 240,
			layer = 2,
			font_size = tweak_data.menu.pd2_small_font_size,
			font = tweak_data.menu.pd2_medium_font,
			color =  CColor or Color(255, 255, 94, 0) / 255,
			text = utf8.to_upper(managers.localization:text("bm_menu_stats_reduction_highest"))
		})
	}
	local x = 0
	local y = 20
	local text_panel = nil
	local text_columns = {
		{
			size = 135,
			name = "name",
			color = CColor3 or Color(255, 255, 128, 0) / 255
		},
		{
			size = 70,
			name = "reduction_basic",
			align = "right"
		},
		{
			align = "right",
			name = "reduction_highest",
			size = 95
		}
	}
	self._player_stats_texts_add = {}
	self._additional_player_stats = stats_panel:panel()

	for i, stat in ipairs(data) do
		panel = self._additional_player_stats:panel({
			name = "weapon_stats_add",
			h = 20,
			x = 0,
			layer = 1,
			y = y,
			w = self._additional_player_stats:w()
		})

		x = 2
		y = y + 20
		self._player_stats_texts_add[stat.name] = {}

		for _, column in ipairs(text_columns) do
			text_panel = panel:panel({
				layer = 0,
				x = x,
				w = column.size,
				h = panel:h()
			})
			self._player_stats_texts_add[stat.name][column.name] = text_panel:text({
				text = "0",
				layer = 1,
				font_size = tweak_data.menu.pd2_small_font_size,
				font = tweak_data.menu.pd2_medium_font,
				align = column.align,
				alpha = column.alpha,
				blend_mode = column.blend,
				color = column.color or tweak_data.screen_colors.text
			})
			x = x + column.size

			if column.name == "name" then
				self._player_stats_texts_add[stat.name].name:set_text(managers.localization:to_upper_text("bm_menu_" .. stat.name))
				if stat.name == "dmg_red" or stat.name == "dmg_red_ds_example" then
					_add_lines(text_panel, y_pspe, i)
				end
				if stat.name == "arrow" then
					self._player_stats_texts_add[stat.name][column.name]:set_text("")
				end
			end
		end
	end
end

local _get_armor_stats = PlayerInventoryGui._get_armor_stats
function PlayerInventoryGui:_get_armor_stats(name)
	local bm_armor_tweak = tweak_data.blackmarket.armors[name]
	local upgrade_level = bm_armor_tweak.upgrade_level
	local base_stats, mods_stats, skill_stats = _get_armor_stats(self, name)
	local bonus_health = CatLib:fetch_henchmans_upgrades("crew_healthy", "crew_add_health", 0) * 10
	local bonus_stamina = CatLib:fetch_henchmans_upgrades("crew_motivated", "crew_add_stamina", 0)
	local bonus_dodge = CatLib:fetch_henchmans_upgrades("crew_evasive", "crew_add_dodge", 0) * 100
	local bonus_armor = CatLib:fetch_henchmans_upgrades("crew_sturdy", "crew_add_armor", 0) * 10
	local bonus_stealth = CatLib:fetch_henchmans_upgrades("crew_quiet", "crew_add_concealment", 0)

	for i, stat in ipairs(self._stats_shown) do
		if stat.name == "armor" then
			skill_stats.armor.value = skill_stats.armor.value + bonus_armor
		elseif stat.name == "health" then
			skill_stats.health.value = skill_stats.health.value + bonus_health
		elseif stat.name == "concealment" then
			skill_stats.concealment.value = skill_stats.concealment.value + bonus_stealth
		elseif stat.name == "stamina" then
			skill_stats.stamina.value = skill_stats.stamina.value + bonus_stamina
		elseif stat.name == "dodge" then
			skill_stats.dodge.value = skill_stats.dodge.value + bonus_dodge
		elseif stat.name == "bonus_crit_dmg" then
            local base = tweak_data.upgrades.critical_base_damage * 100
            local mod = managers.player:get_bonus_critical_damage() * 100
            base_stats[stat.name] = {
                value = base
            }
            skill_stats[stat.name] = {
                value = mod + (managers.player:upgrade_value("player", "critical_damage_card", 0) * 100)
            }
		elseif stat.name == "damage_shake" then
			skill_stats.damage_shake.value = 0
			local base = managers.player:body_armor_value("defense", upgrade_level, nil, 1)
            local mod = managers.player:bonus_damage_defense()
			base_stats[stat.name] = {
                value = base + managers.player:get_PlayerPerLevelStats().defense
            }
			skill_stats[stat.name] = {
                value = mod
            }
		end
		skill_stats[stat.name].skill_in_effect = skill_stats[stat.name].skill_in_effect or skill_stats[stat.name].value ~= 0
	end

	if managers.player:has_category_upgrade("player", "health_decrease") and bonus_health > 0 then
		local health_multiplier = managers.player:health_skill_multiplier()
		local health_decrease = managers.player:upgrade_value("player", "health_decrease", 1)
		local max_health = (PlayerDamage._HEALTH_INIT + managers.player:health_skill_addend()) * health_multiplier * 2

		local fake_health_to_armor = max_health * health_decrease * managers.player:upgrade_value("player", "armor_increase", 1)
		local fake_health = max_health * health_decrease

		local real_health = (max_health + bonus_health) * health_decrease
		local real_health_to_armor = real_health * managers.player:upgrade_value("player", "armor_increase", 1)

		local diff_health = math.abs(fake_health - real_health) * 3
		local diff_armor = math.abs(fake_health_to_armor - real_health_to_armor) * managers.player:upgrade_value("player", "armor_multiplier", 1)

		local bonus_armor_scenario = 0

		if bonus_armor > 0 then
			bonus_armor_scenario = bonus_armor_scenario + bonus_armor * managers.player:upgrade_value("player", "armor_multiplier", 1)
			diff_armor = diff_armor - bonus_armor
		end

		skill_stats.health.value = skill_stats.health.value - diff_health
		skill_stats.armor.value = skill_stats.armor.value + diff_armor + bonus_armor_scenario
	end

   if managers.player:has_category_upgrade("player", "bonus_from_crit") then
		local crit = managers.player:critical_hit_chance()
		local hp_from_crit = crit * managers.player:upgrade_value("player", "bonus_from_crit", 1) * 100
		local value = skill_stats.health.value + hp_from_crit
        local skill_in_effect = value ~= 0
		skill_stats.health.value = skill_stats.health.value + hp_from_crit
        skill_stats.health.skill_in_effect = skill_in_effect
	end

	return base_stats, mods_stats, skill_stats
end
