local schinese = Idstring("schinese"):key() == SystemInfo:language():key()

local function is_different_state(ignore_if_nil)
	local state = game_state_machine and game_state_machine:current_state_name()

	if ignore_if_nil and not state then
		return false
	end

	if state ~= "menu_main" then
		return true
	end

	return false
end

local function main_menu_loaded()
	local state = game_state_machine and game_state_machine:current_state_name()
	if state == "menu_main" then
		return true
	end

	return false
end

local function FOverhaul_data_loaded()
	if next(FOverhaul._data) then
		return true
	end

	return false
end

local function try_again_after_delay()
	DelayedCalls:Add("attempt____", 3, function()
		FOverhaul:EL()
	end)
end

function FOverhaul:EL()
	if not FOverhaul or not FOverhaul._data then
		try_again_after_delay()
		return
	end

	if is_different_state(true) then
		return
	end

	if not FOverhaul_data_loaded then
		try_again_after_delay()
		return
	end

	if FOverhaul._data.error_logger then
		if main_menu_loaded then
			FOverhaul:check_error_data_and_notify()
		else
			try_again_after_delay()
		end
	end

	if main_menu_loaded then
		FOverhaul:check_version()
	end
end

FOverhaul:EL()

FOverhaul.messages = {
	"OnPlayerDamageMelee",
	"OnEnemyShotChk",
	"OnLockPicked",
	"OnCriticalDamage"
}

for _, custom_message in pairs(FOverhaul.messages) do
	CatLib:AddNewMessage(custom_message)
end

function PlayerManager:remove_temporary_property(prop)
	return self._temporary_properties:remove_property(prop)
end

function PlayerManager:has_property(prop)
	return self._properties:has_property(prop)
end

function PlayerManager:attempt_ability(ability)
	if not self:player_unit() then
		return false
	end

	local local_peer_id = managers.network:session():local_peer():id()
	local has_no_grenades = self:get_grenade_amount(local_peer_id) == 0
	local is_downed = game_state_machine:verify_game_state(GameStateFilters.downed)
	local swan_song_active = managers.player:has_activate_temporary_upgrade("temporary", "berserker_damage_multiplier")
	local in_decay_state = self:get_property("decaying", false)
	is_downed = is_downed and not self:has_category_upgrade("player", "activate_ability_downed")

	if has_no_grenades or is_downed or swan_song_active or in_decay_state then
		return false
	end

	local attempt_func = self["_attempt_" .. ability]

	if attempt_func and not attempt_func(self) then
		return false
	end

	local tweak = tweak_data.blackmarket.projectiles[ability]

	if tweak and tweak.sounds and tweak.sounds.activate then
		self:player_unit():sound():play(tweak.sounds.activate)
	end

	self:add_grenade_amount(-1)
	self._message_system:notify("ability_activated", nil, ability)

	return true
end

function PlayerManager:chk_player_per_level_stats()
	self.PlayerStatsPerLevel = {}

	local per_level = {
		def = {50, 1, 3}
	}

	local player_level = managers.experience:current_level()
	local stat_def = 0
	for i = 1, player_level do
		if i <= per_level["def"][1] then
			stat_def = stat_def + per_level["def"][2]
		else
			stat_def = stat_def + per_level["def"][3]
		end
	end

	self.PlayerStatsPerLevel.defense = stat_def
end

function PlayerManager:get_PlayerPerLevelStats()
	return self.PlayerStatsPerLevel
end

function PlayerManager:defense_reduction(def)
	def = def * self:get_temporary_property("defense_reduction", 1)
	return def
end

function PlayerManager:combined_damage_defense()
	local def = 0

	def = def + self:body_armor_value("defense", nil, 1)
	def = def + self:bonus_damage_defense()
	def = def + self.PlayerStatsPerLevel.defense
	def = def + self:get_property("style_absorption", 0)

	def = def * self:get_property("defense_up_scav", 1)
	def = def * self:get_temporary_property("defense_up_scav_after", 1)

	if self:is_semi_wraith() then
		def = def * self:upgrade_value("player", "damage_defense_multiplier", 1)
	end

	def = self:defense_reduction(def)

	return def
end

function PlayerManager:bonus_damage_defense()
	local bonus_def = 0

	return bonus_def
end

function PlayerManager:extend_temporary_upgrade(category, upgrade, time)
	local upgrade_value = self:upgrade_value(category, upgrade)

	if upgrade_value == 0 then
		return
	end

	if not self._temporary_upgrades[category] then
		return
	end

	if not self._temporary_upgrades[category][upgrade] then
		return
	end

	local orig_expire_time = self._temporary_upgrades[category][upgrade].expire_time

	self._temporary_upgrades[category][upgrade] = {
		expire_time = orig_expire_time + time
	}
end

function PlayerManager:cal_conditional_damage_reduction()
	local multiplier = 1
	local upgrades = {
		underdog_value = CatLib:check_upgrade_value("temporary", "dmg_dampener_outnumbered", 1, 1),
		close_contact_value = CatLib:check_upgrade_value("temporary", "dmg_dampener_close_contact", 1, 1),
		out_num_infi = CatLib:check_upgrade_value("temporary", "dmg_dampener_outnumbered_strong", 1, 1),
		bite_the_bullet = CatLib:check_upgrade_value("player", "bite_the_bullet_base", "damage_reduction", 1),
		revive_dmg_red = CatLib:check_upgrade_value("player", "dmg_red_revived", "reduction", 1),
	}

	for i, upg in pairs(upgrades) do
		multiplier = multiplier * upg
	end

	if managers.player:has_category_upgrade("player", "passive_damage_reduction") then
		multiplier = multiplier * (managers.player:team_upgrade_value("damage_dampener", "team_damage_reduction") - (1 - managers.player:team_upgrade_value("damage_dampener", "team_damage_reduction")))
	end

	return multiplier
end

function PlayerManager:damage_reduction_skill_multiplier_hehe()
	local multiplier = 1
	multiplier = multiplier * self:upgrade_value("player", "damage_dampener", 1)
	multiplier = multiplier * self:upgrade_value("player", "health_damage_reduction", 1)
	multiplier = multiplier * self._properties:get_property("revive_damage_reduction", 1)

	local dmg_red_mul = self:team_upgrade_value("damage_dampener", "team_damage_reduction", 1)

	multiplier = multiplier * dmg_red_mul

	return multiplier
end

function PlayerManager:fetch_health_regen_per_seconds_percentages()
	local pl_man_health_regen = self:health_regen()
	local health_regen_seconds_perc = 0

	health_regen_seconds_perc = health_regen_seconds_perc + CatLib:check_upgrade_value("player", "hostage_health_regen_addend", 1, 0)

	return (health_regen_seconds_perc + pl_man_health_regen) * 100
end

function PlayerManager:fetch_health_flat_regen_per_seconds()
	local health_regen_per_seconds = 0

	health_regen_per_seconds = health_regen_per_seconds + CatLib:fetch_henchmans_upgrades("crew_regen", "crew_health_regen", 0)

	return health_regen_per_seconds * 10
end

function PlayerManager:fetch_health_flat_regen_per_kill()
	local health_regen = 0

	health_regen = health_regen + self:upgrade_value("player", "headshot_regen_health_bonus", 0)
	health_regen = health_regen + self:upgrade_value("player", "armor_health_store_amount", 0)
	health_regen = health_regen + self:upgrade_value("player", "wild_health_amount", 0)
	health_regen = health_regen + self:upgrade_value("player", "pocket_ecm_heal_on_kill", 0)
	health_regen = health_regen + CatLib:check_upgrade_value("player", "tag_team_base", "kill_health_gain", 0)

	return health_regen * 10
end

function PlayerManager:fetch_health_regen_per_kill_percentages()
	local health_regen_percentage = 0
	health_regen_percentage = health_regen_percentage + CatLib:check_upgrade_value("temporary", "melee_life_leech", 1, 0)
	health_regen_percentage = health_regen_percentage + self:upgrade_value("player", "melee_kill_life_leech", 0)

	return health_regen_percentage * 100
end

function PlayerManager:_attempt_bleed_ability()
	local upgd_base = self:upgrade_value("player", "bleed_ability_base")
	local heal = upgd_base.heal

	if not self:has_category_upgrade("player", "bleed_ability_base") then
		return false
	end

	if next(CustomDOTManager:GetBleedingEnemies()) == nil then
		return false
	end

	CustomDOTManager:_bleed_ability(heal)

	return true
end

function PlayerManager:BleedHeal(heal)
	local character_damage = self:local_player():character_damage()
	local max_health = (PlayerDamage._HEALTH_INIT + self:health_skill_addend()) * self:health_skill_multiplier()

	if character_damage:get_real_health() + heal > max_health then
		self:ChkOverheal(heal)

		return
	end
	character_damage:restore_health(heal, true)
end

function PlayerManager:ChkOverheal(heal)
	local upgd_base = self:upgrade_value("player", "bleed_ability_base")
	local duration = upgd_base.overheal_duration
	local max_overheal = upgd_base.overheal
	local character_damage = self:local_player():character_damage()
	local max_health = (PlayerDamage._HEALTH_INIT + self:health_skill_addend()) * self:health_skill_multiplier()

	local current_health_plus_heal = character_damage:get_real_health() + heal
	local diff_overheal_current_health_plus_heal = math.abs(max_health - current_health_plus_heal)
	local overheal = math.min(max_health + diff_overheal_current_health_plus_heal, max_health * max_overheal)
	local cal_ovrheal_for_playerdamage = math.abs(max_health - overheal)

	self:activate_temporary_property("overheal_bleed", duration, cal_ovrheal_for_playerdamage)

	character_damage:set_health(overheal)
	managers.hud:activate_teammate_ability_radial(HUDManager.PLAYER_PANEL, duration)

	local text = "YOU GOT OVERHEALED AND GAINED ".. cal_ovrheal_for_playerdamage*10 .. " BONUS MAX HEALTH!"
	if schinese then
		text = "你受到了过量治疗并且获得了 ".. cal_ovrheal_for_playerdamage*10 .. " 点最大血上限加成！"
	end
	managers.hud:show_hint({
		time = 3,
		text = text
	})

	DelayedCalls:Add("reset_healthMax", duration, function()
		local cal_damage_taken = math.max(0, max_health - character_damage:get_real_health())
		character_damage:set_health(math.abs(max_health - cal_damage_taken))
		self:remove_temporary_property("overheal_bleed")
	end)
end

function PlayerManager:_attempt_archer_ability()
	local upgd = self:upgrade_value("player", "archer_reload")
	local duration = upgd.duration
	local bonus_rate_of_fire = upgd.bonus_rate_of_fire
	local pierce_thru = self:upgrade_value("player", "archer_pierce_through", 0)

	if not self:is_current_weapon_of_category("bow", "crossbow") then
		return false
	end

	if not self:has_category_upgrade("player", "archer_reload") then
		return false
	end

	self:activate_temporary_property("reload_archer", duration, bonus_rate_of_fire)
	self:activate_temporary_property("archer_pierce_through_arrows", duration, pierce_thru)
	managers.hud:activate_teammate_ability_radial(HUDManager.PLAYER_PANEL, duration)
	self:archer_reload()
	self._kills_archer = 0

	return true
end

function PlayerManager:archer_reload()
	if not self:player_unit() then
		return
	end

	local secondary_unit = self:player_unit():inventory():unit_by_selection(1)
	local secondary_base = alive(secondary_unit) and secondary_unit:base()
	local primary_unit = self:player_unit():inventory():unit_by_selection(2)
	local primary_base = alive(primary_unit) and primary_unit:base()
	local can_reload_primary = primary_base and primary_base.can_reload and primary_base:can_reload() and primary_unit:base():is_category("bow", "crossbow")
	local can_reload_secondary = secondary_base and secondary_base.can_reload and secondary_base:can_reload() and secondary_unit:base():is_category("bow", "crossbow")

	local function reload()
		if not self:has_active_temporary_property("reload_archer") then
			self:unregister_message(Message.OnWeaponFired, "archer_reload")
			return
		end

		if can_reload_primary then
			primary_base:on_reload()
			managers.statistics:reloaded()
			managers.hud:set_ammo_amount(primary_base:selection_index(), primary_base:ammo_info())
		end

		if can_reload_secondary then
			secondary_base:on_reload()
			managers.statistics:reloaded()
			managers.hud:set_ammo_amount(secondary_base:selection_index(), secondary_base:ammo_info())
		end
	end

	self:register_message(Message.OnWeaponFired, "archer_reload", reload)
end

function PlayerManager:_attempt_quick_fix_stymulant()
	local upgd = self:upgrade_value("player", "quick_fix")
	local duration = upgd.duration
	local delay = upgd.delay
	local movement_speed = upgd.movement_speed
	local ms_duration = upgd.ms_duration
	local inv_duration = upgd.inv_duration
	local inc_based_on_missing_health = 0

	if not self:has_category_upgrade("player","quick_fix") then
		return false
	end

	if self:has_active_temporary_property("quick_fix_healing_period") or self:has_active_temporary_property("qf_movement_speed") then
		return false
	end

	self:set_property("kills", 0)

	self:activate_temporary_property("qf_movement_speed", ms_duration, movement_speed + self:upgrade_value("player", "qf_bonus_movement_speed", 0))
	self:quick_fix_inulnerable_init(inv_duration, inc_based_on_missing_health)
	self:quick_fix_stamina_regen_init(ms_duration)

	DelayedCalls:Add("qf_delay", delay, function()
		self:activate_temporary_property("quick_fix_healing_period", duration)
		managers.hud:activate_teammate_ability_radial(HUDManager.PLAYER_PANEL, duration)
	end)

	return true
end

function PlayerManager:_attempt_wraith_form()
	local upgd = self:upgrade_value("player", "wraith_form_base")
	local player_unit = self:player_unit()

	if not upgd then
		return false
	end

	if self._wraith_form then
		return false
	end

	if managers.groupai:state():whisper_mode() then
		local text = "YOU CANNOT UNLEASH WRAITH IN STEALTH!"
		managers.hud:show_hint({
			time = 3,
			text = text
		})
		return false
	end

	self._wraith_form = true
	self._wraith_energy = upgd.energy
	self._wraith_energy_max = upgd.energy
	self._wraith_energy_drain_table = upgd.energy_drain_table
	self._wraith_health_absorb = upgd.health_on_kill
	self._wraith_health_stored = 0
	managers.hud:set_wraith_energy_bar(1, self._wraith_energy)
	self:set_cop_attention("cloaked")

	managers.environment_controller:set_wraith_effect(true)
	FOverhaul:add_simple_updator(nil, "_upd_wraith_functions", self)
    player_unit:camera()._camera_unit:base():_wraith_form(true)
	return true
end

function PlayerManager:set_cop_attention(data, get_data_only)
	local preset = {}

	if data == "cloaked" then
		preset = {
			"pl_civilian"
		}
	end

	if data == "uncloak" then
		preset = {
			"pl_friend_combatant_cbt",
			"pl_friend_non_combatant_cbt",
			"pl_foe_combatant_cbt_stand",
			"pl_foe_non_combatant_cbt_stand"
		}
	end

	if get_data_only then
		return preset
	end

	self:player_unit():movement():set_attention_settings(preset, true)
	DelayedCalls:Add("plswakeup", 0.1, function()
		-- dumb solution to "wake up" cops
		self:player_unit():movement():current_state():_upd_attention()
	end)
end

function PlayerManager:_upd_wraith_functions()
	self:_mark_enemies_wraith()
	self:_passive_energy_drain_wraith()
end

function PlayerManager:_get_wraith_energy()
	return self._wraith_energy
end

function PlayerManager:_passive_energy_drain_wraith()
	local t = Application:time()
	local upgd = self:upgrade_value("player", "wraith_form_base")

	if not self:player_unit() then
		return
	end

	if not self._wraith_energy then
		return
	end

	if not self._drain_t then
		self._drain_t = t + upgd.passive_drain_interval
		return
	end

	if self._drain_t and t < self._drain_t then
		return
	end

	self:_wraith_energy_manager(upgd.passive_energy_drain)
	self._drain_t = t + upgd.passive_drain_interval
end

function PlayerManager:_wraith_energy_manager(change)
	if not self._wraith_energy then
		return
	end

	self._wraith_energy = math.clamp(self._wraith_energy + change, 0, self._wraith_energy_max)

	managers.hud:set_wraith_energy_bar(self._wraith_energy/self._wraith_energy_max, self._wraith_energy)

	if self._wraith_energy <= 0 then
		self:_wraith_form_end()
	end
end

function PlayerManager:_drain_wraith_energy(health, check)
	if not self._wraith_energy_drain_table then
		return
	end

	local step = math.clamp(math.floor(health/100), 1, #self._wraith_energy_drain_table)
	local drain = self._wraith_energy_drain_table[step]

	if check then
		return drain
	end

	self:_wraith_energy_manager(-drain)
end

function PlayerManager:_check_wraith_attack(unit_health)
	if not self._wraith_energy then
		return false
	end

	local energy_cost = self:_drain_wraith_energy(unit_health, true)
	if energy_cost > self._wraith_energy then
		local text = "YOUR ENERGY IS TOO LOW!"
		managers.hud:show_hint({
			time = 2,
			text = text
		})
		return false
	end

	return true
end

function PlayerManager:_wraith_form_end()
	local player_unit = self:player_unit()

	self:set_cop_attention("uncloak")
	FOverhaul:remove_updates("_upd_wraith_functions")
	managers.environment_controller:set_wraith_effect(false)
	player_unit:camera()._camera_unit:base():_wraith_form(false)
	self._wraith_form = false
	self:replenish_grenades(60)
	self:_absorb_health_wraith(true)
end

function PlayerManager:_mark_enemies_wraith()
	if not self:player_unit() then
		return
	end

	local upgd = self:upgrade_value("player", "wraith_form_base")
	local range = upgd.mark_range
	local slotmask = managers.slot:get_mask("enemies")
	local enemy_units = World:find_units_quick("sphere", self:player_unit():movement():m_pos(), range, slotmask)
	for _, unit in pairs(enemy_units) do
		if unit then
			if unit:character_damage() and not unit:character_damage():dead() then
				unit:contour():add("mark_enemy", nil, 0.5, Color(0.8, 0, 0.1))
			end
		end
	end
end

function PlayerManager:_wraith_attack()
    local player_unit = self:player_unit()
	local upgd = self:upgrade_value("player", "wraith_form_base")
	local t = Application:time()

    if not player_unit then
        return
    end

	if self._cooldown_attack and t < self._cooldown_attack then
		return
	end

    local range = upgd.wraith_attack_range
    local damage = 0
    local slotmask = managers.slot:get_mask("enemies")
	local camera = player_unit:camera()
	local from = camera:position()
	local distance = range
	local to = from + (camera:forward() * distance)
	local ignore_units = {player_unit}

    local ray_hit = World:raycast("ray", from, to, "slot_mask", slotmask, "ignore_unit", ignore_units)
    if ray_hit and alive(ray_hit.unit) and ray_hit.unit:character_damage() and not ray_hit.unit:character_damage():dead() then
    	local max_health = ray_hit.unit:character_damage():get_enemy_max_health(ray_hit.unit)
		if self:_check_wraith_attack(max_health) then
			damage = max_health
        	ray_hit.unit:character_damage():damage_simple({
        	    variant = "wraith",
        	    damage = damage,
        	    attacker_unit = player_unit,
        	    pos = Vector3(0, 0, 0),
        	    attack_dir = Vector3(0, 0, 0)
        	})
			ray_hit.unit:contour():remove("mark_enemy")
			self:_drain_wraith_energy(max_health)
			self:_do_cam_effect()
			self:_absorb_health_wraith()
			self:_activate_bonus_ms_wraith()
			self._cooldown_attack = t + 0.3
		end
	end
end

function PlayerManager:_activate_bonus_ms_wraith()
	local base = self:upgrade_value("player", "wraith_form_base")
	self:activate_temporary_property("wraith_bonus_ms", base.bonus_ms_duration, base.bonus_ms_on_kill)
end

function PlayerManager:_absorb_health_wraith(absorb_health)
	if not self._wraith_health_stored or not self._wraith_health_absorb then
		return
	end

	if not self:player_unit() then
		return
	end

	if self:player_unit():character_damage():is_downed() then
		return
	end

	if absorb_health then
		local pdamage = self:player_unit():character_damage()
		local max_health = (PlayerDamage._HEALTH_INIT + self:health_skill_addend()) * self:health_skill_multiplier()
		local current_health = pdamage:get_real_health()
		local health_increase = max_health * self._wraith_health_stored
		pdamage:set_health(current_health + health_increase)

		return
	end

	self._wraith_health_stored = self._wraith_health_stored + self._wraith_health_absorb
end

function PlayerManager:_do_cam_effect()
    local player_unit = self:player_unit()
    local normal_fov = player_unit:camera()._camera_object:fov()
    local target_fov = normal_fov - 30
    local duration = 0.2

    if not player_unit or not normal_fov then
        return
    end

	player_unit:camera()._camera_unit:base():animate_fov(target_fov, 0.25)
	managers.environment_controller:set_wraith_attack_effect(true, "set_blurzone", player_unit:key(), 1, player_unit:position(), 1000, 0, true)

    DelayedCalls:Add("PlayerManager_FovRestore", duration, function()
		player_unit:camera()._camera_unit:base():animate_fov(normal_fov, 0.25)
		managers.environment_controller:set_wraith_attack_effect(false, "set_blurzone", player_unit:key(), 0)
    end)
end

function PlayerManager:semi_wraith_form()
	local t = Application:time()
	local base_upgrade = self:upgrade_value("player", "semi_wraith_base")
	local ext_duration = self:upgrade_value("player", "semi_wraith_bonus_duration", 0)
	local form_duration = base_upgrade.duration + ext_duration

	if self:is_semi_wraith() then
		return
	end

	if self._semi_wraith_t and t < self._semi_wraith_t then
		return
	end

	if self:player_unit():character_damage():is_downed() then
		return
	end

	self._semi_wraith_t = t + base_upgrade.cooldown + form_duration
	self._semi_wraith_form = true
	managers.hud:activate_teammate_ability_radial(HUDManager.PLAYER_PANEL, form_duration)

	DelayedCalls:Add("semi_wraith_end", form_duration, function()
		self._semi_wraith_form = false
	end)
end

function PlayerManager:is_wraith()
	return self._wraith_form
end

function PlayerManager:is_semi_wraith()
	return self._wraith_form or self._semi_wraith_form
end

function PlayerManager:_attempt_bite_the_bullet_ability()
	local upgd = self:upgrade_value("player", "bite_the_bullet_base")
	local bonus_duration = self:upgrade_value("player", "bonus_duration_bite_the_bullet", 0)
	local duration = upgd.duration + bonus_duration
	local damage_reduction = upgd.damage_reduction
	local character_damage = self:local_player():character_damage()

	if not self:has_category_upgrade("player", "bite_the_bullet_base") then
		return false
	end

	if self:has_active_temporary_property("dmg_red_bite_the_bullet") or self:has_active_temporary_property("armor_to_health_bite_the_bullet") then
		return false
	end

	self:set_property("current_health_before_use", self:player_unit():character_damage():get_real_health())

	local health_before_use = self:get_property("current_health_before_use")
	local armor_to_health = self:convert_armor_bite_the_bullet()
	local set_max_health = self:convert_armor_to_health_bite_the_bullet()

	self._message_system:register(Message.OnPlayerDamage, "shockwave_init", callback(self, self, "_shockwave_init"))
	self._message_system:register(Message.OnPlayerDamageMelee, "shockwave_init_melee", callback(self, self, "_shockwave_init"))
	self:activate_temporary_property("dmg_red_bite_the_bullet", duration, damage_reduction)
	self:activate_temporary_property("armor_to_health_bite_the_bullet", duration, armor_to_health)
	self:set_property("stored_damage_to_heal", 0)
	self:bite_the_bullet_stored_health_init(health_before_use, set_max_health)
	self:shockwave_healing()

	character_damage:set_health(set_max_health)
	character_damage:set_armor(0)
	character_damage:block_armor_regen()

	DelayedCalls:Add("finish_bite_the_bullet", duration, function()
		local _set_max_health = self:_upd_real_health()
		local diff = math.abs(math.max(0,_set_max_health - self:player_unit():character_damage():get_real_health()))
		character_damage:enable_armor_regen_and_restore()
		character_damage:set_health(_set_max_health - diff)
		character_damage:restore_health(self:get_property("stored_damage_to_heal", 0), true)
		self._message_system:unregister(Message.OnPlayerDamage, "shockwave_init")
		self._message_system:unregister(Message.OnPlayerDamageMelee, "shockwave_init_melee")
		self:remove_property("current_health_before_use")
		self:send_last_shockwave_upg()
	end)

	return true
end

function PlayerManager:_upd_real_health()
	return self:convert_armor_to_health_bite_the_bullet()
end

function PlayerManager:shockwave_healing()
	if self:has_category_upgrade("player", "shockwave_healing") then
		local function shockwave_heal(unit, attack_data)
			local character_damage = self:local_player():character_damage()
			if attack_data.variant == "shockwave" and attack_data.attacker_unit == self:player_unit() then
				character_damage:restore_health(attack_data.damage * self:upgrade_value("player", "shockwave_healing", 0), true)
			end
		end
		self._message_system:register(Message.OnEnemyShot, "shockwave_heal", shockwave_heal)
	else
		self._message_system:unregister(Message.OnEnemyShot, "shockwave_heal")
	end
end

function PlayerManager:_shockwave_init(attack_data)
	local player_unit = self:player_unit()

	if not player_unit then
		return
	end

	local damage_taken = attack_data.raw_damage_btb
	local upgd = self:upgrade_value("player", "bite_the_bullet_base")
	local bonus_damage = self:upgrade_value("player", "shockwave_bonus_damage", 0)
	local range = upgd.shockwave_range
	local shockwave_damage = upgd.shockwave_dmg + bonus_damage
	local damage = damage_taken * shockwave_damage
	local slotmask = managers.slot:get_mask("enemies")
	local units = World:find_units_quick("sphere", player_unit:movement():m_pos(), range, slotmask)
	for e_key, unit in pairs(units) do
		if alive(unit) and unit:character_damage() and not unit:character_damage():dead() then
			unit:character_damage():damage_simple({
			variant = "shockwave",
			damage = damage,
			attacker_unit = player_unit,
			pos = Vector3(0, 0, 0),
			attack_dir = Vector3(0, 0, 0)
			})
		end
	end

	if self:has_category_upgrade("player", "bite_the_bullet_stored_damage") then
		local stored_damage = damage_taken * self:upgrade_value("player", "bite_the_bullet_stored_damage", 0)
		self:add_to_property("stored_damage_to_heal", stored_damage)
		self:bite_the_bullet_store_health_upd(self:get_property("stored_damage_to_heal", 0))
	end
end

function PlayerManager:quick_fix_inulnerable_init(inv_duration, inc_based_on_missing_health)
	if self:has_category_upgrade("player", "qf_invulnerable_inc") then
		local health_ratio = self:player_unit():character_damage():health_ratio()
		local multiplier = 1
		local per_step_seconds = self:upgrade_value("player", "qf_invulnerable_inc")
		local damage_health_ratio = 1 - CatLib:dec_round(health_ratio, 2)
		inc_based_on_missing_health = inc_based_on_missing_health + math.min(multiplier * per_step_seconds * damage_health_ratio, 1.5)
	end

	self:activate_temporary_property("qf_invulnerable_dur", inv_duration + inc_based_on_missing_health, 0)
	managers.hud:activate_teammate_immunity_radial(inv_duration + inc_based_on_missing_health)
end

function PlayerManager:quick_fix_stamina_regen_init(movement_duration)
	if self:has_category_upgrade("player", "qf_stamina_regen") then
		local base = self:upgrade_value("player", "qf_stamina_regen")
		local bonus = base.bonus
		local player_unit = self:player_unit()
		local recharge = player_unit:movement():_max_stamina() * bonus
		player_unit:movement():add_stamina(recharge)

		self:activate_temporary_property("qf_bonus_ms_duration_window", movement_duration)
	end
end

Hooks:PostHook(PlayerManager, "check_skills", "check_skills_foverhaul", function(self)
	self:chk_player_per_level_stats()

	if self:has_category_upgrade("player", "style_rank") then
		local function _on_enter_daredevil_stacking()
			if not self._coroutine_mgr:is_running("daredevil_stacking") then
				self:add_coroutine("daredevil_stacking", PlayerAction.Daredevil)
			end
		end
		self._message_system:register(Message.OnEnemyKilled, "daredevil_stacking", _on_enter_daredevil_stacking)
	else
		self._message_system:unregister(Message.OnEnemyKilled, "daredevil_stacking")
	end

	if self:has_category_upgrade("player", "swan_song_ace") then
		self._message_system:register(Message.OnEnemyKilled, "killed_while_decaying", callback(self, self, "_swan_song_aced_clbk"))
	else
		self._message_system:unregister(Message.OnEnemyKilled, "killed_while_decaying")
	end

	if self:has_category_upgrade("player", "archer_base") then
		local function _on_enter_archer_stacking()
			if not self._coroutine_mgr:is_running("archer_stacking") and self:is_current_weapon_of_category("bow", "crossbow") then
				self:add_coroutine("archer_stacking", PlayerAction.Archer)
			end
		end
		self._message_system:register(Message.OnEnemyShotChk, "archer_stacking", _on_enter_archer_stacking)
	else
		self._message_system:unregister(Message.OnEnemyShotChk, "archer_stacking")
	end

	if self:lifesteal() > 0 or self:has_category_upgrade("player", "style_lifesteal") then
		self._message_system:register(Message.OnEnemyShot, "lifesteal_healing", callback(self, self, "lifesteal_init"))
	else
		self._message_system:unregister(Message.OnEnemyShot, "lifesteal_healing")
	end

	if self:has_category_upgrade("player", "crit_heal") then
		self._block_update_occurs = nil
		self._message_system:register(Message.OnCriticalDamage, "low_profile_vamp", callback(self, self, "low_profile_vamp"))
	else
		self._message_system:unregister(Message.OnCriticalDamage, "low_profile_vamp")
	end

	if self:has_category_upgrade("player", "lockpicking_genius") or self:has_category_upgrade("player", "lockpicking_bonus_ms") then
		self._message_system:register(Message.OnLockPicked, "lockpicking_g", callback(self, self, "_lockpicking_g"))
	else
		self._message_system:unregister(Message.OnLockPicked, "lockpicking_g")
	end

	if self:has_category_upgrade("player", "armor_depleted_stagger_shot") then
		self._message_system:register(Message.OnWeaponFired, "stop_coroutine_dn", callback(self, self, "_stop_coroutine_dn"))
	else
		self._message_system:unregister(Message.OnWeaponFired, "stop_coroutine_dn")
	end
end)

function PlayerManager:_stop_coroutine_dn(weapon_unit)
	if weapon_unit ~= self:equipped_weapon_unit() then
		return
	end

	if not self._coroutine_mgr:is_running(PlayerAction.DireNeed) then
		return
	end

	self._coroutine_mgr:remove_coroutine(PlayerAction.DireNeed)

	-- for the sake of custom hud(s)
	self:send_message(Message.SetWeaponStagger, nil, false)
	self:send_message(Message.ResetStagger, nil)
end

function PlayerManager:_lockpicking_g()
	if managers.player:has_category_upgrade("player", "lockpicking_genius") and not managers.player:has_active_temporary_property("picked_a_lock") then
		managers.player:activate_temporary_property("picked_a_lock", managers.player:upgrade_value("player", "lockpicking_genius"))
	end

	managers.player:activate_temporary_property("lkp_bonus_ms", managers.player:upgrade_value("player", "lockpicking_bonus_ms")[2], managers.player:upgrade_value("player", "lockpicking_bonus_ms")[1])
end

function PlayerManager:_swan_song_aced_clbk()
	if self:get_property("decaying") then
		self:activate_temporary_property("pause_decay", 0.5)
		self:add_to_property("killed_decay", 1)
	end
end

Hooks:PostHook(PlayerManager, "init", "init______", function(self)
	self._converts = {}
	self.PlayerStatsPerLevel = {}
end)

function PlayerManager:add_converted_enemy(unit)
	table.insert(self._converts, unit)
end

function PlayerManager:remove_converted_enemy(unit_key)
	for i, enemy in pairs(self._converts) do
		if enemy:key() == unit_key then
			table.remove(self._converts, i)

			break
		end
	end
end

function PlayerManager:chk_wild_kill_counter(killed_unit, variant, attacker)
	local player_unit = self:player_unit()

	if not player_unit then
		return
	end

	if CopDamage.is_civilian(killed_unit:base()._tweak_table) then
		return
	end

	local damage_ext = player_unit:character_damage()

	if damage_ext and (managers.player:has_category_upgrade("player", "wild_health_amount") or managers.player:has_category_upgrade("player", "wild_armor_amount")) then
		self._wild_kill_triggers = self._wild_kill_triggers or {}
		local t = Application:time()

		while self._wild_kill_triggers[1] and self._wild_kill_triggers[1] <= t do
			table.remove(self._wild_kill_triggers, 1)
		end

		if tweak_data.upgrades.wild_max_triggers_per_time <= #self._wild_kill_triggers then
			return
		end

		local trigger_cooldown = tweak_data.upgrades.wild_trigger_time or 30
		local wild_health_amount = managers.player:upgrade_value("player", "wild_health_amount", 0)
		local wild_armor_amount = managers.player:upgrade_value("player", "wild_armor_amount", 0)
		local less_health_wild_armor = managers.player:upgrade_value("player", "less_health_wild_armor", 0)
		local less_armor_wild_health = managers.player:upgrade_value("player", "less_armor_wild_health", 0)
		local less_health_wild_cooldown = managers.player:upgrade_value("player", "less_health_wild_cooldown", 0)
		local less_armor_wild_cooldown = managers.player:upgrade_value("player", "less_armor_wild_cooldown", 0)
		local missing_health_ratio = math.clamp(1 - damage_ext:health_ratio(), 0, 1)
		local missing_armor_ratio = math.clamp(1 - damage_ext:armor_ratio(), 0, 1)
		local bonus_armor_restore = 1

		if less_health_wild_armor ~= 0 and less_health_wild_armor[1] ~= 0 then
			local missing_health_stacks = math.floor(missing_health_ratio / less_health_wild_armor[1])
			bonus_armor_restore = bonus_armor_restore + less_health_wild_armor[2] * missing_health_stacks
		end

		if less_armor_wild_health ~= 0 and less_armor_wild_health[1] ~= 0 then
			local missing_armor_stacks = math.floor(missing_armor_ratio / less_armor_wild_health[1])
			wild_health_amount = wild_health_amount + less_armor_wild_health[2] * missing_armor_stacks
		end

		local armor_restore_total = (damage_ext:_max_armor() * wild_armor_amount) * bonus_armor_restore

		if attacker ~= self:player_unit() then
			armor_restore_total = armor_restore_total * 0.5
			wild_health_amount = wild_health_amount * 0.5
		end

		damage_ext:restore_health(wild_health_amount, true, false)
		damage_ext:restore_armor(armor_restore_total)

		if less_health_wild_cooldown ~= 0 and less_health_wild_cooldown[1] ~= 0 then
			local missing_health_stacks = math.floor(missing_health_ratio / less_health_wild_cooldown[1])
			trigger_cooldown = trigger_cooldown - less_health_wild_cooldown[2] * missing_health_stacks
		end

		if less_armor_wild_cooldown ~= 0 and less_armor_wild_cooldown[1] ~= 0 then
			local missing_armor_stacks = math.floor(missing_armor_ratio / less_armor_wild_cooldown[1])
			trigger_cooldown = trigger_cooldown - less_armor_wild_cooldown[2] * missing_armor_stacks
		end

		local trigger_time = t + math.max(trigger_cooldown, 0)
		local insert_index = #self._wild_kill_triggers

		while insert_index > 0 and trigger_time < self._wild_kill_triggers[insert_index] do
			insert_index = insert_index - 1
		end

		table.insert(self._wild_kill_triggers, insert_index + 1, trigger_time)
	end
end

function PlayerManager:_is_biker_maxed_out()
	if not self:has_category_upgrade("player", "wild_maxed_damage_reduction") then
		return false
	end

	if self._wild_kill_triggers then
		if tweak_data.upgrades.wild_max_triggers_per_time <= #self._wild_kill_triggers then
			return true
		end
	end

	return false
end

function PlayerManager:get_bonus_critical_damage()
	local addend = 0

	addend = addend + self:upgrade_value("player", "bonus_critical_damage", 0)

	return addend
end

function PlayerManager:lifesteal()
	local lifesteal = 0

	lifesteal = lifesteal + self:get_property("lifesteal_style", 0)

	return lifesteal
end

function PlayerManager:lifesteal_init(unit, attack_data)
	if self:lifesteal() <= 0 then
		return
	end

	if not self:player_unit() then
		return
	end

	if attack_data.attacker_unit ~= self:player_unit() then
		return
	end

	if attack_data.variant ~= "bullet" then
		return
	end

	if not attack_data.base_damage then
		return
	end

	if self:get_property("decaying") then
		return
	end

	if self:player_unit():character_damage():is_downed() then
		return
	end

	local base_damage = attack_data.base_damage
	local healing_reduction = self:upgrade_value("player", "healing_reduction", 1)
	local omni_increase = 1
	local style_rank = self:get_property("style_killing_spree", 0)

	if style_rank == 6 then
		omni_increase = self:upgrade_value("player", "style_lifesteal_increase", 1)
	end

	local heal = base_damage * self:lifesteal() * healing_reduction * omni_increase
	local cap = self:player_unit():character_damage():_max_health() * tweak_data.upgrades.lifesteal_damage_threshold * omni_increase
	local restore_health = math.min(heal, cap)

	self:player_unit():character_damage():change_health(restore_health)
end

function PlayerManager:_upd_vamp_occur()
	self._block_update_occurs = nil
	self._heal_occured = nil
end

function PlayerManager:low_profile_vamp(unit, attack_data)
	local percent_heal = managers.player:upgrade_value("player", "crit_heal").heal
	local heal_cap = managers.player:upgrade_value("player", "crit_heal").cap
	local occurrences_cap = managers.player:upgrade_value("player", "crit_heal").occurs

	if not self._heal_occured or (self._heal_occured or 0) < occurrences_cap then
		self._heal_occured = (self._heal_occured or 0) + 1
		self:player_unit():character_damage():restore_health(math.min(attack_data.damage * percent_heal, heal_cap), true)
	end

	FOverhaul:add_simple_updator(2, "_upd_vamp_occur", self)
end

function PlayerManager:low_profile_immunity_proc()
	if self:has_category_upgrade("player", "execution_immunity") then
		local upgrade_data = self:upgrade_value("player", "execution_immunity")
		local immunity_duration = upgrade_data.immunity_duration
		local reduction =  upgrade_data.reduction
		self:activate_temporary_property("execution_immunity_proc", immunity_duration, reduction)
		managers.hud:activate_teammate_immunity_radial(immunity_duration)
	end
end

function PlayerManager:convert_armor_bite_the_bullet()
	local armor_convert = CatLib:check_upgrade_value("player", "bite_the_bullet_base", "armor_convert" , 1)
	return self:player_unit():character_damage():_raw_max_armor() * armor_convert
end

function PlayerManager:convert_armor_to_health_bite_the_bullet()
	local armor_to_health = self:convert_armor_bite_the_bullet()
	local max_health = self:get_property("current_health_before_use", 0) + armor_to_health

	return max_health
end

function PlayerManager:bite_the_bullet_stored_health_init(health_before_use, max_health_after_use)
	if self:has_category_upgrade("player", "bite_the_bullet_stored_damage") then
		managers.hud:set_stored_health_max(health_before_use/max_health_after_use)
	end
end

function PlayerManager:bite_the_bullet_store_health_upd(damage_taken_stored)
	if self:has_category_upgrade("player", "bite_the_bullet_stored_damage") then
		local real_max_health = self:player_unit():character_damage():_raw_max_health()
		managers.hud:set_stored_health(damage_taken_stored/real_max_health)
	end
end

function PlayerManager:send_last_shockwave_upg()
	local player_unit = self:player_unit()

	if not self:has_category_upgrade("player", "last_shockwave_range") then
		return
	end

	if not player_unit then
		return
	end

	local tase_data = {
		damage = 0,
		variant = "heavy",
		pos = Vector3(0, 0, 0),
		attack_dir = Vector3(0, 0, 0),
		result = {
			variant = "melee",
			type = "taser_tased"
		}
	}

	local slotmask = managers.slot:get_mask("enemies")
	local units = World:find_units_quick("sphere", player_unit:movement():m_pos(), self:upgrade_value("player", "last_shockwave_range"), slotmask)
	for e_key, unit in pairs(units) do
		if alive(unit) and unit:character_damage() and not unit:character_damage():dead() then
			unit:character_damage():_call_listeners(tase_data)
		end
	end
end

local body_armor_regen_multiplier = PlayerManager.body_armor_regen_multiplier
function PlayerManager:body_armor_regen_multiplier(...)
	local multiplier = body_armor_regen_multiplier(self, ...)

	if self:has_category_upgrade("player", "recovery_speed_tier_infamy") then
		local data = self:upgrade_value("player", "recovery_speed_tier_infamy") or {}
        local player_rank = managers.experience:current_rank() or 0
		multiplier = multiplier * math.max((data[4] - (data[1] * math.floor(player_rank / data[2]))), data[3])
	end

	if self:_in_berserker_state() then
		multiplier = multiplier * 0.5
	end

	return multiplier
end

function PlayerManager:increase_bonuses_based_on_infamy(bonus)
	local addend = 0
	local bonus_movement_speed = self:upgrade_value("player", "tier_bonus_movement_speed", 0)
	local bonus_stamina = self:upgrade_value("player", "tier_bonus_stamina_multiplier", 0)

	if self:has_category_upgrade("player", "increase_ms_stamina_infamy") then
		local data = self:upgrade_value("player", "increase_ms_stamina_infamy") or {}
        local player_rank = managers.experience:current_rank() or 0
		addend = addend + math.min((data[4] + (data[1] * math.floor(player_rank / data[2]))), data[3])
	end

	if bonus == "bonus_movement_speed" then
		return bonus_movement_speed + addend
	end

	if bonus == "bonus_stamina" then
		return bonus_stamina + addend
	end
end

local movement_speed_multiplier = PlayerManager.movement_speed_multiplier
function PlayerManager:movement_speed_multiplier(...)
	local multiplier = movement_speed_multiplier(self, ...)

	multiplier = multiplier + self:get_property("style_rank_bonus_ms", 0)

	if self:has_category_upgrade("player", "increase_ms_stamina_infamy") then
		local bonus_ms_tier_bonus = self:increase_bonuses_based_on_infamy("bonus_movement_speed")
		multiplier = multiplier + (bonus_ms_tier_bonus - 1)
	end

	multiplier = multiplier + self:get_temporary_property("qf_movement_speed", 1) - 1
	multiplier = multiplier + self:get_temporary_property("taser_malfunction_bonus", 1) - 1
	multiplier = multiplier + self:get_temporary_property("lkp_bonus_ms", 1) - 1
	multiplier = multiplier - (1 - self:get_temporary_property("brief_slow_swansong", 1))

	if self:is_semi_wraith() then
		multiplier = multiplier + self:upgrade_value("player", "semi_wraith_bonus_ms", 1) - 1
	end

	multiplier = multiplier + self:get_temporary_property("wraith_bonus_ms", 1) - 1

	return multiplier
end

function PlayerManager:mod_movement_penalty(movement_penalty)
	local skill_mods = self:upgrade_value("player", "passive_armor_movement_penalty_multiplier", 1)
	skill_mods = skill_mods * self:upgrade_value("team", "crew_reduce_speed_penalty", 1) * self:upgrade_value("player", "armor_movement_penalty_reduction", 1)

	if skill_mods < 1 and movement_penalty < 1 then
		local penalty = 1 - movement_penalty
		penalty = penalty * skill_mods
		movement_penalty = 1 - penalty
	end

	return movement_penalty
end

function PlayerManager:_escape_taser_action()
	local player_unit = self:player_unit()

	if not self:has_active_temporary_property("taser_malfunction_bonus") then
		local bonus_ms = self:upgrade_value("player", "taser_malfunction").bonus_movement_speed
		local bonus_ms_duration = self:upgrade_value("player", "taser_malfunction").bonus_duration
		self:activate_temporary_property("taser_malfunction_bonus", bonus_ms_duration, bonus_ms)
	end

	if not player_unit then
		return
	end

	local tase_data = {
		damage = 0,
		variant = "heavy",
		pos = Vector3(0, 0, 0),
		attack_dir = Vector3(0, 0, 0),
		result = {
			variant = "melee",
			type = "taser_tased"
		}
	}

	local slotmask = managers.slot:get_mask("enemies")
	local units = World:find_units_quick("sphere", player_unit:movement():m_pos(), self:upgrade_value("player", "escape_taser_redirect"), slotmask)
	for e_key, unit in pairs(units) do
		if alive(unit) and unit:character_damage() and not unit:character_damage():dead() then
			unit:character_damage():_call_listeners(tase_data)
		end
	end
end

local health_skill_multiplier = PlayerManager.health_skill_multiplier
function PlayerManager:health_skill_multiplier(...)
	local multiplier = health_skill_multiplier(self, ...)

	multiplier = multiplier + self:upgrade_value("player", "bs_hp_gf", 1) - 1
	multiplier = multiplier + self:upgrade_value("player", "semi_wraith_bonus_max_hp", 1) - 1

	return multiplier
end

local critical_hit_chance = PlayerManager.critical_hit_chance
function PlayerManager:critical_hit_chance(...)
	local multiplier = critical_hit_chance(self, ...)

	multiplier = multiplier + self:upgrade_value("player", "passive_headshot_damage_multiplier", 0)
	multiplier = multiplier + self:upgrade_value("player", "bonus_crit_chanc", 0)
	multiplier = multiplier + self:upgrade_value("player", "bleed_critical_hit_chance", 0)

	return multiplier
end

function PlayerManager:_heal_converts(base_heal)
	for _, unit in pairs(self._converts) do
		unit:character_damage():restore_health_converts(base_heal * self:upgrade_value("player", "converts_restore_health"))
	end
end

function PlayerManager:health_regen()
	local health_regen = tweak_data.player.damage.HEALTH_REGEN
	health_regen = health_regen + self:temporary_upgrade_value("temporary", "wolverine_health_regen", 0)

	if self:num_local_minions() > 0 and self:has_category_upgrade("player", "hostage_health_regen_addend") then
		local leader_heal = CatLib:upgrade_value_based_on_current_health("player", "hostage_health_regen_addend", tweak_data.upgrades.health_medium, 0, nil, nil, "<", true, true)
		health_regen = health_regen + leader_heal
	end

	health_regen = health_regen + self:upgrade_value("player", "passive_health_regen", 0)

	return health_regen
end

function PlayerManager:converts_health_regen()
	if self:num_local_minions() > 0 then
		local leader_heal = CatLib:upgrade_value_based_on_current_health("player", "hostage_health_regen_addend", tweak_data.upgrades.health_medium, 0, nil, nil, "<", true, true)
		self:_heal_converts(leader_heal)
	end
end

local stamina_multiplier = PlayerManager.stamina_multiplier
function PlayerManager:stamina_multiplier(...)
	local multiplier = stamina_multiplier(self, ...)

	if self:has_category_upgrade("player", "increase_ms_stamina_infamy") then
		local bonus_stamina = self:increase_bonuses_based_on_infamy("bonus_stamina")
		multiplier = multiplier + bonus_stamina - 1
	end

	return multiplier
end

local damage_reduction_skill_multiplier = PlayerManager.damage_reduction_skill_multiplier
function PlayerManager:damage_reduction_skill_multiplier(...)
	local multiplier = damage_reduction_skill_multiplier(self, ...)

	multiplier = multiplier * self:get_temporary_property("dmg_red_bite_the_bullet", 1)
	multiplier = multiplier * self:get_temporary_property("qf_invulnerable_dur", 1)
	multiplier = multiplier * self:get_temporary_property("dmg_red_revived", 1)

	local current_state = self:get_current_state()

	if self:has_category_upgrade("player", "interacting_damage_reduction") and current_state and current_state:_interacting() then
		multiplier = multiplier * self:player_unit():character_damage():interacting_damage_reduction()
	end

	multiplier = multiplier * self:get_temporary_property("execution_immunity_proc", 1)
	multiplier = multiplier * self:get_temporary_property("brief_immunity_swansong", 1)

	local downed_red = self:get_property("dmg_red_downed", false)
	local decaying = self:get_property("decaying", false)
	local biker_maxed_out = self:_is_biker_maxed_out()

	if biker_maxed_out then
		multiplier = multiplier * self:upgrade_value("player", "wild_maxed_damage_reduction", 1)
	end

	if decaying or self:has_active_temporary_property("swansong_dmg_reduction_linger") then
		multiplier = multiplier * self:upgrade_value("player", "swan_song_basic").damage_reduction
	end

	if downed_red then
		return downed_red
	end

	return multiplier
end

local skill_dodge_chance = PlayerManager.skill_dodge_chance
function PlayerManager:skill_dodge_chance(...)
	local chance = skill_dodge_chance(self, ...)

	chance = chance + self:upgrade_value("player", "archer_dodge_bonus", 0)

	return chance
end

function PlayerManager:bleed_tick_healing(bleed_damage)
	if not managers.player:has_category_upgrade("player", "bleeding_vamp") then
		return
	end
	local dot_to_health = bleed_damage * managers.player:upgrade_value("player", "bleeding_vamp")
	self:player_unit():character_damage():restore_health(dot_to_health, true)
end

function PlayerManager:_on_enter_shock_and_awe_event()
	if not self._coroutine_mgr:is_running("automatic_faster_reload") then
		local equipped_unit = self:get_current_state()._equipped_unit
		local data = self:upgrade_value("player", "automatic_faster_reload", nil)
		local is_grenade_launcher = equipped_unit:base():is_category("grenade_launcher")

		if data and equipped_unit and not is_grenade_launcher and equipped_unit:base():is_category("flamethrower", "minigun", "smg", "lmg", "assault_rifle") then
			self._coroutine_mgr:add_and_run_coroutine("automatic_faster_reload", PlayerAction.ShockAndAwe, self, data.target_enemies, data.max_reload_increase, data.min_reload_increase, data.penalty, data.min_bullets, equipped_unit)
		end
	end
end

function PlayerManager:_remove_weapon_id_head_shot_ammo_return(weapon)
	if not self._ammo_refunded_to_weapons then
		return
	end

	for i, weapon_used in pairs(self._ammo_refunded_to_weapons) do
		if weapon_used:get_name_id() == weapon then
			table.remove(self._ammo_refunded_to_weapons, i)
		end
	end
end

function PlayerManager:chk_weapon_id_head_shot_ammo_return(weapon)
	if not self._ammo_refunded_to_weapons then
		return false
	end

	for i, weapon_used in pairs(self._ammo_refunded_to_weapons) do
		if weapon_used:get_name_id() == weapon then
			return true
		end
	end

	return false
end

function PlayerManager:chk_weapons_refunded()
	if not self._ammo_refunded_to_weapons then
		return false
	end

	return self._ammo_refunded_to_weapons
end

function PlayerManager:equipped_weapon_unit()
	local current_state = self:get_current_state()

	if current_state then
		local weapon_unit = current_state._equipped_unit:base()._unit

		return weapon_unit
	end

	return nil
end

function PlayerManager:on_headshot_dealt()
	local player_unit = self:player_unit()
	local weapon_unit = self:equipped_weapon_unit()

	if not player_unit then
		return
	end

	if self:has_category_upgrade("player", "archer_heal_on_headshot") and self:is_current_weapon_of_category("bow", "crossbow") and self:has_active_temporary_property("reload_archer") then
		local heal = CatLib:upgrade_value_based_on_current_health("player", "archer_heal_on_headshot", tweak_data.upgrades.low_hp, 0, nil, nil, "<")
		self:player_unit():character_damage():restore_health(heal, true)
	end

	if self:has_category_upgrade("player", "head_shot_ammo_return") and weapon_unit and weapon_unit:base():fire_mode() == "single" and weapon_unit:base():is_category("smg", "assault_rifle", "snp") then
		self._headshot_required = self:upgrade_value("player", "head_shot_ammo_return").headshots
		self._ammo_refunded_to_weapons = (self._ammo_refunded_to_weapons or {})
		self._headshots_scored = (self._headshots_scored or 0) + 1

		if self._headshots_scored == self._headshot_required then
			local ammo = self:upgrade_value("player", "head_shot_ammo_return").ammo
			self:on_ammo_increase(ammo)
			self._headshots_scored = 0

			if self:has_category_upgrade("player", "head_shot_ammo_return_bonus_damage") then
				table.insert(self._ammo_refunded_to_weapons, weapon_unit:base())
				if FOverhaul._data.ammo_return_vfx then
					managers.hud:activate_bullet_efficiency_vfx()
				end
			end
		end
	end

	self._message_system:notify(Message.OnHeadShot, nil, nil)

	local t = Application:time()

	if self._on_headshot_dealt_t and t < self._on_headshot_dealt_t then
		return
	end

	self._on_headshot_dealt_t = t + (tweak_data.upgrades.on_headshot_dealt_cooldown or 0)
	local damage_ext = player_unit:character_damage()
	local regen_armor_bonus = managers.player:upgrade_value("player", "headshot_regen_armor_bonus", 0)

	if damage_ext and regen_armor_bonus > 0 then
		damage_ext:restore_armor(regen_armor_bonus)
	end

	local regen_health_bonus = managers.player:upgrade_value("player", "headshot_regen_health_bonus", 0)

	if damage_ext and regen_health_bonus > 0 then
		damage_ext:restore_health(regen_health_bonus, true)
	end

	if self:has_category_upgrade("player", "headshot_armor_speed_up") then
		local armor_speed_up = self:upgrade_value("player", "headshot_armor_speed_up")
		damage_ext:_shorten_armor_recovery_timer(armor_speed_up)
	end
end

function PlayerManager:_on_enter_ammo_efficiency_event()
	return
end

function PlayerManager:_in_berserker_state()
	local berserker_state = false

	local berserker_enters = {
		frenzy = managers.player:has_category_upgrade("player", "health_damage_reduction"),
		yakuza = managers.player:has_category_upgrade("player", "armor_regen_damage_health_ratio_multiplier")
	}

	for _, skill_active in pairs(berserker_enters) do
		if skill_active then
			berserker_state = true
		end
	end

	return berserker_state
end

function PlayerManager:archer_bolt_aoe(damage, enemy_unit)
	local player_unit = self:player_unit()

	if not self:is_current_weapon_of_category("bow", "crossbow") or not self:has_category_upgrade("player", "archer_bolt_aoe") or not player_unit then
		return
	end

	local upg = self:upgrade_value("player", "archer_bolt_aoe")
	local aoe_damage = damage * upg.damage
	local slotmask = managers.slot:get_mask("enemies")
	local units = World:find_units_quick("sphere", enemy_unit:movement():m_pos(), upg.radius, slotmask)
	for e_key, unit in pairs(units) do
		if alive(unit) and unit:character_damage() and not unit:character_damage():dead() then
			unit:character_damage():damage_simple({
				variant = "bolt_aoe",
				damage = aoe_damage,
				attacker_unit = player_unit,
				pos = Vector3(0, 0, 0),
				attack_dir = Vector3(0, 0, 0)
			})
		end
	end
end

function PlayerManager:on_killshot(killed_unit, variant, headshot, weapon_id)
	local player_unit = self:player_unit()

	if not player_unit then
		return
	end

	if CopDamage.is_civilian(killed_unit:base()._tweak_table) then
		return
	end

	local weapon_melee = weapon_id and tweak_data.blackmarket and tweak_data.blackmarket.melee_weapons and tweak_data.blackmarket.melee_weapons[weapon_id] and true

	if killed_unit:brain().surrendered and killed_unit:brain():surrendered() and (variant == "melee" or weapon_melee) then
		managers.custom_safehouse:award("daily_honorable")
	end

	managers.modifiers:run_func("OnPlayerManagerKillshot", player_unit, killed_unit:base()._tweak_table, variant)

	local equipped_unit = self:get_current_state()._equipped_unit
	self._num_kills = self._num_kills + 1

	if self._num_kills % self._SHOCK_AND_AWE_TARGET_KILLS == 0 and self:has_category_upgrade("player", "automatic_faster_reload") then
		self:_on_enter_shock_and_awe_event()
	end

	if self:has_category_upgrade("player", "semi_wraith_base") then
		self:semi_wraith_form()
	end

	local selection_index = equipped_unit and equipped_unit:base() and equipped_unit:base():selection_index() or 0
	local update_secondary_reload_primary = selection_index == 1 and self._has_secondary_reload_primary
	local update_primary_reload_secondary = selection_index == 2 and self._has_primary_reload_secondary
	local equipped_weapon_id = equipped_unit and equipped_unit:base() and equipped_unit:base():get_name_id()
	update_secondary_reload_primary = update_secondary_reload_primary and weapon_id == equipped_weapon_id
	update_primary_reload_secondary = update_primary_reload_secondary and weapon_id == equipped_weapon_id
	--- ### UP TO 236U ### ---
	if update_secondary_reload_primary then
		local kills_to_reload = self:upgrade_value("player", "secondary_reload_primary", 10)
		local secondary_kills = self:get_property("secondary_reload_primary_kills", 0) + 1

		if kills_to_reload <= secondary_kills then
			local primary_unit = player_unit:inventory():unit_by_selection(2)
			local primary_base = alive(primary_unit) and primary_unit:base()
			local can_reload = primary_base and primary_base.can_reload and primary_base:can_reload()

			if can_reload then
				primary_base:on_reload()
				managers.statistics:reloaded()
				managers.hud:set_ammo_amount(primary_base:selection_index(), primary_base:ammo_info())
				player_unit:sound():play("perkdeck_activate")

				if self._has_secondary_reload_swap_primary then
					local duration = self:upgrade_value("weapon", "secondary_reload_swap_primary", 3)

					self:activate_temporary_property("intant_swap_to_primary", duration, true)
				end
			end

			secondary_kills = 0
		end

		self:set_property("secondary_reload_primary_kills", secondary_kills)
	elseif update_primary_reload_secondary then
		local kills_to_reload = self:upgrade_value("player", "primary_reload_secondary", 10)
		local primary_kills = self:get_property("primary_reload_secondary_kills", 0) + 1

		if kills_to_reload <= primary_kills then
			local secondary_unit = player_unit:inventory():unit_by_selection(1)
			local secondary_base = alive(secondary_unit) and secondary_unit:base()
			local can_reload = secondary_base and secondary_base.can_reload and secondary_base:can_reload()

			if can_reload then
				secondary_base:on_reload()
				managers.statistics:reloaded()
				managers.hud:set_ammo_amount(secondary_base:selection_index(), secondary_base:ammo_info())
				player_unit:sound():play("perkdeck_activate")

				if self._has_primary_reload_swap_secondary then
					local duration = self:upgrade_value("weapon", "primary_reload_swap_secondary", 3)

					self:activate_temporary_property("intant_swap_to_secondary", duration, true)
				end
			end

			primary_kills = 0
		end

		self:set_property("primary_reload_secondary_kills", primary_kills)
	end

	self._message_system:notify(Message.OnEnemyKilled, nil, equipped_unit, variant, killed_unit)

	if self._saw_panic_when_kill and variant ~= "melee" then
		local equipped_unit = self:get_current_state()._equipped_unit:base()

		if equipped_unit:is_category("saw") then
			local pos = player_unit:position()
			local skill = self:upgrade_value("saw", "panic_when_kill")

			if skill and type(skill) ~= "number" then
				local area = skill.area
				local chance = skill.chance
				local amount = skill.amount
				local enemies = World:find_units_quick("sphere", pos, area, 12, 21)

				for i, unit in ipairs(enemies) do
					if unit:character_damage() then
						unit:character_damage():build_suppression(amount, chance)
					end
				end
			end
		end
	end

	local t = Application:time()
	local damage_ext = player_unit:character_damage()

	if self:has_category_upgrade("player", "style_rank") then
		managers.hud:StyleOnKillshot()
	end

	if self:has_category_upgrade("player", "kill_change_regenerate_speed") then
		local amount = self:body_armor_value("skill_kill_change_regenerate_speed", nil, 1)
		local multiplier = self:upgrade_value("player", "kill_change_regenerate_speed", 0)

		damage_ext:change_regenerate_speed(amount * multiplier, tweak_data.upgrades.kill_change_regenerate_speed_percentage)
	end

	local gain_throwable_per_kill = managers.player:upgrade_value("team", "crew_throwable_regen", 0)

	if gain_throwable_per_kill ~= 0 then
		self._throw_regen_kills = (self._throw_regen_kills or 0) + 1

		if gain_throwable_per_kill < self._throw_regen_kills then
			managers.player:add_grenade_amount(1, true)

			self._throw_regen_kills = 0
		end
	end

	if self:equipment_slot("armor_kit") and self:get_equipment_amount("armor_kit") < 3 then
		self._armor_bag_kills = (self._armor_bag_kills or 0) + 1

		if self._armor_bag_kills >= 30 then
			self:add_equipment_amount("armor_kit", 1, self:equipment_slot("armor_kit"))
			self._armor_bag_kills = 0
		end
	end

	if self:has_activate_temporary_upgrade("temporary", "copr_ability") then
		local kill_life_leech = self:upgrade_value_nil("player", "copr_kill_life_leech")
		local static_damage_ratio = self:upgrade_value_nil("player", "copr_static_damage_ratio")

		if kill_life_leech and static_damage_ratio and damage_ext then
			self._copr_kill_life_leech_num = (self._copr_kill_life_leech_num or 0) + 1

			if kill_life_leech <= self._copr_kill_life_leech_num then
				self._copr_kill_life_leech_num = 0
				local current_health_ratio = damage_ext:health_ratio()
				local wanted_health_ratio = math.floor((current_health_ratio + 0.01 + static_damage_ratio) / static_damage_ratio) * static_damage_ratio
				local health_regen = wanted_health_ratio - current_health_ratio

				if health_regen > 0 then
					damage_ext:restore_health(health_regen)
					damage_ext:on_copr_killshot()
				end
			end
		end
	end

	if self:has_category_upgrade("player", "qf_bonus_movement_speed") then
		if self:has_active_temporary_property("quick_fix_healing_period") then
			self:add_to_property("kills", 1)
			local kills = self:get_property("kills")

			if kills == 1 and self:has_active_temporary_property("qf_bonus_ms_duration_window") then
				self:add_to_temporary_property("qf_movement_speed", tweak_data.upgrades.qf_bonus_ms_duration, 0)
			end

			if kills <= 10 and self:has_category_upgrade("player", "qf_healing_period_extended") then
				local base_values = self:upgrade_value("player", "quick_fix")
				local extended = self:upgrade_value("player", "qf_healing_period_extended") * kills
				local function update_duration()
					CatLib:add_time_temporary_property("quick_fix_healing_period", self:upgrade_value("player", "qf_healing_period_extended"))
					local time_left = CatLib:get_time_from_temporary_property("quick_fix_healing_period")
					managers.hud:activate_teammate_ability_radial(HUDManager.PLAYER_PANEL, time_left, base_values.duration + extended)
				end
				update_duration()
			end
		end
	end

	if self:has_active_temporary_property("reload_archer") and self:is_current_weapon_of_category("bow", "crossbow") then
		local upg = self:upgrade_value("player", "archer_reload")
		local kills = upg.max_extend
		local extend = upg.extend

		local function update_duration()
			CatLib:add_time_temporary_property("reload_archer", extend)
			local time_left = CatLib:get_time_from_temporary_property("reload_archer")
			managers.hud:activate_teammate_ability_radial(HUDManager.PLAYER_PANEL, time_left, upg.duration + extend)
		end

		self._kills_archer = (self._kills_archer or 0) + 1
		if self._kills_archer <= kills then
			update_duration()
		end
	end

	if self:has_category_upgrade("player", "reduce_cooldown_bite_the_bullet") and not (self:has_active_temporary_property("dmg_red_bite_the_bullet") or self:has_active_temporary_property("armor_to_health_bite_the_bullet")) then
		local percentage = self:upgrade_value("player", "reduce_cooldown_bite_the_bullet")
		local time_remaining = self:get_timer_remaining("replenish_grenades") or 0
		if time_remaining > 0 then
			local reduction = time_remaining * percentage
			self:speed_up_grenade_cooldown(reduction)
		end
	end

	if self._on_killshot_t and t < self._on_killshot_t then
		return
	end

	local regen_armor_bonus = self:upgrade_value("player", "killshot_regen_armor_bonus", 0)
	local dist_sq = mvector3.distance_sq(player_unit:movement():m_pos(), killed_unit:movement():m_pos())
	local close_combat_sq = tweak_data.upgrades.close_combat_distance * tweak_data.upgrades.close_combat_distance

	if dist_sq <= close_combat_sq then
		regen_armor_bonus = regen_armor_bonus + self:upgrade_value("player", "killshot_close_regen_armor_bonus", 0)
		local panic_chance = self:upgrade_value("player", "killshot_close_panic_chance", 0)
		panic_chance = managers.modifiers:modify_value("PlayerManager:GetKillshotPanicChance", panic_chance)
		if panic_chance > 0 or panic_chance == -1 then
			local slotmask = managers.slot:get_mask("enemies")
			local units = World:find_units_quick("sphere", player_unit:movement():m_pos(), tweak_data.upgrades.killshot_close_panic_range, slotmask)
			for e_key, unit in pairs(units) do
				if alive(unit) and unit:character_damage() and not unit:character_damage():dead() then
					unit:character_damage():build_suppression(0, panic_chance)
				end
			end
		end
	end

	if self:has_category_upgrade("player", "killshot_close_panic_chance_mindbreaker") then
		local panic_chance = self:upgrade_value("player", "killshot_close_panic_chance_mindbreaker", 0)
		local slotmask = managers.slot:get_mask("enemies")
		local units = World:find_units_quick("sphere", killed_unit:movement():m_pos(), 800, slotmask)
		if math.random() <= panic_chance then
			for e_key, unit in pairs(units) do
				if alive(unit) and unit:character_damage() and not unit:character_damage():dead() then
					unit:character_damage():build_suppression("panic")
					local max_health_damage = self:upgrade_value("player", "mindbreaker_damage", 0)

					if max_health_damage ~= 0 then
						local enemy_max_health = unit:character_damage():get_enemy_max_health(unit)
						if enemy_max_health then
							unit:character_damage():damage_simple({
								variant = "mindbreaker_panic_damage",
								damage = enemy_max_health * max_health_damage,
								attacker_unit = player_unit,
								pos = Vector3(0, 0, 0),
								attack_dir = Vector3(0, 0, 0)
							})
						end
					end
				end
			end
		end
	end

	if damage_ext and regen_armor_bonus > 0 then
		damage_ext:restore_armor(regen_armor_bonus)
	end

	local regen_health_bonus = 0

	if variant == "melee" then
		regen_health_bonus = regen_health_bonus + self:upgrade_value("player", "melee_kill_life_leech", 0)
	end

	if damage_ext and regen_health_bonus > 0 then
		damage_ext:restore_health(regen_health_bonus)
	end

	self._on_killshot_t = t + (tweak_data.upgrades.on_killshot_cooldown or 0)

	if _G.IS_VR then
		local steelsight_multiplier = equipped_unit:base():enter_steelsight_speed_multiplier()
		local stamina_percentage = (steelsight_multiplier - 1) * tweak_data.vr.steelsight_stamina_regen
		local stamina_regen = player_unit:movement():_max_stamina() * stamina_percentage

		player_unit:movement():add_stamina(stamina_regen)
	end
end

function PlayerManager:reload_weapon_on_style_rank()
	local player_unit = self:player_unit()
	local equipped_unit = self:get_current_state()._equipped_unit
	local selection_index = equipped_unit and equipped_unit:base() and equipped_unit:base():selection_index() or 0
	local has_auto_reload_style = self:has_category_upgrade("player", "automatically_reload_style")
	local auto_reload_cooldown = self:upgrade_value("player", "automatically_reload_style")
	local has_cooldown = self:has_active_temporary_property("reload_style_cooldown")

	if has_cooldown then
		return
	end

	if not player_unit then
		return
	end

	if selection_index == 2 and has_auto_reload_style then
		local primary_unit = player_unit:inventory():unit_by_selection(2)
		local primary_base = alive(primary_unit) and primary_unit:base()
		local can_reload = primary_base and primary_base.can_reload and primary_base:can_reload()

		if can_reload then
			primary_base:on_reload()
			managers.statistics:reloaded()
			managers.hud:set_ammo_amount(primary_base:selection_index(), primary_base:ammo_info())
		end
	elseif selection_index == 1 and has_auto_reload_style then
		local secondary_unit = player_unit:inventory():unit_by_selection(1)
		local secondary_base = alive(secondary_unit) and secondary_unit:base()
		local can_reload = secondary_base and secondary_base.can_reload and secondary_base:can_reload()

		if can_reload then
			secondary_base:on_reload()
			managers.statistics:reloaded()
			managers.hud:set_ammo_amount(secondary_base:selection_index(), secondary_base:ammo_info())
		end
	end
	self:activate_temporary_property("reload_style_cooldown", auto_reload_cooldown)
end









-- Copy Pasted from Offyerrocker <3
local function get_as_digested(amount)
	local list = {}

	for i = 1, #amount do
		table.insert(list, Application:digest_value(amount[i], false))
	end

	return list
end

local function make_double_hud_string(a, b)
	return string.format("%01d|%01d", a, b)
end

local function add_hud_item(amount, icon)
	if #amount > 1 then
		managers.hud:add_item_from_string({
			amount_str = make_double_hud_string(amount[1], amount[2]),
			amount = amount,
			icon = icon
		})
	else
		managers.hud:add_item({
			amount = amount[1],
			icon = icon
		})
	end
end

local function set_hud_item_amount(index, amount)
	if #amount > 1 then
		managers.hud:set_item_amount_from_string(index, make_double_hud_string(amount[1], amount[2]), amount)
	else
		managers.hud:set_item_amount(index, amount[1])
	end
end

function PlayerManager:update_grenades_to_peer(peer)
	local peer_id = managers.network:session():local_peer():id()

	if self._global.synced_grenades[peer_id] then
		local grenade = self._global.synced_grenades[peer_id].grenade
		local tweak = tweak_data.blackmarket.projectiles[grenade]
		if tweak.based_on then 
			grenade = tweak.based_on
		end
		local amount = self._global.synced_grenades[peer_id].amount

		peer:send_queued_sync("sync_grenades", grenade, Application:digest_value(amount, false), 0)
	end
end

function PlayerManager:update_grenades_amount_to_peers(grenade, amount, register_peer_id)
	local peer_id = managers.network:session():local_peer():id()
	
		local tweak = tweak_data.blackmarket.projectiles[grenade]
		if tweak.based_on then 
			grenade = tweak.based_on
		end
		
	managers.network:session():send_to_peers_synched("sync_grenades", grenade, amount, register_peer_id or 0)
	self:set_synced_grenades(peer_id, grenade, amount, register_peer_id)
end

--fixes the synced (spoofed) grenade data being used for the actual data- specifically, cooldown
-- updated to 235U
function PlayerManager:add_grenade_amount(amount, sync)
	local peer_id = managers.network:session():local_peer():id()
	local grenade = managers.blackmarket:equipped_grenade()
	local tweak = tweak_data.blackmarket.projectiles[grenade]
	local max_amount = self:get_max_grenades_by_peer_id(peer_id)
	local icon = tweak.icon
	local previous_amount = self._global.synced_grenades[peer_id].amount

	if amount > 0 and tweak.base_cooldown then
		managers.hud:animate_grenade_flash(HUDManager.PLAYER_PANEL)
	end

	amount = math.min(Application:digest_value(previous_amount, false) + amount, max_amount)

	if amount < max_amount and tweak.base_cooldown then
		self:replenish_grenades(tweak.base_cooldown)
	end

	managers.hud:set_teammate_grenades_amount(HUDManager.PLAYER_PANEL, {
		icon = icon,
		amount = amount
	})
	self:update_grenades_amount_to_peers(grenade, amount, sync and peer_id)

	if self:got_max_grenades() and self._coroutine_mgr:is_running("regain_throwable_from_ammo") then
		self:remove_coroutine("regain_throwable_from_ammo")
	end

	return amount
end

-- fixes custom grenades cooldown animation when sped up
function PlayerManager:speed_up_grenade_cooldown(time)
	local timer = self._timers.replenish_grenades

	if not timer then
		return
	end

	timer.t = timer.t - time
	local peer_id = managers.network:session():local_peer():id()
	local grenade = managers.blackmarket:equipped_grenade() or self._global.synced_grenades[peer_id].grenade
	local tweak = tweak_data.blackmarket.projectiles[grenade]
	local time_left = self:get_timer_remaining("replenish_grenades") or 0

	managers.hud:set_player_grenade_cooldown({
		end_time = managers.game_play_central:get_heist_timer() + time_left,
		duration = tweak.base_cooldown
	})
end