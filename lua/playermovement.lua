function PlayerMovement:is_SPOOC_attack_allowed()
	if self._unit:character_damage():get_mission_blocker("invulnerable") or self._unit:character_damage().swansong or managers.player:get_property("decaying") then
		return false
	end

	if self._current_state_name == "driving" then
		return false
	end

	return true
end

local _subtract_stamina = PlayerMovement.subtract_stamina
function PlayerMovement:subtract_stamina(...)
	local wraith_form = managers.player:is_semi_wraith()

	if wraith_form then
		return
	end

	return _subtract_stamina(self, ...)
end

local _set_attention_settings = PlayerMovement.set_attention_settings
function PlayerMovement:set_attention_settings(settings_list, wraith_called)
	local wraith_form = managers.player:is_wraith()

	if wraith_form and not wraith_called then
		-- catch any non wraith calls and reconfigure them
		local list = managers.player:set_cop_attention("cloaked", true)
		_set_attention_settings(self, list)
		return
	end

	return _set_attention_settings(self, settings_list)
end