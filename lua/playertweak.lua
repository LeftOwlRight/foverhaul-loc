Hooks:PostHook(PlayerTweakData, "init", "base_player_stats__foverhaul", function(self)
	self.damage.HEALTH_INIT = 22
	self.damage.ARMOR_INIT = 5
	self.movement_state.standard.movement.speed.STANDARD_MAX = 352
	self.movement_state.standard.movement.speed.RUNNING_MAX = 492
	self.movement_state.standard.movement.speed.CROUCHING_MAX = 246
	self.damage.BLEED_OUT_HEALTH_INIT = 0.4
	self.damage.DOWNED_TIME = 30
	self.damage.DAMAGE_TO_ARMOR = 0.7
	self.damage.REGENERATE_TIME = 20
	self.damage.BASE_HEALTH_REGEN = 0.01
	self.damage.CONSIDER_NO_DAMAGE_TAKEN = 3
	self.damage.MIN_DAMAGE_INTERVAL = 0.25
end)

function PlayerTweakData:_set_singleplayer()
	self.damage.REGENERATE_TIME = 15
end

local function _add_hooks(...)
	local hooks = { ... }
	for _, hook in pairs(hooks) do
		Hooks:PostHook(PlayerTweakData, hook, "pltwk_Foverhaul2024___" .. tostring(_), function(self)
			self.damage.MIN_DAMAGE_INTERVAL = 0.25
		end)
	end
end

_add_hooks("_set_easy_wish", "_set_overkill_290", "_set_sm_wish")