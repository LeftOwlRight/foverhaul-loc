Hooks:PostHook(BlackMarketTweakData, "_init_projectiles", "quick_fix_stymulantandmore", function (self)

    local injectors = {
        {
            name_id = "quick_fix_stymulant",
            base_cooldown = 60,
            max_amount = 2,
            bundle = "berserk"
        },
        {
            name_id = "bite_the_bullet_ability",
            base_cooldown = 80,
            max_amount = 1,
            bundle = "berserk"
        },

        {
            name_id = "archer_ability",
            base_cooldown = 40,
            max_amount = 1,
            bundle = "berserk"
        },

        {
            name_id = "bleed_ability",
            base_cooldown = 60,
            max_amount = 1,
            bundle = "bleed"
        },
        {
            name_id = "wraith_form",
            base_cooldown = nil,
            max_amount = 1,
            bundle = "wraith"
        },
    }

    for i, injector in ipairs(injectors) do
        self.projectiles[injector.name_id] = {
            name_id = "bm_"..injector.name_id.."",
            desc_id = "bm_"..injector.name_id.."_desc",
            ignore_statistics = true,
            icon = injector.icon or injector.name_id,
            ability = injector.name_id,
            based_on = "chico_injector",
            texture_bundle_folder = injector.bundle or "chico",
            base_cooldown = injector.base_cooldown,
            max_amount = injector.max_amount,
            custom = true,
            sounds = {
                activate = "perkdeck_activate",
                cooldown = "perkdeck_cooldown_over"
            }
        }
        table.insert(self._projectiles_index, injector.name_id)
    end
end)