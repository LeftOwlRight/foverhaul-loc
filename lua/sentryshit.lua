if RequiredScript == "lib/units/equipment/sentry_gun/sentrygunbase" then
	SentryGunBase.DEPLOYEMENT_COST = {
		0.7,
		0.75,
		0.85,
	}
	SentryGunBase.ROTATION_SPEED_MUL = {
		1,
		2.25
	}
	SentryGunBase.AMMO_MUL = {
		1,
		1.7
	}
	SentryGunBase.SPREAD_MUL = {
		1,
		0.5
	}
end

if RequiredScript == "lib/units/weapons/sentrygunweapon" then
	SentryGunWeapon._AP_ROUNDS_DAMAGE_MULTIPLIER = 1.65
	SentryGunWeapon._AP_ROUNDS_FIRE_RATE = 2
end