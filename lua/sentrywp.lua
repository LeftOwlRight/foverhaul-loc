local function add_damage_mul(self)
    self._current_damage_mul = self._current_damage_mul + managers.player:upgrade_value("sentry_gun", "damage_multiplier", 1) - 1
end
Hooks:PostHook(SentryGunWeapon, "setup", "foverhaul", add_damage_mul)
Hooks:PostHook(SentryGunWeapon, "_set_fire_mode", "foverhaul", add_damage_mul)