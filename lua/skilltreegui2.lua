function SkillTreeGui:_set_selected_skill_item(item, no_sound)
	if self._selected_item ~= item then
		if self._selected_item then
			self._selected_item:deselect()
		end

		if item then
			if item.tree then
				no_sound = no_sound and self._active_tree == item:tree()
			end

			item:select(no_sound)

			self._selected_item = item
		end
	end

	local text = ""
	local prerequisite_text = ""
	local title_text = ""
	self._prerequisites_links = self._prerequisites_links or {}

	for _, data in ipairs(self._prerequisites_links) do
		if data ~= item then
			data:refresh()
		end
	end

	self._prerequisites_links = {}
	local skill_stat_color = tweak_data.screen_colors.resource
	local color_replace_table = {}
	local tier_bonus_text = ""

	if self._selected_item and self._selected_item._skill_panel then
		local skill_id = self._selected_item._skill_id
		local tweak_data_skill = tweak_data.skilltree.skills[skill_id]
		local points = managers.skilltree:points() or 0
		local basic_cost = managers.skilltree:get_skill_points(skill_id, 1) or 0
		local pro_cost = managers.skilltree:get_skill_points(skill_id, 2) or 0
		local talent = tweak_data.skilltree.skills[skill_id]
		local unlocked = managers.skilltree:skill_unlocked(nil, skill_id)
		local step = managers.skilltree:next_skill_step(skill_id)
		local completed = managers.skilltree:skill_completed(skill_id)
		local points = managers.skilltree:points()
		local spending_money = managers.money:total()
		local skill_descs = tweak_data.upgrades.skill_descs[skill_id] or {
			0,
			0
		}
		local basic_color_index = 1
		local pro_color_index = 2 + (skill_descs[1] or 0)

		if step > 1 then
			basic_cost = utf8.to_upper(managers.localization:text("st_menu_skill_owned"))
			color_replace_table[basic_color_index] = Color(0, 1, 0.7)
		else
			local money_cost = managers.money:get_skillpoint_cost(self._selected_item._tree, self._selected_item._tier, basic_cost)
			local can_afford = basic_cost <= points and money_cost <= spending_money
			color_replace_table[basic_color_index] = can_afford and tweak_data.screen_colors.resource or tweak_data.screen_colors.important_1
			basic_cost = managers.localization:text(basic_cost == 1 and "st_menu_point" or "st_menu_point_plural", {
				points = basic_cost
			}) .. " / " .. managers.experience:cash_string(money_cost)
		end

		if step > 2 then
			pro_cost = utf8.to_upper(managers.localization:text("st_menu_skill_aced"))
			color_replace_table[pro_color_index] = tweak_data.screen_colors.resource
		else
			local money_cost = managers.money:get_skillpoint_cost(self._selected_item._tree, self._selected_item._tier, pro_cost)
			local can_afford = pro_cost <= points and money_cost <= spending_money
			color_replace_table[pro_color_index] = can_afford and tweak_data.screen_colors.resource or tweak_data.screen_colors.important_1
			pro_cost = managers.localization:text(pro_cost == 1 and "st_menu_point" or "st_menu_point_plural", {
				points = pro_cost
			}) .. " / " .. managers.experience:cash_string(money_cost)
		end

		local macroes = {
			basic = basic_cost,
			pro = pro_cost
		}

		for i, d in pairs(skill_descs) do
			macroes[i] = d
		end

		title_text = utf8.to_upper(managers.localization:text(tweak_data.skilltree.skills[skill_id].name_id))
		text = managers.localization:text(tweak_data_skill.desc_id, macroes)

		if self._selected_item._tier then
			if not unlocked then
				local point_spent = managers.skilltree:points_spent(self._selected_item._tree) or 0
				local tier_unlocks = managers.skilltree:tier_cost(self._selected_item._tree, self._selected_item._tier) or 0
				local points_needed = tier_unlocks - point_spent
				prerequisite_text = prerequisite_text .. managers.localization:text(points_needed == 1 and "st_menu_points_to_unlock_tier_singular" or "st_menu_points_to_unlock_tier", {
					points = points_needed,
					tier = self._selected_item._tier
				}) .. "\n"
			end

			if not tweak_data.skilltree.HIDE_TIER_BONUS then
				local skilltree = tweak_data.skilltree.trees[self._active_tree] and tweak_data.skilltree.trees[self._active_tree].skill or "NIL"
				local tier_descs = tweak_data.upgrades.skill_descs[tostring(skilltree) .. "_tier" .. tostring(self._selected_item._tier)]
				tier_bonus_text = "\n\n" .. utf8.to_upper(managers.localization:text(unlocked and "st_menu_tier_unlocked" or "st_menu_tier_locked")) .. "\n" .. managers.localization:text(tweak_data.skilltree.skills[tweak_data.skilltree.trees[self._selected_item._tree].skill][self._selected_item._tier].desc_id, tier_descs)
			end
		end

		text = text .. tier_bonus_text
		local prerequisites = talent.prerequisites or {}
		local add_prerequisite = true

		for _, prerequisite in ipairs(prerequisites) do
			local unlocked = managers.skilltree:skill_step(prerequisite)

			if unlocked and unlocked == 0 then
				if add_prerequisite then
					prerequisite_text = prerequisite_text .. managers.localization:text("st_menu_prerequisite_following_skill" .. (#prerequisites > 1 and "_plural" or ""))
					add_prerequisite = nil
				end

				prerequisite_text = prerequisite_text .. "   " .. managers.localization:text(tweak_data.skilltree.skills[prerequisite].name_id) .. "\n"

				if self._active_page then
					for _, item in ipairs(self._active_page._items) do
						if item._skill_id == prerequisite then
							item._skill_panel:child("state_image"):set_color(tweak_data.screen_colors.important_1)
							table.insert(self._prerequisites_links, item)
						end
					end
				end
			end
		end
	end

	self._skill_title_panel:child("text"):set_text(title_text)

	local desc_pre_text = self._skill_description_panel:child("prerequisites_text")

	if prerequisite_text == "" then
		desc_pre_text:hide()
		desc_pre_text:set_h(0)
	else
		prerequisite_text = utf8.to_upper(prerequisite_text)

		desc_pre_text:show()
		desc_pre_text:set_text(prerequisite_text)

		local x, y, w, h = desc_pre_text:text_rect()

		desc_pre_text:set_h(h)
	end

	local text_dissected = utf8.characters(text)
	local idsp = Idstring("#")
	local start_ci = {}
	local end_ci = {}
	local first_ci = true

	for i, c in ipairs(text_dissected) do
		if Idstring(c) == idsp then
			local next_c = text_dissected[i + 1]

			if next_c and Idstring(next_c) == idsp then
				if first_ci then
					table.insert(start_ci, i)
				else
					table.insert(end_ci, i)
				end

				first_ci = not first_ci
			end
		end
	end

	if #start_ci == #end_ci then
		for i = 1, #start_ci do
			start_ci[i] = start_ci[i] - ((i - 1) * 4 + 1)
			end_ci[i] = end_ci[i] - (i * 4 - 1)
		end
	end

	text = string.gsub(text, "##", "")
	local desc_text = self._skill_description_panel:child("text")

	desc_text:set_text(text)
	desc_text:set_y(math.round(desc_pre_text:h() * 1.15))
	desc_text:clear_range_color(1, utf8.len(text))

	if #start_ci ~= #end_ci then
		Application:error("SkillTreeGui: Not even amount of ##'s in skill description string!", #start_ci, #end_ci)
	else
		for i = 1, #start_ci do
			desc_text:set_range_color(start_ci[i], end_ci[i], color_replace_table[i] or skill_stat_color)
		end
	end
end