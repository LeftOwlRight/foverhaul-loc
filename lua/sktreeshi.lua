Hooks:PostHook(SkillTreeTweakData, "init", "Skills_Overhaul_2021f", function(self)
-- $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\       $$$$$$$\  $$$$$$$$\  $$$$$$\  $$\   $$\ 
-- $$  __$$\ $$  _____|$$  __$$\ $$ | $$  |      $$  __$$\ $$  _____|$$  __$$\ $$ | $$  |
-- $$ |  $$ |$$ |      $$ |  $$ |$$ |$$  /       $$ |  $$ |$$ |      $$ /  \__|$$ |$$  / 
-- $$$$$$$  |$$$$$\    $$$$$$$  |$$$$$  /        $$ |  $$ |$$$$$\    $$ |      $$$$$  /  
-- $$  ____/ $$  __|   $$  __$$< $$  $$<         $$ |  $$ |$$  __|   $$ |      $$  $$<   
-- $$ |      $$ |      $$ |  $$ |$$ |\$$\        $$ |  $$ |$$ |      $$ |  $$\ $$ |\$$\  
-- $$ |      $$$$$$$$\ $$ |  $$ |$$ | \$$\       $$$$$$$  |$$$$$$$$\ \$$$$$$  |$$ | \$$\ 
-- \__|      \________|\__|  \__|\__|  \__|      \_______/ \________| \______/ \__|  \__| 
--PERK DECK
	for a = 1, 21, 1 do
		self.specializations[a][2].upgrades = {
			"weapon_passive_headshot_damage_multiplier",
			"player_critical_damage_card"
		}
		self.specializations[a][8].upgrades = {
			"passive_doctor_bag_interaction_speed_multiplier",
			"player_weapon_swap_speed_card"
		}
	end

	self.specializations[15][3].upgrades = {
		"player_health_decrease_1",
		"player_armor_increase_1",
		"player_anarchist_damage_redirect"
	}
	self.specializations[16][9].upgrades = {
		"player_passive_loot_drop_multiplier",
		"player_less_armor_wild_cooldown_1",
		"player_wild_maxed_damage_reduction"
	}

	local deck2 = {
		cost = 300,
		desc_id = "menu_deckall_2_desc",
		name_id = "menu_deckall_2",
		upgrades = {
			"weapon_passive_headshot_damage_multiplier",
			"player_critical_damage_card"
		},
		icon_xy = {
			1,
			0
		}
	}
	local deck4 = {
		cost = 600,
		desc_id = "menu_deckall_4_desc",
		name_id = "menu_deckall_4",
		upgrades = {
			"passive_player_xp_multiplier",
			"player_passive_suspicion_bonus",
			"player_passive_armor_movement_penalty_multiplier"
		},
		icon_xy = {
			3,
			0
		}
	}
	local deck6 = {
		cost = 1600,
		desc_id = "menu_deckall_6_desc",
		name_id = "menu_deckall_6",
		upgrades = {
			"armor_kit",
			"player_pick_up_ammo_multiplier"
		},
		icon_xy = {
			5,
			0
		}
	}
	local deck8 = {
		cost = 3200,
		desc_id = "menu_deckall_8_desc",
		name_id = "menu_deckall_8",
		upgrades = {
			"passive_doctor_bag_interaction_speed_multiplier",
			"player_weapon_swap_speed_card"
		},
		icon_xy = {
			7,
			0
		}
	}

	table.insert(self.specializations, --Low Profile
		{
			name_id = "crimson_perk",
			desc_id = "crimson_perk_desc",
			{
				upgrades = {
					"player_bonus_from_crit",
					"exceed_crit_scaling"
				},
				texture_bundle_folder = "berserk",
				cost = 200,
				icon_xy = {
					6, 0
				},
				name_id = "crimson_deck1",
				desc_id = "crimson_deck_desc1"
			},
			deck2,
			{
				upgrades = {
					"player_crit_heal"
				},
				texture_bundle_folder = "berserk",
				cost = 400,
				icon_xy = {
					1, 0
				},
				name_id = "crimson_deck2",
				desc_id = "crimson_deck_desc2"
			},
			deck4,
			{
				upgrades = {
					"player_bonus_from_crit_2",
					"exceed_crit_scaling_2"
				},
				texture_bundle_folder = "berserk",
				cost = 1000,
				icon_xy = {
					7, 0
				},
				name_id = "crimson_deck3",
				desc_id = "crimson_deck_desc3"
			},
			deck6,
			{
				upgrades = {
					"player_execution_crit",
					"player_execution_crit_scaling"
				},
				dlc = "hlm2_deluxe",
				cost = 2400,
				icon_xy = {
					2, 5
				},
				name_id = "crimson_deck4",
				desc_id = "crimson_deck_desc4"
			},
			deck8,
			{
				upgrades = {
					"player_bonus_crit_chanc",
					"player_bonus_from_crit_3",
					"player_execution_crit_2",
					"player_execution_crit_scaling_2",
					"player_execution_immunity",
					"player_passive_loot_drop_multiplier"
				},
				cost = 4000,
				icon_xy = {
					2, 0
				},
				texture_bundle_folder = "berserk",
				name_id = "crimson_deck5",
				desc_id = "crimson_deck_desc5"
			}
		}
	)

	table.insert(self.specializations, --Quick Fix
		{
			name_id = "lust_name",
			desc_id = "lust_name_desc",
			{
				upgrades = {
					"player_quick_fix",
					"quick_fix_stymulant"
				},
				cost = 200,
				texture_bundle_folder = "berserk",
				icon_xy = {
					0,
					0
				},
				name_id = "lust_deck1",
				desc_id = "lust_deck_desc1"
			},
			deck2,
			{
				upgrades = {
					"player_qf_bonus_movement_speed",
					"player_qf_stamina_regen"
				},
				cost = 400,
				texture_bundle_folder = "berserk",
				icon_xy = {
					5,
					0
				},
				name_id = "lust_deck2",
				desc_id = "lust_deck_desc2"
			},
			deck4,
			{
				upgrades = {
					"player_qf_invulnerable_inc"
				},
				cost = 1000,
				texture_bundle_folder = "berserk",
				icon_xy = {
					4,
					0
				},
				name_id = "lust_deck3",
				desc_id = "lust_deck_desc3"
			},
			deck6,
			{
				upgrades = {
					"player_bs_hp_gf"
				},
				cost = 2400,
				texture_bundle_folder = "copr",
				icon_xy = {
					1,
					0
				},
				name_id = "lust_deck4",
				desc_id = "lust_deck_desc4"
			},
			deck8,
			{
				upgrades = {
					"player_qf_healing_period_extended",
					"player_passive_loot_drop_multiplier",
					"player_qf_interacting_cooldown_decrease"
				},
				cost = 4000,
				texture_bundle_folder = "berserk",
				icon_xy = {
					3,
					0
				},
				name_id = "lust_deck5",
				desc_id = "lust_deck_desc5"
			},
		}
	)

	table.insert(self.specializations, --Veteran
		{
			name_id = "toxin_name",
			desc_id = "toxin_desc",
			{
				upgrades = {
					"bite_the_bullet_ability",
					"bite_the_bullet_base"
				},
				cost = 200,
				texture_bundle_folder = "toxin",
				icon_xy = {
					0,
					0
				},
				name_id = "toxin_deck1",
				desc_id = "toxin_deck_desc1"
			},
			deck2,
			{
				upgrades = {
					"player_bite_the_bullet_stored_damage"
				},
				cost = 400,
				texture_bundle_folder = "toxin",
				icon_xy = {
					1,
					0
				},
				name_id = "toxin_deck2",
				desc_id = "toxin_deck_desc2"
			},
			deck4,
			{
				upgrades = {
					"player_reduce_cooldown_bite_the_bullet"
				},
				cost = 1000,
				texture_bundle_folder = "toxin",
				icon_xy = {
					2,
					0
				},
				name_id = "toxin_deck3",
				desc_id = "toxin_deck_desc3"
			},
			deck6,
			{
				upgrades = {
					"player_bite_the_bullet_stored_damage_2",
					"player_bonus_duration_bite_the_bullet"
				},
				cost = 2400,
				texture_bundle_folder = "toxin",
				icon_xy = {
					3,
					0
				},
				name_id = "toxin_deck4",
				desc_id = "toxin_deck_desc4"
			},
			deck8,
			{
				upgrades = {
					"player_shockwave_bonus_damage",
					"player_shockwave_healing",
					"player_last_shockwave_range",
					"player_passive_loot_drop_multiplier"
				},
				cost = 4000,
				texture_bundle_folder = "toxin",
				icon_xy = {
					4,
					0
				},
				name_id = "toxin_deck5",
				desc_id = "toxin_deck_desc5"
			},
		}
	)

	table.insert(self.specializations, --Daredevil
	{
		name_id = "daredevil_name",
		desc_id = "daredevil_desc",
		{
			upgrades = {
				"style_rank",
			},
			cost = 200,
			texture_bundle_folder = "daredevil",
			icon_xy = {
				0,
				0
			},
			name_id = "daredevil_deck1",
			desc_id = "daredevil_deck_desc1"
		},
		deck2,
		{
			upgrades = {
				"daredevil_damage_absorption",
				"style_lifesteal"
			},
			cost = 400,
			texture_bundle_folder = "daredevil",
			icon_xy = {
				1,
				0
			},
			name_id = "daredevil_deck2",
			desc_id = "daredevil_deck_desc2"
		},
		deck4,
		{
			upgrades = {
				"style_pierce_body_armor"
			},
			cost = 1000,
			texture_bundle_folder = "daredevil",
			icon_xy = {
				2,
				0
			},
			name_id = "daredevil_deck3",
			desc_id = "daredevil_deck_desc3"
		},
		deck6,
		{
			upgrades = {
				"style_rate_of_fire"
			},
			cost = 2400,
			texture_bundle_folder = "daredevil",
			icon_xy = {
				3,
				0
			},
			name_id = "daredevil_deck4",
			desc_id = "daredevil_deck_desc4"
		},
		deck8,
		{
			upgrades = {
				"player_style_lifesteal_increase",
				"bonus_hp_enemy_damage_style",
				"automatically_reload_style",
				"player_passive_loot_drop_multiplier"
			},
			cost = 4000,
			texture_bundle_folder = "daredevil",
			icon_xy = {
				4,
				0
			},
			name_id = "daredevil_deck5",
			desc_id = "daredevil_deck_desc5"
		},
	}
)

table.insert(self.specializations, --Archer
	{
		name_id = "archer_name",
		desc_id = "archer_desc",
		{
			upgrades = {
				"player_archer_base",
			},
			cost = 200,
			texture_bundle_folder = "archer",
			icon_xy = {
				0,
				0
			},
			name_id = "archer_deck1",
			desc_id = "archer_deck_desc1"
		},
		deck2,
		{
			upgrades = {
				"player_archer_dodge_bonus_1",
			},
			cost = 400,
			icon_xy = {
				1,
				2
			},
			name_id = "archer_deck2",
			desc_id = "archer_deck_desc2"
		},
		deck4,
		{
			upgrades = {
				"player_archer_dodge_bonus_2",
				"player_archer_bolt_aoe"
			},
			cost = 1000,
			texture_bundle_folder = "archer",
			icon_xy = {
				1,
				0
			},
			name_id = "archer_deck3",
			desc_id = "archer_deck_desc3"
		},
		deck6,
		{
			upgrades = {
				"player_archer_dodge_bonus_3"
			},
			cost = 2400,
			icon_xy = {
				3,
				2
			},
			name_id = "archer_deck4",
			desc_id = "archer_deck_desc4"
		},
		deck8,
		{
			upgrades = {
				"archer_ability",
				"player_archer_reload",
				"player_archer_heal_on_headshot",
				"player_archer_pierce_through",
				"player_passive_loot_drop_multiplier"
			},
			cost = 4000,
			texture_bundle_folder = "archer",
			icon_xy = {
				2,
				0
			},
			name_id = "archer_deck5",
			desc_id = "archer_deck_desc5"
		},
	}
)

table.insert(self.specializations, --bleed
	{
		name_id = "bleed_name",
		desc_id = "bleed_desc",
		{
			upgrades = {
				"player_bleed_bullets"
			},
			cost = 200,
			texture_bundle_folder = "bleed",
			icon_xy = {
				0,
				0
			},
			name_id = "bleed_deck1",
			desc_id = "bleed_deck_desc1"
		},
		deck2,
		{
			upgrades = {
				"player_bleeding_enemy_dmg_red",
				"player_bleeding_vamp"
			},
			cost = 400,
			texture_bundle_folder = "bleed",
			icon_xy = {
				1,
				0
			},
			name_id = "bleed_deck2",
			desc_id = "bleed_deck_desc2"
		},
		deck4,
		{
			upgrades = {
				"player_bleed_bonus_damage_taken",
			},
			cost = 1000,
			texture_bundle_folder = "bleed",
			icon_xy = {
				2,
				0
			},
			name_id = "bleed_deck3",
			desc_id = "bleed_deck_desc3"
		},
		deck6,
		{
			upgrades = {
				"player_bleed_critical_hit",
				"player_bleed_critical_hit_chance"
			},
			cost = 2400,
			texture_bundle_folder = "bleed",
			icon_xy = {
				3,
				0
			},
			name_id = "bleed_deck4",
			desc_id = "bleed_deck_desc4"
		},
		deck8,
		{
			upgrades = {
				"player_bleed_ability_base",
				"bleed_ability",
				"player_passive_loot_drop_multiplier"
			},
			cost = 4000,
			texture_bundle_folder = "bleed",
			icon_xy = {
				4,
				0
			},
			name_id = "bleed_deck5",
			desc_id = "bleed_deck_desc5"
		},
	}
)

table.insert(self.specializations, --wraith
	{
		name_id = "demon_name",
		desc_id = "demon_desc",
		{
			upgrades = {
				"player_semi_wraith_bonus_ms",
				"player_semi_wraith_base",
				"player_damage_defense_multiplier"
			},
			cost = 200,
			texture_bundle_folder = "wraith",
			icon_xy = {
				3,
				0
			},
			name_id = "demon_deck1",
			desc_id = "demon_deck_desc1"
		},
		deck2,
		{
			upgrades = {
				"player_semi_wraith_heal_bullet"
			},
			cost = 400,
			texture_bundle_folder = "wraith",
			icon_xy = {
				2,
				0
			},
			name_id = "demon_deck2",
			desc_id = "demon_deck_desc2"
		},
		deck4,
		{
			upgrades = {
				"player_semi_wraith_bonus_max_hp",
				"player_semi_wraith_bonus_duration"
			},
			cost = 1000,
			texture_bundle_folder = "copr",
			icon_xy = {
				1,
				0
			},
			name_id = "demon_deck3",
			desc_id = "demon_deck_desc3"
		},
		deck6,
		{
			upgrades = {
				"player_semi_wraith_lifeline"
			},
			cost = 2400,
			texture_bundle_folder = "wraith",
			icon_xy = {
				1,
				0
			},
			name_id = "demon_deck4",
			desc_id = "demon_deck_desc4"
		},
		deck8,
		{
			upgrades = {
				"wraith_form",
				"player_wraith_form_base",
				"player_passive_loot_drop_multiplier"
			},
			cost = 4000,
			texture_bundle_folder = "wraith",
			icon_xy = {
				0,
				0
			},
			name_id = "demon_deck5",
			desc_id = "demon_deck_desc5"
		},
	}
)


-- ________________________________________________________________________________________________________________________________________________

--  $$$$$$\  $$\   $$\ $$$$$$\ $$\       $$\             $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\ 
-- $$  __$$\ $$ | $$  |\_$$  _|$$ |      $$ |            \__$$  __|$$  __$$\ $$  _____|$$  _____|
-- $$ /  \__|$$ |$$  /   $$ |  $$ |      $$ |               $$ |   $$ |  $$ |$$ |      $$ |      
-- \$$$$$$\  $$$$$  /    $$ |  $$ |      $$ |               $$ |   $$$$$$$  |$$$$$\    $$$$$\    
--  \____$$\ $$  $$<     $$ |  $$ |      $$ |               $$ |   $$  __$$< $$  __|   $$  __|   
-- $$\   $$ |$$ |\$$\    $$ |  $$ |      $$ |               $$ |   $$ |  $$ |$$ |      $$ |      
-- \$$$$$$  |$$ | \$$\ $$$$$$\ $$$$$$$$\ $$$$$$$$\          $$ |   $$ |  $$ |$$$$$$$$\ $$$$$$$$\ 
--  \______/ \__|  \__|\______|\________|\________|         \__|   \__|  \__|\________|\________|
--SKILL TREE



    self.skills.combat_engineering = {
		{
			upgrades = {
				"trip_mine_explosion_size_multiplier_1",
                "player_trip_mine_deploy_time_multiplier_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"trip_mine_damage_multiplier_1",
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_combat_engineering_CET",
		desc_id = "menu_combat_engineering_desc_CET",
		icon_xy = {
			1,
			5
		}
	}
    self.skills.hitman = {
		{
			upgrades = {
				"weapon_silencer_recoil_index_addend",
				"weapon_silencer_enter_steelsight_speed_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"weapon_silencer_spread_index_addend",
				"weapon_silencer_damage_multiplier_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_unseen_strike_beta",
		desc_id = "menu_unseen_strike_beta_desc",
		icon_xy = {
			4,
			4
		}
	}
	self.skills.silence_expert = { --highvt
		{
			upgrades = {
				"player_marked_enemy_extra_damage",
				"player_mark_enemy_time_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_marked_inc_dmg_distance_1",
				"weapon_steelsight_highlight_specials",
				"player_mark_enemy_time_multiplier_2",
				"player_marked_distance_mul"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_spotter_teamwork_beta",
		desc_id = "menu_spotter_teamwork_beta_desc",
		icon_xy = {
			8,
			2
		}
	}
    self.skills.more_fire_power = {
		{
			upgrades = {
				"shape_charge_quantity_increase_1",
				"trip_mine_quantity_increase_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"shape_charge_quantity_increase_2",
				"trip_mine_quantity_increase_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_more_fire_power_CET",
		desc_id = "menu_more_fire_power_desc_CET",
		icon_xy = {
			9,
			7
		}
	}
	self.skills.bandoliers = {
		{
			upgrades = {
				"extra_ammo_multiplier1",
				"player_pick_up_ammo_multiplier_skill",
				"player_recovery_speed_tier_infamy" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_pick_up_ammo_multiplier_skill_2",
				"player_regain_throwable_from_ammo_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_bandoliers_beta",
		desc_id = "menu_bandoliers_beta_desc",
		icon_xy = {
			3,
			0
		}
	}
	self.skills.equilibrium = {
		{
			upgrades = {
				"pistol_swap_speed_multiplier"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"pistol_enter_steelsight_speed_multiplier"
			},
			cost = self.costs.pro
		},
		name_id = "menu_equilibrium_beta",
		desc_id = "menu_equilibrium_beta_desc",
		icon_xy = {
			11,
			11
		}
	}
    self.skills.oppressor = {
		{
			upgrades = {
				"player_flashbang_multiplier_1",
				"player_armor_regen_time_mul_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_flashbang_multiplier_2",
				"player_damage_defense_effi"
			},
			cost = self.costs.pro
		},
		name_id = "menu_oppressor_beta_RSLC",
		desc_id = "menu_oppressor_beta_desc_RSLC",
		icon_xy = {
			2,
			12
		}
	}
	self.skills.body_expertise = {
		{
			upgrades = {
				"weapon_automatic_head_shot_add_1",
				"player_level_interaction_timer_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"weapon_automatic_head_shot_add_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_body_expertise_beta",
		desc_id = "menu_body_expertise_beta_desc",
		icon_xy = {
			10,
			3
		}
	}
	self.skills.unseen_strike = { --assassin
		{
            upgrades = {
				"player_tier_bonus_stamina_multiplier",
				"player_tier_bonus_movement_speed",
				"player_increase_ms_stamina_infamy",
				"player_critical_silenced_1",
				"player_bonus_critical_damage"
            },
            cost = self.costs.default
        },
        {
            upgrades = {
				"player_critical_silenced_2",
				"player_bonus_critical_damage_2"
            },
            cost = self.costs.pro
        },
        name_id = "menu_spotter_teamwork_beta_SKL",
        desc_id = "menu_spotter_teamwork_beta_desc_SKL",
        icon_xy = {
			10,
			14
        }
    }
    self.skills.scavenger = {
        {
			upgrades = {
				"temporary_damage_speed_multiplier_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
                "player_team_damage_speed_multiplier_send",
				"player_defense_up_scav"
			},
			cost = self.costs.pro
		},
		name_id = "menu_scavenger_beta_SVUP",
		desc_id = "menu_scavenger_beta_desc_SVUP",
		icon_xy = {
			10,
            9
        }
    }
    self.skills.drill_expert = {
		{
			upgrades = {
				"player_drill_speed_multiplier1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_drill_speed_multiplier2"
			},
			cost = self.costs.pro
		},
		name_id = "menu_drill_expert_beta_DTE",
		desc_id = "menu_drill_expert_beta_desc_DTE",
		icon_xy = {
			3,
			6
		}
    }
    self.skills.hardware_expert = {
		{
			upgrades = {
				"player_drill_fix_interaction_speed_multiplier",
				"player_trip_mine_deploy_time_multiplier_2",
				"player_drill_alert",
				"player_silent_drill"
			},
			cost = self.costs.default
		},
				{
			upgrades = {
				"player_drill_autorepair_1",
				"player_drill_fix_interaction_speed_multiplier_2"
			},
			cost = self.costs.pro
		},
				name_id = "menu_hardware_expert_beta",
		desc_id = "menu_hardware_expert_beta_desc",
		icon_xy = {
			9,
			6
		}
	}

	self.skills.kick_starter = {
		{
			upgrades = {
				"player_drill_autorepair_2",
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_drill_melee_hit_restart_chance_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_kick_starter_beta",
		desc_id = "menu_kick_starter_beta_desc",
		icon_xy = {
			9,
			8
		}
    }
	self.skills.jack_of_all_trades = {
		{
			upgrades = {
				"sentry_gun_damage_multiplier_1",
				"sentry_gun_armor_multiplier2_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"sentry_gun_armor_multiplier2_2",
                "sentry_gun_damage_multiplier_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_eco_sentry_beta",
		desc_id = "menu_eco_sentry_beta_desc",
		icon_xy = {
			9,
			2
		}
	}
	self.skills.trigger_happy = {
		{
			upgrades = {
				"pistol_stacking_hit_damage_multiplier_1",
				"player_reload_speed_tier_infamy" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"pistol_stacking_hit_damage_multiplier_2",
				"pistol_critical_hit_bonus_dmg"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_trigger_happy_beta",
		desc_id = "menu_trigger_happy_beta_desc",
		icon_xy = {
			11,
			2
		}
	}
	self.skills.messiah = {
		{
			upgrades = {
				"player_messiah_revive_from_bleed_out_1",
				"player_reload_speed_tier_infamy" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_recharge_messiah_1",
				"player_messiah_revive_from_bleed_out_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_pistol_beta_messiah",
		desc_id = "menu_pistol_beta_messiah_desc",
		icon_xy = {
			2,
			9
		}
	}
	self.skills.ammo_reservoir = {
		{
			upgrades = {
				"temporary_no_ammo_cost_1",
				"player_bullet_storm_base_dr"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"temporary_no_ammo_cost_2",
				"player_bullet_storm_base_dr_2"
			},
			cost = self.costs.pro
		},
		name_id = "menu_ammo_reservoir_beta",
		desc_id = "menu_ammo_reservoir_beta_desc",
		icon_xy = {
			4,
			5
		}
	}
	self.skills.running_from_death = {
		{
			upgrades = {
				"player_cheat_death_chance_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_revived_health_regain_cheat_death",
				"player_cheat_death_chance_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_running_from_death_beta",
		desc_id = "menu_running_from_death_beta_desc",
		icon_xy = {
			11,
			5
		}
	}
	self.skills.feign_death = {
		{
			upgrades = {
				"player_bleed_out_health_multiplier_2",
				"player_bleed_out_time"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_instakill_downed",
				"player_damage_reduction_downed"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_feign_death",
		desc_id = "menu_feign_death_desc",
		icon_xy = {
			11,
			14
		}
	}
		self.skills.perseverance = { --swan song
			{
				upgrades = {
					"swan_song_basic",
				},
				cost = self.costs.hightier
			},
			{
				upgrades = {
					"swan_song_ace"
				},
				cost = self.costs.hightierpro
			},
			name_id = "menu_perseverance_beta",
			desc_id = "menu_perseverance_beta_desc",
			icon_xy = {
				5,
				12
			}
		}
	self.skills.up_you_go = {
		{
			upgrades = {
				"player_temp_increased_movement_speed_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_revived_damage_resist_1",
				"player_revived_health_regain"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_up_you_go_beta",
		desc_id = "menu_up_you_go_beta_desc",
		icon_xy = {
			11,
			4
		}
	}
	self.skills.second_chances = {
		{
			upgrades = {
				"player_tape_loop_duration_1",
				"player_tape_loop_duration_2",
				"player_pick_lock_easy_speed_multiplier",
				"player_lockpicking_bonus_ms"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_pick_lock_hard",
				"player_pick_lock_easy_speed_multiplier_2",
				"player_lockpicking_genius"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_second_chances_beta",
		desc_id = "menu_second_chances_beta_desc",
		icon_xy = {
			10,
			4
		}
	}
	self.skills.triathlete = {
		{
			upgrades = {
				"cable_tie_quantity",
				"cable_tie_interact_speed_multiplier"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"team_damage_hostage_absorption"
			},
			cost = self.costs.pro
		},
		name_id = "menu_triathlete_beta_toidi",
		desc_id = "menu_triathlete_beta_desc_toidi",
		icon_xy = {
			4,
			7
		}
	}
	self.skills.shock_and_awe = {
		{
			upgrades = {
				"player_automatic_mag_increase_1",
				"player_dmr_bonus_damage_health"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_automatic_faster_reload_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_shock_and_awe_beta",
		desc_id = "menu_shock_and_awe_beta_desc",
		icon_xy = {
			10,
			2
		}
	}
	self.skills.fire_trap = {
		{
			upgrades = {
				"trip_mine_fire_trap_1",
				"player_level_interaction_timer_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"trip_mine_fire_trap_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_fire_trap_beta",
		desc_id = "menu_fire_trap_beta_desc",
		icon_xy = {
			9,
			9
		}
	}
	self.skills.fire_control = {
		{
			upgrades = {
				"player_run_and_shoot_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_weapon_movement_stability_1",
				"player_hip_fire_accuracy_inc_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_fire_control_beta",
		desc_id = "menu_fire_control_beta_desc",
		icon_xy = {
			7,
			10
		}
	}
	self.skills.stable_shot = {
		{
			upgrades = {
				"player_weapon_accuracy_increase_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_weapon_accuracy_increase_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_stable_shot_beta",
		desc_id = "menu_stable_shot_beta_desc",
		icon_xy = {
			0,
			5
		}
	}
	self.skills.steady_grip = {
		{
			upgrades = {
				"player_stability_increase_bonus_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_stability_increase_bonus_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_steady_grip_beta",
		desc_id = "menu_steady_grip_beta_desc",
		icon_xy = {
			9,
			11
		}
	}
	self.skills.sentry_targeting_package = {
		{
			upgrades = {
				"sentry_gun_spread_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"sentry_gun_rot_speed_multiplier",
				"sentry_gun_extra_ammo_multiplier_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_sentry_targeting_package_beta",
		desc_id = "menu_sentry_targeting_package_beta_desc",
		icon_xy = {
			9,
			1
		}
	}
	self.skills.engineering = {
		{
			upgrades = {
				"sentry_gun_silent"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"sentry_gun_ap_bullets",
				"sentry_gun_fire_rate_reduction_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_engineering_beta",
		desc_id = "menu_engineering_beta_desc",
		icon_xy = {
			9,
			3
		}
	}
	self.skills.eco_sentry = {
		{
			upgrades = {
				"deploy_interact_faster_2"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"second_deployable_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_jack_of_all_trades_beta",
		desc_id = "menu_jack_of_all_trades_beta_desc",
		icon_xy = {
			9,
			4
		}
	}
	self.skills.defense_up = {
		{
			upgrades = {
				"sentry_gun_cost_reduction_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"sentry_gun_shield",
				"sentry_gun_cost_reduction_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_defense_up_beta",
		desc_id = "menu_defense_up_beta_desc",
		icon_xy = {
			9,
			0
		}
	}
	self.skills.speedy_reload = {
		{
			upgrades = {
				"assault_rifle_reload_speed_multiplier",
				"smg_reload_speed_multiplier",
				"snp_reload_speed_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"temporary_single_shot_fast_reload_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_speedy_reload_beta",
		desc_id = "menu_speedy_reload_beta_desc",
		icon_xy = {
			8,
			3
		}
	}
	self.skills.rifleman = {
		{
			upgrades = {
				"weapon_enter_steelsight_speed_multiplier",
				"player_steelsight_normal_movement_speed"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"assault_rifle_move_spread_index_addend",
				"snp_move_spread_index_addend",
				"smg_move_spread_index_addend"
			},
			cost = self.costs.pro
		},
		name_id = "menu_rifleman_beta",
		desc_id = "menu_rifleman_beta_desc",
		icon_xy = {
			6,
			5
		}
	}
	self.skills.fast_fire = {
		{
			upgrades = {
				"weapon_knock_down_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"weapon_piercing_chance"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_fast_fire_beta",
		desc_id = "menu_fast_fire_beta_desc",
		icon_xy = {
			6,
			13
		}
	}
	self.skills.tower_defense = {
		{
			upgrades = {
				"sentry_gun_quantity_1",
				"player_level_interaction_timer_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"sentry_gun_quantity_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_tower_defense_beta",
		desc_id = "menu_tower_defense_beta_desc",
		icon_xy = {
			9,
			5
		}
	}
	self.skills.juggernaut = {
		{
			upgrades = {
				"player_armor_multiplier",
				"player_armor_movement_penalty_reduction",
				"player_recovery_speed_tier_infamy" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"body_armor6",
				"player_iron_man_damage_redirect_bonus"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_juggernaut_beta",
		desc_id = "menu_juggernaut_beta_desc",
		icon_xy = {
			3,
			1
		}
	}
	self.skills.control_freak = {
		{
			upgrades = {
				"player_convert_enemies_damage_multiplier_1",
				"player_passive_convert_enemies_health_multiplier_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_convert_enemies_damage_multiplier_2",
				"player_passive_convert_enemies_health_multiplier_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_control_freak_beta",
		desc_id = "menu_control_freak_beta_desc",
		icon_xy = {
			1,
			10
		}
	}
	self.skills.heavy_impact = {
		{
			upgrades = {
				"player_killshot_close_panic_chance_mindbreaker_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_killshot_close_panic_chance_mindbreaker_2",
				"mindbreaker_damage"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_wolverine_beta",
		desc_id = "menu_wolverine_beta_desc",
		icon_xy = {
			7,
			0
		}
	}
	self.skills.combat_medic = {
		{
			upgrades = {
				"player_revive_damage_reduction_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_dmg_red_revived",
				"player_bonus_healing"
			},
			cost = self.costs.pro
		},
		name_id = "menu_combat_medic_beta",
		desc_id = "menu_combat_medic_beta_desc",
		icon_xy = {
			5,
			7
		}
	}
	self.skills.show_of_force = {
		{
			upgrades = {
				"player_interacting_damage_reduction"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_level_2_armor_addend",
				"player_level_3_armor_addend",
				"player_level_4_armor_addend"
			},
			cost = self.costs.pro
		},
		name_id = "menu_show_of_force_beta",
		desc_id = "menu_show_of_force_beta_desc",
		icon_xy = {
			8,
			9
		}
	}
	self.skills.inspire = {
		{
			upgrades = {
				"player_morale_boost",
				"player_revive_interaction_speed_multiplier" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"cooldown_long_dis_revive_1",
				"player_inspire_stats"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_inspire_beta",
		desc_id = "menu_inspire_beta_desc",
		icon_xy = {
			4,
			9
		}
	}
	self.skills.single_shot_ammo_return = {
		{
			upgrades = {
				"snp_graze_damage_1",
				"player_revive_interaction_speed_multiplier" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"snp_graze_damage_2",
				"player_graze_critical_damage_mul"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_sniper_graze_damage",
		desc_id = "menu_sniper_graze_damage_desc",
		icon_xy = {
			11,
			9
		}
	}
	self.skills.fast_learner = {
		{
			upgrades = {
				"player_revive_damage_reduction_level_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_revive_damage_reduction_level_2"
			},
			cost = self.costs.pro
		},
		name_id = "menu_fast_learner_beta",
		desc_id = "menu_fast_learner_beta_desc",
		icon_xy = {
			0,
			10
		}
	}
	self.skills.insulation = {
		{
			upgrades = {
				"player_taser_malfunction_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_taser_self_shock",
				"player_escape_taser_1",
				"player_escape_taser_redirect"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_insulation_beta",
		desc_id = "menu_insulation_beta_desc",
		icon_xy = {
			3,
			5
		}
	}
	self.skills.bloodthirst = {
		{
			upgrades = {
				"player_shorter_melee_delay",
				"player_shorter_melee_cooldown"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_melee_armor_restore"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_bloodthirst",
		desc_id = "menu_bloodthirst_desc",
		icon_xy = {
			7,
			8
		}
	}
	self.skills.wolverine = {
		{
			upgrades = {
				"player_ber_swap_speed",
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_melee_damage_stacking_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_heavy_impact_beta",
		desc_id = "menu_heavy_impact_beta_desc",
		icon_xy = {
			11,
			6
		}
	}
	self.skills.frenzy = {
		{
			upgrades = {
				"player_healing_reduction_1",
				"player_health_damage_reduction_1",
				"player_max_health_reduction_1",
				"player_reload_speed_tier_infamy" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_healing_reduction_2",
				"player_health_damage_reduction_2",
				"player_max_health_reduction_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_frenzy",
		desc_id = "menu_frenzy_desc",
		icon_xy = {
			11,
			8
		}
	}
	self.skills.ecm_2x = {
		{
			upgrades = {
				"ecm_jammer_quantity_increase_1",
				"player_tier_bonus_stamina_multiplier",
				"player_tier_bonus_movement_speed",
				"player_increase_ms_stamina_infamy"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"ecm_jammer_duration_multiplier_2",
				"ecm_jammer_feedback_duration_boost_2",
				"ecm_jammer_affects_pagers"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_ecm_2x_beta",
		desc_id = "menu_ecm_2x_beta_desc",
		icon_xy = {
			3,
			4
		}
	}
	self.skills.martial_arts = {
		{
			upgrades = {
				"player_non_special_melee_multiplier_1",
				"player_melee_damage_multiplier_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_melee_knockdown_mul"
			},
			cost = self.costs.pro
		},
		name_id = "menu_martial_arts_beta",
		desc_id = "menu_martial_arts_beta_desc",
		icon_xy = {
			4,
			0
		}
	}
	self.skills.drop_soap = {
		{
			upgrades = {
				"player_melee_damage_health_ratio_multiplier",
				"player_damage_health_ratio_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_damage_health_ratio_multiplier_2",
				"player_melee_damage_health_ratio_multiplier_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_drop_soap_beta",
		desc_id = "menu_drop_soap_beta_desc",
		icon_xy = {
			2,
			2
		}
	}
	self.skills.steroids = {
		{
			upgrades = {
				"player_non_special_melee_multiplier_2"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_melee_damage_multiplier_2"
			},
			cost = self.costs.pro
		},
		name_id = "menu_steroids_beta",
		desc_id = "menu_steroids_beta_desc",
		icon_xy = {
			1,
			3
		}
	}
	self.skills.backstab = {
		{
			upgrades = {
				"player_detection_risk_add_crit_chance_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_detection_risk_add_crit_chance_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_backstab_beta",
		desc_id = "menu_backstab_beta_desc",
		icon_xy = {
			0,
			12
		}
	}
	self.skills.spotter_teamwork = {
		{
			upgrades = {
				"head_shot_ammo_return_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"head_shot_ammo_return_2",
				"head_shot_ammo_return_bonus_damage"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_single_shot_ammo_return_beta",
		desc_id = "menu_single_shot_ammo_return_beta_desc",
		icon_xy = {
			8,
			4
		}
	}
	self.skills.iron_man = {
		{
			upgrades = {
				"team_armor_regen_time_multiplier",
				"player_damage_reduction_to_armor"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_shield_knock",
				"player_base_knock_back_chance"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_iron_man_beta",
		desc_id = "menu_iron_man_beta_desc",
		icon_xy = {
			8,
			10
		}
	}
	self.skills.dire_need = {
		{
			upgrades = {
				"player_armor_depleted_stagger_shot_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_heal_on_dodge"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_dire_need_beta",
		desc_id = "menu_dire_need_beta_desc",
		icon_xy = {
			6,
			12
		}
	}
	self.skills.black_marketeer = {
		{
			upgrades = {
				"player_hostage_health_regen_addend_1",
				"player_revive_interaction_speed_multiplier" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_converts_restore_health"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_black_marketeer_beta",
		desc_id = "menu_black_marketeer_beta_desc",
		icon_xy = {
			7,
			14
		}
	}
	self.skills.jail_diet = {
		{
			upgrades = {
				"player_detection_risk_add_dodge_chance_1",
				"player_tier_bonus_stamina_multiplier",
				"player_increase_ms_stamina_infamy",
				"player_tier_bonus_movement_speed"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_detection_risk_add_dodge_chance_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_jail_diet_beta",
		desc_id = "menu_jail_diet_beta_desc",
		icon_xy = {
			5,
			13
		}
	}
	self.skills.jail_workout = {
		{
			upgrades = {
				"player_buy_bodybags_asset",
				"player_additional_assets",
				"player_cleaner_cost_multiplier",
				"player_buy_spotter_asset"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_standstill_omniscience",
				"player_sec_camera_highlight_mask_off",
				"player_special_enemy_highlight_mask_off"
			},
			cost = self.costs.pro
		},
		name_id = "menu_chameleon_beta",
		desc_id = "menu_jail_workout_beta_desc",
		icon_xy = {
			6,
			10
		}
	}
	self.skills.chameleon = {
		{
			upgrades = {
				"player_mask_off_pickup",
				"player_small_loot_multiplier_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_small_loot_multiplier_2"
			},
			cost = self.costs.pro
		},
		name_id = "menu_jail_workout_beta",
		desc_id = "menu_chameleon_beta_desc",
		icon_xy = {
			3,
			13
		}
	}
	self.skills.joker = {
		{
			upgrades = {
				"player_convert_enemies",
				"player_convert_enemies_max_minions_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_minion_master_health_multiplier",
				"player_convert_enemies_health_mul",
				"player_convert_enemies_max_minions_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_joker_beta",
		desc_id = "menu_joker_beta_desc",
		icon_xy = {
			6,
			8
		}
	}
	self.skills.cable_guy = {
		{
			upgrades = {
				"player_intimidate_range_mul",
				"player_intimidate_aura",
				"player_civ_intimidation_mul"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_convert_enemies_interaction_speed_multiplier"
			},
			cost = self.costs.pro
		},
		name_id = "menu_cable_guy_beta",
		desc_id = "menu_cable_guy_beta_desc",
		icon_xy = {
			2,
			8
		}
	}
	self.skills.awareness = {
		{
			upgrades = {
				"player_movement_speed_multiplier",
				"player_climb_speed_multiplier_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_can_free_run",
				"player_run_and_reload"
			},
			cost = self.costs.pro
		},
		name_id = "menu_awareness_beta",
		desc_id = "menu_awareness_beta_desc",
		icon_xy = {
			10,
			6
		}
	}
	self.skills.scavenging = {
		{
			upgrades = {
				"player_increased_pickup_area_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_double_drop_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_scavenging_beta",
		desc_id = "menu_scavenging_beta_desc",
		icon_xy = {
			8,
			11
		}
	}
	self.skills.expert_handling = {
		{
			upgrades = {
				"pistol_stacked_accuracy_bonus_1",
				"pistol_stacked_reload_bonus"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"pistol_pierce_chance",
				"pistol_fall_off_range_inc"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_expert_handling",
		desc_id = "menu_expert_handling_desc",
		icon_xy = {
			7,
			12
		}
	}
	self.skills.overkill = {
		{
			upgrades = {
				"player_overkill_damage_multiplier",
				"player_recovery_speed_tier_infamy" --tier bonus
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_overkill_damage_multiplier_2",
				"shotgun_consume_no_ammo_chance_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_overkill_beta",
		desc_id = "menu_overkill_beta_desc",
		icon_xy = {
			3,
			2
		}
	}
	self.skills.shotgun_impact = {
		{
			upgrades = {
				"shotgun_recoil_index_addend",
				"shotgun_damage_multiplier_1"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"shotgun_damage_multiplier_2",
				"shotgun_pierce_chance"
			},
			cost = self.costs.pro
		},
		name_id = "menu_shotgun_impact_beta",
		desc_id = "menu_shotgun_impact_beta_desc",
		icon_xy = {
			4,
			1
		}
	}
	self.skills.underdog = {
		{
			upgrades = {
				"player_damage_multiplier_outnumbered"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_damage_dampener_outnumbered"
			},
			cost = self.costs.pro
		},
		name_id = "menu_underdog_beta",
		desc_id = "menu_underdog_beta_desc",
		icon_xy = {
			2,
			1
		}
	}
	self.skills.portable_saw = {
		{
			upgrades = {
				"saw_secondary"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"saw_extra_ammo_multiplier",
				"player_saw_speed_multiplier_2",
				"saw_lock_damage_multiplier_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_portable_saw_beta",
		desc_id = "menu_portable_saw_beta_desc",
		icon_xy = {
			0,
			1
		}
	}
	self.skills.carbon_blade = {
		{
			upgrades = {
				"saw_enemy_slicer",
				"saw_consume_no_ammo_chance_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"saw_ignore_shields_1",
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_carbon_blade_beta",
		desc_id = "menu_carbon_blade_beta_desc",
		icon_xy = {
			0,
			2
		}
	}
	self.skills.far_away = {
		{
			upgrades = {
				"shotgun_fall_off_range_inc"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"shotgun_steelsight_accuracy_inc_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_far_away_beta",
		desc_id = "menu_far_away_beta_desc",
		icon_xy = {
			8,
			5
		}
	}
	self.skills.gun_fighter = {
		{
			upgrades = {
				"pistol_fire_rate_multiplier"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"pistol_damage_addend_1"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_gun_fighter_beta",
		desc_id = "menu_gun_fighter_beta_desc",
		icon_xy = {
			7,
			11
		}
	}
	self.skills.dance_instructor = {
		{
			upgrades = {
				"pistol_magazine_capacity_inc_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"pistol_magazine_capacity_inc_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_dance_instructor",
		desc_id = "menu_dance_instructor_desc",
		icon_xy = {
			11,
			0
		}
	}
	self.skills.akimbo = {
		{
			upgrades = {
				"akimbo_recoil_index_addend_2"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"akimbo_extra_ammo_multiplier_1",
				"akimbo_recoil_index_addend_3"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_akimbo_skill_beta",
		desc_id = "menu_akimbo_skill_beta_desc",
		icon_xy = {
			3,
			11
		}
	}
	self.skills.prison_wife = {
		{
			upgrades = {
				"player_headshot_regen_armor_bonus_1"
			},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"player_headshot_regen_armor_bonus_2",
				"player_headshot_armor_speed_up"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_prison_wife_beta",
		desc_id = "menu_prison_wife_beta_desc",
		icon_xy = {
			6,
			11
		}
	}
	self.skills.pack_mule = {
		{
			upgrades = {
				"carry_throw_distance_multiplier",
				"player_run_with_any_bag"
			},
			cost = self.costs.default
		},
		{
			upgrades = {
				"player_armor_carry_bonus_1",
				"player_jump_modifier_mul"
			},
			cost = self.costs.pro
		},
		name_id = "menu_pack_mule_beta",
		desc_id = "menu_pack_mule_beta_desc",
		icon_xy = {
			8,
			8
		}
	}

end)