tweak_data.hud_icons.quick_fix_stymulant = {
    texture = "guis/textures/pd2/equipment_02",
    texture_rect = {
        96,
        0,
        32,
        32
    }
}
tweak_data.hud_icons.bite_the_bullet_ability = {
    texture = "guis/dlcs/berserk/textures/pd2/hud_bite_the_bullet_ability",
    texture_rect = {
        0,
        0,
        32,
        32
    }
}

tweak_data.hud_icons.archer_ability = {
    texture = "guis/textures/pd2/equipment_02",
    texture_rect = {
        96,
        0,
        32,
        32
    }
}
tweak_data.hud_icons.bleed_ability = {
    texture = "guis/dlcs/bleed/textures/pd2/bleed_ability",
    texture_rect = {
        0,
        0,
        32,
        32
    }
}
tweak_data.hud_icons.wraith_form = {
    texture = "guis/dlcs/wraith/textures/pd2/wraith_form",
    texture_rect = {
        0,
        0,
        32,
        32
    }
}