Hooks:PostHook(UpgradesTweakData, "init", "Skills_Overhaul_2021f", function(self)
	self.values.player.body_armor = {
		armor = {
			0,
			1.5,
			3,
			4,
			6,
			7,
			13
		},
		movement = {
			1.0,
			0.99,
			0.95,
			0.9,
			0.8,
			0.7,
			0.55
		},
		concealment = {
			30,
			28,
			23,
			21,
			18,
			12,
			1
		},
		dodge = {
			0.05,
			0,
			-0.1,
			-0.15,
			-0.2,
			-0.25,
			-0.55
		},
		damage_shake = {
			2.2,
			1,
			0.8,
			0.7,
			0.6,
			0.5,
			0.4
		},
		stamina = {
			1.025,
			1,
			0.95,
			0.9,
			0.85,
			0.8,
			0.7
		},
		defense = {
			0,
			40,
			100,
			140,
			180,
			240,
			300
		}
	}
	self.values.player.body_armor.skill_max_health_store = {
		14,
		13.5,
		12.5,
		12,
		10.5,
		9.5,
		4
	}
	self.values.player.body_armor.skill_kill_change_regenerate_speed = {
		14,
		13.5,
		12.5,
		12,
		10.5,
		9.5,
		4
	}
	self.values.player.body_armor.skill_ammo_mul = {
		1,
		1.02,
		1.04,
		1.06,
		1.8,
		1.1,
		1.12
	}
    self.values.trip_mine.damage_multiplier = {
		2
	}
	self.values.trip_mine.quantity = {
		5,
		13
	}
    self.values.trip_mine.explosion_size_multiplier_1 = {
		1.2
	}
    self.values.shape_charge.quantity = {
		2,
		4
	}
    self.values.player.trip_mine_deploy_time_multiplier = {
		0.8,
		0.5
	}
	self.values.weapon.silencer_damage_multiplier = {
		1.1,
		1.14
	}
	self.values.weapon.silencer_spread_index_addend = {
		2
	}
	self.values.weapon.silencer_recoil_index_addend = {
		2
	}
	self.values.weapon.silencer_enter_steelsight_speed_multiplier = {
		2.25
	}
    self.values.player.flashbang_multiplier = {
		0.8,
		0.25
	}
    self.values.temporary.damage_speed_multiplier = {
		{
			1.15,
			5
		}
	}
    self.values.player.drill_speed_multiplier = {
		0.85,
		0.6
	}
    self.values.player.drill_autorepair_1 = {
		0.20
	}
	self.values.player.drill_autorepair_2 = {
		0.35
	}
    self.values.player.cheat_death_chance = {
		0.25,
		0.45
	}
	self.values.weapon.automatic_head_shot_add = {
		0.4,
		0.75
	}
	self.values.snp.graze_damage = {
		{
			radius = 80,
			damage_factor = 0.2,
			damage_factor_headshot = 0.4
		},
		{
			radius = 120,
			damage_factor = 0.4,
			damage_factor_headshot = 0.8
		}
	}
	self.values.player.messiah_revive_from_bleed_out = {
		1,
		2
	}
	self.values.temporary.increased_movement_speed = {
		{
			1.3,
			5
		}
	}
	self.values.player.revived_health_regain = {
		1.2
	}
	self.values.player.revived_health_regain_cheat_death = {
		1.3
	}
	self.values.player.pick_up_ammo_multiplier = {
		1.15,
		1.75
	}
	self.values.player.pick_up_ammo_multiplier_skill = {
		1.2,
		1.4
	}
	self.values.player.deploy_interact_faster = {
		0.5
	}
	self.values.player.marked_inc_dmg_distance = {
		{
			1000,
			1.4
		}
	}
	self.values.player.marked_enemy_damage_mul = 1.15
	self.values.player.healing_reduction = {
		0.25,
		0
	}
	self.values.player.health_damage_reduction = {
		0.85,
		0.75
	}
	self.values.player.head_shot_ammo_return = {
		{
			headshots = 4,
			ammo = 1,
			time = 9999999999999999999 --not used
		},
		{
			headshots = 3,
			ammo = 1,
			time = 9999999999999999999 --not used
		}
	}
	self.values.player.head_shot_ammo_return_bonus_damage = {
		1.5
	}
	self.values.player.level_interaction_timer_multiplier = {
		{
			0.01, --% per infamy rank
			5, --per >what< infamy rank,
			0.7, --max
			0.9 --flat %
		}
	}
	self.values.pistol.stacking_hit_damage_multiplier = {
		{
			max_stacks = 3,
			max_time = 5,
			damage_bonus = 0.14
		},
		{
			max_stacks = 3,
			max_time = 5,
			damage_bonus = 0.2
		}
	}
	self.values.pistol.stacked_accuracy_bonus = {
		{
			max_stacks = 3,
			accuracy_bonus = 0.1,
			max_time = 5
		}
	}
	self.values.pistol.stacked_reload_bonus = {
		true
	}
	self.values.pistol.reload_speed_multiplier = {
		1.5
	}
	self.values.team.damage = {
		hostage_absorption = {
			0.4
		},
		hostage_absorption_limit = 5
	}
	self.values.player.double_drop = {
		4
	}
	self.values.sentry_gun.damage_multiplier = {
		1.15,
		1.25
	}
	self.values.sentry_gun.rot_speed_multiplier = {
		2
	}
	self.values.sentry_gun.extra_ammo_multiplier = {
		2
	}
	self.values.player.automatic_faster_reload = {
		{
			min_reload_increase = 1.1,
			penalty = 1.3,
			target_enemies = 2,
			min_bullets = 20,
			max_reload_increase = 1.6
		}
	}
	self.values.assault_rifle.reload_speed_multiplier = {
		1.15
	}
	self.values.smg.reload_speed_multiplier = {
		1.15
	}
	self.values.snp.reload_speed_multiplier = {
		1.15
	}
	self.values.player.mark_enemy_time_multiplier = {
		1.5,
		2
	}
	self.values.player.armor_multiplier = {
		1.4
	}
	self.values.player.iron_man_damage_redirect_bonus = {
		0.1
	}
	self.values.sentry_gun.armor_multiplier2 = {
		1.5,
		2
	}
	self.values.player.bleed_out_health_multiplier = {
		1.4,
		1.8
	}
	self.values.player.instakill_downed = {
		0.15
	}
	self.values.player.damage_reduction_downed = {
		0.2
	}
	self.values.temporary.berserker_damage_multiplier = {
		{
			1,
			2.5
		},
		{
			1,
			3.75
		}
	}
	self.values.temporary.reload_weapon_faster = {
		{
			1.3,
			10
		}
	}
	self.values.player.bleed_out_time = {
		10
	}
	self.values.player.hostage_health_regen_addend = {
		{
			0.02,
			2.5,
			2
		}
	}
	self.values.player.minion_dmg_red = {
		{
			0.01,
			5,
			0.18
		}
	}
	self.berserker_movement_speed_multiplier = 0.5
	self.values.player.minion_master_health_multiplier = {
		1.2
	}
	self.values.weapon.knock_down = {
		0.25
	}
	self.values.player.melee_damage_health_ratio_multiplier = {
		1.8,
		2.25
	}
	self.values.player.damage_health_ratio_multiplier = {
		0.6,
		0.8
	}
	self.values.cooldown.long_dis_revive = {
		{
			0.6,
			15
		}
	}
	self.values.weapon.piercing_chance = {
		0.35
	}
	self.values.player.revive_interaction_speed_multiplier = {
		{
			0.75, --flat
			0.02, --per infamy rank
			3, --per >what< infamy rank
			0.5 --max
		}
	}
	self.values.temporary.revive_damage_reduction = {
		{
			1,
			12
		}
	}
	self.values.player.revive_damage_reduction = {
		0.8
	}
	self.morale_boost_speed_bonus = 1.15
	self.morale_boost_reload_speed_bonus = 1.15
	self.morale_boost_time = 20
	self.values.temporary.revived_damage_resist = {
		{
			0.6,
			5
		}
	}
	self.values.player.interacting_damage_reduction = {
		{
			base = 0.8,
			max_bonus = 0.5
		}
	}
	self.morale_boost_base_cooldown = 8
	self.values.player.taser_malfunction = {
		{
			interval = 1,
			chance_to_trigger = 0.35,
			bonus_movement_speed = 1.08,
			bonus_duration = 4
		}
	}
	self.values.player.escape_taser = {
		2
	}
	self.values.player.escape_taser_redirect = {
		300
	}
	self.values.player.melee_damage_dampener = {
		0.5
	}
	self.values.player.max_health_reduction = {
		0.3,
		0.15
	}
	self.values.player.melee_damage_stacking = {
		{
			max_multiplier = 15,
			melee_multiplier = 0.8
		}
	}
	self.values.player.melee_kill_increase_reload_speed = {
		{
			1.3,
			8
		}
	}
	self.values.player.melee_knockdown_mul = {
		1.5
	}
	self.values.player.non_special_melee_multiplier = {
		1.25,
		2.25
	}
	self.values.player.melee_damage_multiplier = {
		1.25,
		2.25
	}
	self.values.player.killshot_close_panic_chance_mindbreaker = {
		0.2,
		0.35
	}
	self.values.player.mindbreaker_damage = {
		0.03
	}
	self.values.player.detection_risk_add_crit_chance = {
		{
			0.02,
			3,
			"below",
			40,
			0.3
		},
		{
			0.02,
			1,
			"below",
			40,
			0.5
		}
	}
	self.values.team.armor.regen_time_multiplier = {
		0.65
	}
	self.values.player.passive_loot_drop_multiplier = {
		1.1,
		1.65
	}
	self.values.player.small_loot_multiplier = {
		1.15,
		1.35
	}
	self.values.player.minion_master_speed_multiplier = {
		1.07
	}
	self.values.player.increased_pickup_area = {
		1.35
	}
	self.values.player.ber_swap_speed = {
		1.66
	}
	self.values.player.passive_convert_enemies_health_multiplier = {
		0.6,
		0.2
	}
	self.values.temporary.overkill_damage_multiplier = {
		{
			1.35,
			20
		},
		{
			1.65,
			20
		}
	}
	self.values.shotgun.consume_no_ammo_chance2 = {
		0.15
	}
	self.values.temporary.dmg_multiplier_outnumbered = {
		{
			1.08,
			10
		}
	}
	self.values.temporary.dmg_dampener_outnumbered = {
		{
			0.92,
			10
		}
	}
	self.values.shotgun.damage_multiplier = {
		1.05,
		1.1
	}
	self.values.saw.lock_damage_multiplier = {
		1.2,
		1.6
	}
	self.values.saw.enemy_slicer = {
		5
	}
	self.values.saw.consume_no_ammo_chance = {
		0.08
	}
	self.low_hp = 0.3
	-- self.health_okay = 0.75 NOT USED
	self.health_medium = 0.5
	self.values.shotgun.steelsight_accuracy_inc = {
		0.4
	}
	self.values.pistol.damage_addend = {
		1.5,
		1.6
	}
	self.values.player.regain_throwable_from_ammo = {
		{
			chance = 0.05,
			chance_inc = 0.01
		}
	}
	self.values.player.drill_fix_interaction_speed_multiplier = {
		0.75,
		0.5
	}
	self.values.player.bullet_storm_base_dr = {
		3,
		5
	}
	self.values.player.quick_fix = {
		{
			duration = 4,
		 	heal = 0.015,
			delay = 0.5,
			ms_duration = 1.25,
			inv_duration = 0.5,
			movement_speed = 1.2,
			heal_interval = 0.25,
			missing_health = 0.04
		}
	}
	self.values.player.qf_bonus_movement_speed = {
		0.2
	}
	self.qf_bonus_ms_duration = 0.75
	self.values.player.qf_invulnerable_inc = {
		2.5 -- 0.025s
	}
	self.values.player.qf_healing_period_extended = {
		0.5
	}
	self.values.player.qf_interacting_cooldown_decrease = {
		{
			threshold = 0.5,
			amount = 0.09
		}
	}
	self.values.player.bonus_from_crit = {
		0.3,
		0.5,
		0.7
	}
	self.values.player.bs_hp_gf = {
		1.3
	}
	self.values.player.qf_stamina_regen = {
		{
			bonus = 0.5
		}
	}
	self.values.player.execution_crit = {
		0.005,
		0.02
	}
	self.values.player.execution_crit_scaling = {
		0.05,
		0.1
	}
	self.values.player.bonus_crit_chanc = {
		0.2
	}
	self.values.player.crit_heal = {
		{
			heal = 0.05,
			cap = 2,
			occurs = 3
		}
	}
	self.values.player.execution_immunity = {
		{
			immunity_duration = 1.5,
			reduction = 0.1
		}
	}
	self.values.player.bonus_healing = {
		1.4
	}
	self.values.player.dmg_red_revived = {
		{
			duration = 60,
			reduction = 0.95
		}
	}
	self.first_aid_kit = {
		revived_damage_reduction = {
			{
				0.7,
				5
			},
			{
				0.4,
				10
			}
		}
	}
	self.values.player.passive_headshot_damage_multiplier = { -- passive_critical_hit_chance
		0.05
	}
	self.values.player.heal_on_dodge = {
		{
			heal = 0.005,
			duration = 2,
			cooldown = 7,
			increased = 0.01
		}
	}
	self.values.player.inspire_stats = {
		{
			penalty = 0.05,
			min = 0.6,
			max = 1
		}
	}
	self.values.player.weapon_accuracy_increase = {
		1,
		2
	}
	self.values.player.stability_increase_bonus_1 = {
		1
	}
	self.values.player.stability_increase_bonus_2 = {
		1
	}
	self.values.pistol.pierce_chance = {
		0.3
	}
	self.values.pistol.magazine_capacity_inc = {
		5,
		15
	}
	self.values.pistol.enter_steelsight_speed_multiplier = {
		2
	}
	self.values.player.passive_critical_hit_chance = {
		0.15
	}
	self.values.player.exceed_crit_scaling = {
		0.15,
		0.25
	}
	self.values.player.bonus_crit_damage = {
		3,
		2
	}
	self.values.player.crit_dmg_per_chanc = {
		0.02,
		0.02
	}
	self.values.player.crit_multiplier = {
		1.4,
		1.7
	}
	self.critical_bonus_damage_threshold = 0.3
	self.values.player.bite_the_bullet_base = {
		{
			duration = 8,
			damage_reduction = 0.6,
			armor_convert = 1,
			shockwave_dmg = 0.3,
			shockwave_range = 1200
		}
	}
	self.values.player.bite_the_bullet_stored_damage = {
		0.04,
		0.08
	}
	self.values.player.bonus_duration_bite_the_bullet = {
		4
	}
	self.values.player.reduce_cooldown_bite_the_bullet = {
		0.08
	}
	self.values.player.shockwave_bonus_damage = {
		0.2
	}
	self.values.player.shockwave_healing = {
		0.15
	}
	self.shockwave_cooldown = 0.3
	self.values.player.last_shockwave_range = {
		800
	}
	self.values.player.level_2_armor_addend = {
		2
	}
	self.values.player.level_3_armor_addend = {
		2
	}
	self.values.player.level_4_armor_addend = {
		2
	}
	self.values.player.style_rank = {
		{
			duration = 8,
			bonus_ms = 0.035,
			bonus_dmg = 0.8,
			max_stacks = 6
		}
	}
	self.values.player.style_lifesteal = {
		0.005
	}
	self.values.player.style_lifesteal_increase = {
		1.5
	}
	self.values.player.bonus_hp_enemy_damage_style = {
		0.02
	}
	self.values.player.automatically_reload_style = {
		15
	}
	self.values.player.daredevil_damage_absorption = {
		50
	}
	self.values.player.style_pierce_body_armor = {
		0.05
	}
	self.values.player.style_rate_of_fire = {
		0.0333
	}
	self.values.player.weapon_swap_speed_card = {
		1.15
	}
	self.values.player.critical_damage_card = {
		0.1
	}
	self.values.player.reload_speed_tier_infamy = {
		{
			0.01, --% per infamy rank
			10, --per >what< infamy rank,
			0.15, --max
			0.05 --flat %
		}
	}
	self.values.player.increase_ms_stamina_infamy = {
		{
			0.005, --% per infamy rank
			10, --per >what< infamy rank,
			0.05, --max
			0 --flat %
		}
	}
	self.values.player.recovery_speed_tier_infamy = {
		{
			0.025, --% per infamy rank
			5, --per >what< infamy rank,
			0.85, --max
			0.95 --flat %
		}
	}
	self.values.player.tier_bonus_stamina_multiplier = {
		1.15
	}
	self.values.player.tier_bonus_movement_speed = {
		1.02
	}
	self.values.player.walk_speed_multiplier = {
		1
	}
	self.values.player.run_speed_multiplier = {
		1.1
	}
	self.values.player.archer_base = {
		{
			bonus_reload_speed = 0.08,
			max_time = 8,
			max_stacks = 5
		}
	}
	self.values.player.archer_reload = {
		{
			bonus_rate_of_fire = 5, -- +1
			duration = 8,
			extend = 1.25,
			max_extend = 4
		}
	}
	self.values.player.archer_dodge_bonus = {
		0.2,
		0.4,
		0.6
	}
	self.values.player.archer_heal_on_headshot = {
		{
			1,
			2
		}
	}
	self.values.player.armor_depleted_stagger_shot = {
		8,
		6
	}
	self.values.player.archer_bolt_aoe = {
		{
			radius = 500,
			damage = 0.2
		}
	}
	self.values.player.archer_pierce_through = {
		1
	}
	self.values.player.bleed_bullets = {
		{
			damage = 1, -- * dot_tick_period
			max_health_damage = 0.02, -- * dot_tick_period
			duration = 10,
			cooldown = 20,
			anim_chance = 0,
			dot_tick_period = 0.5
		}
	}
	self.values.player.bleed_critical_hit_chance = {
		0.1
	}
	self.values.player.bleeding_enemy_dmg_red = {
		0.9
	}
	self.values.player.bleed_critical_hit = {
		1.5
	}
	self.values.player.bleed_ability_base = {
		{
			heal = 0.1,
			overheal = 1.5,
			overheal_duration = 30,
			heal_effectiveness_three_enemies = 0.3,
			heal_effectiveness_six_enemies = 0.15,
			per_enemy_cooldown_reduction = 1.75,
			per_enemy_cooldown_reduction_duration = 4
		}
	}
	self.values.player.bleed_bonus_damage_taken = {
		1.1
	}
	self.values.player.bleeding_vamp = {
		0.05
	}
	self.values.player.defense_up_scav = {
		{
			1.2,
			3
		}
	}
	self.values.player.anarchist_damage_redirect = {
		1
	}
	self.values.player.passive_always_regen_armor = {
		8
	}
	self.values.player.damage_reduction_to_armor = {
		0.8
	}
	self.values.player.armor_movement_penalty_reduction = {
		0.9
	}
	self.values.player.headshot_regen_armor_bonus = {
		1,
		2
	}
	self.values.player.base_knock_back_chance = {
		{
			0.5,
			0.2
		}
	}
	self.values.shotgun.fall_off_range_inc = {
		1.5
	}
	self.values.player.passive_health_regen = {
		0.02
	}
	self.values.player.armor_regen_damage_health_ratio_multiplier = {
		0.1,
		0.25,
		0.4
	}
	self.values.temporary.single_shot_fast_reload = {
		{
			1.5,
			4
		}
	}
	self.values.player.pick_lock_easy_speed_multiplier = {
		0.8,
		0.5
	}
	self.values.player.lockpicking_genius = {
		8
	}
	self.values.player.lockpicking_bonus_ms = {
		{
			1.1,
			8
		}
	}
	self.values.player.armor_regen_time_mul = {
		0.95
	}
	self.values.team.crew_dmgmul = {
		{
			1.1,
			1.25,
			1.4
		}
	}
	self.values.team.crew_bag_movement = {
		{
			1.1,
			1.2,
			1.3
		}
	}
	self.values.player.swan_song_basic = {
		{
			health_restore = 1,
			health_decay_per_tick = 0.05,
			tick_period = 0.35,
			damage_reduction = 0.05,
			cooldown = 20
		}
	}
	self.values.player.swan_song_ace = {
		{
			enemies_kill_req = 5,
			set_health = 0.2,
			reduced_cooldown = 20
		}
	}
	self.values.pistol.critical_hit_bonus_dmg = {
		0.4
	}
	self.values.player.graze_critical_damage_mul = {
		1.25
	}
	self.values.player.melee_armor_restore = {
		{
			0.1,
			0.2
		}
	}
	self.values.player.shorter_melee_delay = {
		0.5
	}
	self.values.player.shorter_melee_cooldown = {
		0.75
	}
	self.values.player.critical_silenced = {
		0.25,
		0.6
	}
	self.values.player.extra_ammo_multiplier = {
		1.3
	}
	self.values.team.armor.regen_time_multiplier = {
		0.8
	}
	self.brief_slow_swan_song = 0.3
	self.values.shotgun.pierce_chance = {
		0.3
	}
	self.values.player.bonus_critical_damage = {
		0.25,
		0.4
	}
	self.values.player.wild_health_amount = {
		0.5
	}
	self.values.player.wild_armor_amount = {
		0.1
	}
	self.values.player.less_armor_wild_cooldown = {
		{
			0.1, -- % missing
			0.5  -- cd reduction
		}
	}
	self.values.player.less_health_wild_cooldown = {
		{
			0.1, -- % missing
			0.5 -- cd reduction
		}
	}
	self.wild_trigger_time = 8
	self.wild_max_triggers_per_time = 8
	self.values.player.wild_maxed_damage_reduction = {
		0.9
	}
	self.values.player.headshot_armor_speed_up = {
		0.1
	}
	self.values.player.damage_defense_multiplier = {
		2
	}
	self.bleed_ability_heal_cap_per_enemy = 10
	self.values.player.damage_defense_effi = {
		0.75
	}
	self.values.player.converts_restore_health = {
		0.75
	}
	self.values.player.convert_enemies_damage_multiplier = {
		1.25,
		1.5
	}
	self.values.player.convert_enemies_health_mul = {
		1.2
	}
	self.values.pistol.fall_off_range_inc = {
		1.25
	}
	self.values.player.jump_modifier_mul = {
		1.5
	}
	self.values.player.run_with_any_bag = {
		true
	}
	self.values.player.armor_carry_bonus = {
		0.0175
	}
	self.values.player.wraith_form_base = {
		{
			passive_drain_interval = 2.5,
			passive_energy_drain = -10,
			energy = 200,
			wraith_attack_range = 150,
			energy_drain_table = {
				10,
				25,
				30,
				60,
				80,
				100
			},
			mark_range = 2000,
			health_on_kill = 0.04,
			bonus_ms_on_kill = 1.4,
			bonus_ms_duration = 1.25
		}
	}
	self.values.player.semi_wraith_bonus_ms = {
		1.2
	}
	self.values.player.semi_wraith_base = {
		{
			cooldown = 8,
			duration = 4.25
		}
	}
	self.values.player.semi_wraith_heal_bullet = {
		{
			chance = 0.5,
			max_heal = 10,
			heal_from_bullet_damage = 0.4
		}
	}
	self.values.player.semi_wraith_bonus_max_hp = {
		1.2
	}
	self.values.player.semi_wraith_bonus_duration = {
		2.25
	}
	self.values.player.semi_wraith_lifeline = {
		{
			health_set = 1,
			cooldown = 60
		}
	}
	self.values.player.dmr_bonus_damage_health = {
		0.03
	}



	self.max_damage_knock_back = 20
	self.percentage_headshot_damage = 0.1
	self.headshot_damage_cap = 150
	self.execution = (9808432 * 9808432)
	self.critical_base_damage = 1.75
	self.lifesteal_damage_threshold = 0.025
	self.projectiles_hs_damage = 2
	self.percentage_headshot_damage_minimal = 0.5
	self.sniper_ds_defense_debuff = { 8, 0.5 }


	self.difficulty_adjustments = {
		overkill = 0.95,
		overkill_145 = 0.95,
		easy_wish = 0.95,
		overkill_290 = 0.9,
		sm_wish = 0.8
	}


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS--  --DEFINITIONS-- --DEFINITIONS--
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	self.definitions.player_damage_defense_multiplier = {
		name_id = "menu_player_damage_defense_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "damage_defense_multiplier",
			category = "player"
		}
	}
	self.definitions.player_damage_defense_effi = {
		name_id = "menu_player_damage_defense_effi",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "damage_defense_effi",
			category = "player"
		}
	}
	self.definitions.player_dmr_bonus_damage_health = {
		name_id = "menu_player_dmr_bonus_damage_health",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "dmr_bonus_damage_health",
			category = "player"
		}
	}
	self.definitions.player_semi_wraith_lifeline = {
		name_id = "menu_player_semi_wraith_lifeline",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "semi_wraith_lifeline",
			category = "player"
		}
	}
	self.definitions.player_semi_wraith_bonus_duration = {
		name_id = "menu_player_semi_wraith_bonus_duration",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "semi_wraith_bonus_duration",
			category = "player"
		}
	}
	self.definitions.player_semi_wraith_bonus_max_hp = {
		name_id = "menu_player_semi_wraith_bonus_max_hp",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "semi_wraith_bonus_max_hp",
			category = "player"
		}
	}
	self.definitions.player_semi_wraith_heal_bullet = {
		name_id = "menu_player_semi_wraith_heal_bullet",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "semi_wraith_heal_bullet",
			category = "player"
		}
	}
	self.definitions.wraith_form = {
		category = "grenade"
	}
	self.definitions.player_semi_wraith_base = {
		name_id = "menu_player_semi_wraith_base",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "semi_wraith_base",
			category = "player"
		}
	}
	self.definitions.player_semi_wraith_bonus_ms = {
		name_id = "menu_player_semi_wraith_bonus_ms",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "semi_wraith_bonus_ms",
			category = "player"
		}
	}
	self.definitions.player_run_with_any_bag = {
		name_id = "menu_player_run_with_any_bag",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "run_with_any_bag",
			category = "player"
		}
	}
	self.definitions.player_wraith_form_base = {
		name_id = "menu_player_wraith_form_base",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "wraith_form_base",
			category = "player"
		}
	}
	self.definitions.player_jump_modifier_mul = {
		name_id = "menu_player_jump_modifier_mul",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "jump_modifier_mul",
			category = "player"
		}
	}
	self.definitions.pistol_fall_off_range_inc = {
		name_id = "menu_player_fall_off_range_inc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "fall_off_range_inc",
			category = "pistol"
		}
	}
	self.definitions.player_convert_enemies_health_mul = {
		name_id = "menu_player_convert_enemies_health_mul",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "convert_enemies_health_mul",
			category = "player"
		}
	}
	self.definitions.player_bleeding_vamp = {
		name_id = "menu_player_bleeding_vamp",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleeding_vamp",
			category = "player"
		}
	}
	self.definitions.player_converts_restore_health = {
		name_id = "menu_player_converts_restore_health",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "converts_restore_health",
			category = "player"
		}
	}
	self.definitions.player_wild_maxed_damage_reduction = {
		name_id = "menu_player_wild_maxed_damage_reduction",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "wild_maxed_damage_reduction",
			category = "player"
		}
	}
	self.definitions.player_headshot_armor_speed_up = {
		name_id = "menu_player_headshot_armor_speed_up",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "headshot_armor_speed_up",
			category = "player"
		}
	}
	self.definitions.player_bonus_critical_damage_2 = {
		name_id = "menu_player_bonus_critical_damage",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "bonus_critical_damage",
			category = "player"
		}
	}
	self.definitions.player_bonus_critical_damage = {
		name_id = "menu_player_bonus_critical_damage",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_critical_damage",
			category = "player"
		}
	}
	self.definitions.shotgun_pierce_chance = {
		name_id = "menu_shotgun_inspire_stats",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "pierce_chance",
			category = "shotgun"
		}
	}
	self.definitions.player_critical_silenced_2 = {
		name_id = "menu_player_critical_silenced",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "critical_silenced",
			category = "player"
		}
	}
	self.definitions.player_critical_silenced_1 = {
		name_id = "menu_player_critical_silenced",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "critical_silenced",
			category = "player"
		}
	}
	self.definitions.player_shorter_melee_cooldown = {
		name_id = "menu_player_shorter_melee_cooldown",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "shorter_melee_cooldown",
			category = "player"
		}
	}
	self.definitions.player_shorter_melee_delay = {
		name_id = "menu_player_shorter_melee_delay",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "shorter_melee_delay",
			category = "player"
		}
	}
	self.definitions.player_melee_armor_restore = {
		name_id = "menu_player_melee_armor_restore",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "melee_armor_restore",
			category = "player"
		}
	}
	self.definitions.player_graze_critical_damage_mul = {
		name_id = "menu_player_interacting_damage_reduction",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "graze_critical_damage_mul",
			category = "player"
		}
	}
	self.definitions.player_interacting_damage_reduction = {
		name_id = "menu_player_interacting_damage_reduction",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "interacting_damage_reduction",
			category = "player"
		}
	}
	self.definitions.pistol_critical_hit_bonus_dmg = {
		name_id = "menu_pistol_critical_hit_bonus_dmg",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "critical_hit_bonus_dmg",
			category = "pistol"
		}
	}
	self.definitions.player_lockpicking_bonus_ms = {
		name_id = "menu_player_lockpicking_bonus_ms",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "lockpicking_bonus_ms",
			category = "player"
		}
	}
	self.definitions.player_lockpicking_genius = {
		name_id = "menu_player_lockpicking_genius",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "lockpicking_genius",
			category = "player"
		}
	}
	self.definitions.player_pick_lock_easy_speed_multiplier_2 = {
		name_id = "menu_player_pick_lock_easy_speed_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "pick_lock_easy_speed_multiplier",
			category = "player"
		}
	}
	self.definitions.swan_song_basic = {
		name_id = "menu_player_swan_song_basic",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "swan_song_basic",
			category = "player"
		}
	}
	self.definitions.swan_song_ace = {
		name_id = "menu_player_swan_song_ace",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "swan_song_ace",
			category = "player"
		}
	}
	self.crew_ability_definitions.crew_bag_movement = {
		name_id = "menu_crew_bag_movement",
		icon = "crew_bag_movement"
	}
	self.crew_ability_definitions.crew_dmgmul = {
		name_id = "menu_crew_dmgmul",
		icon = "crew_dmgmul"
	}
	self.definitions.mindbreaker_damage = {
		name_id = "menu_player_mindbreaker_damage",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "mindbreaker_damage",
			category = "player"
		}
	}
	self.definitions.head_shot_ammo_return_bonus_damage = {
		name_id = "menu_player_head_shot_ammo_return_bonus_damage",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "head_shot_ammo_return_bonus_damage",
			category = "player"
		}
	}
	self.definitions.shotgun_fall_off_range_inc = {
		name_id = "menu_player_fall_off_range_inc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "fall_off_range_inc",
			category = "shotgun"
		}
	}
	self.definitions.player_base_knock_back_chance = {
		name_id = "menu_player_base_knock_back_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "base_knock_back_chance",
			category = "player"
		}
	}
	self.definitions.player_armor_movement_penalty_reduction = {
		name_id = "menu_player_armor_movement_penalty_reduction",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "armor_movement_penalty_reduction",
			category = "player"
		}
	}
	self.definitions.player_damage_reduction_to_armor = {
		name_id = "menu_player_damage_reduction_to_armor",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "damage_reduction_to_armor",
			category = "player"
		}
	}
	self.definitions.player_anarchist_damage_redirect = {
		name_id = "menu_player_anarchist_damage_redirect",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "anarchist_damage_redirect",
			category = "player"
		}
	}
	self.definitions.player_iron_man_damage_redirect_bonus = {
		name_id = "menu_player_iron_man_damage_redirect_bonus",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "iron_man_damage_redirect_bonus",
			category = "player"
		}
	}
	self.definitions.player_defense_up_scav = {
		name_id = "menu_player_defense_up_scav",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "defense_up_scav",
			category = "player"
		}
	}
	self.definitions.player_revived_health_regain_cheat_death = {
		name_id = "menu_player_revived_health_regain_cheat_death",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "revived_health_regain_cheat_death",
			category = "player"
		}
	}
	self.definitions.player_escape_taser_redirect = {
		name_id = "menu_player_escape_taser_redirect",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "escape_taser_redirect",
			category = "player"
		}
	}
	self.definitions.player_bleed_bonus_damage_taken = {
		name_id = "menu_player_bleed_bonus_damage_taken",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_bonus_damage_taken",
			category = "player"
		}
	}
	self.definitions.player_bleed_critical_hit_chance= {
		name_id = "menu_player_bleed_critical_hit_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_critical_hit_chance",
			category = "player"
		}
	}
	self.definitions.player_bleeding_enemy_dmg_red= {
		name_id = "menu_player_bleeding_enemy_dmg_red",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleeding_enemy_dmg_red",
			category = "player"
		}
	}
	self.definitions.player_bleed_ability_base = {
		name_id = "menu_player_bleed_ability_base",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_ability_base",
			category = "player"
		}
	}
	self.definitions.bleed_ability = {
		category = "grenade"
	}
	self.definitions.player_bleed_critical_hit = {
		name_id = "menu_player_bleed_critical_hit",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_critical_hit",
			category = "player"
		}
	}
	self.definitions.player_bleed_critical_hit_chance = {
		name_id = "menu_player_bleed_critical_hit_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_critical_hit_chance",
			category = "player"
		}
	}
	self.definitions.player_bleed_bullets = {
		name_id = "menu_player_bleed_bullets",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_bullets",
			category = "player"
		}
	}
	self.definitions.player_style_lifesteal_increase = {
		name_id = "menu_player_style_lifesteal_increase",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "style_lifesteal_increase",
			category = "player"
		}
	}
	self.definitions.player_archer_pierce_through = {
		name_id = "menu_archer_pierce_through",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "archer_pierce_through",
			category = "player"
		}
	}
	self.definitions.player_archer_bolt_aoe = {
		name_id = "menu_archer_bolt_aoe",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "archer_bolt_aoe",
			category = "player"
		}
	}
	self.definitions.player_archer_heal_on_headshot = {
		name_id = "menu_archer_heal_on_headshot",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "archer_heal_on_headshot",
			category = "player"
		}
	}
	self.definitions.player_archer_dodge_bonus_3 = {
		name_id = "menu_archer_dodge_bonus",
		category = "feature",
		upgrade = {
			value = 3,
			upgrade = "archer_dodge_bonus",
			category = "player"
		}
	}
	self.definitions.player_archer_dodge_bonus_2 = {
		name_id = "menu_archer_dodge_bonus",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "archer_dodge_bonus",
			category = "player"
		}
	}
	self.definitions.player_archer_dodge_bonus_1 = {
		name_id = "menu_archer_dodge_bonus",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "archer_dodge_bonus",
			category = "player"
		}
	}
	self.definitions.player_archer_reload = {
		name_id = "menu_archer_reload",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "archer_reload",
			category = "player"
		}
	}
	self.definitions.player_archer_base = {
		name_id = "menu_archer_base",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "archer_base",
			category = "player"
		}
	}
	self.definitions.archer_ability = {
		category = "grenade"
	}
	self.definitions.player_recovery_speed_tier_infamy = {
		name_id = "menu_recovery_speed_tier_infamy",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "recovery_speed_tier_infamy",
			category = "player"
		}
	}
	self.definitions.player_increase_ms_stamina_infamy = {
		name_id = "menu_increase_ms_stamina_infamy",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "increase_ms_stamina_infamy",
			category = "player"
		}
	}
	self.definitions.player_reload_speed_tier_infamy = {
		name_id = "menu_reload_speed_tier_infamy",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "reload_speed_tier_infamy",
			category = "player"
		}
	}
	self.definitions.player_critical_damage_card = {
		name_id = "menu_critical_damage_card",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "critical_damage_card",
			category = "player"
		}
	}
	self.definitions.player_weapon_swap_speed_card = {
		name_id = "menu_weapon_swap_speed_card",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "weapon_swap_speed_card",
			category = "player"
		}
	}
	self.definitions.player_damage_reduction_downed = {
		name_id = "menu_damage_reduction_downed",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "damage_reduction_downed",
			category = "player"
		}
	}
	self.definitions.stacking_trigger_happy_2 = {
		name_id = "menu_stacking_trigger_happy",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "stacking_trigger_happy",
			category = "pistol"
		}
	}
	self.definitions.stacking_trigger_happy = {
		name_id = "menu_stacking_trigger_happy",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "stacking_trigger_happy",
			category = "pistol"
		}
	}
	self.definitions.style_rate_of_fire = {
		name_id = "menu_style_rate_of_fire",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "style_rate_of_fire",
			category = "player"
		}
	}
	self.definitions.style_pierce_body_armor = {
		name_id = "menu_style_pierce_body_armor",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "style_pierce_body_armor",
			category = "player"
		}
	}
	self.definitions.daredevil_damage_absorption = {
		name_id = "menu_daredevil_damage_absorption",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "daredevil_damage_absorption",
			category = "player"
		}
	}
	self.definitions.player_last_shockwave_range = {
		name_id = "menu_last_shockwave_range",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "last_shockwave_range",
			category = "player"
		}
	}
	self.definitions.player_shockwave_healing = {
		name_id = "menu_shockwave_healing",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "shockwave_healing",
			category = "player"
		}
	}
	self.definitions.player_shockwave_bonus_damage = {
		name_id = "menu_shockwave_bonus_damage",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "shockwave_bonus_damage",
			category = "player"
		}
	}
	self.definitions.player_bonus_duration_bite_the_bullet = {
		name_id = "menu_bonus_duration_bite_the_bullet",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_duration_bite_the_bullet",
			category = "player"
		}
	}
	self.definitions.player_reduce_cooldown_bite_the_bullet = {
		name_id = "menu_reduce_cooldown_bite_the_bullet",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "reduce_cooldown_bite_the_bullet",
			category = "player"
		}
	}
	self.definitions.player_bite_the_bullet_stored_damage = {
		name_id = "menu_bite_the_bullet_stored_damage",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bite_the_bullet_stored_damage",
			category = "player"
		}
	}
	self.definitions.player_bite_the_bullet_stored_damage_2 = {
		name_id = "menu_bite_the_bullet_stored_damage",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "bite_the_bullet_stored_damage",
			category = "player"
		}
	}
	self.definitions.automatically_reload_style = {
		name_id = "menu_automatically_reload_style",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "automatically_reload_style",
			category = "player"
		}
	}
	self.definitions.bonus_hp_enemy_damage_style = {
		name_id = "menu_bonus_hp_enemy_damage_style",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_hp_enemy_damage_style",
			category = "player"
		}
	}
	self.definitions.style_lifesteal = {
		name_id = "menu_style_lifesteal",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "style_lifesteal",
			category = "player"
		}
	}
	self.definitions.style_rank = {
		name_id = "menu_style_rank",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "style_rank",
			category = "player"
		}
	}
	self.definitions.weapon_passive_headshot_damage_multiplier = {
		name_id = "menu_weapon_headshot_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "passive_headshot_damage_multiplier",
			category = "player"
		}
	}
	self.definitions.bite_the_bullet_ability = {
		category = "grenade"
	}
	self.definitions.bite_the_bullet_base = {
		name_id = "menu_bite_the_bullet_base",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bite_the_bullet_base",
			category = "player"
		}
	}
	self.definitions.quick_fix_stymulant = {
		category = "grenade"
	}
	self.definitions.player_weapon_accuracy_increase_2 = {
		name_id = "menu_player_weapon_accuracy_increase",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "weapon_accuracy_increase",
			category = "player"
		}
	}
	self.definitions.crit_dmg_per_chanc_2 = {
		name_id = "menu_crit_dmg_per_chanc",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "crit_dmg_per_chanc",
			category = "player"
		}
	}
	self.definitions.crit_dmg_per_chanc = {
		name_id = "menu_crit_dmg_per_chanc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "crit_dmg_per_chanc",
			category = "player"
		}
	}
	self.definitions.bonus_crit_damage = {
		name_id = "menu_bonus_crit_damage",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_crit_damage",
			category = "player"
		}
	}
	self.definitions.crit_multiplier = {
		name_id = "menu_crit_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "crit_multiplier",
			category = "player"
		}
	}
	self.definitions.bonus_crit_damage_2 = {
		name_id = "menu_bonus_crit_damage",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "bonus_crit_damage",
			category = "player"
		}
	}
	self.definitions.crit_multiplier_2 = {
		name_id = "menu_crit_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "crit_multiplier",
			category = "player"
		}
	}
	self.definitions.exceed_crit_scaling_2 = {
		name_id = "menu_passive_critical_hit_chance",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "exceed_crit_scaling",
			category = "player"
		}
	}
	self.definitions.exceed_crit_scaling = {
		name_id = "menu_passive_critical_hit_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "exceed_crit_scaling",
			category = "player"
		}
	}
	self.definitions.passive_critical_hit_chance = {
		name_id = "menu_passive_critical_hit_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "passive_critical_hit_chance",
			category = "player"
		}
	}
	self.definitions.pistol_enter_steelsight_speed_multiplier = {
		name_id = "menu_pistol_enter_steelsight_speed_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "enter_steelsight_speed_multiplier",
			category = "pistol"
		}
	}
	self.definitions.pistol_magazine_capacity_inc_2 = {
		name_id = "menu_pistol_magazine_capacity_inc",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "magazine_capacity_inc",
			category = "pistol"
		}
	}
	self.definitions.pistol_magazine_capacity_inc_1 = {
		name_id = "menu_pistol_magazine_capacity_inc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "magazine_capacity_inc",
			category = "pistol"
		}
	}
	self.definitions.pistol_pierce_chance = {
		name_id = "menu_pistol_inspire_stats",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "pierce_chance",
			category = "pistol"
		}
	}
	self.definitions.player_inspire_stats = {
		name_id = "menu_player_inspire_stats",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "inspire_stats",
			category = "player"
		}
	}
	self.definitions.player_heal_on_dodge = {
		name_id = "menu_player_heal_on_dodge",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "heal_on_dodge",
			category = "player"
		}
	}
	self.definitions.player_dmg_red_revived = {
		name_id = "menu_player_dmg_red_revived",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "dmg_red_revived",
			category = "player"
		}
	}
	self.definitions.player_tier_bonus_movement_speed = {
		name_id = "menu_player_tier_bonus_movement_speed",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "tier_bonus_movement_speed",
			category = "player"
		}
	}
	self.definitions.player_tier_bonus_stamina_multiplier = {
		name_id = "menu_player_tier_bonus_stamina_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "tier_bonus_stamina_multiplier",
			category = "player"
		}
	}
	self.definitions.player_bonus_healing = {
		name_id = "menu_player_bonus_healing",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_healing",
			category = "player"
		}
	}
	self.definitions.player_execution_immunity = {
		name_id = "menu_player_execution_immunity",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "execution_immunity",
			category = "player"
		}
	}
	self.definitions.player_crit_heal = {
		name_id = "menu_player_crit_heal",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "crit_heal",
			category = "player"
		}
	}
	self.definitions.player_bonus_crit_chanc = {
		name_id = "menu_player_bonus_crit_chanc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_crit_chanc",
			category = "player"
		}
	}
	self.definitions.player_execution_crit_scaling_2 = {
		name_id = "menu_player_execution_crit_scaling",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "execution_crit_scaling",
			category = "player"
		}
	}
	self.definitions.player_execution_crit_scaling = {
		name_id = "menu_player_execution_crit_scaling",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "execution_crit_scaling",
			category = "player"
		}
	}
	self.definitions.player_execution_crit_2 = {
		name_id = "menu_player_execution_crit",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "execution_crit",
			category = "player"
		}
	}
	self.definitions.player_execution_crit = {
		name_id = "menu_player_execution_crit",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "execution_crit",
			category = "player"
		}
	}
	self.definitions.player_detection_risk_damage_multiplier_2 = {
		name_id = "menu_player_detection_risk_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "detection_risk_damage_multiplier",
			category = "player"
		}
	}
	self.definitions.player_detection_risk_damage_multiplier = {
		name_id = "menu_player_detection_risk_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "detection_risk_damage_multiplier",
			category = "player"
		}
	}
	self.definitions.player_bonus_from_crit_3 = {
		name_id = "menu_player_bonus_from_crit",
		category = "feature",
		upgrade = {
			value = 3,
			upgrade = "bonus_from_crit",
			category = "player"
		}
	}
	self.definitions.player_bonus_from_crit_2 = {
		name_id = "menu_player_bonus_from_crit",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "bonus_from_crit",
			category = "player"
		}
	}
	self.definitions.player_bonus_from_crit = {
		name_id = "menu_player_bonus_from_crit",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bonus_from_crit",
			category = "player"
		}
	}
	self.definitions.player_qf_interacting_cooldown_decrease = {
		name_id = "menu_player_qf_interacting_cooldown_decrease",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "qf_interacting_cooldown_decrease",
			category = "player"
		}
	}
	self.definitions.player_qf_healing_period_extended = {
		name_id = "menu_player_qf_healing_period_extended",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "qf_healing_period_extended",
			category = "player"
		}
	}
	self.definitions.player_qf_stamina_regen = {
		name_id = "menu_player_qf_stamina_regen",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "qf_stamina_regen",
			category = "player"
		}
	}
	self.definitions.player_bs_hp_gf = {
		name_id = "menu_player_bs_hp_gf",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bs_hp_gf",
			category = "player"
		}
	}
	self.definitions.player_qf_invulnerable_inc = {
		name_id = "menu_player_qf_invulnerable_inc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "qf_invulnerable_inc",
			category = "player"
		}
	}
	self.definitions.player_qf_bonus_movement_speed = {
		name_id = "menu_player_qf_bonus_movement_speed",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "qf_bonus_movement_speed",
			category = "player"
		}
	}
	self.definitions.player_quick_fix = {
		name_id = "quick_fix",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "quick_fix",
			category = "player"
		}
	}
	self.definitions.shotgun_consume_no_ammo_chance_1 = {
		name_id = "menu_saw_consume_no_ammo_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "consume_no_ammo_chance2",
			category = "shotgun"
		}
	}
	self.definitions.player_overkill_damage_multiplier_2 = {
		name_id = "menu_player_overkill_damage_multiplier",
		category = "temporary",
		upgrade = {
			value = 2,
			upgrade = "overkill_damage_multiplier",
			category = "temporary"
		}
	}
	self.definitions.player_minion_dmg_red = {
		name_id = "menu_minion_dmg_red",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "minion_dmg_red",
			category = "player"
		}
	}
	self.definitions.pistol_stacked_reload_bonus = {
		name_id = "menu_stacked_reload_bonus",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "stacked_reload_bonus",
			category = "pistol"
		}
	}
	self.definitions.player_ber_swap_speed = {
		name_id = "menu_ber_swap_speed",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "ber_swap_speed",
			category = "player"
		}
	}
	self.definitions.player_instakill_downed = {
		name_id = "menu_instakill_downed",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "instakill_downed",
			category = "player"
		}
	}
	self.definitions.player_bullet_storm_base_dr_2 = {
		name_id = "menu_bullet_storm_base_dr",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "bullet_storm_base_dr",
			category = "player"
		}
	}
	self.definitions.player_bullet_storm_base_dr = {
		name_id = "menu_bullet_storm_base_dr",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bullet_storm_base_dr",
			category = "player"
		}
	}
	self.definitions.player_shotgun_saw_dmg_inc = {
		name_id = "menu_shotgun_saw_dmg_inc",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "shotgun_saw_dmg_inc",
			category = "player"
		}
	}
	self.definitions.weapon_piercing_chance = {
		name_id = "menu_weapon_piercing_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "piercing_chance",
			category = "weapon"
		}
	}
	self.definitions.player_drill_fix_interaction_speed_multiplier_2 = {
		name_id = "menu_player_drill_fix_interaction_speed_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "drill_fix_interaction_speed_multiplier",
			category = "player"
		}
	}
	self.definitions.player_healing_bonus = {
		name_id = "menu_player_healing_bonus",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "healing_bonus",
			category = "player"
		}
	}
	self.definitions.player_damage_health_ratio_multiplier_2 = {
		name_id = "menu_player_damage_health_ratio_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "damage_health_ratio_multiplier",
			category = "player"
		}
	}
	self.definitions.player_melee_damage_health_ratio_multiplier_2 = {
		name_id = "menu_player_melee_damage_health_ratio_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "melee_damage_health_ratio_multiplier",
			category = "player"
		}
	}
	self.definitions.player_pick_up_ammo_multiplier_skill = {
		name_id = "menu_player_pick_up_ammo_multiplier_skill",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "pick_up_ammo_multiplier_skill",
			category = "player"
		}
	}
	self.definitions.player_pick_up_ammo_multiplier_skill_2 = {
		name_id = "menu_player_pick_up_ammo_multiplier_skill_2",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "pick_up_ammo_multiplier_skill",
			category = "player"
		}
	}
	self.definitions.saw_consume_no_ammo_chance_1 = {
		name_id = "menu_saw_consume_no_ammo_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "consume_no_ammo_chance",
			category = "saw"
		}
	}
	self.definitions.player_small_loot_multiplier_1 = {
		name_id = "menu_player_small_loot_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "small_loot_multiplier",
			category = "player"
		}
	}
	self.definitions.player_small_loot_multiplier_2 = {
		name_id = "menu_player_small_loot_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "small_loot_multiplier",
			category = "player"
		}
	}
	self.definitions.player_passive_loot_drop_multiplier_2 = {
		name_id = "menu_player_passive_loot_drop_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "passive_loot_drop_multiplier",
			category = "player"
		}
	}
	self.definitions.player_melee_knockdown_mul = {
		name_id = "menu_player_melee_knockdown_mul",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "melee_knockdown_mul",
			category = "player"
		}
	}
	self.definitions.player_non_special_melee_multiplier_2 = {
		name_id = "menu_player_non_special_melee_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "non_special_melee_multiplier",
			category = "player"
		}
	}
	self.definitions.player_melee_damage_multiplier_2 = {
		name_id = "menu_player_melee_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "melee_damage_multiplier",
			category = "player"
		}
	}
	self.definitions.player_non_special_melee_multiplier_1 = {
		name_id = "menu_player_non_special_melee_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "non_special_melee_multiplier",
			category = "player"
		}
	}
	self.definitions.player_melee_damage_multiplier_1 = {
		name_id = "menu_player_melee_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "melee_damage_multiplier",
			category = "player"
		}
	}
	self.definitions.player_max_health_reduction_2 = {
		name_id = "menu_player_max_health_reduction",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "max_health_reduction",
			category = "player"
		}
	}
	self.definitions.player_max_health_reduction_1 = {
		name_id = "menu_player_max_health_reduction",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "max_health_reduction",
			category = "player"
		}
	}
	self.definitions.player_killshot_close_panic_chance_mindbreaker_1 = {
		name_id = "menu_player_killshot_close_panic_chance_mindbreaker",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "killshot_close_panic_chance_mindbreaker",
			category = "player"
		}
	}
	self.definitions.player_killshot_close_panic_chance_mindbreaker_2 = {
		name_id = "menu_player_killshot_close_panic_chance_mindbreaker",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "killshot_close_panic_chance_mindbreaker",
			category = "player"
		}
	}
	self.definitions.player_melee_damage_dampener_1 = {
		name_id = "menu_player_melee_damage_dampener",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "melee_damage_dampener",
			category = "player"
		}
	}
	self.definitions.player_taser_malfunction_1 = {
		name_id = "menu_player_taser_malf",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "taser_malfunction",
			category = "player"
		}
	}
	self.definitions.cooldown_long_dis_revive_1 = {
		name_id = "menu_cooldown_long_dis_revive",
		category = "cooldown",
		upgrade = {
			value = 1,
			upgrade = "long_dis_revive",
			category = "cooldown"
		}
	}
	self.definitions.player_bleed_out_time = {
		name_id = "menu_player_bleed_out_time",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_out_time",
			category = "player"
		}
	}
	self.definitions.player_cheat_death_chance_1 = {
		name_id = "menu_player_cheat_death_chance",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "cheat_death_chance",
			category = "player"
		}
	}
	self.definitions.player_cheat_death_chance_2 = {
		name_id = "menu_player_cheat_death_chance",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "cheat_death_chance",
			category = "player"
		}
	}
	self.definitions.player_bleed_out_health_multiplier_2 = {
		name_id = "menu_player_bleed_out_health_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "bleed_out_health_multiplier",
			category = "player"
		}
	}
	self.definitions.player_bleed_out_health_multiplier = {
		name_id = "menu_player_bleed_out_health_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "bleed_out_health_multiplier",
			category = "player"
		}
	}
	self.definitions.player_revived_health_regain = {
		name_id = "menu_revived_health_regain",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "revived_health_regain",
			category = "player"
		}
	}
	self.definitions.player_mark_enemy_time_multiplier = {
		name_id = "menu_player_mark_enemy_time_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "mark_enemy_time_multiplier",
			category = "player"
		}
	}
	self.definitions.player_mark_enemy_time_multiplier_2 = {
		name_id = "menu_player_mark_enemy_time_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "mark_enemy_time_multiplier",
			category = "player"
		}
	}
	self.definitions.sentry_gun_armor_multiplier2_1 = {
		name_id = "menu_sentry_gun_armor_multiplier2",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "armor_multiplier2",
			synced = true,
			category = "sentry_gun"
		}
	}
	self.definitions.sentry_gun_armor_multiplier2_2 = {
		name_id = "menu_sentry_gun_armor_multiplier2",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "armor_multiplier2",
			synced = true,
			category = "sentry_gun"
		}
	}
	self.definitions.sentry_gun_damage_multiplier_1 = {
		name_id = "menu_sentry_gun_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "damage_multiplier",
			synced = true,
			category = "sentry_gun"
		}
	}
	self.definitions.sentry_gun_damage_multiplier_2 = {
		name_id = "menu_sentry_gun_damage_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "damage_multiplier",
			synced = true,
			category = "sentry_gun"
		}
	}
    self.definitions.deploy_interact_faster_2 = {
		name_id = "menu_deploy_interact_faster",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "deploy_interact_faster",
			category = "player"
		}
	}
	self.definitions.player_temp_increased_movement_speed_1 = {
		name_id = "menu_player_temp_increased_movement_speed",
		category = "temporary",
		upgrade = {
			value = 1,
			upgrade = "increased_movement_speed",
			category = "temporary"
		}
	}
	self.definitions.player_messiah_revive_from_bleed_out_1 = {
		name_id = "menu_player_pistol_revive_from_bleed_out",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "messiah_revive_from_bleed_out",
			category = "player"
		}
	}
	self.definitions.player_messiah_revive_from_bleed_out_2 = {
		name_id = "menu_player_pistol_revive_from_bleed_out",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "messiah_revive_from_bleed_out",
			category = "player"
		}
	}
    self.definitions.temporary_damage_speed_multiplier_1 = {
        category = "temporary",
		name_id = "menu_temporary_damage_speed_1",
		upgrade = {
		   category = "temporary",
		   upgrade = "damage_speed_multiplier",
           value =  1
        }
    }
	self.definitions.weapon_silencer_damage_multiplier_1 = {
	    category = "feature",
		name_id = "silencer_damage_multiplier",
		upgrade = {
		    category = "weapon",
			upgrade = "silencer_damage_multiplier",
			value = 1
		}
	}
    self.definitions.weapon_silencer_damage_multiplier_2 = {
        category = "feature",
    	name_id = "silencer_damage_multiplie",
    	upgrade = {
    	   category = "weapon",
    	   upgrade = "silencer_damage_multiplier",
		   value = 2
		}
    }
    self.definitions.player_trip_mine_deploy_time_multiplier_1 = {
		incremental = true,
		name_id = "menu_player_trip_mine_deploy_time_multiplier",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "trip_mine_deploy_time_multiplier",
			category = "player"
		}
	}
	self.definitions.player_trip_mine_deploy_time_multiplier_2 = {
		incremental = true,
		name_id = "menu_player_trip_mine_deploy_time_multiplier",
		category = "feature",
		upgrade = {
			value = 2,
			upgrade = "trip_mine_deploy_time_multiplier",
			category = "player"
		}
	}
end)