_G.FOverhaul = _G.FOverhaul or {}
FOverhaul._path = ModPath
FOverhaul._data_path = SavePath .. "FOverhaul_data.txt"
FOverhaul._data = {
	Exec_indi = true,
    style_exec_indi = 1,
	rainbow_color_exec = false,
	rainbow_speed_exec = 5,
	text_dur_exec = 2,
	daredevil_absorb_bar = true,
	swan_song_style_indi = 1,
	SwanSong_fod = true,
	ammo_return_vfx = true,
	ammo_return_vfx_rainbow = false,
	dire_need_vfx = true,
	block_popup = false,
	enemy_plate = true,
	error_logger = true,
	wraith_energy_enabled = true,
	wraith_hint_enabled = true,
	life_line_wraith_cooldown_enabled = true,
	wraith_heal_bullet_enabled = true,
	def_debuff_vis = true,
	newVersion_si = false
}

function FOverhaul:HidePopUp()
	FOverhaul._data.newVersion_si = true
	FOverhaul:Save()
end

function FOverhaul:check_version()
	local function read_file(file_path)
		local file = io.open(file_path, "r")

		if not file then
			--log("Could not open file " .. tostring(file_path))
			return nil
		end

		local content = file:read("*all")

		file:close()

		return content
	end

	local function extract_version(file_content)
		local data = json.decode(file_content)
		local version = data.version
		return version
	end

	local cnt = read_file(FOverhaul._path .. "mod.txt")

	if cnt then
		local version = extract_version(cnt)
    	if version then
			if not FOverhaul._data.OverhaulVersion then
				FOverhaul._data.OverhaulVersion = version
				FOverhaul:reload(true, FOverhaul._data.newVersion_si)
				return
			end

			if FOverhaul._data.OverhaulVersion ~= version then
				FOverhaul._data.OverhaulVersion = version
				FOverhaul:reload(true, FOverhaul._data.newVersion_si)
			end
		end
	end
end

local function round(num, decimals)
	decimals = math.pow(10, decimals or 0)
	num = num * decimals

	if num >= 0 then
		 num = math.floor(num + 0.5)
	else
 		num = math.ceil(num - 0.5)
	end

	return num / decimals
end

function FOverhaul:Save()
	local file = io.open( self._data_path, "w+" )
	if file then
		file:write( json.encode( self._data ) )
		file:close()
	end
end

function FOverhaul:Load()
	local file = io.open( self._data_path, "r" )
	if file then
		self._data = json.decode( file:read("*all") )
		file:close()
	end
end

function FOverhaul:remind_later(clbk)
	if clbk and package.config:sub(1,1) ~= '\\' then
		self:_invalid_OS()
		return
	end

	local currentTime = os.time()
	local elapsedTime = math.abs(currentTime - (FOverhaul._data.last_ran_popup or 0))
	if clbk then
		FOverhaul._data.remind_later_popup = clbk
		FOverhaul._data.last_ran_popup = currentTime
	end

	if elapsedTime >= 3600 or not FOverhaul._data.remind_later_popup then
		FOverhaul._data.last_ran_popup = currentTime
		FOverhaul._data.remind_later_popup = false
		return true
	end

	FOverhaul:Save()
	return false
end

function FOverhaul:block_()
	FOverhaul._data.block_popup = true
	FOverhaul:Save()
end

function FOverhaul:_attempt_delete()
	local steam_install_paths = {
        "C:/Program Files (x86)/Steam/steamapps/common/PAYDAY 2",
        "C:/Program Files/Steam/steamapps/common/PAYDAY 2",
		"D:/Steam/steamapps/common/PAYDAY 2",
		"E:/Steam/steamapps/common/PAYDAY 2"
    }

	if package.config:sub(1,1) ~= '\\' then
		self:_invalid_OS()
		return
	end

	if package.config:sub(1,1) == '\\' then
    	for _, path in ipairs(steam_install_paths) do
    	    local find_path = 'dir "' .. path .. '" /b'
    	    local status = os.execute(find_path)
    	    if status == 0 then
				self:_confirm_deletion(path)
				return
			end
    	end
	end

	self:_on_failed_deletion()
end

function FOverhaul:_invalid_OS()
	local dialog_data = {
		title = "[Freyah's Skills Overhaul] - INVAILD OS",
		text = "\nSorry, this tool works only for Windows systems."
	}
	local yes_button = {
		text = "OKAY",
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button
	}
	managers.system_menu:show(dialog_data)
end

function FOverhaul:_confirm_deletion(path)
	local dialog_data = {
		title = "[Freyah's Skills Overhaul] - AUTOMATIC REMOVAL",
		text = "\nPAYDAY 2 has been found at this path: \n" .. tostring(path) .. "\n\nDo you wish to find and delete \"Player Stats Panel Enhancements\"?\n\n[WARNING!] Please make sure that this is the right path of your PAYDAY 2 directory."
	}
	local yes_button = {
		text = "YES",
		cancel_button = true,
		callback_func = callback(self, self, "_delete_folder", path)
	}
	local no_button = {
		text = "NO",
		cancel_button = true
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}
	managers.system_menu:show(dialog_data)
end

function FOverhaul:_delete_folder(path)
	if not path then
		self:_on_failed_deletion()
	end
	local pspe_folder = "/mods/Player Stats Panel Enhancements"
	local del = 'rd /s /q "' .. tostring(path) .. pspe_folder .. '"'
	local deletion = os.execute(del)
	if deletion == 0 then
		--log("Folder deleted successfully")
		self:_on_successful_deletion()
	else
		--log("Failed to delete folder")
		self:_on_failed_deletion()
	end
end

function FOverhaul:_on_successful_deletion()
	local dialog_data = {
		title = "[Freyah's Skills Overhaul] - AUTOMATIC REMOVAL",
		text = "\n\"Player Stats Panel Enhancements\" has been removed successfully!\n\nThe game will reload now."
	}
	local yes_button = {
		text = "OKAY",
		cancel_button = true,
		callback_func = callback(self, self, "_re_reload_lua")
	}
	dialog_data.button_list = {
		yes_button
	}
	managers.system_menu:show(dialog_data)
end

function FOverhaul:_on_failed_deletion()
	local dialog_data = {
		title = "[Freyah's Skills Overhaul] - AUTOMATIC REMOVAL",
		text = "\n\"Player Stats Panel Enhancements\" or PAYDAY 2 mods folder was not found.\n\nPlease remove \"Player Stats Panel Enhancements\" manually."
	}
	local yes_button = {
		text = "OKAY",
		cancel_button = true,
	}
	dialog_data.button_list = {
		yes_button
	}
	managers.system_menu:show(dialog_data)
end

function FOverhaul:_re_reload_lua()
	local state = game_state_machine:current_state_name()
	if setup and setup.load_start_menu then
		if state == "menu_main" and not Network:multiplayer() then
			setup:load_start_menu()
		end

		if Network:multiplayer() then
			managers.menu:get_menu("menu_main").callback_handler:_dialog_leave_lobby_yes()
			setup:load_start_menu()
		end
	end
end

function FOverhaul:_go_to_propostion_page_popup()
	local dialog_data = {
		title = "[Freyah's Skills Overhaul] - FEEDBACK",
		text = "\nThis will open a new tab in your browser with a google forms link where you can type any suggestions you have.\n\nDo you wish to proceed?"
	}
	local yes_button = {
		text = "YES",
		cancel_button = true,
		callback_func = callback(self, self, "_go_to_propostion_page")
	}
	local no_button = {
		text = "NO",
		cancel_button = true,
	}
	dialog_data.button_list = {
		yes_button,
		no_button
	}
	managers.system_menu:show(dialog_data)
end

function FOverhaul:_go_to_propostion_page()
	if package.config:sub(1,1) ~= '\\' then
		self:_invalid_OS()
		return
	end

	local url = "https://forms.gle/shRyQBr25tLRhfYb6"
	os.execute("start " .. url)
end

function FOverhaul:_go_to_changelogs()
	if package.config:sub(1,1) ~= '\\' then
		self:_invalid_OS()
		return
	end

	local url = "https://modworkshop.net/mod/32323?tab=changelog"
	os.execute("start " .. url)
end

function FOverhaul:reload(newVer, silent)
	FOverhaul:Save()
	FOverhaul:Load()

	local dialog_data = {
		title = "[Freyah's Skills Overhaul]",
		text = "Options have been saved and loaded!"
	}
	local yes_button = {
		text = "Close",
		cancel_button = true,
	}
	local hide_button = {
		text = "Do not show again",
		cancel_button = true,
		callback_func = callback(self, self, "HidePopUp")
	}
	local changelog_button = {
		text = "changelog",
		cancel_button = true,
		callback_func = callback(self, self, "_go_to_changelogs")
	}

	dialog_data.button_list = {
		yes_button
	}

	if newVer then
		yes_button.text = "Thanks"
		dialog_data.title = "[Freyah's Skills Overhaul]"
		dialog_data.text = "Welcome to new version!\nMod options have been reloaded to ensure everything is setup properly, enjoy!\n\n(This message will only show once when there is new update)"

		dialog_data.button_list = {
			yes_button,
			hide_button,
			changelog_button
		}
	end

	if not silent then
		managers.system_menu:show(dialog_data)
	end
end

Hooks:Add("MenuManagerInitialize", "MenuManagerInitializeFOverhaul", function(menu_manager)
	FOverhaul:Load()
	MenuCallbackHandler.FOverhaul_change_style_exec_indi = function(self,item)
		FOverhaul._data.style_exec_indi = item:value()
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_Exec_indi = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.Exec_indi = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_rainbow_color_exec = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.rainbow_color_exec = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_enemy_plate = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.enemy_plate = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_error_logger = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.error_logger = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.text_dur_exec_change_callback = function(self,item)
		local value = round(item:value(),1)
		FOverhaul._data.text_dur_exec = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.rainbow_speed_exec_change_callback = function(self,item)
		local value = round(item:value())
		FOverhaul._data.rainbow_speed_exec = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_daredevil_absorb_bar = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.daredevil_absorb_bar = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.FOverhaul_change_swan_song_style_indi = function(self,item)
		FOverhaul._data.swan_song_style_indi = item:value()
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_SwanSong_fod = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.SwanSong_fod = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_ammo_return = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.ammo_return_vfx = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_ammo_return_rainbow = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.ammo_return_vfx_rainbow = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_dire_need_vfx = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.dire_need_vfx = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.FOverhaul_reset_updator_callback = function(self,item)
		FOverhaul:clear_updates()
	end
	MenuCallbackHandler.FOverhaul_go_to_propostion_page_callback = function(self,item)
		FOverhaul:_go_to_propostion_page_popup()
	end
	MenuCallbackHandler.callback_FOverhaul_wraith_energy_enabled = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.wraith_energy_enabled = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_wraith_hint_enabled = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.wraith_hint_enabled = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_life_line_wraith_cooldown_enabled = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.life_line_wraith_cooldown_enabled = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_wraith_heal_bullet_enabled = function(self,item)
		local value = item:value() == "on"
		FOverhaul._data.wraith_heal_bullet_enabled = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.FOverhaul_force_save_and_load = function(self)
		FOverhaul:reload()
	end
	MenuCallbackHandler.callback_FOverhaul_def_debuff = function(self, item)
		local value = item:value() == "on"
		FOverhaul._data.def_debuff_vis = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.callback_FOverhaul_newVersion = function(self, item)
		local value = item:value() == "on"
		FOverhaul._data.newVersion_si = value
		FOverhaul:Save()
	end
	MenuCallbackHandler.FOverhaul_closed = function(self)
		FOverhaul:Save()
	end

	MenuHelper:LoadFromJsonFile(FOverhaul._path .. "menu/options.txt", FOverhaul, FOverhaul._data)
	MenuHelper:LoadFromJsonFile(FOverhaul._path .. "menu/daredevil_options.txt", FOverhaul, FOverhaul._data)

	local options_ = { "execution", "swan", "ammorefund", "sly", "wraith" }

	for _, t in pairs(options_) do
		MenuHelper:LoadFromJsonFile(FOverhaul._path .. "menu/options_" .. t .. ".txt", FOverhaul, FOverhaul._data)
	end
end)