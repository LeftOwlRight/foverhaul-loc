{
	"menu_id" : "FOverhaul_menu",
	"parent_menu_id" : "blt_options",
	"title" : "FOverhaul_title",
	"description":"FOverhaul_desc",
	"back_callback" : "FOverhaul_closed",
	"items" : [
		{
			"type" : "divider",
			"size" : 0
		},
		{
			"type" : "button",
			"title" : "FOverhaul_reset_updator_title",
			"description":"FOverhaul_reset_updator_desc",
			"id" : "FOverhaul_reset_updator",
			"callback" : "FOverhaul_reset_updator_callback"
		},
		{
			"type" : "button",
			"title" : "FOverhaul_force_save_and_load_title",
			"description":"FOverhaul_force_save_and_load_desc",
			"id" : "FOverhaul_force_save_and_load",
			"callback" : "FOverhaul_force_save_and_load"
		},
		{
			"type" : "button",
			"title" : "FOverhaul_go_to_propostion_page_title",
			"description":"FOverhaul_go_to_propostion_page_desc",
			"id" : "FOverhaul_go_to_propostion_page",
			"callback" : "FOverhaul_go_to_propostion_page_callback"
		},
		{
			"type" : "toggle",
			"id" : "FOverhaul_newVersion",
			"title" : "FOverhaul_newVersion_title",
			"description" : "FOverhaul_newVersion_desc",
			"callback" : "callback_FOverhaul_newVersion",
			"value" : "newVersion_si",
			"default_value" : false
		},
		{
			"type" : "divider",
			"size" : 10
		},
		{
			"type" : "toggle",
			"id" : "FOverhaul_def_debuff",
			"title" : "FOverhaul_def_debuff_title",
			"description" : "FOverhaul_def_debuff_desc",
			"callback" : "callback_FOverhaul_def_debuff",
			"value" : "def_debuff_vis",
			"default_value" : true
		},
		{
			"type" : "toggle",
			"id" : "FOverhaul_enemy_plate",
			"title" : "FOverhaul_enemy_plate_title",
			"description" : "FOverhaul_enemy_plate_desc",
			"callback" : "callback_FOverhaul_enemy_plate",
			"value" : "enemy_plate",
			"default_value" : true
		},
		{
			"type" : "toggle",
			"id" : "FOverhaul_error_logger",
			"title" : "FOverhaul_error_logger_title",
			"description" : "FOverhaul_error_logger_desc",
			"callback" : "callback_FOverhaul_error_logger",
			"value" : "error_logger",
			"default_value" : true
		},
		{
			"type" : "divider",
			"size" : 20
		}
	]
}