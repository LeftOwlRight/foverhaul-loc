_G.PSPE = _G.PSPE or {}
PSPE._path = ModPath
PSPE._data_path = SavePath .. "PSPE_data.txt"
PSPE._data = {
	PSPE_vanillafont = true,
	PSPE_disable_cosmetic_lines = false,
	PSPE_disable_cosmetic_split = false,
	PSPE_colorv_5 = 16,
	PSPE_colorv_7 = 16,
	PSPE_colorv_3 = 16
}

function PSPE:Save()
	local file = io.open( self._data_path, "w+" )
	if file then
		file:write( json.encode( self._data ) )
		file:close()
	end
end

function PSPE:Load()
	local file = io.open( self._data_path, "r" )
	if file then
		self._data = json.decode( file:read("*all") )
		file:close()
	end
end

Hooks:Add("MenuManagerInitialize", "MenuManagerInitializePSPE", function(menu_manager)
	MenuCallbackHandler.PSPE_on_color_change_callback = function(self,item)
		PSPE._data.PSPE_color = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_color2_change_callback = function(self,item)
		PSPE._data.PSPE_color_2 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_color3_change_callback = function(self,item)
		PSPE._data.PSPE_color_3 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorh_change_callback = function(self,item)
		PSPE._data.PSPE_colorh = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorh2_change_callback = function(self,item)
		PSPE._data.PSPE_colorh_2 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorh3_change_callback = function(self,item)
		PSPE._data.PSPE_colorh_3 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorv_change_callback = function(self,item)
		PSPE._data.PSPE_colorv = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorv2_change_callback = function(self,item)
		PSPE._data.PSPE_colorv_2 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorv3_change_callback = function(self,item)
		PSPE._data.PSPE_colorv_3 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorv4_change_callback = function(self,item)
		PSPE._data.PSPE_colorv_4 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorv5_change_callback = function(self,item)
		PSPE._data.PSPE_colorv_5 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_colorv6_change_callback = function(self,item)
		PSPE._data.PSPE_colorv_6 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_color_lines_change_callback = function(self,item)
		PSPE._data.PSPE_color_lines = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_color_lines2_change_callback = function(self,item)
		PSPE._data.PSPE_color_lines2 = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.callback_PSPE_vanillafont = function(self,item)
		local value = item:value() == "on"
		PSPE._data.PSPE_vanillafont = value
		PSPE:Save()
	end
	MenuCallbackHandler.callback_PSPE_disable_cosmetic_lines = function(self,item)
		local value = item:value() == "on"
		PSPE._data.PSPE_disable_cosmetic_lines = value
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_on_cosmetic_split_change_callback = function(self,item)
		PSPE._data.PSPE_cosmetic_split = item:value()
		PSPE:Save()
	end
	MenuCallbackHandler.callback_PSPE_disable_cosmetic_split = function(self,item)
		local value = item:value() == "on"
		PSPE._data.PSPE_disable_cosmetic_split = value
		PSPE:Save()
	end
	MenuCallbackHandler.PSPE_closed = function(self)
		PSPE:Save()
	end
	PSPE:Load()
	MenuHelper:LoadFromJsonFile(PSPE._path .. "menu/options_pspe.txt", PSPE, PSPE._data)
end)